## Item Tree

Crossref Nexus uses the Cayenne REST API concept of an Item Tree, which is a structure representing an Item (any object
we know about, such as an Article, Book, ORCID user, etc), the identifiers it's represented by (DOIs, ISSNs, ORCID iDs),
and its relationships to other objects. It combines all of these trees into one graph that connects all Research Items.

An Item Tree is how an asserting party describes a cross-section of the Research Nexus. For example, it could represent
an Article, its connection to a Journal, its Authors, etc.

When an Item Tree is ingested into the Nexus, we identify all of the Items mentioned in the Item Tree, and then record
the connections that the Item Tree describes.

An Item Tree is sent by an asserting party. That could be a Crossref member (they send depsoit XML which is transformed
into an Item Tree), a Crossref Agent (e.g. the Wikipedia agent), Community Agent (e.g. Plaudit) or a peer organisation (
e.g. ORCID). Each Item Tree refers to Items that may or may not already exist in the Nexus. Some of these Items may '
belong' to the asserting party and some might not. Rather than trying to resolve who owns what, Nexus allows any
assertions to be made by any accredited party, and records who made them.

### Item Tree Structure

At the top of an Item Tree is an Item. This could be any research object that we know about, or don't know about yet.
Each Item has zero, one or more of:

- Item Identifiers (using a shared vocabulary such as DOI or ORCID)
- Property Statements. For example, that it has a given title, or abstract, or date of publication.
- Relationship Statements. These include a relationship type (such as "cites") and a link to another Item.

That other Item in a Relationship is just like the root item, and can contain the same fields. Because an Item can
include a relationship to another Item, and that Item can do the same, it forms a recursive tree structure.

An Item may not have an Item Identifier. For example, an Article page may have a 'author' relationship to another item,
and we know the name but not the ORCID iD of that author.

## Nexus

The Nexus is a database that represents the sum of our knowledge. It is the result of incorporating all Item Trees
together, and finding the overlaps between them.

The same concepts of Items, Item Identifiers, Property Statements and Relationship Statements apply.

The Nexus differs from the Item Tree in two main ways:

1. We resolve every Item into the global Item Ontology.
2. Item Trees don't exist in abstract. Every Item Tree is constructed and sent into the Nexus by an asserting party. All
   of the statements in the Item Tree (i.e assertion of a Property or Relationship) are inserted into the Item Graph
   with provenance.

### Identified and Blank Items

Items exist purely to have Property Statements and Link Statements made about them. Where two Item Trees want to talk
about the same Item, they must use an Item Identifier (such as a DOI or ORCID iD) to indicate which Item they are
talking about.

Some Items don't have Item Identifiers. These are called Blank Nodes, a concept borrowed from RDF. Blank Node Items are
still considered Items, and can still be linked from Items. But because we have no more information about the Item other
than the Item's Properties and its parent in the Item Tree.

So, whilst an Item Tree is a tree of Items with Item Identifiers, the Nexus is a graph of Items, identified by their
Item Identifier.

### Item ontology

The Item ontology includes all Research Items, such as Articles, Journals, ORCID iDs, etc. It assigns each Item a PK (
Primary Key), which is a numerical ID for that Item.

We do not record concepts like ownership in the Item Ontology: it is simply a list of things that we know that someone
says exist.

### Item Identifiers

Item Identifiers are strings that can be associated with Items. They enable us to use a shared vocabulary to talk about
the same Item. They can include Persistent IDs, such as DOIs, ORCID iDs, and other IDs, such as ISSNs.

Not every Item has an Item Identifier. For example, an Author or Funder for whom we don't have an ORCID or Funder ID
yet.

Some Items can have two or more Item Identifiers. For example Journal may have a Journal DOI, an Electronic ISSN and a
Print ISSN.

We maintain strict rules about how Item Identifiers relate to Items. If we don't, it is simply impossible to combine all
of the data. The Nexus is the control point through which we merge all knowledge.

### Property Statements

These are chunks of statements that describe Items, for example the title or date of publication. Each Statement

### Link Statements.

Every Item in the Nexus has a numerical ID, which enables them to be linked. This ID is different from Identifiers such
as DOIs, becuase

## Ingesting Item Trees

When Item Trees are ingested in to the Nexus we take a two-stage process: Reconciliation and Assertion.

The Reconciliation stage defines the way that we combine all of our Asserting Parties' statements. This is the way that
we specify that two members of our community are talking about the same thing.

The Nexus isn't simply all Item Trees thrown together in a soup. Whist this would be simple, it would make the data
impossible to actuallly use.

### Item Reconciliation

In the Reconciliation stage, we identify every Item that's mentioned in the Item Tree. So these rules are applied to
every Item in the Tree recursively.

Reconciliation can be expressed as "An Item Identifier can point to only one Item. More than one Item Identifier can
point to a given item. Item Identifiers can always be added to Items as long as they don't create a conflict. An Item
Tree reconciliation is not allowed to introduce a conflict."

Rules:

1. Does the Item there zero Item Identifiers?
    - Yes: Item has no Item Identifiers to reconcile, and is a Blank Node. A Blank Node is assigned a blank identifier
      based on the hash of the Item's properties and the ID of its parent. **Create new Item PK in the Nexus and stop.**
    - No: Item has some Item Identifiers to reconcile, continue.
2. Partition the Item Identifiers into those that are already known in the Nexus and linked to an Item, and those that
   are not yet known.
3. Are there one or more unknown Item Identifiers, and zero known Item Identifiers?
    - Yes: This is a new Item with new Item Identifiers. **Create new Item PK in the Nexus, add and link the Item
      Identifiers and stop.**
    - No: This is a mixture of known and unknown Item Identifiers, continue.
4. Are one or more known Item Identifiers and zero or more unknown Item Identifiers.
5. Do all of the same known Item Identifiers belong to the same Item?
    - Yes: All Identifiers agree on which Item PK to use. **Use that Item PK and link any unknown Item Identifiers"
    - No: Not all identifiers agree. **Raise Conflict Error and stop**.

### Relation Reconciliation

After the Item reconciliation process has completed, every item in the Item Tree will have an Item PK associated with
it.

The next step is to reconcile Relationships to Relationship PKs. Every relationship is a triple recording the Item PK of
the subject, the Item PK of the object, and the relationship type. A Relationship PK in the Nexus is neither true or
false, it just records the concept of the Relationship.

### Assertion

# TODOs

## Relation Type registry

- Register mappings from external vocabularies (e.g. DataCite, Funder Ingestion, ROR) into our own relation types.
- Register mapping 'lenses' to external vocabularies so we can expose our graph with the types mapped (e.g. Content
  Negotiation, Funder Registry, Scholix)

## Test

- Whole test suite for the semantics of the Resolution.

## Triggers

- Add event trigger

## Browse APIs

## Projections

- Cayenne JSON ES doc
- Scholix

## Blank nodes

- Entail context of parent ID in blank node ID so that it can only be referred to.

# Model

- Property statements
- Property statement merge semantics

# Spring

springify cayenne has conflicts on log4j, so can't use logger just yet.