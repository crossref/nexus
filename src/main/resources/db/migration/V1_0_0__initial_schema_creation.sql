CREATE TABLE user_agent
(
    pk SERIAL PRIMARY KEY NOT NULL,
    product VARCHAR NOT NULL,
    version VARCHAR NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE UNIQUE INDEX user_agent_idx ON
user_agent (
    product,
    version
);

-- A batch of envelopes sent in response to user input.
CREATE TABLE envelope_batch
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    ext_trace VARCHAR NULL,
    user_agent_pk INTEGER NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (user_agent_pk) REFERENCES user_agent (pk)
);

CREATE INDEX ON
envelope_batch (ext_trace);

CREATE TABLE envelope
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    envelope_batch_pk BIGINT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (envelope_batch_pk) REFERENCES envelope_batch (pk)
);

CREATE INDEX ON
envelope (envelope_batch_pk);

-- An Item is any 'thing' that exists, including Research Objects.
CREATE TABLE item
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE host (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    val VARCHAR NOT NULL,
    val_null BOOLEAN NOT NULL
);

CREATE UNIQUE INDEX ON
host (
    val,
    val_null
);

CREATE TABLE scheme (
    pk SMALLSERIAL PRIMARY KEY NOT NULL,
    val VARCHAR NOT NULL,
    val_null BOOLEAN NOT NULL
);

CREATE UNIQUE INDEX ON
scheme (
    val,
    val_null
);

-- An Item Identifier is a URI, which is associated with exactly one Item.
-- Identifiers from arbitrary websites can get big, hence no length constraint on suffix.
-- There is no performance difference with PostgreSQL.
-- NULL is significant in the URI model.
CREATE TABLE item_identifier
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    scheme_pk SMALLINT NOT NULL,
    port INT NOT NULL,
    host_pk BIGINT NOT NULL,
    scheme_specific_part VARCHAR NOT NULL,
    item_pk BIGINT NOT NULL, -- references item (pk)
    -- This isn't a foreign key, it links to the IdentifierType constants in code.
    type_id INTEGER NOT NULL,
    FOREIGN KEY (scheme_pk) REFERENCES scheme (pk),
    FOREIGN KEY (host_pk) REFERENCES host (pk)
);

-- Most unique field first for efficient index:
-- The path (which comprises most of the) scheme-specific-part is the basis for nearly all difference.
CREATE INDEX ON
item_identifier (item_pk);

CREATE UNIQUE INDEX ON
item_identifier (
    scheme_specific_part,
    host_pk,
    scheme_pk,
    port
);

-- A vocabulary of relationship types.
CREATE TABLE relationship_type
(
    pk SERIAL PRIMARY KEY NOT NULL,
    value VARCHAR(190) NOT NULL,
    UNIQUE (value)
);

-- Queue of signals that an item needs re-rendering.
CREATE TABLE item_render_status_queue (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    item_pk BIGINT NOT NULL, -- references item (pk)
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX item_render_status_queue_item_pk ON
item_render_status_queue(item_pk);

-- A collection of items and their rendered content types
CREATE TABLE rendered_item
(
    pk SERIAL PRIMARY KEY NOT NULL,
    item_pk BIGINT NOT NULL, -- references item (pk)
    hash VARCHAR NOT NULL,
    CURRENT BOOLEAN NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
    public_content_type_id INTEGER NULL,
    internal_content_type_id INTEGER NULL
);

CREATE INDEX ON
rendered_item (CURRENT);

CREATE INDEX ON
rendered_item (updated_at);

CREATE TABLE open_cursor
(
    cursor_id CHAR(36) PRIMARY KEY NOT NULL,
    last_used_at TIMESTAMP WITH TIME ZONE NOT NULL,
    last_key INT NOT NULL
);

CREATE INDEX ON
open_cursor (last_used_at);

-- Queue of signals that an item tree assertion needs matching.
CREATE TABLE item_tree_match_queue
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    item_tree_assertion_pk BIGINT NULL,
    PARTITION INTEGER NOT NULL
);

CREATE INDEX item_tree_match_queue_partition_pk
  ON
item_tree_match_queue(
    PARTITION,
    item_tree_assertion_pk
);

CREATE TABLE item_tree_assertion
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    item_tree JSONB NOT NULL,
    party_pk BIGINT NOT NULL, -- references item (pk)
    root_item_pk BIGINT NOT NULL, -- references item (pk)
    envelope_pk BIGINT NOT NULL,
    FOREIGN KEY (envelope_pk) REFERENCES envelope (pk)
);

-- Allow us to retrieve the most recent (root_pk, party_pk) entry.
CREATE INDEX item_tree_assertion_latest ON
item_tree_assertion (
    root_item_pk,
    party_pk,
    ingested_at DESC
);

-- Insert into the item_tree_assertion table and enqueue that assertion for matching.
CREATE FUNCTION item_tree_assertion_trigger_f()
    RETURNS TRIGGER
    LANGUAGE plpgsql AS
$$
BEGIN
    INSERT
    INTO
    item_tree_match_queue (
        item_tree_assertion_pk,
        PARTITION
    )
VALUES (
    NEW.pk,
    1
),
(
    NEW.pk,
    2
);

RETURN NULL;
END;

$$;

-- Changes to the item_tree_assertion_current table are DELETE then INSERT, not UPDATE.
-- This gives a new PK each time. So we don't need to handle UPDATE.
CREATE TRIGGER item_tree_assertion_trigger
    AFTER
INSERT
    ON
    item_tree_assertion
    FOR EACH ROW
EXECUTE FUNCTION item_tree_assertion_trigger_f();

CREATE TABLE relationship_current
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    state BOOLEAN NOT NULL,
    item_tree_asserting_party_pk BIGINT NOT NULL, -- references item (pk)
    root_item_pk BIGINT NOT NULL, -- references item (pk)
    assertion_pk BIGINT NOT NULL,
    node_position INT NOT NULL,
    matching_party_pk BIGINT NOT NULL, -- references item (pk)
    matching_party_scope INT NOT NULL,
    subject_item_pk BIGINT NOT NULL, -- references item (pk)
    relationship_type_pk INT NOT NULL,
    object_item_pk BIGINT NOT NULL, -- references item (pk)
    sync_id BIGINT NOT NULL,
    FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type (pk)
);

-- For insertion maintenance.
CREATE INDEX ON
relationship_current (
    root_item_pk,
    item_tree_asserting_party_pk,
    matching_party_pk,
    matching_party_scope
);

-- Used for selecting affected nodes for item_tree_update_queue.
CREATE INDEX subject_root_lookup ON
relationship_current (
    object_item_pk,
    root_item_pk
);

CREATE TABLE relationship_history
(
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    ingested_at TIMESTAMP WITH TIME ZONE NOT NULL,
    state BOOLEAN NOT NULL,
    item_tree_asserting_party_pk BIGINT NOT NULL, -- references item (pk)
    root_item_pk BIGINT NOT NULL, -- references item (pk)
    assertion_pk BIGINT NOT NULL,
    node_position INT NOT NULL,
    matching_party_pk BIGINT NOT NULL, -- references item (pk)
    matching_party_scope INT NOT NULL,
    subject_item_pk BIGINT NOT NULL, -- references item (pk)
    relationship_type_pk INT NOT NULL,
    object_item_pk BIGINT NOT NULL, -- references item (pk)
    sync_id BIGINT NOT NULL,
    FOREIGN KEY (relationship_type_pk) REFERENCES relationship_type (pk)
);

CREATE TABLE registration_agency (
    pk SMALLSERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL
);

CREATE UNIQUE INDEX ON
registration_agency (name);

CREATE TABLE ra_lookup (
    pk SERIAL PRIMARY KEY NOT NULL,
    doi_prefix VARCHAR NOT NULL,
    registration_agency_pk SMALLINT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL,
    try_count SMALLINT DEFAULT 0 NOT NULL,
    FOREIGN KEY (registration_agency_pk) REFERENCES registration_agency (pk)
);

CREATE UNIQUE INDEX ON
ra_lookup (doi_prefix);

CREATE INDEX ON
ra_lookup (try_count);

CREATE TABLE identifier_ra_lookup (
    identifier_pk BIGINT PRIMARY KEY NOT NULL,
    ra_lookup_pk INT NOT NULL,
    sync_id BIGINT NOT NULL,
    FOREIGN KEY (identifier_pk) REFERENCES item_identifier (pk),
    FOREIGN KEY (ra_lookup_pk) REFERENCES ra_lookup (pk) ON DELETE CASCADE
);

CREATE INDEX ON
identifier_ra_lookup (ra_lookup_pk);

-- Canonical Item Trees, assembled from item_tree_assertions.
CREATE TABLE item_tree
(
    root_item_pk BIGINT PRIMARY KEY NOT NULL,
    rendered_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
    item_tree JSONB NOT NULL,
    update_count INT NOT NULL
);

-- Queue of signals that an item tree needs rebuilding.
CREATE TABLE item_tree_update_queue (
    pk BIGSERIAL PRIMARY KEY NOT NULL,
    root_item_pk BIGINT NOT NULL, -- references item (pk)
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX item_tree_update_queue_item_pk ON
item_tree_update_queue(root_item_pk);

CREATE TABLE datacite_type_lookup (
    identifier_pk BIGINT PRIMARY KEY NOT NULL,
    try_count SMALLINT DEFAULT 0 NOT NULL,
    FOREIGN KEY (identifier_pk) REFERENCES item_identifier (pk)
);

CREATE TABLE item_type (
    pk SMALLSERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL
);

CREATE UNIQUE INDEX ON
item_type (name);

CREATE TABLE item_subtype (
    pk SMALLSERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL
);

CREATE UNIQUE INDEX ON
item_subtype (name);

CREATE TABLE item_info (
    root_item_pk BIGINT PRIMARY KEY NOT NULL,
    type_pk SMALLINT NULL,
    subtype_pk SMALLINT NULL,
    sync_id BIGINT NOT NULL,
    FOREIGN KEY (type_pk) REFERENCES item_type (pk),
    FOREIGN KEY (subtype_pk) REFERENCES item_subtype (pk)
);

CREATE TABLE relationship_api (
    pk BIGINT PRIMARY KEY NOT NULL,
    asserted_at TIMESTAMP WITH TIME ZONE NOT NULL,
    item_tree_asserting_party_pk BIGINT NOT NULL,
    matching_party_pk BIGINT NOT NULL,
    subject_item_pk BIGINT NOT NULL,
    subject_type_pk SMALLINT NULL,
    subject_subtype_pk SMALLINT NULL,
    subject_ra_pk INT NULL,
    relationship_type_pk INT NOT NULL,
    object_item_pk BIGINT NOT NULL,
    object_type_pk SMALLINT NULL,
    object_subtype_pk SMALLINT NULL,
    object_ra_pk SMALLINT NULL,
    FOREIGN KEY (subject_type_pk) REFERENCES item_type (pk),
    FOREIGN KEY (subject_subtype_pk) REFERENCES item_subtype (pk),
    FOREIGN KEY (subject_ra_pk) REFERENCES registration_agency (pk),
    FOREIGN KEY (object_type_pk) REFERENCES item_type (pk),
    FOREIGN KEY (object_subtype_pk) REFERENCES item_subtype (pk),
    FOREIGN KEY (object_ra_pk) REFERENCES registration_agency (pk)
);

CREATE INDEX datacite_data_citations_idx ON
relationship_api (
    relationship_type_pk,
    object_ra_pk,
    pk,
    asserted_at
);

CREATE INDEX stm_data_citations_idx ON
relationship_api (
    relationship_type_pk,
    item_tree_asserting_party_pk,
    object_subtype_pk,
    pk,
    asserted_at
);

CREATE INDEX event_data_idx ON
relationship_api (
    relationship_type_pk,
    pk,
    asserted_at
);

CREATE TABLE relationship_sync (
    sync_id BIGSERIAL PRIMARY KEY NOT NULL,
    ready_to_sync BOOLEAN NOT NULL
);

-- Used for relationship sync.
CREATE INDEX ON relationship_history (sync_id);

CREATE INDEX ON relationship_current (sync_id);

CREATE INDEX ON item_info (sync_id);

CREATE INDEX ON identifier_ra_lookup (sync_id);

-- Some empty tables (e.g. scheme) will be immediately used and will never meet the default table heuristics.
-- So run the analyzer to get a good picture of the initial state.
ANALYZE;
