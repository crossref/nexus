-- Update this to add queue partition #3

-- Insert into the item_tree_assertion table and enqueue that assertion for matching.
CREATE OR REPLACE FUNCTION item_tree_assertion_trigger_f()
    RETURNS TRIGGER
    LANGUAGE plpgsql AS
$$
BEGIN
    INSERT
    INTO
    item_tree_match_queue (
        item_tree_assertion_pk,
        PARTITION
    )
VALUES (
    NEW.pk,
    1
),
(
    NEW.pk,
    2
),
(
    NEW.pk,
    3
);

RETURN NULL;
END;

$$;