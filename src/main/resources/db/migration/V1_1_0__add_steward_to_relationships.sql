ALTER TABLE item_info ADD steward_pk bigint NULL;

ALTER TABLE relationship_api ADD subject_steward_pk bigint NULL, ADD object_steward_pk bigint NULL;

CREATE INDEX relationship_object_lookup ON relationship_current (subject_item_pk, item_tree_asserting_party_pk, relationship_type_pk);

CREATE INDEX object_steward_asserted_by_idx ON
relationship_api (
    object_steward_pk,
    item_tree_asserting_party_pk,
    pk,
    asserted_at
);

CREATE INDEX object_steward_rel_type_idx ON
relationship_api (
    object_steward_pk,
    relationship_type_pk,
    pk,
    asserted_at
);

CREATE INDEX subject_steward_asserted_by_idx ON
relationship_api (
    subject_steward_pk,
    item_tree_asserting_party_pk,
    pk,
    asserted_at
);
