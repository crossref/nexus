package org.crossref.messaging.core

import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel

/**
 * A [MessageChannel] from which messages can be deleted.
 */
interface DeletableChannel : MessageChannel {
    /**
     * Delete the given message from this channel, blocking indefinitely if necessary.
     *
     * @param message the [Message] to delete.
     *
     * @return true if the message is deleted, false if not including a timeout of an interrupt of the delete
     */
    fun delete(message: Message<*>): Boolean = delete(message, MessageChannel.INDEFINITE_TIMEOUT)

    /**
     * Delete the given message from this channel, blocking until the message is deleted or the specified timeout
     * period elapses.
     *
     * @param message the [Message] to delete.
     * @param timeout the timeout in milliseconds or [MessageChannel.INDEFINITE_TIMEOUT].
     *
     * @return true if the message is deleted, false if not including a timeout of an interrupt of the delete
     */
    fun delete(message: Message<*>, timeout: Long): Boolean
}
