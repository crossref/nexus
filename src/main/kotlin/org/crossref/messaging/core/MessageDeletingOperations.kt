package org.crossref.messaging.core

import org.springframework.messaging.Message

/**
 * Operations for deleting messages from a destination.
 *
 * @param D the destination type.
 */
interface MessageDeletingOperations<D> {
    /**
     * Delete the given message from the given destination.
     *
     * @param destination the target destination.
     * @param message the message to delete.
     */
    fun delete(destination: D, message: Message<*>)

    /**
     * Receive a message from the given destination, without deleting it.
     *
     * @param destination the target destination.
     *
     * @return the received message, possibly `null` if the message could not be received, for example due to a timeout.
     */
    fun receiveNoDelete(destination: D): Message<*>?
}
