package org.crossref.messaging.aws.autoconfig

import org.crossref.messaging.aws.autoconfig.AwsAutoConfig.AWS_ENDPOINT_OVERRIDE
import org.crossref.messaging.aws.sqs.*
import org.crossref.messaging.core.MessageProcessor
import org.crossref.messaging.core.QueuePoller
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.messaging.converter.MessageConverter
import software.amazon.awssdk.awscore.defaultsmode.DefaultsMode
import software.amazon.awssdk.services.sqs.SqsAsyncClient
import java.net.URI

@Configuration(proxyBeanMethods = false)
@ConditionalOnProperty(name = [SqsAutoConfig.SQS_ENABLED], havingValue = "true")
class SqsAutoConfig {
    companion object {
        const val SQS_ENABLED = "sqs-enabled"
        const val SQS_PARALLEL_POLLERS = "sqs-parallel-pollers"
        const val SQS_BATCH_SIZE = "sqs-batch-size"
        const val SQS_WAIT_TIME_SECONDS = "sqs-wait-time-seconds"
    }

    @Bean
    fun sqsMessageHandler(messageConverter: MessageConverter, environment: Environment): SqsMessageHandler =
        SqsMessageHandler(messageConverter, environment)

    @Bean
    fun sqsAsyncClient(
        @Value("\${$AWS_ENDPOINT_OVERRIDE:}") awsEndpointOverride: String?
    ): SqsAsyncClient =
        SqsAsyncClient.builder()
            .defaultsMode(DefaultsMode.AUTO)
            .apply {
                if (!awsEndpointOverride.isNullOrBlank()) {
                    endpointOverride(URI(awsEndpointOverride))
                }
            }
            .build()

    @Bean
    fun sqsTemplate(
        sqsAsyncClient: SqsAsyncClient,
        messageConverter: MessageConverter,
        @Value("\${$SQS_BATCH_SIZE:10}") sqsBatchSize: Int,
        @Value("\${$SQS_WAIT_TIME_SECONDS:10}") sqsWaitTimeSeconds: Int
    ): SqsTemplate = SqsTemplate(sqsAsyncClient, messageConverter, sqsBatchSize, sqsWaitTimeSeconds)

    @Bean
    fun sqsListenerContainer(
        sqsTemplate: SqsTemplate,
        sqsMessageHandler: SqsMessageHandler,
        queuePoller: QueuePoller<String>,
        @Value("\${$SQS_PARALLEL_POLLERS:1}") defaultParallelPollers: Int
    ): SqsListenerContainer = SqsListenerContainer(sqsTemplate, sqsMessageHandler, queuePoller, defaultParallelPollers)

    @Bean
    fun sqsQueuePoller(sqsTemplate: SqsTemplate, messageProcessor: MessageProcessor): QueuePoller<String> =
        SqsQueuePoller(sqsTemplate, messageProcessor)

    @Bean
    fun sqsMessageProcessor(sqsMessageHandler: SqsMessageHandler): MessageProcessor =
        SqsMessageProcessor(sqsMessageHandler)
}
