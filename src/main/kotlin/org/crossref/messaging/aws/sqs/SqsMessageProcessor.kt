package org.crossref.messaging.aws.sqs

import org.apache.commons.lang3.mutable.MutableBoolean
import org.crossref.messaging.core.MessageProcessor
import org.springframework.messaging.Message
import org.springframework.messaging.MessagingException
import org.springframework.messaging.core.MessagePostProcessor

class SqsMessageProcessor(
    private val sqsMessageHandler: SqsMessageHandler,
) : MessageProcessor {
    override fun processMessage(message: Message<*>, messagePostProcessor: MessagePostProcessor?) {
        val wasSuccessful = try {
            sqsMessageHandler.handleMessage(message)
            true
        } catch (me: MessagingException) {
            false
        }
        (message.headers[SqsQueue.WAS_SUCCESSFUL] as MutableBoolean).setValue(wasSuccessful)
        messagePostProcessor?.postProcessMessage(message)
    }
}
