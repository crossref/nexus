package org.crossref.messaging.aws.sqs

/**
 * Defines the deletion policy to use after a SQS message has been processed by a [SqsListener] annotated function.
 *
 * The default policy is [SUCCESS] but this can be overridden using [SqsListener.deletePolicy]
 *
 * The available options are:
 *
 * [ALWAYS]: always delete the processed message from the queue, even if processing fails.
 *
 * [NEVER]: never delete the processed message from the queue, even if processing succeeds.
 *
 * [SUCCESS]: delete the message if processing succeeds but not if it fails.
 *
 * [FAIL]: delete the message if processing fails but not if it succeeds.
 */
enum class SqsDeletePolicy {
    ALWAYS, NEVER, SUCCESS, FAIL
}
