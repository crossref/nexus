package org.crossref.messaging.aws.sqs

import org.crossref.messaging.core.MessageProcessor
import org.crossref.messaging.core.QueuePoller
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.messaging.Message
import org.springframework.messaging.core.MessagePostProcessor
import org.springframework.scheduling.annotation.Async

class SqsQueuePoller(
    private val sqsTemplate: SqsTemplate,
    private val messageProcessor: MessageProcessor
) : QueuePoller<String> {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private var runState = false

    @Async
    override fun pollQueue(queueIdentifier: String, messagePostProcessor: MessagePostProcessor?) {
        logger.info("Setting up queue poller for $queueIdentifier")
        while (runState) {
            logger.debug("Polling queue $queueIdentifier")
            val message = sqsTemplate.receiveNoDelete(queueIdentifier)
            if (message != null) {
                (message.payload as List<*>).forEach {
                    if (it is Message<*>) {
                        messageProcessor.processMessage(it, messagePostProcessor)
                    }
                }
            }
        }
    }

    override fun startPolling() {
        runState = true
    }

    override fun stopPolling() {
        runState = false
    }
}
