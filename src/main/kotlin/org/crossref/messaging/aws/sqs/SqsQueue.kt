package org.crossref.messaging.aws.sqs

import org.apache.commons.lang3.mutable.MutableBoolean
import org.crossref.messaging.core.DeletableChannel
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.messaging.Message
import org.springframework.messaging.MessageChannel
import org.springframework.messaging.PollableChannel
import org.springframework.messaging.support.GenericMessage
import software.amazon.awssdk.services.sqs.SqsAsyncClient
import software.amazon.awssdk.services.sqs.SqsClient
import software.amazon.awssdk.services.sqs.model.DeleteMessageRequest
import software.amazon.awssdk.services.sqs.model.ReceiveMessageRequest
import software.amazon.awssdk.services.sqs.model.SendMessageRequest
import java.util.concurrent.ExecutionException
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

/**
 * A [SqsClient] based [MessageChannel] implementation for sending and receiving messages to / from an AWS SQS queue.
 *
 * It supports short and long polling depending on configured `waitTimeSeconds`, as per the SQS API spec.
 *
 * It does not automatically delete received messages from the queue, meaning repeated calls could return the same
 * message, so consumers should be idempotent.
 *
 * @param sqsAsyncClient a configured client used to communicate with the AWS SQS service.
 * @param queueName the name of the SQS queue this channel represents.
 * @param queueUrl the URL of the queue as provided by the SQS service.
 * @param batchSize the number of messages to receive per request.
 * @param waitTimeSeconds wait time of the `ReceiveMessage` SQS API action.
 */
class SqsQueue(
    private val sqsAsyncClient: SqsAsyncClient,
    val queueName: String,
    private val queueUrl: String,
    private val batchSize: Int,
    private val waitTimeSeconds: Int,
) : PollableChannel, DeletableChannel {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        const val QUEUE_NAME = "queueName"
        const val RECEIPT_HANDLE = "receiptHandle"
        const val WAS_SUCCESSFUL = "wasSuccessful"
    }

    override fun send(message: Message<*>, timeout: Long): Boolean = try {
        sendMessage(message, timeout)
        true
    } catch (e: Exception) {
        logger.error("Error sending message: ${e.message}")
        false
    }

    override fun receive(): Message<*>? = receive(MessageChannel.INDEFINITE_TIMEOUT)

    override fun receive(timeout: Long): Message<*>? = try {
        receiveMessage(timeout)
    } catch (e: Exception) {
        logger.error("Error receiving message: ${e.message}")
        null
    }

    override fun delete(message: Message<*>, timeout: Long): Boolean = try {
        deleteMessage(message, timeout)
        true
    } catch (e: Exception) {
        logger.error("Error deleting message: ${e.message}")
        false
    }

    private fun sendMessage(message: Message<*>, timeout: Long) {
        waitForFuture(
            sqsAsyncClient.sendMessage(
                SendMessageRequest.builder().queueUrl(queueUrl).messageBody(message.payload.toString()).build()
            ), timeout
        )
    }

    private fun receiveMessage(timeout: Long): Message<*>? {
        val messages = waitForFuture(
            sqsAsyncClient.receiveMessage(
                ReceiveMessageRequest.builder().queueUrl(queueUrl).maxNumberOfMessages(batchSize)
                    .waitTimeSeconds(waitTimeSeconds).build()
            ), timeout
        ).messages()
        return if (messages.isEmpty()) {
            null
        } else {
            GenericMessage(messages.map {
                GenericMessage(
                    it.body(), mapOf(
                        QUEUE_NAME to queueName,
                        RECEIPT_HANDLE to it.receiptHandle(),
                        // a bit of a cludge because message headers are immutable but this value needs changing
                        WAS_SUCCESSFUL to MutableBoolean()
                    )
                )
            })
        }
    }

    private fun deleteMessage(message: Message<*>, timeout: Long) {
        waitForFuture(
            sqsAsyncClient.deleteMessage(
                DeleteMessageRequest.builder().queueUrl(queueUrl)
                    .receiptHandle(message.headers[RECEIPT_HANDLE] as String).build()
            ), timeout
        )
    }

    private fun <T : Any?> waitForFuture(future: Future<T>, timeout: Long): T {
        try {
            return if (timeout == MessageChannel.INDEFINITE_TIMEOUT) {
                future.get()
            } else {
                future.get(timeout, TimeUnit.MILLISECONDS)
            }
        } catch (e: Exception) {
            throw if (e is ExecutionException) {
                // unwrap the concurrent execution exception and throw the cause
                e.cause as Throwable
            } else {
                e
            }
        }
    }
}
