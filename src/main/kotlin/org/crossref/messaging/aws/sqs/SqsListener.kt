package org.crossref.messaging.aws.sqs

/**
 * Annotation for mapping a [org.springframework.messaging.Message] onto a listener function by matching the SQS queue
 * name.
 *
 * Annotated functions must contain a single argument which will be the extracted message payload. If the argument type
 * is [String] then it will contain the message payload as JSON. Otherwise Spring will attempt to convert the payload
 * to the argument type using a [org.springframework.messaging.converter.MessageConverter].
 *
 * Annotated functions should throw a [org.springframework.messaging.MessagingException] if message processing fails.
 *
 * @param queueNameEnvVar the environment variable to lookup for the name of the SQS queue being listened to.
 * @param deletePolicy the delete policy to use when message has been processed. Default is [SqsDeletePolicy.SUCCESS]
 * @param parallelPollers the number of pollers to run in parallel. Zero or less reverts to the global default.
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class SqsListener(
    val queueNameEnvVar: String,
    val deletePolicy: SqsDeletePolicy = SqsDeletePolicy.SUCCESS,
    val parallelPollers: Int = 0
)
