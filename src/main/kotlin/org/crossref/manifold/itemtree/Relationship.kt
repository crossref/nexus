package org.crossref.manifold.itemtree

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.OffsetDateTime

data class RelationshipType(val typ: String, val pk: Long? = null) {
    companion object;

    /**
     * Return a copy of this object with the given PK.
     */
    fun withPk(newPk: Long) = RelationshipType(typ, newPk)
}

/**
 * An item Tree that records a tree, but with the asserting party recorded at each point.
 * This based on [org.crossref.manifold.itemtree.Item] tree with assertion info grafted on.
 */
data class Relationship(
    /**
     * Type of this Relationship.
     */
    @JsonProperty("relationship-type")
    @get:JsonProperty("relationship-type")
    val relTyp: String,

    /**
     * The Item object that is the object of this Relationship.
     * This is the point of recursion in the tree.
     */
    @JsonProperty("object")
    @get:JsonProperty("object")
    val obj: Item,

    /**
     * Multiple parties making this assertion.
     * Allow this to be constructed with a default asserted by no-one.
     */
    @JsonProperty("asserted-by")
    @get:JsonProperty("asserted-by")
    val assertedBy: Item? = null,

    @JsonProperty("asserted-at")
    @get:JsonProperty("asserted-at")
    val assertedAt: OffsetDateTime? = null,

    @JsonIgnore
    val relTypePk: Int? = null,
) {
    companion object

    /**
     * Return a copy of this object with the Item.
     */
    fun withItem(newItem: Item) = this.copy(obj = newItem)

    fun withPk(newPk: Int?): Relationship = this.copy(relTypePk = newPk)
}
