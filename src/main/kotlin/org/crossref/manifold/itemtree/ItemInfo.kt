package org.crossref.manifold.itemtree

data class ItemInfo(
    val rootItemPk: Long,
    val typePk: Int?,
    val subtypePk: Int?,
    val stewardPk: Long?
)
