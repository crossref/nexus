package org.crossref.manifold.itemtree

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer

/**
 * Serialize ItemIdentifier to a string.
 */
class IdentifierSerializer @JvmOverloads constructor(t: Class<Identifier?>? = null) :
    StdSerializer<Identifier>(t) {
    override fun serialize(identifier: Identifier?, generator: JsonGenerator?, provider: SerializerProvider?) {
        identifier?.let {
            generator?.writeString(it.uri.toString())
        }

    }
}
