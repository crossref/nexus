package org.crossref.manifold.itemtree

import org.crossref.manifold.itemtree.compact.CompactItemTree

/**
 * Entry in the canonical Item Tree table.
 * Stores the current canonical version of an Item Tree, along with fields brought out for indexing.
 */
data class CanonicalItemTreeEntry(
    val tree: CompactItemTree,
    val rootItemPk: Long,
    val type: String?,
    val subtype: String?
)