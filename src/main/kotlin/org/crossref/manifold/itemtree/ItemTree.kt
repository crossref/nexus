package org.crossref.manifold.itemtree

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.OffsetDateTime


/**
 * An Item Tree.
 * Wraps an Item node, which forms the root of the tree inside.
 * Using this class makes it easier to tell the difference between a recursive tree and a single item.
 *
 */
data class ItemTree(
    @JsonProperty("root")
    val root: Item,

    // Node indexes need building on construction.
    // This class is immutable, and so is the node index that goes along with it.
    @JsonIgnore
    private val nodeIndexes: Map<Item, Int> = constructNodeIndexes(root),
) {

    /**
     * For an item in the tree, retrieve its position index in the tree.
     */
    @JsonIgnore
    fun getPosition(item: Item): Int? = nodeIndexes.get(item)

    /**
     * For an index position, find that node.
     */
    @JsonIgnore
    fun getNode(position: Int): Item? = nodeIndexes.firstNotNullOfOrNull { (k, v) -> if (v == position) k else null }

    companion object {
           /**
         * Construct a map of each node to an ID that's unique within this tree.
         */
        fun constructNodeIndexes(root: Item) =
            root.toSequence().mapIndexed { index, item -> item to index }.toMap()

    }

    fun countItems() = this.root.countItems()

    /**
     * Retrieve the set of indexed nodes in the tree.
     */
    @JsonIgnore
    fun getNodes() = nodeIndexes.entries.map { Pair(it.key, it.value) }

    @JsonIgnore
    fun getType() = root.getPropertyText("type")
    @JsonIgnore
    fun getSubType() = root.getPropertyText("subtype")

    /**
     * Add the asserting party PK and timestamp to all property and relationship assertions.
     */
    fun annotateAssertionInTree(partyPk: Long, assertedAt: OffsetDateTime): ItemTree {
        val party = Item(pk = partyPk)

        fun recurse(item: Item): Item =
            item.withProperties(item.properties.map { it.copy(assertedBy = party, assertedAt = assertedAt) })
                .withRelationships(item.rels.map { it.copy(assertedBy = party, assertedAt = assertedAt, obj = recurse(it.obj)) })

        return ItemTree(recurse(this.root))
    }
}