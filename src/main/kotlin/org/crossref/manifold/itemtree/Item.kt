package org.crossref.manifold.itemtree

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import org.crossref.manifold.identifiers.IdentifierType


//@JsonPropertyOrder("identifiers", "blank", "relationships", "properties")
data class Item(
    /**
     * Identifiers by which this Item is known. There can be zero, one or more Item Identifiers. Ordered.
     * In some cases the order may be useful, e.g. sorting Identifiers by preference.
     */
    @JsonProperty("identifiers")
    @get:JsonProperty("identifiers")
    val identifiers: List<Identifier> = emptyList(),

    /**
     * Relationships to other Items. This is ordered, giving users the ability to consider order meaningful or not.
     * In some cases the order may be useful.
     */
    @JsonProperty("relationships")
    @get:JsonProperty("relationships")
    val rels: List<Relationship> = emptyList(),

    /**
     * A map of properties attached to this Item, e.g. title, along with the asserting party for each.
     */
    @JsonProperty("properties")
    @get:JsonProperty("properties")
    val properties: List<Properties> = emptyList(),

    /**
     * Primary Identifier for this Item. May be null when not known.
     */
    val pk: Long? = null,
) {
    companion object


    /**
     * Copy constructor to add new ID.
     */
    fun withPk(newPk: Long) = Item(identifiers, rels, properties, newPk)
    fun withIdentifiers(newIdentifiers: List<Identifier>): Item = Item(newIdentifiers, rels, properties, pk)
    fun withIdentifier(newIdentifier: Identifier): Item = this.withIdentifiers(listOf(newIdentifier))


    /** Replace all relationships with a new set.
     */
    fun withRelationships(newRelationships: List<Relationship>) = Item(
        identifiers,
        newRelationships,
        properties,
        pk
    )

    fun withRelationship(relationship: Relationship): Item =
        this.withRelationships(this.rels + relationship)

    fun hasNoIdentifiers(): Boolean = this.identifiers.isEmpty()

    /** When an Item is unambiguous we can find it in the graph.
     * If it's ambiguous, we might find two Items, or be unable to find any Items.
     * Jackson will try to serialize this field as a getter without the JsonIgnore.
     */
    @JsonIgnore
    fun isAmbiguous(): Boolean = this.identifiers.count() > 1

    fun withProperties(newProperties: List<Properties>) = Item(identifiers, rels, newProperties, pk)
    fun withPropertiesFromMap(newProperties: Map<String, Any?>) =
        this.withProperties(listOf(propertiesFromSimpleMap(newProperties)))

    /**
     * Search the tree for subject, relationshipType, object triple.
     * Naive brute-force Look for these at any level of the tree.
     */
    fun containsTriple(subjectUri: String, relationshipType: String, objectUri: String, assertedBy: String): Boolean {
        fun recurse(item: Item): Boolean =
            if (item.identifiers.any { it.uri.toString() == subjectUri } &&
                item.rels.any { rel ->
                    rel.relTyp == relationshipType
                            && rel.obj.identifiers.any { it.uri.toString() == objectUri }
                            && rel.assertedBy?.identifiers?.any { it.uri.toString() == assertedBy } == true
                })
                true
            else
                item.rels.any { recurse(it.obj) }

        return recurse(this)
    }

    fun containsProperty(subjectUri: String, property: String, value: String, assertedBy: String): Boolean {
        fun recurse(item: Item): Boolean =
            if (item.identifiers.any { it.uri.toString() == subjectUri } &&
                item.properties.any { it.values[property]?.textValue() == value
                        && it.assertedBy?.identifiers?.any { id -> id.uri.toString() == assertedBy} == true })
                true
            else
                item.rels.any { recurse(it.obj) }

        return recurse(this)
    }

    fun countItems() : Int = 1 + this.rels.sumOf { it.obj.countItems() }

    /**
     * Flatten this tree into a list of items.
     * This operation is deterministic, because the relationships are stored as ordered lists.
     * It always returns the same result for a given tree.
     */
    fun toSequence(): List<Item> = listOf(this) + this.rels.flatMap { it.obj.toSequence() }

    /**
     * Return a sequence of relationships that comprise this tree.
     * Items in subject and object position will be full items (i.e. subtrees).
     */
    private fun toRelationshipSequence(): List<Relationship> = toSequence().flatMap { it.rels }

    /**
     * Return a sequence of relationships of the given type, found anywhere in the tree.
     * Items in subject and object position will be full items (i.e. subtrees).
     */
    fun findRelationships(relationshipType: String) = toRelationshipSequence().filter { it.relTyp == relationshipType }

    /**
     * Return this tree, with relationships of a given type removed.
     */
    fun filterByRelationship(predicate: (Relationship) -> Boolean): Item {
        fun recurse(item: Item) : Item =
            item.copy(rels = item.rels.filter { predicate(it)}.map { it.copy(obj = recurse(it.obj)) } )

        return recurse(this)
    }

    fun getPropertyText(key: String, default: String? = null) =
        this.properties.firstOrNull()?.values?.get(key)?.textValue() ?: default

    fun getPropertyArray(key: String) : List<JsonNode>? =
        this.properties.firstOrNull()?.values?.get(key)?.let {
            if (it.isArray) {
                it.toList()
            } else null
    }

    fun getPropertyArrayFirstValue(key: String) : String? =
        getPropertyArray(key)?.firstOrNull()?.get("value")?.textValue()

    fun getPropertyObject(key: String) : JsonNode? =
        this.properties.firstOrNull()?.values?.get(key)?.let {
            if (it.isObject) {
                it
            } else null
        }

    fun getProperty(key: String) : JsonNode? {
        val value = this.properties.firstOrNull()?.values?.get(key)
        return when {
            value == null -> null
            value.isObject -> value
            value.isArray -> value.toList().firstOrNull()
            else -> null
        }
    }

    fun identifiersOfType(identifierType: IdentifierType): List<Identifier> =
        this.identifiers.filter { it.type== identifierType }

    /**
     * Get all direct descendants of the given relationship type.
     */
    fun childrenWithRel(relType: String) : List<Item> = this.rels.filter { it.relTyp == relType }.map { it.obj }

}
