package org.crossref.manifold.itemtree.compact

import com.fasterxml.jackson.annotation.JsonProperty


data class CompactItem(
    /**
     * Relationships to other Items. This is ordered, giving users the ability to consider order meaningful or not.
     * In some cases the order may be useful.
     */
    @JsonProperty("R")
    @get:JsonProperty("R")
    val rels: List<CompactRelationship> = emptyList(),

    /**
     * A map of properties attached to this Item, e.g. title, along with the asserting party for each.
     */
    @JsonProperty("P")
    @get:JsonProperty("P")
    val properties: List<CompactProperties> = emptyList(),

    /**
     * Primary Identifier for this Item. May be null when not known.
     */
    @JsonProperty("T")
    @get:JsonProperty("T")
    val pk: Long? = null,
) {
    companion object

    /**
     * Flatten this tree into a list of items.
     * This operation is deterministic, because the relationships are stoerd as ordered lists.
     * It always returns the same result for a given tree.
     */
    fun toSequence(): List<CompactItem> = listOf(this) + this.rels.flatMap { it.obj.toSequence() }

    /**
     * Recursive check that the two trees are identical, except disregard the assertion time.
     */
    fun equalsIgnoringTime(other: CompactItem): Boolean =
        this.properties.zip(other.properties)
            .all { (old, new) -> old.values == new.values &&
                    old.assertedBy == new.assertedBy } &&
                // Zip takes shorter length. Check that they have the same length.
                this.rels.count() == other.rels.count() &&
                this.rels.zip(other.rels).all { (old, new) ->
                    old.relTypePk == new.relTypePk &&
                            old.assertedBy == new.assertedBy &&
                            new.obj.equalsIgnoringTime(old.obj)
                }
}
