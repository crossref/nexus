package org.crossref.manifold.itemtree.compact

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ObjectNode
import org.crossref.manifold.matching.RelationshipMatch
import java.time.OffsetDateTime


/**
 * An Item Tree.
 * Wraps an Item node, which forms the root of the tree inside.
 * Using this class makes it easier to tell the difference between a recursive tree and a single item.
 *
 * This class also maintains an index for each node in the tree. This is useful for referencing and retrieving
 * nodes in the tree by their indexed position in the tree.
 *
 * NOTE ON JSON: Because of the nodeIndexes, this class needs specific JSON Serialization.
 * This class can be serialized two different ways:
 *  - 'standard' - ItemTreeSerializer and ItemTreeDeserializer. Stores all fields, for use in batch files etc.
 *  - 'compact' - ItemTreeCompactSerializer and ItemTreeCompactDeserializer. This stores only PKs, not identifiers. For use in the database.
 *
 *  Because there's a choice, these are not attached as annotations, and are instead attached directly to JsonMappers
 *  depending on context.
 */
data class CompactItemTree(
    @JsonProperty("R")
    @get:JsonProperty("R")
    val root: CompactItem,
) {

    /**
     * For an item in the tree, retrieve its position index in the tree.
     */
    @JsonIgnore
    fun getPosition(item: CompactItem, nodeIndexes: Map<CompactItem, Int>): Int? = nodeIndexes.get(item)

    /**
     * For an index position, find that node.
     */
    @JsonIgnore
    fun getNode(position: Int, nodeIndexes: Map<CompactItem, Int>): CompactItem? = nodeIndexes.firstNotNullOfOrNull { (k, v) -> if (v == position) k else null }

    companion object {
        /**
         * Construct a map of each node to an ID that's unique within this tree.
         */
        fun constructNodeIndexes(root: CompactItem) =
            root.toSequence().mapIndexed { index, item -> item to index }.toMap()

        /**
         * Given a set of current ItemTree assertions and RelationshipMatch assertions.
         * Currently, grafts only at the first level.
         * Currently, doesn't merge relationship assertions back in. This will be needed when they are matched.
         */
        fun graft(trees: Collection<CompactItemTree>, relationshipMatches: Collection<RelationshipMatch>): CompactItemTree? {
            // If this check ever does fail, check in three steps for a bit more diagnostic info.
            check(
                trees.map { it.root.pk }.toSet().count() == 1
            ) { "All trees must be for the same root node. Got ${trees.map { it.root.pk }}" }

            // There may be 0 relationship matches. If so, 0 is fine.
            check(
                relationshipMatches.map { it.rootItemPk }.toSet().count() <= 1
            ) { "All relationships must be for the same root node. Got ${trees.map { relationshipMatches.map { it.rootItemPk } }}" }

            check((trees.map { it.root.pk } + relationshipMatches.map { it.rootItemPk }).toSet().count() == 1
            ) { "All trees and relationships must be for the same root node." }

            val roots = trees.map { it.root }
            val newRoot = if (roots.isNotEmpty()) {
                // Combine all relationships togehter. Don't try to resolve any.
                val allRels = roots.flatMap { it.rels }

                val newPropertyList = pickLatestProperties(roots)

                CompactItem(pk = roots.first().pk, rels = allRels, properties = listOf(newPropertyList))
            } else null


            return newRoot?.let { CompactItemTree(it) }
        }

        /**
         * Combine properties from a number of CompactItems into one single CompactProperties object.
         * Where there are conflicting values for a property key, use a strategy to pick one:
         *  - Where a value for a key is present in one or more assertions with a date, use the most recently asserted one.
         *  - Where there are no dates, pick an arbitrary one.
         */
        private fun pickLatestProperties(roots: List<CompactItem>): CompactProperties {
            // Sort latest first, so we can find the latest of each key.
            val sortedProps = roots.flatMap { it.properties }.sortedByDescending { it.assertedAt ?: OffsetDateTime.MIN }

            val propertyKeys = sortedProps.mapNotNull {
                (if (it.values.isObject) {
                    it.values
                } else null)?.fieldNames()?.asSequence()?.toList()
            }.flatten().toSet()

            val newProps = ObjectNode(JsonNodeFactory.instance)
            for (keyName in propertyKeys) {
                // Linear search for the first value that matches the key.
                val found = sortedProps.firstNotNullOfOrNull { it.values.get(keyName) }

                newProps.set<ObjectNode>(keyName, found)
            }
            return CompactProperties(newProps)
        }
    }

    /**
     * Retrieve the set of indexed nodes in the tree.
     */
    fun getNodes(nodeIndexes: Map<CompactItem, Int>) = nodeIndexes.entries.map { Pair(it.key, it.value) }
}