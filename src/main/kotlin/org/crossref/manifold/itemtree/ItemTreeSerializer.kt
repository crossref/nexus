package org.crossref.manifold.itemtree

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.ser.std.StdSerializer


/**
 * Standard serializer for ItemTree.
 * This is needed because there's an alternative [ItemTreeCompactSerializer].
 */
class ItemTreeSerializer @JvmOverloads constructor(t: Class<ItemTree?>? = null) :
    StdSerializer<ItemTree>(t) {
    override fun serialize(itemTree: ItemTree, generator: JsonGenerator?, provider: SerializerProvider?) {
        if (generator != null) {
            generator.writeStartObject()
            generator.writeObjectField("root", itemTree.root)
            generator.writeEndObject()
        }
    }
}
