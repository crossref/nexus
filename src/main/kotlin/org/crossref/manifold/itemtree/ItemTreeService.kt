package org.crossref.manifold.itemtree

import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.ItemTreeAssertionDao
import org.crossref.manifold.itemtree.compact.CompactItem
import org.crossref.manifold.itemtree.compact.CompactItemTree
import org.crossref.manifold.itemtree.compact.CompactProperties
import org.crossref.manifold.itemtree.compact.CompactRelationship
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.crossref.manifold.retrieval.IdentifierRetriever
import org.springframework.stereotype.Service

/**
 * Store and retrieve [ItemTree]s, via their [CompactItemTree] representation.
 */
@Service
class ItemTreeService(
    private val itemTreeDao : CanonicalItemTreeDao,
    private val itemTreeAssertionDao: ItemTreeAssertionDao,
    private val relationshipTypeRegistry: RelationshipTypeRegistry,
    private val itemDao: ItemDao
) {

    /**
     * Transform one or more Item Trees to CompactItemTree for storage.
     * This assumes that all Items with Identifiers have been resolved, and those that haven't, can't be (e.g. they are
     * reified nodes that don't have identifiers).
     *
     * If any relationship types aren't recognised, they are dropped.
     */
    fun toCompact(trees: List<ItemTree>) : List<CompactItemTree> {
        fun recurse(item: Item) : CompactItem {
            // If relationships are unrecognised, they are dropped.
            val rels = item.rels.mapNotNull { rel -> relationshipTypeRegistry.resolve(rel.relTyp)?.let {
                val obj = recurse(rel.obj)
                val assertedBy = rel.assertedBy?.let { recurse(it) }
                CompactRelationship(obj, assertedBy, it, assertedAt = rel.assertedAt)
            } }

            val props= item.properties.map { prop ->
                val assertedBy = prop.assertedBy?.let { recurse(it) }
                CompactProperties(prop.values, assertedBy = assertedBy, assertedAt = prop.assertedAt)
            }

            return CompactItem(rels, props, item.pk)
        }

        return trees.map{CompactItemTree(recurse(it.root))}
    }

    fun toCompact(tree: ItemTree) : CompactItemTree =
        toCompact(listOf(tree)).first()


    /**
     * Convert one or more compact item tree into a normal one, with identifiers and relationship types.
     */
    fun fromCompact(trees: List<CompactItemTree>) : List<ItemTree> {
        // Batched lookup of all itemPks.
        val itemPks = trees.flatMap { tree -> tree.root.toSequence().mapNotNull { it.pk } }.toSet()

        val assertingPartyPks = trees.flatMap { tree ->
            tree.root.toSequence()
                .flatMap {
                    it.rels.mapNotNull {
                        it.assertedBy?.let { it.pk }
                    } +
                    it.properties.mapNotNull {
                        it.assertedBy?.let { it.pk } } }
                }.toSet()

        val lookup = IdentifierRetriever(itemDao)
        lookup.prime(itemPks)
        lookup.prime(assertingPartyPks)

        fun recurse(item: CompactItem): Item {
            val identifiers = lookup.get(item.pk) ?: emptyList()

            val rels = item.rels.mapNotNull { rel ->
                relationshipTypeRegistry.reverse(rel.relTypePk)?.let {
                val obj = recurse(rel.obj)
                val assertedBy = rel.assertedBy?.let { recurse(it) }
                Relationship(it, obj, assertedBy, rel.assertedAt, rel.relTypePk)
            } }

            val props= item.properties.map { prop ->
                val assertedBy = prop.assertedBy?.let {
                    recurse(it)
                }
                Properties(prop.values, assertedBy = assertedBy, assertedAt = prop.assertedAt)
            }

            return Item(identifiers, rels, props, item.pk)
        }

        return trees.map{ItemTree(recurse(it.root))}
    }

    /**
     * Given a set of ItemPks, return the canonical ItemTree entry, both in compact and in full form.
     */
    fun getCanonicalItemTrees(itemPks: List<Long>): List<Pair<CanonicalItemTreeEntry, ItemTree>> {
        val compacts = itemTreeDao.get(itemPks)
        val fullTrees = fromCompact(compacts.map { it.tree })
        return compacts.zip(fullTrees)
    }

    fun getCanonicalItemTree(pk: Long): Pair<CanonicalItemTreeEntry, ItemTree>? = getCanonicalItemTrees(listOf(pk)).firstOrNull()

}

