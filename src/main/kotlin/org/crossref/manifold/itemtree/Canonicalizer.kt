package org.crossref.manifold.itemtree

import org.crossref.manifold.itemtree.compact.CompactItemTree
import org.crossref.manifold.matching.RelationshipMatch
import org.springframework.stereotype.Service

@Service
class Canonicalizer {
    /**
     * Given a set of current ItemTree assertions and RelationshipMatch assertions.
     *
     * As yet unimplemented:
     *  - pulling in related item trees for leaf nodes
     *  - selection in the spirit of the 'default' perspective
     *  - merging / enforcing one property assertion. Especially important when there are conflicting values which need to be single, e.g. type subtype.
     *
     */
    fun canonicalize(
        trees: Collection<CompactItemTree>,
        relationshipMatches: Collection<RelationshipMatch>,
    ): CanonicalItemTreeEntry? {

        // If this check ever does fail, check in three steps for a bit more diagnostic info.
        check(
            trees.map { it.root.pk }.toSet().count() == 1
        ) { "All trees must be for the same root node. Got ${trees.map { it.root.pk }}" }

        // There may be 0 relationship matches. If so, 0 is fine.
        check(
            relationshipMatches.map { it.rootItemPk }.toSet().count() <= 1
        ) {
            "All relationships must be for the same root node. Got ${trees.map { relationshipMatches.map { it.rootItemPk } }}" }


        check((trees.map { it.root.pk } + relationshipMatches.map { it.rootItemPk }).toSet().count() == 1
        ) { "All trees and relationships must be for the same root node." }

        // Union all the trees and relationship matches.
        return CompactItemTree.graft(trees, relationshipMatches)?.let { grafted ->
            // In theory, it's possible we get a tree with an unresolved root.
            // Ignore this, it's meaningless to store it as we couldn't then locate it!
            grafted.root.pk?.let { rootPk ->
                val type = grafted.root.properties.firstNotNullOfOrNull { it.values["type"]?.asText() }
                val subType = grafted.root.properties.firstNotNullOfOrNull { it.values["subtype"]?.asText() }

                // Current behaviour is to not make any further choices about the tree.
                CanonicalItemTreeEntry(grafted, rootPk, type, subType)
            }
        }
    }
}
