package org.crossref.manifold.itemtree

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import org.crossref.manifold.identifiers.IdentifierType
import java.net.URI


/**
 * An Item Identifier within an Item Tree.
 * @param uri URL of the identifier.
 * @param pk primary key of the ItemIdentifier (not its corresponding Item!) in the Item Graph. This is null until it's resolved.
 *
 * Usually constructed with [IdentifierParser] which takes an input string, normalizes, and detects the type.
 *
 */
@JsonDeserialize(using = IdentifierDeserializer::class)
@JsonSerialize(using = IdentifierSerializer::class)
data class Identifier(
    @JsonProperty("uri")
    @get:JsonProperty("uri")
    val uri: URI,

    val type: IdentifierType,

    @JsonIgnore
    val pk: Long? = null
) {
    companion object

    @JsonIgnore
    fun getScheme(): String? = uri.scheme

    @JsonIgnore
    fun getHost(): String? = uri.host

    /**
     * Get the host for display purposes.
     * Return "none" on those rare occasions where there isn't one.
     */
    @JsonIgnore
    fun getHostDisplay(): String = uri.host ?: "none"

    /** Default -1 if not supplied.
     */
    @JsonIgnore
    fun getPort(): Int = uri.port

    @JsonIgnore
    fun getPath(): String? = uri.path

    @JsonIgnore
    fun getQuery(): String? = uri.query

    @JsonIgnore
    fun getFragment(): String? = uri.fragment

    /**
     * Return a copy of this object with the given PK.
     */
    fun withPk(newPk: Long) = this.copy(pk = newPk)
}

