package org.crossref.manifold.itemtree

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import org.crossref.manifold.identifiers.IdentifierParser
import java.io.IOException

/**
 * Deserialize ItemIdentifiers. Disregard pk, as these should not be persisted.
 */
class IdentifierDeserializer : StdDeserializer<Identifier>(IdentifierDeserializer::class.java) {
    @Throws(IOException::class, JsonProcessingException::class)
    override fun deserialize(p: JsonParser, ctxt: DeserializationContext?): Identifier {
        val oc: ObjectCodec = p.getCodec()
        val node: JsonNode = oc.readTree(p)

        val uri = node.asText()

        // Run this through the parser. Its behaviour is defined to be stable.
        return IdentifierParser.parse(uri)
    }
}