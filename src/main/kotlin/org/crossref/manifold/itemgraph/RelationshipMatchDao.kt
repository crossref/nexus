package org.crossref.manifold.itemgraph

import org.crossref.manifold.matching.RelationshipMatch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet
import java.time.OffsetDateTime

@Repository
class RelationshipMatchDao(
    jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        const val CHUNK_SIZE = 100
    }

    /**
     * Assert a chunk of Relationship Matches.
     *
     * Relationship Matches subject-relationship-object triples are asserted within a group of:
     * (itemTreeAssertingParty, matchingParty, scope, rootItemPk)
     *
     * Any matches may be supplied to this function. BUT within an invocation, every group must contain all assertions
     * for that group. This is because that group's worth of assertions are replaced wholesale.
     *
     * This means that if an item tree is being matched, all matches for that item tree must be supplied.
     *
     * If there are deadlocks (occasionally happens due to concurrent inserts) these are not handled here. This runs in
     * a transaction managed higher in the call stack, so retrying here after a failure would only run in an aborted
     * transaction.
     *
     * @param[matches] collection of match, along with the
     */
    fun assertMatches(matches: Collection<RelationshipMatch>, syncId: Long) {

        // If we get no matches we can skip. 
        if (matches.isEmpty()) {
            return
        }

        logger.debug("Assert ${matches.count()} matches")
        matches.forEach {
            logger.debug("{}", it)
        }

        // Find the distinct values that cover the chunks of matches we want to replace.
        data class Quad(val itemTreeAssertingParty: Long, val matchingParty: Long, val scope: Int, val rootItemPk: Long)

        val groups = matches.map { match ->
            Quad(
                match.itemTreeAssertingParty,
                match.matchingParty,
                match.scope,
                match.rootItemPk
            )
        }.distinct()

        groups.chunked(CHUNK_SIZE) { chunk ->
            // Sort to avoid deadlocks.
            val sorted = chunk.sortedWith(
                compareBy({ it.rootItemPk },
                    { it.itemTreeAssertingParty },
                    { it.matchingParty },
                    { it.scope })
            )

            // Move current statements across to history, marking state as false.
            // We'll replace with a new set of assertions.
            // Because all new assertions will be different (due to different assertion_pk) this is correct behaviour.
            // Uses the relationship_current index.
            npTemplate.update(
                """
                    WITH deleted
                    AS
                        (DELETE FROM relationship_current
                        WHERE
                            (root_item_pk, item_tree_asserting_party_pk, matching_party_pk, matching_party_scope) 
                            IN
                            (:values)
                        RETURNING
                            pk,
                            asserted_at,
                            ingested_at,
                            FALSE,
                            item_tree_asserting_party_pk,
                            root_item_pk,
                            assertion_pk,
                            node_position,
                            matching_party_pk,
                            matching_party_scope,
                            subject_item_pk,
                            relationship_type_pk,
                            object_item_pk)
                    INSERT INTO relationship_history (
                        pk,
                        asserted_at,
                        ingested_at,
                        state,
                        item_tree_asserting_party_pk,
                        root_item_pk,
                        assertion_pk,
                        node_position,
                        matching_party_pk,
                        matching_party_scope,
                        subject_item_pk,
                        relationship_type_pk,
                        object_item_pk,
                        sync_id)
                    SELECT
                        pk,
                        asserted_at,
                        ingested_at,
                        FALSE,
                        item_tree_asserting_party_pk,
                        root_item_pk,
                        assertion_pk,
                        node_position,
                        matching_party_pk,
                        matching_party_scope,
                        subject_item_pk,
                        relationship_type_pk,
                        object_item_pk,
                        :syncId
                    FROM deleted;""",
                mapOf(
                    "values" to sorted.map {
                        arrayOf(
                            it.rootItemPk,
                            it.itemTreeAssertingParty,
                            it.matchingParty,
                            it.scope
                        )
                    }, "syncId" to syncId
                ))
        }

        // If the same item tree was submitted twice in a batch, it would result in the same matches.
        // If these were inserted in one batch they would violate the unique constraint.
        // Guard against that here by de-duplicating identical statements (else we'd need to do it in SQL).
        val distinctMatches = matches.distinctBy {
            listOf(
                it.subjectPk,
                it.relationshipType,
                it.objectPk,
                it.itemTreeAssertingParty,
                it.rootItemPk,
                it.nodePosition,
                it.matchingParty,
                it.scope
            )
        }

        // Sort on the unique fields to avoid deadlocks.
        val sortedDistinct = distinctMatches.sortedWith(compareBy(
                {it.rootItemPk},
                {it.itemTreeAssertingParty},
                {it.matchingParty},
                {it.scope},
                {it.subjectPk},
                {it.relationshipType},
                {it.objectPk}))

        val props = sortedDistinct.map { match ->
            mapOf(
                "asserted_at" to match.itemTreeAssertedAt,
                "state" to match.state,
                "item_tree_asserting_party_pk" to match.itemTreeAssertingParty,
                "root_item_pk" to match.rootItemPk,
                "assertion_pk" to match.assertionPk,
                "node_position" to match.nodePosition,
                "matching_party_pk" to match.matchingParty,
                "matching_party_scope" to match.scope,
                "subject_item_pk" to match.subjectPk,
                "relationship_type_pk" to match.relationshipType,
                "object_item_pk" to match.objectPk,
                "syncId" to syncId
            )
        }.toTypedArray()

        npTemplate.batchUpdate(
            """
            INSERT INTO relationship_current (
                asserted_at,
                state,
                item_tree_asserting_party_pk,
                root_item_pk,
                assertion_pk,
                node_position,
                matching_party_pk,
                matching_party_scope,
                subject_item_pk,
                relationship_type_pk,
                object_item_pk,
                sync_id)
            VALUES (
                :asserted_at,
                :state,
                :item_tree_asserting_party_pk,
                :root_item_pk,
                :assertion_pk,
                :node_position,
                :matching_party_pk,
                :matching_party_scope,
                :subject_item_pk,
                :relationship_type_pk,
                :object_item_pk,
                :syncId)""",
            props,
        )
    }

    fun getAllCurrentForRoot(itemPks: Set<Long>): Collection<RelationshipMatch> =
        if (itemPks.isEmpty()) {
            emptyList()
        } else {
            val allCurrentForRoot = mutableListOf<RelationshipMatch>()
            itemPks.chunked(1000) { chunk ->
                allCurrentForRoot.addAll(
                    npTemplate.query(
                        """
                        SELECT asserted_at,
                            ingested_at,
                            state,
                            item_tree_asserting_party_pk,
                            root_item_pk,
                            assertion_pk,
                            node_position,
                            matching_party_pk,
                            matching_party_scope,
                            subject_item_pk,
                            relationship_type_pk,
                            object_item_pk
                        FROM relationship_current
                        WHERE root_item_pk IN (:root_item_pks)
                        """.trimIndent(), mapOf("root_item_pks" to chunk)
                    ) { it, _ ->
                        rowMapper(it)
                    }
                )
            }
            allCurrentForRoot
        }

    private fun rowMapper(row: ResultSet): RelationshipMatch =
        RelationshipMatch(
            row.getLong("item_tree_asserting_party_pk"),
            row.getObject("asserted_at", OffsetDateTime::class.java),
            row.getLong("root_item_pk"),
            row.getLong("assertion_pk"),
            row.getInt("node_position"),
            row.getLong("matching_party_pk"),
            row.getInt("matching_party_scope"),
            row.getLong("subject_item_pk"),
            row.getInt("relationship_type_pk"),
            row.getLong("object_item_pk"),
            row.getBoolean("state")
        )
}