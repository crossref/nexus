package org.crossref.manifold.itemgraph

import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.IngestionDao
import org.crossref.manifold.itemtree.ItemTreeService
import org.crossref.manifold.itemtree.compact.CompactItemTree
import org.crossref.manifold.util.logDuration
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

data class CompactItemTreeAssertion(
    val tree: CompactItemTree,
    val assertingPartyPk: Long,
    val rootItemPk: Long,
    val assertedAt: OffsetDateTime,
    val envelopePk: Long,
)

@Service
class ItemGraph(
    private val resolver: Resolver,
    private val ingestionDao: IngestionDao,
    private val itemTreeService: ItemTreeService,
    private val itemTreeAssertionDao: ItemTreeAssertionDao,
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Each party can only make one item assertion per item tree, per batch. To assert two would be a conflict.
     * Take a batch and return a structure guaranteed to contain only one item tree assertion per root node per party.
     * If there are duplicates, take the last one in the list and log errors.
     *
     * @return map. Keys are pairs of: (party pk, root pk). Value is the Item Tree and the containing envelope.
     */
    fun toDistinctCompactItemTreeAssertions(batches: Collection<EnvelopeBatch>): Collection<CompactItemTreeAssertion> {
        // Collect Item Trees (and a reference to their envelopes) by Pair(party, root pk).
        val assertions = batches.flatMap { batch ->
            batch.envelopes.flatMap { envelope ->
                envelope.itemTrees.mapNotNull {
                    val partyPk = envelope.assertion.assertingParty.pk
                    val rootPk = it.root.pk

                    if (partyPk != null && rootPk != null) {
                        Pair(Pair(partyPk, rootPk), Pair(it, envelope))
                    } else null
                }
            }
        }.groupBy({ it.first }, {it.second})

        // Pick one ItemTree assertion per (party, root pk) pair.
        val chosen = assertions.mapNotNull { (partyItem, treeEnvelopes) ->
            if (treeEnvelopes.isNotEmpty()) {

                val chosen = treeEnvelopes.maxBy { it.second.assertion.assertedAt }

                val compactItemTree = itemTreeService.toCompact(chosen.first)

                if (treeEnvelopes.count() != 1) {
                    val (partyPk, itemPk) = partyItem
                    val envelopePks = treeEnvelopes.map { it.second.pk }
                    logger.debug(
                        "Found conflicting item tree assertions for item PK {} by party {}. Picking: {}. Envelopes: {}.",
                        itemPk,
                        partyPk,
                        chosen.first.root,
                        envelopePks
                    )
                }

                chosen.second.assertion.assertingParty.pk?.let { partyPk ->
                    chosen.second.pk?.let { envelopePk ->
                        chosen.first.root.pk?.let { rootPk ->
                            CompactItemTreeAssertion(
                                compactItemTree,
                                partyPk,
                                rootPk,
                                chosen.second.assertion.assertedAt,
                                envelopePk
                            )
                        }
                    }
                }

            } else null
        }

        return chosen
    }

    /**
     * Ingest item trees form a collection of envelope batches.
     * Optionally supply a merge function which allows the caller to merge the new data with the old (or decide not to re-assert).
     * If the merge function is supplied, it will be called with the previous current item tree assertion (or null if there wasn't one) and the new one.
     * If the merge function returns null, no assertion will be made. This can allow redundant data insertions.
     * If no merge function is supplied, the item tree will be inserted as-is.
     */
    fun ingest(batches: Collection<EnvelopeBatch>, merge: ((CompactItemTree?, CompactItemTree) -> CompactItemTree?)? = null) {

        // Read-only resolve all the identifiers, user agents and relationship types found in the collection of Envelope Batches.
        // Return those batches, partially resolved.
        val readOnlyResolved = logDuration(this.logger, "resolve RO") {
            resolver.resolveRO(batches)
        }

        // Resolve read-write those identifiers that aren't yet resolved, and wait for them to flush.
        logDuration(this.logger, "insert missing identifiers RW") {
            resolver.insertIdentifiers(readOnlyResolved)
        }

        // Create any new user agents
        logDuration(this.logger, "resolve user agents RW") {
            resolver.createUserAgents (readOnlyResolved)
        }

        // Now re-run read-write resolution to pick up the ones we inserted.
        val readWriteResolved = logDuration(this.logger, "final resolve RO") {
            resolver.resolveRO(readOnlyResolved, true)
        }

        val envelopes = logDuration(this.logger, "create envelopes") {
            ingestionDao.createEnvelopes(ingestionDao.createEnvelopeBatches(readWriteResolved))
        }

        // Attach the asserting party and timestamp to each node in the tree when it's stored.
        // Means when we retrieve it we can take the tree and mix it with others.
        val withAssertingParty = envelopes.map { it.annotateAssertingPartyInTree()}

        // Now guarantee there's only one assertion per tree per party, and convert to compact item tree assertions.
        val itemTreeAssertions = toDistinctCompactItemTreeAssertions(withAssertingParty)

        // If there's no merge function, insert directly
        val merged = if (merge == null) {
            itemTreeAssertions
        } else {
            // if there is a merge function, go through the item tree assertions and look up the previous assertion,
            // pass to the callback to see what it wants to do with it.
            itemTreeAssertions.mapNotNull { assertion ->
                val previousItemTree = itemTreeAssertionDao.getCurrentByRootParty(assertion.rootItemPk, assertion.assertingPartyPk)
                val result = merge(previousItemTree, assertion.tree)
                // If it returns null, then remove that assertion. Otherwise, replace its result in the assertion and assert it.
                if (result != null) {
                    assertion.copy(tree = result)
                } else {
                    null
                }
            }
        }

        logDuration(this.logger, "assert item trees") {
            try {
                itemTreeAssertionDao.assertItemTrees(merged)
            } catch (error: Exception) {
                logger.error("Failed to insert item tree assertions: ${error.message}")
            }
        }
    }
}
