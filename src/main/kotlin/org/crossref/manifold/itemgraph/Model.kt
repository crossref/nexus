package org.crossref.manifold.itemgraph

import org.crossref.manifold.itemtree.Item
import java.time.OffsetDateTime

data class RelationshipItem(
    val collectionPk: Int?,
    val typePk: Int?
)

data class RelationshipStatement(
    val subjectItemPk: Long,
    val relationshipTypePk: Int,
    val objectItemPk: Long,
    val updatedAt: OffsetDateTime,
    val pk: Long? = null,
    val subj: RelationshipItem,
    val obj: RelationshipItem,
    val assertedByPk: Long,
    val linkedByPk: Long
)

/**
 * Minimal relationship for internal use.
 */
data class SimpleRelationship(
    val subjectItemPk: Long,
    val relationshipTypePk: Int,
    val objectItemPk: Long,
)

/**
 * An itemgraph by a member of an Item Tree. Describes who is trying to say what about the Item Tree, and what relationship they have to the Identifiers-to-Register.
 * @param assertedAt date time at which the assertions are made.
 * @param assertingParty the party making the assumption.
 */
data class ItemTreeAssertion(
    // Date time at which the assertions are made.
    val assertedAt: OffsetDateTime,

    // Identifier of the Authority.
    val assertingParty: Item,
) {
    fun withAssertingParty(newAssertingParty: Item): ItemTreeAssertion =
        ItemTreeAssertion(assertedAt, newAssertingParty)
}
