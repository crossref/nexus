package org.crossref.manifold.itemgraph

import org.crossref.manifold.db.CursorDao
import org.crossref.manifold.db.Fetched
import org.crossref.manifold.db.getNullableInt
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.time.OffsetDateTime
import java.util.*

/**
 * DAO for querying "relationships", which is the relationship table, but also joins to other tables.
 */
@Repository
class RelationshipDao(
    jdbcTemplate: JdbcTemplate,
    private val cursorDao: CursorDao,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val relStmtMapper: RowMapper<RelationshipStatement> = RowMapper { rs, _ ->
        val subj = RelationshipItem(
            rs.getNullableInt("subject_type_pk"),
            rs.getNullableInt("subject_subtype_pk")
        )
        val obj = RelationshipItem(
            rs.getNullableInt("object_type_pk"),
            rs.getNullableInt("object_subtype_pk")
        )

        RelationshipStatement(
            pk = rs.getLong("pk"),
            subjectItemPk = rs.getLong("subject_item_pk"),
            relationshipTypePk = rs.getInt("relationship_type_pk"),
            objectItemPk = rs.getLong("object_item_pk"),
            updatedAt = rs.getObject("asserted_at", OffsetDateTime::class.java),
            subj = subj,
            obj = obj,
            assertedByPk = rs.getLong("item_tree_asserting_party_pk"),
            linkedByPk = rs.getLong("matching_party_pk")
        )
    }

    private val simpleRelationshipMapper: RowMapper<SimpleRelationship> = RowMapper { rs, _ ->
        SimpleRelationship(
            subjectItemPk = rs.getLong("subject_item_pk"),
            relationshipTypePk = rs.getInt("relationship_type_pk"),
            objectItemPk = rs.getLong("object_item_pk"),
        )
    }

    /**
     * Fetch a SimpleRelationship given the Subject PK, Object PK, Relationship Type PK, Asserting Party Pk.
     */
    fun fetchSimpleRelationships(
        subjectItemPk: Long? = null,
        objectItemPk: Long? = null,
        relationshipTypePk: Int,
        assertedByPk: Long,
        limit: Int = 1000,
    ): Collection<SimpleRelationship> {
        val predicates = mutableListOf(
            "relationship_type_pk = :relationship_type_pk",
            "item_tree_asserting_party_pk = :item_tree_asserting_party_pk"
        )
        val filters = mutableMapOf(
            "relationship_type_pk" to relationshipTypePk,
            "item_tree_asserting_party_pk" to assertedByPk,
            "limit" to limit
        )
        subjectItemPk?.also {
            predicates.add("subject_item_pk = :subject_item_pk")
            filters["subject_item_pk"] = it
        }
        objectItemPk?.also {
            predicates.add("object_item_pk = :object_item_pk")
            filters["object_item_pk"] = it
        }

        return npTemplate.query(
            """
            SELECT
                subject_item_pk,
                relationship_type_pk,
                object_item_pk,
                item_tree_asserting_party_pk
            FROM relationship_current
            WHERE ${predicates.joinToString(" AND ")}
            LIMIT :limit
            """.trimIndent(),
            filters,
            simpleRelationshipMapper
        )
    }

    @Transactional(readOnly = true)
    fun fetchRelationships(
        cursor: String? = null,
        subjectItemPk: Long? = null,
        subjectRegistrationAgencyPk: Int? = null,
        subjectStewardPk: Long? = null,
        objectItemPk: Long? = null,
        objectRegistrationAgencyPk: Int? = null,
        objectStewardPk: Long? = null,
        relationshipTypePk: Int? = null,
        fromUpdatedTime: OffsetDateTime? = null,
        untilUpdatedTime: OffsetDateTime? = null,
        rows: Int,
        subjectTypePk: Int? = null,
        subjectSubtypePk: Int? = null,
        objectTypePk: Int? = null,
        objectSubtypePk: Int? = null,
        assertedByPk: Long? = null,
        notAssertedByPk: Long? = null,
        linkedByPk: Long? = null,
        notLinkedByPk: Long? = null,
    ): Fetched<RelationshipStatement> {
        val includeLastKey = cursor != null && cursor != "*"

        // filter predicates
        val filters = mutableMapOf<String, Any>()

        /* If subjectItemPk is not null, assume the following are.
         *      subjectType, subjectSubtype, subjectRegistrationAgency
         *
         * If objectItemPk is not null, assume the following are.
         *      objectType, objectSubtype, objectRegistrationAgency
         */
        val sql = buildQuery(
            subjectItemPk,
            subjectRegistrationAgencyPk?.takeIf { subjectItemPk == null },
            subjectStewardPk?.takeIf { subjectItemPk == null },
            objectItemPk,
            objectRegistrationAgencyPk?.takeIf { objectItemPk == null },
            objectStewardPk?.takeIf { objectItemPk == null },
            relationshipTypePk,
            fromUpdatedTime,
            untilUpdatedTime,
            subjectTypePk?.takeIf { subjectItemPk == null },
            subjectSubtypePk?.takeIf { subjectItemPk == null },
            objectTypePk?.takeIf { objectItemPk == null },
            objectSubtypePk?.takeIf { objectItemPk == null },
            assertedByPk,
            notAssertedByPk,
            linkedByPk,
            notLinkedByPk,
            includeLastKey,
            filters
        )

        if (cursor == null || cursor == "*") {
            val fetched =
                npTemplate.query("$sql ORDER by pk LIMIT $rows", filters, relStmtMapper)
            if (fetched.isEmpty()) {
                return Fetched(emptyList(), rows, null)
            }

            if (cursor == "*") {

                // Retrieved relationship statements will always have a primary key.
                val lastKey = fetched.last().pk!!
                val nextCursor = UUID.randomUUID().toString()
                cursorDao.addCursor(nextCursor, lastKey)
                return Fetched(fetched, rows, nextCursor)
            } else {
                return Fetched(fetched, rows, null)
            }
        } else {
            filters["lastKey"] = cursorDao.getLastKey(cursor)
            val fetched =
                npTemplate.query(
                    "$sql ORDER by pk LIMIT $rows",
                    filters,
                    relStmtMapper
                )
            if (fetched.isEmpty()) {
                return Fetched(emptyList(), rows, null)
            }
            val nextCursor = UUID.randomUUID().toString()
            cursorDao.addCursor(nextCursor, fetched.last().pk!!)
            return Fetched(fetched, rows, nextCursor)
        }
    }

    private fun buildQuery(
        subjectItemPk: Long?,
        subjectRegistrationAgencyPk: Int?,
        subjectStewardPk: Long?,
        objectItemPk: Long?,
        objectRegistrationAgencyPk: Int?,
        objectStewardPk: Long?,
        relationshipTypePk: Int?,
        fromUpdatedTime: OffsetDateTime?,
        untilUpdatedTime: OffsetDateTime?,
        subjectTypePk: Int?,
        subjectSubTypePk: Int?,
        objectTypePk: Int?,
        objectSubtypePk: Int?,
        assertedByPk: Long?,
        notAssertedByPk: Long?,
        linkedByPk: Long?,
        notLinkedByPk: Long?,
        includeLastKey: Boolean,
        filters: MutableMap<String, Any>
    ): String {
        val relMatchSql = StringBuilder(
            """
            SELECT
                pk,
                subject_item_pk,
                relationship_type_pk,
                object_item_pk,
                asserted_at,
                subject_type_pk,
                subject_subtype_pk,
                subject_ra_pk,
                object_type_pk,
                object_subtype_pk,
                object_ra_pk,
                item_tree_asserting_party_pk,
                matching_party_pk
            FROM relationship_api
            """
        )
        val predicates = mutableListOf<String>()

        subjectItemPk?.also {
            filters["subjectItemPk"] = it
            predicates.add("subject_item_pk = :subjectItemPk")
        }
        relationshipTypePk?.also {
            filters["relationshipTypePk"] = it
            predicates.add("relationship_type_pk = :relationshipTypePk")
        }
        objectItemPk?.also {
            filters["objectItemPk"] = it
            predicates.add("object_item_pk = :objectItemPk")
        }
        subjectRegistrationAgencyPk?.also {
            filters["subjectRegistrationAgencyPk"] = it
            predicates.add("subject_ra_pk = :subjectRegistrationAgencyPk")
        }
        subjectStewardPk?.also {
            filters["subjectStewardPk"] = it
            predicates.add("subject_steward_pk = :subjectStewardPk")
        }
        objectRegistrationAgencyPk?.also {
            filters["objectRegistrationAgencyPk"] = it
            predicates.add("object_ra_pk = :objectRegistrationAgencyPk")
        }
        objectStewardPk?.also {
            filters["objectStewardPk"] = it
            predicates.add("object_steward_pk = :objectStewardPk")
        }
        subjectTypePk?.also {
            filters["subjectTypePk"] = it
            predicates.add("subject_type_pk = :subjectTypePk")
        }
        subjectSubTypePk?.also {
            filters["subjectSubtypePk"] = it
            predicates.add("subject_subtype_pk = :subjectSubtypePk")
        }
        objectTypePk?.also {
            filters["objectTypePk"] = it
            predicates.add("object_type_pk = :objectTypePk")
        }
        objectSubtypePk?.also {
            filters["objectSubtypePk"] = it
            predicates.add("object_subtype_pk = :objectSubtypePk")
        }
        fromUpdatedTime?.also {
            filters["fromUpdatedTime"] = it
            predicates.add("asserted_at >= :fromUpdatedTime")
        }
        untilUpdatedTime?.also {
            filters["untilUpdatedTime"] = it
            predicates.add("asserted_at <= :untilUpdatedTime")
        }
        assertedByPk?.also {
            filters["assertedByPk"] = it
            predicates.add("item_tree_asserting_party_pk = :assertedByPk")
        }
        notAssertedByPk?.also {
            filters["notAssertedByPk"] = it
            predicates.add("item_tree_asserting_party_pk != :notAssertedByPk")
        }
        linkedByPk?.also {
            filters["linkedByPk"] = it
            predicates.add("matching_party_pk = :linkedByPk")
        }
        notLinkedByPk?.also {
            filters["notLinkedByPk"] = it
            predicates.add("matching_party_pk != :notLinkedByPk")
        }
        if (includeLastKey) {
            predicates.add("pk > :lastKey")
        }
        if (predicates.isNotEmpty()) {
            relMatchSql.append(predicates.joinToString(prefix = "WHERE ", separator = " AND "))
        }
        return relMatchSql.toString()
    }
}
