package org.crossref.manifold.itemgraph

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.sql.ResultSet

@Repository
class ItemTreeUpdateQueueDao(
    jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Called when the given set of Item Pks has changed. Enqueue the PKs of item trees
     * that may be affected by those items changing.
     *
     * @param[itemPks] collection if item PKs that may have changed
     */
    fun enqueueAffectedTrees(itemPks: Set<Long>) {
        logger.info("Enqueue for ${itemPks.count()} trees.")
        itemPks.chunked(1000).forEach { chunk ->

            // Sort to avoid contention.
            val pks = chunk.sorted()

            // These items need updating.
            // Avoid duplicates in the queue, but these aren't enforced in the schema, as that would
            // introduce locks. Instead, query before each insertion. If there are duplicates then the item
            // will be processed twice, which isn't a problem.
            npTemplate.batchUpdate(
                """INSERT INTO item_tree_update_queue (root_item_pk, updated_at)
                   SELECT :root_item_pk, NOW() 
                   WHERE NOT EXISTS (SELECT root_item_pk FROM item_tree_update_queue WHERE root_item_pk = :root_item_pk)
                   """.trimIndent(),
                pks.map { mapOf("root_item_pk" to it) }.toTypedArray()
            )

            // Look up items with a relationship to this and mark those as needing re-rendering too.
            npTemplate.update(
                """WITH root_pks AS (
                     SELECT DISTINCT ON (root_item_pk) root_item_pk, NOW() 
                     FROM relationship_current 
                     WHERE object_item_pk IN (:item_pks)),
                   new_pks AS (
                     SELECT root_item_pk
                     FROM root_pks
                     WHERE NOT EXISTS (
                       SELECT * FROM item_tree_update_queue q
                       WHERE q.root_item_pk = root_pks.root_item_pk))
                    INSERT INTO item_tree_update_queue (root_item_pk, updated_at) 
                    SELECT root_item_pk, NOW() FROM new_pks;
                   """.trimIndent(),
                mapOf("item_pks" to pks)
            )
        }
    }

    /**
     * Get current length of queue.
     */
    fun queueLength(): Long =
        npTemplate.query("SELECT count(pk) AS count FROM item_tree_update_queue") { rs: ResultSet, _: Int ->
            rs.getLong(
                "count"
            )
        }.first()

    /**
     * Get from the queue and call callback function with PKs.
     * Always call callback, even if there are no results.
     * This enables the callback to notice and stop polling if it wants.
     */
    @Transactional
    fun getFromQueue(count: Int, f: ((itemPkBatch: Set<Long>) -> Unit)) {
        val params = mapOf("count" to count)

        val itemPks = npTemplate.query(
            """            
            DELETE FROM item_tree_update_queue
            WHERE pk IN (
              SELECT pk
              FROM item_tree_update_queue
              FOR UPDATE SKIP LOCKED
              LIMIT :count
            )
            RETURNING root_item_pk;
        """.trimIndent(),
            params
        ) { rs: ResultSet, _: Int -> rs.getLong("root_item_pk") }

        val distinct = itemPks.toSet()
        logger.debug("Dequeue ${itemPks.count()} of which ${distinct.count()} distinct.")

        f(distinct)
    }

}