package org.crossref.manifold.rendering

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.tags.Tag
import org.crossref.manifold.api.Configuration.API
import org.crossref.manifold.api.Configuration.BASE_URL
import org.crossref.manifold.api.Entry
import org.crossref.manifold.api.Feed
import org.crossref.manifold.api.Link
import org.crossref.manifold.api.item.RenderedItemsRequest
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.CanonicalItemTreeEntry
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.ItemTreeService
import org.crossref.manifold.rendering.Configuration.ITEMS_API
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.RENDER_S3_BUCKET
import org.crossref.manifold.retrieval.IdentifierRetriever
import org.crossref.manifold.util.ApiParameter
import org.crossref.manifold.util.MimeType
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import org.springframework.web.util.UriComponentsBuilder
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

const val ATOM_MIME_TYPE = "application/atom+xml"

/**
 * REST API for metadata versions
 */
@Tag(name = "Items")
@RestController
@ConditionalOnProperty(prefix = RENDERING, name = [ITEMS_API])
class ItemsController(
    val renderedItemsService: RenderedItemsService,
    val renderer: ItemTreeRenderer,
    val itemTreeService: ItemTreeService,
    val resolver: Resolver,
    val itemDao: ItemDao,
    @Value("\${$API.$BASE_URL:}") val baseUrl: String,
    @Value("\${$RENDERING.$RENDER_S3_BUCKET}") val bucket: String,
) {

    val mapper: ObjectMapper = XmlMapper().configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true)

    fun getLinkBuilder(
        path: String,
        contentType: String? = null,
        version: String? = null,
    ): UriComponentsBuilder {
        val rootUrl = baseUrl.ifEmpty { ServletUriComponentsBuilder.fromCurrentContextPath().build() }
        val builder = ServletUriComponentsBuilder.fromUriString("$rootUrl/beta/$path")

        if (contentType != null) {
            // Content types have "+" which needs encoding.
            builder.queryParam("content-type", URLEncoder.encode(contentType, StandardCharsets.UTF_8))
        }

        if (version != null) {
            builder.queryParam("version", version)
        }

        return builder
    }

    fun getItemsLink(
        identifier: String?,
        contentType: String? = null,
        version: String? = null,
    ): String {
        val builder = getLinkBuilder("items", contentType, version)
        if (identifier != null) {
            // SICI identifiers can contain ';' which is missed by the Spring UriComponentsBuilder, but significant
            // for routing.
            val clean = identifier.replace(";", "%3B")
            builder.pathSegment(clean)
        }
        return builder.build().toString()
    }

    fun getFeedLink(
        identifier: String?,
        contentType: String? = null,
    ): String {
        val builder = getLinkBuilder("items/feed.xml", contentType)
        if (identifier != null) {
            builder.queryParam("item", identifier)
        }

        return builder.build().toString()
    }

    fun getFeedNextLink(
        identifier: String? = null,
        contentType: String? = null,
        rows: Int?,
        cursor: Long?,
        fromDate: String?,
        untilDate: String?,
    ): String {
        val builder = getLinkBuilder("items/feed.xml", contentType)
        if (identifier != null) {
            builder.queryParam("item", identifier)
        }

        if (cursor != null) {
            builder.queryParam("cursor", cursor)
        }

        if (rows != null) {
            builder.queryParam("rows", rows)
        }

        if (fromDate != null) {
            builder.queryParam("fromDate", fromDate)
        }

        if (untilDate != null) {
            builder.queryParam("untilDate", untilDate)
        }

        return builder.build().toString()
    }

    @Operation(
        summary = """
            Retrieves the current metadata for a given Item using its identifier.""",
        description = """This endpoints always serves the latest version of the based on render time and weighted content type.
Note that due to limitations of Swagger, it's not possible to try this out in the browser. Instead, replace {*identifier} with a URL-escaped DOI
or other identifier.
        """,
        parameters = [
            Parameter(
                name = "identifier",
                description = "The identifier of the Item, either its URL identifier, or its PK",
                examples = [
                    ExampleObject(
                        value = "https://doi.org/10.5555/12345678",
                        summary = "DOI",
                        name = "",
                    ),
                ],
            ),
            Parameter(
                name = "version",
                description = "The version of the item to retrieve. This value supplied from the Atom Feed.",
                examples = [
                    ExampleObject(
                        value = "123",
                        summary = "Version",
                        name = "",
                    ),
                ],
            ),
            Parameter(
                name = "content-type",
                description = "The content-type of the item to retrieve, for example 'application/citeproc+json', 'application/crossref.vnd.unixsd+xml",
                examples = [
                    ExampleObject(
                        value = MimeType.CITEPROC_JSON,
                        summary = "Content Type",
                        name = "",
                    ),
                ],
            ),
        ],
    )
    @GetMapping("/beta/items/{*identifier}")
    fun getLiveMetadata(
        @PathVariable
        identifier: String,
        @RequestParam(value = "content-type", required = false)
        contentType: String?,
    ): ResponseEntity<String> {

        val cleanId = identifier.removePrefix("/")

        val itemPk = resolver.resolveRO(IdentifierParser.parse(cleanId)).pk
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't retrieve data for that item.")

        val retrievedTree = itemTreeService.getCanonicalItemTree(itemPk)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't retrieve data for that item.")

        return liveRender(retrievedTree.first, retrievedTree.second, contentType, cleanId)
    }


    private fun liveRender(
        itemTreeEntry: CanonicalItemTreeEntry,
        itemTree: ItemTree,
        contentType: String?,
        identifier: String
    ): ResponseEntity<String> {
        val renderer = if (contentType != null) {
            val resolvedContentType = ContentType.fromMimeType(contentType) ?: throw ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Unrecognised content type $contentType"
            )
            renderer.getRendersForTypeAndContentType(
                itemTreeEntry.type,
                itemTreeEntry.subtype,
                resolvedContentType,
                itemTree
            ).firstOrNull()
        } else {
            renderer.getRenderersForType(itemTreeEntry.type, itemTreeEntry.subtype, itemTree).firstOrNull()
        }

        if (renderer == null) {
            throw ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Could not find suitable content type renderer for request."
            )
        }

        val content = renderer.render(itemTree)

        val headers = HttpHeaders()

        // Requested type (e.g. application/citeproc+json) may be different from the public response (application/json)
        val feedUrl = getFeedLink(identifier, contentType)

        headers.add("Content-Type", renderer.publicContentType().mimeType)
        headers.add("Link", "<$feedUrl>, rel=\"feed\"")

        return ResponseEntity.ok()
            .headers(headers)
            .body(content)
    }

    @Operation(
        summary = """Atom feed of updated metadata. This is currently experimental, and the rendered metadata may not be complete or accurate.""",
        description = """The feed for each item is available as a Link HTTP header, per the Atom specification. You can
use this endpoint to query the feed for a specific item, or content type, or combination.
 
This follows the Atom specification, so you can use the 'next' links to paginate. 

The entries show the list of recently updated items that match the filters. Each entry shows the Item's metadata using three links with different rel types:

 - "alternate" links to that Item in its default content type.
 - "related" links to that Item with a specific content type (the one that was rendered).
 - "enclosure" provides an experimental AWS S3 URI that allows users to make direct requester-pays requests.

        """,
        parameters = [
            Parameter(
                name = "item",
                description = "The identifier of the Item, either its URL identifier, or its PK",
                examples = [
                    ExampleObject(
                        value = "https://doi.org/10.5555/12345678",
                        summary = "DOI",
                        name = "",
                    ),
                ],
            ),
            Parameter(
                name = "content-type",
                description = "The content-type of the item to retrieve, for example 'application/citeproc+json', 'application/crossref.vnd.unixsd+xml",
                examples = [
                    ExampleObject(
                        value = MimeType.CITEPROC_JSON,
                        summary = "Content Type",
                        name = "",
                    ),
                ],
            ),
        ]
    )
    @GetMapping("/beta/items/feed.xml")
    fun getFeed(
        @ApiParameter
        @Schema(required = false, hidden = true)
        request: RenderedItemsRequest,
    ): ResponseEntity<String> {
        val headers = HttpHeaders()
        headers.add("Content-Type", MediaType.APPLICATION_XML_VALUE)

        val contentType = request.contentType
        val resolvedContentType = request.resolvedContentType

        // If the item identifier is supplied, look up the tree for its identifier.
        // But if it is supplied, it must exist.
        val item = if (!request.item.isNullOrEmpty()) {
            resolver.resolveRO(IdentifierParser.parse(request.item)).pk?.let {
                itemTreeService.getCanonicalItemTree(it)?.second?.root
            } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find item: $request.item")
        } else null

        val renders = renderedItemsService.getItems(request, item?.pk)
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find item: $item")

        if (!contentType.isNullOrBlank() && resolvedContentType == null) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect content type specified - $contentType")
        }

        val title = if (item == null) {
            "Crossref Metadata feed for items"
        } else {
            val id = item.identifiers.firstOrNull()?.uri?.toString() ?: "Item ${item.pk}"
            "Crossref Metadata feed for $id"
        } + if (contentType.isNullOrBlank()) {
            " in all content types"
        } else {
            " in $contentType"
        }


        val feedLink = getFeedLink(
            request.item,
            contentType,
        )

        val links = mutableListOf(
            Link(
                href = feedLink,
                rel = "self",
                type = ATOM_MIME_TYPE,
            ),
            Link(
                href = feedLink,
                rel = "first",
                type = ATOM_MIME_TYPE,
            ),
        )

        if (renders.any()) {
            links.add(
                Link(
                    href = getFeedNextLink(
                        request.item,
                        rows = request.maxRows,
                        cursor = renders.lastOrNull()?.pk,
                        fromDate = request.fromDate,
                        untilDate = request.untilDate,
                    ),
                    rel = "next",
                    type = ATOM_MIME_TYPE,
                ),
            )
        }

        // The set of responses may be for various items, so retrieve the identifier for each.
        val retriever = IdentifierRetriever(itemDao)
        retriever.prime(renders.map { it.itemPk }.toSet())

        val feed = Feed(
            id = feedLink,
            title = title,
            links = links,
            entries = renders.map { r ->
                val identifier = retriever.get(r.itemPk)?.firstOrNull()?.uri?.toString()

                // This view is the specific versioned one. Use for the ID as well as the 'related' link.
                val versioned = getItemsLink(identifier, r.internalContentType.mimeType, r.pk.toString())

                Entry(
                    title = "Item ${r.itemPk} in ${r.internalContentType.mimeType}",
                    id = versioned,
                    updated = r.updatedAt.toString(),
                    links = listOf(
                        Link(
                            href = getItemsLink(identifier),
                            rel = "alternate",
                            type = r.internalContentType.mimeType,
                        ),
                        Link(
                            href = versioned,
                            rel = "related",
                            type = r.internalContentType.mimeType,
                        ),
                        Link(
                            href = "s3://$bucket/${r.getKey()}",
                            rel = "enclosure",
                            type = r.internalContentType.mimeType,
                        ),
                    ),
                )
            },
            updated = renders.firstOrNull()?.updatedAt.toString(),
        )

        mapper.enable(SerializationFeature.INDENT_OUTPUT)
        val xml = mapper.writeValueAsString(feed)

        return ResponseEntity.ok()
            .headers(headers)
            .body(xml)
    }
}
