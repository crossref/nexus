package org.crossref.manifold.rendering

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.crossref.manifold.itemtree.ItemTreeService
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.RENDER_QUEUE_PROCESSOR_ENABLED
import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit

@Component
@ConditionalOnProperty(prefix = RENDERING, name = [RENDER_QUEUE_PROCESSOR_ENABLED])
class RenderQueueProcessor(
    private val renderStatusDao: RenderStatusDao,
    @Value("\${$RENDERING.$BATCH_SIZE:100}") private val batchSize: Int,

    private val itemTreeRenderer: ItemTreeRenderer,
    private val itemTreeService: ItemTreeService,

    ) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)
    val mapper = jacksonObjectMapper()

    @Scheduled(
        initialDelayString = "\${$RENDERING.$INITIAL_DELAY:0}",
        fixedDelayString = "\${$RENDERING.$FIXED_DELAY:1}",
        timeUnit = TimeUnit.SECONDS
    )
    fun run() {
        logger.debug("Checking for stale items that need to be rendered")

        try {
        var lastBatchCount : Int? = null
        while (lastBatchCount == null || lastBatchCount > 0) {

            // Make sure we terminate the loop if there's a failure polling.
            lastBatchCount = 0

            renderStatusDao.getFromQueue(batchSize) { itemPksToRender ->

                // Set the count, so we know whether to loop next time.
                lastBatchCount = itemPksToRender.count()

                // We may get a chunk with duplicates from the queue.
                val distinctItemPks = itemPksToRender.distinct()

                if (itemPksToRender.isNotEmpty()) {
                    logger.info("Processing stale items: $itemPksToRender from render queue.")
                }

                val itemTrees = itemTreeService.getCanonicalItemTrees(distinctItemPks)

                for ((entry, itemTree) in itemTrees) {
                    try {
                        itemTreeRenderer.renderAll(entry, itemTree)
                    } catch (e: Exception) {
                        // There could be errors caused by drift between database and queue.
                        // Report errors but don't mark the message as having failed.
                        logger.error("Failed to process tree for ${entry.rootItemPk}: ${e.message}, ${e.stackTraceToString()}")
                    }
                }
            }
        }
    } catch (e: Exception) {
        logger.error("Error rendering: $e", e)
        }
    }

}
