package org.crossref.manifold.rendering

import org.crossref.manifold.rendering.Configuration.RENDERING
import org.crossref.manifold.rendering.Configuration.RENDER_S3_BUCKET
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Repository
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetObjectRequest
import software.amazon.awssdk.services.s3.model.PutObjectRequest

@Repository
@ConditionalOnProperty(prefix = RENDERING, name = [RENDER_S3_BUCKET])
class RenderedItemStorageDao(
    private val s3Client: S3Client,
    @Value("\${$RENDERING.$RENDER_S3_BUCKET}") val bucket: String,
    private val renderedItemDao: RenderedItemDao,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    init {
        if (bucket.isBlank()) {
            throw IllegalArgumentException(
                """
                RenderedItemStorageDao expects to be able to save rendered items to S3 but no bucket was specified,
                either directly or via $RENDERING.$RENDER_S3_BUCKET.
                """,
            )
        }
    }

    fun storeItem(renderedItem: RenderedItem) {
        logger.debug("Uploading rendered item ${renderedItem.pk} to $bucket")
        s3Client.putObject(
            PutObjectRequest.builder()
                .bucket(bucket)
                .key(renderedItem.getKey())
                .contentType(renderedItem.publicContentType.mimeType)
                .build(),
                    RequestBody.fromString(renderedItem.content),
                )
    }

    /**
     * @return the RenderedItem @param[renderedItem] with its
     * content populated
     */
    fun fetchContent(renderedItem: RenderedItem): RenderedItem {
        s3Client.getObject(
            GetObjectRequest.builder()
                .bucket(bucket)
                .key(renderedItem.getKey())
                .build(),
        ).use {
            renderedItem.content = it.buffered().reader().readText()
            return renderedItem
        }
    }

    fun getItem(itemPk: Long, contentType: ContentType): RenderedItem? =
        renderedItemDao.getRenderedItem(itemPk, contentType)?.let { renderedItem ->
            s3Client.getObject(
                GetObjectRequest.builder()
                    .bucket(bucket)
                    .key(renderedItem.getKey())
                    .build(),
            ).use {
                renderedItem.content = it.buffered().reader().readText()
                return renderedItem
            }
        }

}
