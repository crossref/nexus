package org.crossref.manifold.rendering

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.sql.Types
import java.time.OffsetDateTime
import java.util.*

@Repository
class RenderedItemDao(
    jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    private val renderedItemMapper: RowMapper<RenderedItem> = RowMapper { rs, _ ->
        RenderedItem(
            pk = rs.getLong("pk"),
            itemPk = rs.getLong("item_pk"),
            publicContentType = ContentType.fromId(rs.getInt("public_content_type_id"))!!,
            internalContentType = ContentType.fromId(rs.getInt("internal_content_type_id"))!!,
            current = rs.getBoolean("current"),
            hash = rs.getString("hash"),
            updatedAt = rs.getObject("updated_at", OffsetDateTime::class.java)
        )
    }

    var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Transactional
    fun createRenderedItems(
        renderedItems: Collection<RenderedItem>,
    ) {
        // Sort to make insertion order stable, and avoid deadlocks.
        val sorted =
            renderedItems.sortedWith(compareBy({ it.itemPk }, { it.internalContentType }, { it.publicContentType }))

        val props = sorted.map { renderedItem ->
            MapSqlParameterSource(
                mapOf(
                    "item_pk" to renderedItem.itemPk,
                    "current" to renderedItem.current,
                    "public_content_type_id" to renderedItem.publicContentType.id,
                    "internal_content_type_id" to renderedItem.internalContentType.id,
                    "hash" to renderedItem.hash,
                    "updated_at" to renderedItem.updatedAt,
                ),
            ).apply { registerSqlType("updated_at", Types.TIMESTAMP_WITH_TIMEZONE) }
        }.toTypedArray()

        npTemplate.batchUpdate(
            """
            UPDATE rendered_item 
            SET current = false
            WHERE item_pk = :item_pk and internal_content_type_id = :internal_content_type_id
            """,
            props,
        )

        npTemplate.batchUpdate(
            """
            INSERT INTO rendered_item ( item_pk, public_content_type_id, internal_content_type_id, hash, current, updated_at )
            VALUES ( :item_pk, :public_content_type_id, :internal_content_type_id, :hash, :current, :updated_at );
            """,
            props,
        )
    }

    fun getRenderedItem(itemPk: Long, internalContentType: ContentType): RenderedItem? {
        val renderedItems = npTemplate.query(
            """
            select pk, item_pk, internal_content_type_id, public_content_type_id, hash, current, updated_at
            from rendered_item 
            where item_pk = :item_pk and internal_content_type_id = :internal_content_type_id and current = true
            """,
            mapOf(
                "item_pk" to itemPk,
                "internal_content_type_id" to internalContentType.id,
            ),
            renderedItemMapper,
        )

        return renderedItems.firstOrNull()
    }

    fun getRenderedItemVersion(itemPk: Long, internalContentType: ContentType, pk: Long): RenderedItem? {
        val renderedItems = npTemplate.query(
            """
            select pk, item_pk, internal_content_type_id, public_content_type_id, hash, current, updated_at
            from rendered_item 
            where item_pk = :item_pk and internal_content_type_id = :internal_content_type_id and pk = :pk
            """,
            mapOf(
                "item_pk" to itemPk,
                "internal_content_type_id" to internalContentType.id,
                "pk" to pk,
            ),
            renderedItemMapper,
        )

        return renderedItems.firstOrNull()
    }

    /**
     * Gets all current rendered items for @param[itemPk], where current
     * means the latest version, there should only be one per content type
     */
    fun getRenderedItems(itemPk: Long): Collection<RenderedItem?> {
        return npTemplate.query(
            """
            select pk, item_pk, internal_content_type_id, public_content_type_id, hash, current, updated_at
            from rendered_item
            where item_pk = :item_pk and current = true
            """,
            mapOf(
                "item_pk" to itemPk,
            ),
            renderedItemMapper,
        )
    }

    /**
     * Gets all rendered items (current or historical) for @param[itemPk], where current
     * means the latest version, there should only be one per content type
     */
    fun getHistoricalRenderedItems(itemPk: Long, internalContentType: String): Collection<RenderedItem> =
        ContentType.fromMimeType(internalContentType)?.let {
            npTemplate.query(
                """
            select pk, item_pk, internal_content_type_id, public_content_type_id, hash, current, updated_at
            from rendered_item
            where item_pk = :item_pk and internal_content_type_id = :internal_content_type_id
            """,
                mapOf(
                    "item_pk" to itemPk,
                    "internal_content_type_id" to it.id
                ),
                renderedItemMapper,
            )
        } ?: emptyList()


    /**
     * Gets the render history of @param[itemPk] by optional @param[contentType] and ordered
     * by the renders last updated date.
     */
    fun getRenderedItemHistory(
        itemPk: Long,
        contentType: ContentType? = null,
    ): Collection<RenderedItem> {
        return queryRenderedItems(
            RenderedItemQuery(
                itemPk = itemPk,
                contentType = contentType,
            ),
        )
    }

    /**
     * Gets rendered items by @param[query]
     */
    fun queryRenderedItems(query: RenderedItemQuery): Collection<RenderedItem> {
        val sql = StringBuilder()
        sql.append(
            """
            select pk, item_pk, public_content_type_id, internal_content_type_id, hash, current, updated_at
            from rendered_item 
            """,
        )

        val where = StringJoiner(" AND ", " WHERE ", "").setEmptyValue("")

        query.itemPk?.let {
            where.add("item_pk = :item_pk")
        }

        query.contentType?.let {
            where.add("internal_content_type_id = :internal_content_type_id")
        }

        query.cursor?.let {
            where.add("pk < :cursor ")
        }

        query.fromDateTime?.let {
            where.add("updated_at >= :from_date")
        }

        query.untilDateTime?.let {
            where.add("updated_at <= :until_date")
        }

        sql.append(where)
        sql.append(" order by pk desc limit :size")

        logger.debug(sql.toString())

        return npTemplate.query(
            sql.toString(),
            mapOf(
                "internal_content_type_id" to query.contentType?.id,
                "size" to query.size,
                "cursor" to query.cursor,
                "from_date" to query.fromDateTime,
                "until_date" to query.untilDateTime,
                "item_pk" to query.itemPk,
            ),
            renderedItemMapper,
        )
    }
}
