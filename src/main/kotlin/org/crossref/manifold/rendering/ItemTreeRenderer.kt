package org.crossref.manifold.rendering

import org.apache.commons.codec.digest.DigestUtils
import org.crossref.manifold.itemtree.CanonicalItemTreeEntry
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.rendering.Configuration.RENDERER_ENABLED
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

@Component
@ConditionalOnProperty(prefix = RENDERING, name = [RENDERER_ENABLED])
class ItemTreeRenderer(
    private val renderedItemDao: RenderedItemDao,
    private val renderedItemStorageDao: RenderedItemStorageDao,
    private val renderers: Collection<ContentTypeRenderer>,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * For a set of collections, return the set of renderers that produce all the content types for those collections.
     * Most highly weighted first.
     */
    fun getRenderersForType(type: String?, subtype: String?, itemTree: ItemTree? = null): Collection<ContentTypeRenderer> =
        renderers.filter { it.offer(type, subtype, itemTree) }.sortedBy { it.internalContentType().weight }.reversed()

    fun getRendersForTypeAndContentType(type: String?, subtype: String?, contentType: ContentType, itemTree: ItemTree?) =
        renderers.filter { it.offer(type, subtype, itemTree) }.filter { it.internalContentType() == contentType }

    fun getRendererForContentType(contentType: ContentType) : ContentTypeRenderer? =
        renderers.firstOrNull { it.internalContentType() == contentType }

    /**
     * Render an Item Tree with the given ContentTypeRenderer and store it.
     * If there was no change, don't re-store.
     */
    fun render(itemTree: ItemTree?, renderer: ContentTypeRenderer) {
        checkNotNull(itemTree) { "Expected $itemTree to be non null" }
        checkNotNull(itemTree.root.pk) { "Expected $itemTree to have a pk" }

        logger.debug("Rendering ${itemTree.root.pk} with ${renderer.internalContentType().toString()}")

        val publicContentType = renderer.publicContentType()
        val internalContentType = renderer.internalContentType()
        val updatedAt = OffsetDateTime.now()

        val content = renderer.render(itemTree)

        // Some renderers optionally produce content, so check here if anything was
        // rendered before doing anything else
        if (content != null) {
            val hash = DigestUtils.md5Hex(content.toByteArray())
            val existing = renderedItemDao.getRenderedItem(itemTree.root.pk, internalContentType)

            // If this is rendered for the first time, or it changed
            if (existing == null || existing.hash != hash) {
                val renderedItem = RenderedItem(
                    itemPk = itemTree.root.pk,
                    publicContentType = publicContentType,
                    internalContentType = internalContentType,
                    current = true,
                    updatedAt = updatedAt,
                    content = content,
                    hash = hash,
                )
                renderedItemDao.createRenderedItems(listOf(renderedItem))

                val dbRenderedItem = renderedItemDao.getRenderedItem(renderedItem.itemPk, renderedItem.internalContentType)

                renderedItemStorageDao.storeItem(renderedItem.copy(pk = dbRenderedItem?.pk))
            }
        }
    }


    fun renderAll(entry: CanonicalItemTreeEntry, itemTree: ItemTree) {

        // The collections that this item is in determine what renderers.
        val renderers = getRenderersForType(entry.type, entry.subtype, itemTree)

        renderers.forEach {
            render(itemTree, it)
        }
    }

    /**
     * Renders the @param[itemTree] as @param[contentType]
     * supplied
     */
    fun renderContentType(itemTree: ItemTree?, contentType: ContentType) {
        val renderer = getRendererForContentType(contentType)
        if (renderer != null) {
            render(itemTree, renderer)
        } else {
            logger.error("Can't find renderer for content type $contentType")
        }
    }

    /**
     * Gets the RenderedItem for @param[itemTree] based on the @param[contentType].
     *
     * @return if the RenderedItem already exists in storage then it will be loaded,
     * if not it will be rendered and then loaded
     */
    fun getRenderedItemTree(itemTree: ItemTree, contentType: ContentType): RenderedItem? {
        checkNotNull(itemTree.root.pk) { "Expected $itemTree to have a pk" }

        val rendered = renderedItemStorageDao.getItem(itemTree.root.pk, contentType)
            ?.let {
                renderContentType(itemTree, contentType)
                renderedItemStorageDao.getItem(itemTree.root.pk, contentType)
            }
        return rendered
    }
}
