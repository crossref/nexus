package org.crossref.manifold.rendering

import org.crossref.manifold.itemtree.ItemTree

/**
 * A contract for ItemTree renderers
 *
 * The render function should render an Item as a string
 * based on the ItemTree renderer ContentType
 */
interface ContentTypeRenderer {
    /**
     * Specific content type that this renders, e.g. `application/vnd.crossref.unixsd+xml`
     */
    fun internalContentType(): ContentType

    /**
     * The content type that should be shown in response headers to the user. e.g `application/xml`.
     * Simpler than internalContentType, don't include the "vnd" extension, as it makes display simpler for the browser.
     */
    fun publicContentType(): ContentType

    fun offer(type: String?, subtype: String?, itemTree: ItemTree? = null, context: RenderingContext? = null): Boolean
    fun render(itemTree: ItemTree, context: RenderingContext? = null): String?
}


/**
 * An optional context that can provide additional data to the renderer
 */
interface RenderingContext