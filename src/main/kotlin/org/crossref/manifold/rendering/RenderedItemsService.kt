package org.crossref.manifold.rendering

import org.crossref.manifold.api.item.RenderedItemsRequest
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.rendering.Configuration.ITEMS_API
import org.crossref.manifold.rendering.Configuration.RENDERING
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service

@Service
@ConditionalOnProperty(prefix = RENDERING, name = [ITEMS_API])
class RenderedItemsService(
    private val renderedItemDao: RenderedItemDao,
    private val resolver: Resolver,
) {
    /**
     * Gets rendered items based on @param[renderedItemsRequest]
     *
     * If the request specifies to limit by an items pk or identifier and that item is not found
     * then null is returned
     */
    fun getItems(itemsRequest: RenderedItemsRequest, itemPk: Long?): Collection<RenderedItem>? {
        val cursor = itemsRequest.cursor
        val resolvedContentType = itemsRequest.resolvedContentType

        return renderedItemDao.queryRenderedItems(
            RenderedItemQuery(
                itemPk,
                itemsRequest.maxRows,
                resolvedContentType,
                cursor,
                itemsRequest.fromDateTime,
                itemsRequest.untilDateTime,
            ),
        )
    }
}
