package org.crossref.manifold.rendering

import org.crossref.manifold.util.MimeType
import org.springframework.http.MediaType
import java.time.OffsetDateTime

/**
 * IDs are referenced in the database, so must remain stable.
 */
enum class ContentType(val id: Int, val mimeType: String, val weight: Int) {
    CITEPROC_JSON(1, MimeType.CITEPROC_JSON, 2),

    // This is the elastic document before the points of conversion
    // to Citeproc json
    CITEPROC_JSON_ELASTIC(2, MimeType.CITEPROC_JSON_ELASTIC,1),

    CITATION_JSON(3, MimeType.CITATION_JSON, 1),

    MEMBER_JSON(4, MimeType.MEMBER_JSON,1),

    // This is the elastic document before the points of conversion
    // to member json
    MEMBER_JSON_ELASTIC(5, MimeType.MEMBER_JSON_ELASTIC, 1),

    ORGANIZATION_JSON(6, MimeType.ORGANIZATION_JSON, 1),

    // This is the elastic document before the points of conversion
    // to organization json
    ORGANIZATION_JSON_ELASTIC(7, MimeType.ORGANIZATION_JSON_ELASTIC, 2),

    // <crossref_result> XML, a public output format as an alternative to JSON
    UNIXSD(8, MimeType.UNIXSD_XML, 1),

    // Generic JSON, used for the public content type.
    APPLICATION_JSON(9, MediaType.APPLICATION_JSON_VALUE, 1),

    // Generic XML, used for the public content type.
    APPLICATION_XML(10, MediaType.APPLICATION_XML_VALUE, 1),

    ITEM_TREE_JSON(11, MimeType.ITEM_TREE_JSON, 1),

    // <doi_batch> suitable for deposit
    DEPOSIT_XML(12, MimeType.DEPOSIT_XML, 1),

    PREPRINT_MARPLE(13, MimeType.PREPRINT_MARPLE, 1)

    ;

    override fun toString(): String = mimeType

    companion object {
        fun fromMimeType(mimeType: String): ContentType? = ContentType.values().firstOrNull { it.mimeType == mimeType }

        fun fromId(id: Int): ContentType? = ContentType.values().firstOrNull { it.id == id }
    }
}

data class RenderedItem(
    val pk: Long? = null,
    val itemPk: Long,
    val publicContentType: ContentType,
    val internalContentType: ContentType,
    val current: Boolean,
    val updatedAt: OffsetDateTime?,
    val hash: String? = null,
    var content: String? = null
) {
    /**
     * Create a key for storing in the S3 bucket. This must remain stable, as it is used to retrieve
     * rendered items.
     *
     * The format designed to be compact and hierarchical, and is made up of 3 numbers: /«item-pk»/«version-pk»/«content-type-id».
     *
     * The reasoning behind this is that it allows for effective indexing and at the same time by having the content-type-id as the suffix
     * we are able to set S3 notification rules based on the type.
     */
    fun getKey() : String {
        check(pk != null) {
            "getKey can only be called when the render has been retrieved from the database, and has a pk"
        }

        return "$itemPk/$pk/${internalContentType.id}"
    }
}

data class RenderedItemQuery(
    val itemPk: Long? = null,
    val size: Int = 100,
    val contentType: ContentType? = null,
    val cursor: Long? = null,
    val fromDateTime: OffsetDateTime? = null,
    val untilDateTime: OffsetDateTime? = null,
)
