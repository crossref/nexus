package org.crossref.manifold.api

import com.fasterxml.jackson.annotation.JsonValue

enum class ApiMessageType {
    RELATIONSHIP_LIST;

    /**
     * Converts enum name to kebab case (assuming UPPER_UNDERSCORE naming convention is used).
     */
    @JsonValue
    fun getLabel(): String = name.lowercase().replace('_', '-')
}
