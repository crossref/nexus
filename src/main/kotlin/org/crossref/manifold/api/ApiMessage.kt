package org.crossref.manifold.api

abstract class ApiMessage(
    val nextCursor: String?,
    val itemsPerPage: Int
)
