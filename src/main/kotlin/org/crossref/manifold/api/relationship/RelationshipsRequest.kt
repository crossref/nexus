package org.crossref.manifold.api.relationship

import org.crossref.manifold.api.ApiRequest
import java.time.OffsetDateTime

class RelationshipsRequest(
    cursor: String?,
    rows: Int,
    val subjectId: String?,
    val subjectCollection: String?,
    val subjectType: String?,
    val subjectSteward: String?,
    val subjectRegistrationAgency: String?,
    val objectId: String?,
    val objectCollection: String?,
    val objectType: String?,
    val objectSteward: String?,
    val objectRegistrationAgency: String?,
    val relationshipType: String?,
    val fromUpdatedTime: OffsetDateTime?,
    val untilUpdatedTime: OffsetDateTime?,
    val assertedBy: String?,
    val notAssertedBy: String?,
    val linkedBy: String?,
    val notLinkedBy: String?
) : ApiRequest(cursor, rows)
