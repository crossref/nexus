/**
 * Data structures for the API.
 */
package org.crossref.manifold.api

import java.time.OffsetDateTime

/**
 * Container for all API responses.
 */
data class ResponseEnvelope<K>(
    val status: String,
    val messageType: String,
    val messageVersion: String = "1.0.0",
    val message: K
)


/**
 * List of Relationship types.
 */
data class RelationshipTypesView(
    val items: List<String>
)

data class RelationshipView(
    val subject: ItemView,
    val `object`: ItemView,
    val relationshipType: String,
    val updatedTime: OffsetDateTime,
    val assertedBy: String?,
    val linkedBy: String?
)

data class ItemView(
    val id: String?,
    val collection: String?,
    val type: String?
)

data class IdentifierView(
    val identifierType: String,
    val identifier: String
)
