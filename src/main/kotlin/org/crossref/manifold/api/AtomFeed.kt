package org.crossref.manifold.api

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

/**
 * Represents an Atom feed link element.
 *
 * All properties have default values to force a
 * no argument constructor user for xml deserialization
 */
data class Link(
    @field:JacksonXmlProperty(isAttribute = true)
    val href: String = "",
    @field:JacksonXmlProperty(isAttribute = true)
    val rel: String = "",
    @field:JacksonXmlProperty(isAttribute = true)
    val type: String = "",
)

/**
 * Represents an Atom feed entry element.
 *
 * All properties have default values to force a
 * no argument constructor user for xml deserialization
 */
data class Entry(
    val id: String = "",
    val title: String = "",
    val updated: String = "",
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JacksonXmlProperty(localName = "link")
    val links: Collection<Link> = listOf(),
)

/**
 * Represents an Atom feed feed element.
 *
 * All properties have default values to force a
 * no argument constructor user for xml deserialization
 */
@JacksonXmlRootElement(localName = "feed")
data class Feed(
    @field:JacksonXmlProperty(isAttribute = true)
    val xmlns: String = "http://www.w3.org/2005/Atom",
    val id: String = "",
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JacksonXmlProperty(localName = "link")
    val links: Collection<Link> = listOf(),
    val title: String = "",
    val updated: String = "",
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JacksonXmlProperty(localName = "entry")
    val entries: Collection<Entry> = listOf(),
)
