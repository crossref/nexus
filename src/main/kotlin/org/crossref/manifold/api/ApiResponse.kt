package org.crossref.manifold.api

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import org.springframework.http.HttpStatus

@JsonPropertyOrder("status", "messageType", "messageVersion", "message")
abstract class ApiResponse<M : ApiMessage?> (
    val status: HttpStatus,
    val messageType: ApiMessageType,
    val messageVersion: String,
    val message: M
)
