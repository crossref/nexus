package org.crossref.manifold.api

abstract class ApiRequest (
    val cursor: String?,
    val rows: Int?
)
