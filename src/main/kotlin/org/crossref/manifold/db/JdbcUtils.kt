package org.crossref.manifold.db

import java.sql.ResultSet

fun ResultSet.getNullableInt(columnLabel: String): Int? = getInt(columnLabel).let {
    if (wasNull()) null else it
}
