package org.crossref.manifold.lookup

import org.crossref.manifold.itemtree.Identifier

interface RaLookUp {
    suspend fun getRaFor(doi: Identifier): String?

    suspend fun fetchRaFor(prefixes: Set<String>)

    fun cacheEnabled(): Boolean

    fun cacheSize(): Int?
}