package org.crossref.manifold.lookup

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.time.OffsetDateTime

@Repository
class RALookupDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    fun getUntriedRALookups() = getRALookups("=")

    fun getTriedRALookups() = getRALookups(">")

    fun getRegistrationAgenciesByName(): Map<String, Int> =
        jdbcTemplate.query("SELECT name, pk FROM registration_agency") { rs, _ ->
            rs.getString("name") to rs.getInt("pk")
        }.toMap()

    fun addRegistrationAgencies(registrationAgencies: Set<String>) {
        if (registrationAgencies.isNotEmpty()) {
            npTemplate.batchUpdate(
                """
                INSERT INTO registration_agency (name)
                SELECT (:name)
                WHERE NOT EXISTS (
                    SELECT pk
                    FROM registration_agency
                    WHERE name = :name)
                ON CONFLICT (name) DO NOTHING
                """,
                registrationAgencies.sorted().map {
                    mapOf("name" to it)
                }.toTypedArray()
            )
        }
    }

    @Transactional
    fun updateResolvedRALookups(raLookups: List<Pair<String, Int>>) {
        raLookups.sortedBy { it.first }.chunked(1000) { chunk ->
            if (chunk.isNotEmpty()) {
                val chunkArray = chunk.map {
                    mapOf(
                        "doi_prefix" to it.first,
                        "registration_agency_pk" to it.second
                    )
                }.toTypedArray()
                /*
                 * Done in two queries because we only want to update prefixes with changed RAs but we want to reset
                 * the try count for all prefixes.
                 */
                npTemplate.batchUpdate(
                    """
                    UPDATE ra_lookup
                    SET registration_agency_pk = :registration_agency_pk, updated_at = NOW()
                    WHERE NOT EXISTS (
                        SELECT pk 
                        FROM ra_lookup 
                        WHERE doi_prefix = :doi_prefix
                        AND registration_agency_pk = :registration_agency_pk
                    )
                    AND doi_prefix = :doi_prefix
                    """, chunkArray
                )
                npTemplate.batchUpdate(
                    """
                    UPDATE ra_lookup
                    SET try_count = 1
                    WHERE doi_prefix = :doi_prefix
                    AND try_count != 1
                    """, chunkArray
                )
            }
        }
    }

    @Transactional
    fun updateUnresolvedRALookups(prefixes: Set<String>) {
        prefixes.chunked(1000) { chunk ->
            if (chunk.isNotEmpty()) {
                val chunkArray = chunk.map {
                    mapOf(
                        "doi_prefix" to it
                    )
                }.toTypedArray()
                /*
                 * Done in two queries because we only want to update prefixes that have changed (i.e. weren't null
                 * before but are now) but we want to increment the try count for all prefixes.
                 */
                npTemplate.batchUpdate(
                    """
                    UPDATE ra_lookup
                    SET registration_agency_pk = NULL, updated_at = NOW()
                    WHERE NOT EXISTS (
                        SELECT pk 
                        FROM ra_lookup 
                        WHERE doi_prefix = :doi_prefix 
                        AND registration_agency_pk IS NULL
                    )
                    AND doi_prefix = :doi_prefix
                    """, chunkArray
                )
                npTemplate.batchUpdate(
                    """
                    UPDATE ra_lookup
                    SET try_count = try_count + 1
                    WHERE doi_prefix = :doi_prefix
                    """, chunkArray
                )
            }
        }
    }

    fun removeTriedRALookups(maxTries: Int) {
        jdbcTemplate.update("DELETE FROM ra_lookup WHERE try_count >= ?", maxTries)
    }

    fun addPrefixes(prefixes: Set<String>) {
        prefixes.sorted().chunked(1000) { chunk ->
            if (chunk.isNotEmpty()) {
                npTemplate.batchUpdate(
                    """
                    INSERT INTO ra_lookup (doi_prefix, registration_agency_pk, updated_at)
                    SELECT :prefix, NULL, NOW()
                    WHERE NOT EXISTS (
                        SELECT pk 
                        FROM ra_lookup 
                        WHERE doi_prefix = :prefix)
                    ON CONFLICT (doi_prefix) DO NOTHING
                    """, chunk.map { mapOf("prefix" to it) }.toTypedArray()
                )
            }
        }
    }

    fun addIdentifierPrefixes(syncId: Long, identifierPrefixes: Map<Long, String>) {
        identifierPrefixes.toSortedMap().entries.chunked(1000) { chunk ->
            if (chunk.isNotEmpty()) {
                // It's possible not all of these identifiers were inserted so check first.
                npTemplate.batchUpdate(
                    """
                    INSERT INTO identifier_ra_lookup (identifier_pk, ra_lookup_pk, sync_id)
                    SELECT :identifierPk, pk, :syncId
                    FROM ra_lookup 
                    WHERE EXISTS (
                        SELECT pk 
                        FROM item_identifier 
                        WHERE pk = :identifierPk)
                    AND doi_prefix = :doiPrefix
                    ON CONFLICT (identifier_pk) DO NOTHING
                    """,
                    chunk.map { mapOf("identifierPk" to it.key, "doiPrefix" to it.value, "syncId" to syncId) }
                        .toTypedArray()
                )
            }
        }
    }

    fun flagRALookupsForResync(syncId: Long, updatedSince: OffsetDateTime): Int =
        jdbcTemplate.update(
            """
            UPDATE identifier_ra_lookup iral
            SET sync_id = ?
            FROM ra_lookup ral
            WHERE iral.ra_lookup_pk = ral.pk
            AND ral.updated_at >= ?
            """.trimIndent(), syncId, updatedSince
        )

    fun getRegistrationAgencyPkFromName(raName: String): Int? =
        jdbcTemplate.queryForList("SELECT pk FROM registration_agency WHERE name = ?", Int::class.java, raName)
            .firstOrNull()

    private fun getRALookups(operator: String): Set<String> =
        jdbcTemplate.queryForList(
            "SELECT doi_prefix FROM ra_lookup WHERE try_count $operator 0",
            String::class.java
        ).toSet()
}
