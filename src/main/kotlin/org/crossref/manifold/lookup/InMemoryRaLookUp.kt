package org.crossref.manifold.lookup

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import org.crossref.manifold.identifiers.parsers.Doi
import org.crossref.manifold.itemtree.Identifier
import java.util.concurrent.ConcurrentHashMap

class InMemoryRaLookUp : RaLookUp {
    private val doiPrefixes = ConcurrentHashMap<String, String>()

    private val mutex = Mutex()

    override suspend fun fetchRaFor(prefixes: Set<String>) {
        var notCached = prefixes.filterNot { doiPrefixes.containsKey(it) }

        if (notCached.isNotEmpty()) {
            mutex.withLock {
                // check again in case it has been obtained in the meantime
                notCached = prefixes.filterNot { doiPrefixes.containsKey(it) }

                if (notCached.isNotEmpty()) {
                    doiPrefixes.putAll(RAResolver().lookupRAs(notCached.toSet()).filter { it.second != null }
                        .map { it.first to it.second!! })
                }
            }
        }
    }

    override suspend fun getRaFor(doi: Identifier): String? {
        val prefix = Doi.doiPrefix(doi)

        return if(doiPrefixes.containsKey(prefix)){
            doiPrefixes[prefix]
        }else{
            prefix?.let {
                fetchRaFor(setOf(it))
                doiPrefixes[prefix]
            }
        }
    }

    override fun cacheEnabled(): Boolean = true

    override fun cacheSize(): Int = doiPrefixes.size
}