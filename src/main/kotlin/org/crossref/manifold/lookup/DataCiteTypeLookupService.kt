package org.crossref.manifold.lookup

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.identifiers.parsers.Doi
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Properties
import org.crossref.manifold.itemtree.jsonObjectFromMap
import org.crossref.manifold.modules.consts.Items
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@Service
class DataCiteTypeLookupService(
    private val itemDao: ItemDao,
    private val itemGraph: ItemGraph,
    private val dataCiteTypeLookupDao: DataCiteTypeLookupDao
) {
    companion object {
        const val DATACITE_TYPE_LOOKUP_CRON = "datacite-type-lookup-cron"
        const val DATACITE_TYPE_RETRY_CRON = "datacite-type-retry-cron"
        const val DATACITE_LOOKUP_PAGE_SIZE = 100
        const val MAX_DAILY_TRIES = 7
    }

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Scheduled(cron = "\${${DATACITE_TYPE_LOOKUP_CRON}:-}")
    fun lookupUntriedTypes() {
        lookupTypes(dataCiteTypeLookupDao.getTriedTypeLookups(0))
    }

    @Scheduled(cron = "\${${DATACITE_TYPE_RETRY_CRON}:-}")
    fun lookupTriedTypes() {
        lookupTypes(dataCiteTypeLookupDao.getTriedTypeLookups(MAX_DAILY_TRIES))
    }

    /*
     * Looks up the work type for DataCite DOIs by querying the DataCite API. DOIs are recognised as DataCite if their
     * prefixes have DataCite as their registration agency. Only DOIs with unknown work types are looked up. A work type
     * is known if it has been provided in the subject of a DataCite assertion or if it has already been looked up.
     *
     * Once the work type has been retrieved, a new item tree is submitted for ingestion and canonicalization. Because
     * of the asynchronous nature of this process it's possible a DOI could be looked up multiple times before its work
     * type becomes available, resulting in duplicate item tree submissions. The canonicalizer handles this during the
     * merge phase, overwriting pre-existing values.
     */
    private fun lookupTypes(identifierPksToIgnore: Set<Long>) {
        logger.info("Looking up work types for DataCite DOIs")
        val dataCiteTypeResolver = DataCiteTypeResolver()
        var lastKey = -1L
        do {
            val doisWithUnknownTypes = itemDao.getRADoisWithUnknownTypes("DataCite", lastKey, DATACITE_LOOKUP_PAGE_SIZE)

            if (doisWithUnknownTypes.isNotEmpty()) {
                // Filter out ignored DOIs and map the DOI string to its identifier PK.
                val filteredDois = doisWithUnknownTypes.filterNot { identifierPksToIgnore.contains(it.first) }
                    .associate { Doi.withoutResolver(it.second)!! to it.first }
                if (filteredDois.isNotEmpty()) {
                    logger.info("Looking up work types for ${filteredDois.size} DataCite DOIs")
                    val fetchedTypes = mutableListOf<Pair<String, String?>>()
                    filteredDois.entries.chunked(10)
                        .forEach { chunk ->
                            fetchedTypes.addAll(dataCiteTypeResolver.lookupTypes(chunk.map { it.key }
                                .toSet()))
                        }
                    if (fetchedTypes.isNotEmpty()) {
                        logger.info("Fetched ${fetchedTypes.size} work types")
                        val resolvedAndUnresolvedTypes = fetchedTypes.partition { it.second != null }
                        val resolvedTypes = resolvedAndUnresolvedTypes.first
                        val unresolvedTypes = resolvedAndUnresolvedTypes.second
                        if (resolvedTypes.isNotEmpty()) {
                            logger.info("Fetched ${resolvedTypes.size} resolved types")
                            dataCiteTypeLookupDao.removeTriedTypeLookups(resolvedTypes.map { filteredDois[it.first]!! }
                                .toSet())
                            val typeTrees = resolvedTypes.map {
                                ItemTree(
                                    Item()
                                        .withIdentifier(IdentifierParser.parse(it.first))
                                        .withProperties(
                                            listOf(
                                                Properties(
                                                    jsonObjectFromMap(
                                                        mapOf(
                                                            "type" to "work",
                                                            "subtype" to it.second,
                                                        ),
                                                    ),
                                                ),
                                            )
                                        )
                                )
                            }

                            val typeBatch =
                                EnvelopeBatch(
                                    listOf(
                                        Envelope(
                                            typeTrees,
                                            ItemTreeAssertion(
                                                OffsetDateTime.now(),
                                                Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY)),
                                            ),
                                        )
                                    ),
                                    EnvelopeBatchProvenance(
                                        UserAgentParser.parse("Crossref DataCite Type Lookup/1.0"),
                                        OffsetDateTime.now().toString()
                                    ),
                                )

                            logger.info("Asserting ${typeBatch.envelopes.first().itemTrees.size} work types for DataCite DOIs")

                            /*
                             * Since all of these items have DataCite as their RA and we are asserting their type as Crossref, any
                             * existing item trees will have a different asserting party so the Canonicalizer can handle the merge.
                             */
                            itemGraph.ingest(listOf(typeBatch))
                        }

                        if (unresolvedTypes.isNotEmpty()) {
                            logger.info("Fetched ${unresolvedTypes.size} unresolved types")
                            val identifierPks = unresolvedTypes.map { filteredDois[it.first]!! }.toSet()
                            dataCiteTypeLookupDao.addTriedTypeLookups(identifierPks)
                            dataCiteTypeLookupDao.incrementTriedTypeLookups(identifierPks)
                        }
                    }
                }
                lastKey = doisWithUnknownTypes.last().first
            }
        } while (doisWithUnknownTypes.size == DATACITE_LOOKUP_PAGE_SIZE)
    }
}
