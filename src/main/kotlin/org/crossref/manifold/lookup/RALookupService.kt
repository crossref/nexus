package org.crossref.manifold.lookup

import org.crossref.manifold.relationship.RelationshipSyncDao
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@Service
class RALookupService(
    private val raLookupDao: RALookupDao,
    private val relationshipSyncDao: RelationshipSyncDao
) {
    companion object {
        const val RA_LOOKUP_ALL_CRON = "ra-lookup-all-cron"
        const val RA_LOOKUP_NEW_CRON = "ra-lookup-new-cron"
        const val MAX_DAILY_TRIES = 7
    }

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Scheduled(cron = "\${$RA_LOOKUP_NEW_CRON:-}")
    fun lookupUntriedRAs() {
        lookupRAs(raLookupDao.getUntriedRALookups())
    }

    @Scheduled(cron = "\${$RA_LOOKUP_ALL_CRON:-}")
    fun lookupTriedRAs() {
        lookupRAs(raLookupDao.getTriedRALookups())
        raLookupDao.removeTriedRALookups(MAX_DAILY_TRIES)
    }

    private fun lookupRAs(storedRALookups: Set<String>) {
        logger.info("Looking up RAs for DOI prefixes")

        val updatedSince = OffsetDateTime.now()

        if (storedRALookups.isNotEmpty()) {
            logger.info("Looking up RAs for ${storedRALookups.size} DOI prefixes")
            val fetchedRALookups = mutableListOf<Pair<String, String?>>()
            storedRALookups.chunked(100).forEach { fetchedRALookups.addAll(RAResolver().lookupRAs(it.toSet())) }
            if (fetchedRALookups.isNotEmpty()) {
                logger.info("Fetched ${fetchedRALookups.size} RAs")
                val resolvedAndUnresolvedRAs = fetchedRALookups.partition { it.second != null }
                val resolvedRAs = resolvedAndUnresolvedRAs.first
                val unresolvedRAs = resolvedAndUnresolvedRAs.second

                if (resolvedRAs.isNotEmpty()) {
                    logger.info("Fetched ${resolvedRAs.size} resolved RAs")
                    // Add any new RAs.
                    raLookupDao.addRegistrationAgencies(resolvedRAs.map { it.second!! }.toSet())

                    // Map RAs to their primary keys
                    val allRAs = raLookupDao.getRegistrationAgenciesByName()
                    raLookupDao.updateResolvedRALookups(resolvedRAs.map { it.first to allRAs[it.second]!! })
                }

                if (unresolvedRAs.isNotEmpty()) {
                    logger.info("Fetched ${unresolvedRAs.size} unresolved RAs")
                    raLookupDao.updateUnresolvedRALookups(unresolvedRAs.map { it.first }.toSet())
                }

                val syncId = relationshipSyncDao.newRelationshipSync()
                raLookupDao.flagRALookupsForResync(syncId, updatedSince)
                relationshipSyncDao.relationshipSyncReady(syncId)
            }
        }
    }
}
