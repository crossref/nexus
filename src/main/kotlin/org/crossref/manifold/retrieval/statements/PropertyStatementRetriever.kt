//package org.crossref.manifold.retrieval.statements
//
//import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
//import org.crossref.manifold.itemgraph.PropertyDao
//import org.crossref.manifold.itemgraph.PropertyAssertionStatement
//import org.springframework.stereotype.Service
//
//@Service
//class PropertyStatementRetriever(
//    private val propertyDao: PropertyDao
//) {
//    /**
//     * Fetch a page of Statement Items starting from the given Statement Pk.
//     * Return an optional starting Statement Pk if there are more items.
//     */
//    fun getPage(
//        startPk: Long,
//        count: Int,
//        filter: PropertyStatementFilter,
//    ): Pair<List<Pair<PropertyAssertionStatement, EnvelopeBatchProvenance>>, Long?> {
//
//        // Fetch one extra as a cursor for the next page, but don't show it.
//        val fetchCount = count + 1
//
//        val result = propertyDao.getPropertyStatementRange(startPk, fetchCount, filter)
//
//        // If we returned less than the desired amount plus one, that means it's the last page.
//        return if (result.count() < fetchCount) {
//            Pair(result, null)
//        } else {
//            // If we returned more than the desired amount, keep only the desired amount and use the extra
//            // as the cursor for the next page.
//            Pair(result.subList(0, count), result.last().first.pk)
//        }
//    }
//}
