package org.crossref.manifold.retrieval

import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemtree.Identifier

/**
 * Caching wrapper around [RelationshipDao] to support efficient retrieval of
 * [Identifier]s by their PKs, in batches. The lifetime of an instance is expected to be the duration of a query to
 * the [RelationshipDao].
 *
 * This is useful for [ItemTreeRetriever]'s work for two reasons:
 * 1. The [ItemTreeRetriever] recurses down levels of the tree, fetching a batch of Items at each level. By
 * batching these we cut down on database chatter.
 * 2. Some Items will appear repeatedly, so it makes sense to cache them.
 *
 * The lifetime of this object should be a single invocation of a [ItemTreeRetriever] call, meaning the data is
 * consistent within the context of the outer API call. It is not intended to persist between API invocations, or to be
 * shared, so no need for invalidation.
 *
 * Not threadsafe, and designed to be called from single threaded context. This is not a context-managed bean.
 */
class IdentifierRetriever(
    private val itemDao: ItemDao,
) {
    /**
     * Map of looked up item PK to ItemIdentifiers.
     */
    private val cache = mutableMapOf<Long, List<Identifier>>()

    /**
     * Retrieve known [Identifier]s for the item PK.
     * @return set of [Identifier]s. Order is not significant.
     */
    fun get(itemPk: Long?) = cache[itemPk]

    fun getOrEmpty(itemPk: Long) = cache.getOrDefault(itemPk, emptyList())

    /** Prime the cache a batch of ItemPks. Don't fetch those that we already have in the cache.
     * @param itemPks set of distinct Item PKs. Set, not just a collection because PKs are expected to be distinct.
     */
    fun prime(
        itemPks: Set<Long>
    ) {
        // For an arbitrary list of Item Pks, we get an arbitrarily sorted list of ItemIdentifier triples.
        val result = itemDao.fetchItemIdentifierPairsForItemPks(itemPks.subtract(cache.keys))

        // Because each Item PK may have resulted in a number of ItemIdentifiers, and they are in arbitrary order,
        // group by Item PK.
        result.groupBy { it.first }.forEach { (itemPk, identifiers) ->
            cache[itemPk] = identifiers.map { (pk, identifier) ->
                identifier
            }
        }
    }
}
