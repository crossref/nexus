package org.crossref.manifold.views

import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.rdf.model.Resource
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree

object RdfConverter {
    /**
     * The multi-model describes every ItemIdentifier as being related to every other ItemIdentifier. This has the
     * disadvantage of multiplying creating a relationship between all pairs of identifiers available for a relationship.
     * However, it does allow users to query the resulting model for links between items by their Identifiers,
     * regardless of which was used.
     */
    fun toMultiModel(itemTree: ItemTree): Model {
        val model = ModelFactory.createDefaultModel()!!

        // Pass in parent's identifiers because the statements will link parent's identifiers to this one's.
        fun recurse(item: Item, parentIdentifierResources: List<Resource>) {
            val itemIdentifierResources = itemIdentifiersToRdfResources(item, model)

            for (relationship in item.rels) {
                val property = model.createProperty("http://id.crossref.org/properties#${relationship.relTyp}")

                for (parentId in parentIdentifierResources) {
                    for (id in itemIdentifierResources) {
                        model.add(model.createStatement(parentId, property, id))
                    }
                }
                recurse(relationship.obj, itemIdentifierResources)
            }
        }

        // Express this item's identifiers as RDF Resource objects.
        val identifiers = itemIdentifiersToRdfResources(itemTree.root, model)

        recurse(itemTree.root, identifiers)

        return model
    }

    private fun itemIdentifiersToRdfResources(
        itemTree: Item,
        model: Model
    ) = itemTree.identifiers.mapNotNull { identifier ->
        if (identifier != null) {
            model.createResource(identifier.uri.toString())
        } else {
            null
        }
    }
}
