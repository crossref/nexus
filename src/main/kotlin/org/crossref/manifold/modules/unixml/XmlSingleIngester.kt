package org.crossref.manifold.modules.unixml

import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.unixml.support.parseUniXmlToEnvelopes
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class XmlSingleIngester(
    private val itemGraph: ItemGraph,
) {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)


    fun ingest(objectKey: String, content: String) {
        logger.debug("Ingest XML: parsing.")
        val envelopeBatches = parseUniXmlToEnvelopes(
            objectKey,
            content,
        )
        logger.debug("Ingest XML: ${envelopeBatches.count()} batches...")
        itemGraph.ingest(envelopeBatches)
        logger.debug("Ingest XML: finished.")
    }

    /**
     * Given a list of pairs of filename and content, ingest them in one transaction.
     */
    fun ingestMany(s3Files: List<Pair<String, String>>) {
        logger.debug("Ingest XML: parsing.")
        val envelopeBatches = s3Files.flatMap { parseUniXmlToEnvelopes(
                it.first,
                it.second,
        )}
        logger.debug("Ingest XML: ${envelopeBatches.count()} batches...")
        itemGraph.ingest(envelopeBatches)
        logger.debug("Ingest XML: finished.")
    }
}
