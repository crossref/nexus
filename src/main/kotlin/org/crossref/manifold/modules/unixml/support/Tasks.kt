/**
 * Does this get generated?
 */
package org.crossref.manifold.modules.unixml.support

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import org.crossref.manifold.itemgraph.ItemGraph
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.InputStreamReader
import java.util.concurrent.atomic.AtomicInteger


const val PARSE_PARALLELISM = 20


/**
 * Maximum number of files to ingest in one session. Intended as a circuit-breaker.
 */
const val MAX_NUM_FILES_TO_INGEST = 50000000

/**
 * For debugging and trying to reproduce behaviour, skip this many files in the .tar.gz.
 */
const val SKIP_FILES = 0

// 100 MB is the largest size of XML file we will ingest, ignore the others.
const val MAX_XML_SIZE = 100000000

class UniXML

var logger: Logger = LoggerFactory.getLogger(UniXML::class.java)


fun shouldSendEntry(entry: ArchiveEntry, result: TarGzResult, skipFiles: Int, maxNumFilesToProcess: Int): Pair<Boolean, Boolean> {
    val isFile = !entry.isDirectory
    val sizeOk = entry.size < MAX_XML_SIZE

    val inDebugRange = result.countSeen in skipFiles until maxNumFilesToProcess

    if (!sizeOk) {
        logger.warn("WARNING! SKIPPING ${entry.name} SIZE ${entry.size} Bytes")
        result.skippedFilenames.add(Pair(entry.name, entry.size))
    }

    val willProcess = isFile && sizeOk && inDebugRange
    val willStop = result.countSeen > maxNumFilesToProcess
    return Pair(willProcess, willStop)
}

/**
 * For a given entry, decide whether it should be sent to a Channel.
 * If so, read and send it.
 */

suspend fun readEntryToChannel(
    entry: InputStreamReader,
    filename: String,
    nameContentChan: Channel<Pair<String, String>>
) {
    try {
        // Buffer XML file into a String so we can send it
        // into the channel after the file closes.
        val text = entry.readText()
        nameContentChan.send(Pair(filename, text))
    } catch (e: Exception) {
        logger.warn("Failed: ${e}")
    }
}


data class TarGzResult(
    var countRead: Long = 0,
    var countSeen: Long = 0,
    var maxSize: Long = 0,
    val skippedFilenames: MutableSet<Pair<String, Long>> = mutableSetOf()
) {
    fun updateAndLogFile(filename: String, size: Long, willProcess: Boolean) {
        countSeen++

        if (willProcess) {
            countRead++
        }

        if (willProcess) {
            logger.debug("Ingest ${filename}, seen ${countSeen}, read $countRead files)")
        } else {
            logger.debug("Skipping $filename size $size bytes")
        }

        if (size > maxSize) {
            logger.info("$filename has new max size: previously $maxSize bytes, now $size bytes in $filename.")
            maxSize = size
        }
    }
}

suspend fun sendTarGzTo(file: File, nameContentChan: Channel<Pair<String, String>>){
    sendTarGzTo(file, nameContentChan, SKIP_FILES, MAX_NUM_FILES_TO_INGEST)
}

suspend fun sendTarGzTo(
    file: File,
    nameContentChan: Channel<Pair<String, String>>,
    skipFiles: Int,
    maxNumFilesToProcess: Int) {
    try {
        val result = TarGzResult()

        val skipped: MutableList<Pair<String, Long>> = mutableListOf()
        file.inputStream().use { reader ->
            val gz = GzipCompressorInputStream(reader)
            val input = TarArchiveInputStream(gz)
            var entry: ArchiveEntry?
            var stop = false
            InputStreamReader(input).use { entryReader ->
                while (run {
                        entry = input.nextTarEntry
                        entry
                    } != null && !stop) {
                    if (entry != null) {
                        val theEntry = entry!!
                        val (willProcess, willStop) = shouldSendEntry(theEntry, result, skipFiles, maxNumFilesToProcess)
                        stop = willStop

                        result.updateAndLogFile(theEntry.name, theEntry.size, willProcess)
                        if (willProcess) {
                            readEntryToChannel(entryReader, theEntry.name, nameContentChan)
                        }
                    }
                }
            }
        }
        logger.info("Finished reading XML .tar.tz")
        logger.info("Skipped: $skipped")
    } catch (e: Exception) {
        logger.warn("Error! $e")
        e.printStackTrace()
    } finally {
        nameContentChan.close()
    }
}

suspend fun ingestXmlFromChannel(
    routineId: Int,
    nameContentChan: Channel<Pair<String, String>>,
    itemGraph: ItemGraph,
    filesRead: AtomicInteger
) {
    logger.info("> ingestXmlFromChannel... $routineId")

    try {
        for ((filename, content) in nameContentChan) {
            logger.info("> XML async ${Thread.currentThread().name}")
            val envelopeBatches = parseUniXmlToEnvelopes(
                filename,
                content,
            )

            itemGraph.ingest(envelopeBatches)

            filesRead.incrementAndGet()
            logger.info("< XML async ${Thread.currentThread().name}")
        }
    } catch (e: Exception) {
        logger.warn("Error! $e")
        e.printStackTrace()
    }

    logger.info("< ingestXmlFromChannel... $routineId")
}

fun insertFromTarGz(file: File, itemGraph: ItemGraph) {
    try {
        val startTime = System.currentTimeMillis()

        // Number of XML files we read out of the snapshot.
        val filesRead = AtomicInteger()

        runBlocking {
            // Receive XML strings from the archive.
            val contentFilenameChannel = Channel<Pair<String, String>>(PARSE_PARALLELISM)

            val gzTask = launch { sendTarGzTo(file, contentFilenameChannel) }

            val parseTasks = (0..PARSE_PARALLELISM).map {
                launch(CoroutineName("parse-xml-$it") + Dispatchers.IO) {
                    logger.info("Start XML ingester on ${Thread.currentThread().name}")
                    ingestXmlFromChannel(it, contentFilenameChannel, itemGraph, filesRead)
                }
            }


            parseTasks.forEach { it.join() }
            gzTask.join()
            val duration = (System.currentTimeMillis() - startTime) / 1000
            logger.info("Took $duration seconds.")
        }
    } catch (e: Exception) {
        logger.warn("Error! $e")
        e.printStackTrace()
    }
}