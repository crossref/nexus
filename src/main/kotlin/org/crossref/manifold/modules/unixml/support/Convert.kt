package org.crossref.manifold.modules.unixml.support

import org.crossref.cayenne
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.identifiers.parsers.Doi
import org.crossref.manifold.ingestion.*
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.crossref.manifold.util.clojure.fromClj
import java.io.InputStreamReader
import java.time.OffsetDateTime
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

/**
 * User Agent string for this module.
 */
const val CROSSREF_XML_INGEST = "Crossref UNIXML Importer/1.0"


/**
 * For a UniXML derived Item Tree, create the Envelope Batch that asserts it.
 */
fun itemTreeToEnvelopeBatch(
    metadataItemTree: ItemTree,
    doi: Identifier,
    trace: String,
): EnvelopeBatch {
    val lessIssn = Metadata.removeKnownAmbiguities(metadataItemTree)

    // Extract the stewardship assertions, as those should be asserted by Crossref, not by the member.
    val stewardMemberIds = lessIssn.root.findRelationships("steward").map { it.obj }.map { Item(it.identifiers) }

    // Remove stewardship from the tree for assertion by the member.
    val lessSteward = ItemTree(lessIssn.root.filterByRelationship { it.relTyp != "steward" })

    val distinctMemberIds = (stewardMemberIds).toSet()

    // The deposited date is expected to be present for most records, but it is not guaranteed.
    // Fall back to using current dateTime if unable to fetch the deposited one.
    val depositedDateTime = if (metadataItemTree.root.properties.isNotEmpty()) {
        val dateTimeStr = metadataItemTree.root.properties[0].values
            .get("deposited")
            ?.get("date-time")
            ?.asText()

        if (dateTimeStr.isNullOrBlank()) OffsetDateTime.now() else OffsetDateTime.parse(dateTimeStr)
    } else {
        OffsetDateTime.now()
    }

    return if (distinctMemberIds.size != 1) {
        val message = "ERROR! Expected to find exactly one member id in this file. Got: $distinctMemberIds"
        logger.error(message)
        throw Exception(message)
    } else {

        val depositingMember = distinctMemberIds.first()

        val crossref = Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY))

        val stewardshipTree = ItemTree(
                Item().withIdentifier(doi).withRelationships(
                        listOf(
                                // "Crossref says that this member is the maintainer of this DOI".
                                Relationship(RelationshipTypes.STEWARD, depositingMember, assertedAt = null),
                        )
                )
        )

        val stewardshipAssertion = ItemTreeAssertion(
                assertedAt = depositedDateTime,
                assertingParty = crossref
        )

        val stewardshipEnvelope = Envelope(listOf(stewardshipTree), stewardshipAssertion)

        // And finally "depositing member says that here's some bibliographic metadata"
        val metadataItemTreeAssertion = ItemTreeAssertion(
            assertedAt = depositedDateTime,
            assertingParty = depositingMember
        )
        val metadataEnvelope = Envelope(listOf(lessSteward), metadataItemTreeAssertion)

        val allEnvelopes =
                listOf(metadataEnvelope) + stewardshipEnvelope

        EnvelopeBatch(allEnvelopes, EnvelopeBatchProvenance(UserAgentParser.parse(CROSSREF_XML_INGEST), trace))
    }
}

/**
 * For a UNIXML document construct an Envelope that asserts the metadata itself.
 */
fun parseUniXmlToEnvelopes(
    trace: String,
    text: String,
): Collection<EnvelopeBatch> {
    logger.debug("Reading UniXML into Item Tree assertion...")

    val queryResultDoi = getQueryResultDoi(text)
    if (queryResultDoi == null) {
        logger.error("Found missing DOI in $trace")
        return emptyList()
    } else {

        return InputStreamReader(text.byteInputStream()).use { stream ->

            // List of pairs of [primary-identifier, item-tree]
            val parsedXmlPairs = cayenne.parseXmlCentred(stream, "query_result")

            // The Metadata Item Trees are those that represent the pure metadata within the XML file.
            val metadataItemTrees = metadataItemTrees(parsedXmlPairs)

            // At this point we'll need to do some patching to get it into an Envelope, and that can happen for each Item
            // tree individually.
            val all = metadataItemTrees.map {
                itemTreeToEnvelopeBatch(
                        it,
                        queryResultDoi,
                        trace,
                )
            }

            // This is a bit of an emergency breaker to find bugs in the parser.
            all.map { removeAmbiguousItems(it) }

        }
    }
}

/**
 * Retrieve the DOI that this XML document is for, indicated in the <query_result> element.
 */
fun getQueryResultDoi(xml: String): Identifier? {
    val factory = DocumentBuilderFactory.newInstance()
    val builder = factory.newDocumentBuilder()

    xml.byteInputStream().use { stream ->

        val document = builder.parse(stream)
        val xpath: XPath = XPathFactory.newInstance().newXPath()
        val result = xpath.evaluate("//query_result/body/query/doi", document, XPathConstants.NODE)

        val doi = if (result is org.w3c.dom.Element) {
            result.firstChild?.textContent
        } else null

        return doi?.let { IdentifierParser.parse(it) }
    }
}

/**
 * Cayenne builds DOIs by string concatenation without trying to encode, but this will result in some invalid URIs.
 *
 * So strip these back to plain Unicode DOIs (e.g. "10.5555/12345678") and parse them to construct the correctly encoded URI.
 *
 */
fun reEncodeDoi(input: String): String {
    val prefix = "https://doi.org/"
    return if (input.startsWith(prefix)) {
        val lessPrefix = input.substring(prefix.length)
        Doi.construct(lessPrefix)?.uri?.toString() ?: input
    } else input
}

private fun metadataItemTrees(parsedXmlPairs: MutableIterable<Any?>) =
    parsedXmlPairs.mapNotNull { tree ->
        // Failure to parse would result in null.
        tree.let { notNullTree ->
            // First item in the pair is the primary identifier for this doc which we don't need.
            val itemTree = notNullTree as Map<*, *>
            ItemTree(Item.fromClj(itemTree) { reEncodeDoi(it) })
        }
    }
