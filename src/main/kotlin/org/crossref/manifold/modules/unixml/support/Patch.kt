package org.crossref.manifold.modules.unixml.support

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.consts.RelationshipTypes

/**
 * These functions patch or extract data from the XML member-supplied metadata.
 */
object Metadata {
    /**
     * For an Item Tree, retrieve all DOIs being registered. Each is represented by a simple Item with the DOI as its
     * identifier.
     * @return Set<Item>
     */
    fun getDoiRegistrations(itemTree: ItemTree): Set<Item> {
        // Set of pairs of content item (e.g. article) to landing page item.
        // The content item only needs its identifier, not the rest of its baggage.
        val itemLandingPages = mutableSetOf<Item>()

        // This recursive function looks at two levels. The `item` variable points to the current node, and then we look
        // at the items via its immediate relations.
        fun recurse(item: Item) {
            // If there's a resource resolution for this, it's a DOI.
            if (item.identifiers.isNotEmpty() &&
                item.rels.any { it.relTyp == RelationshipTypes.RESOURCE_RESOLUTION }) {
                val doiItem = Item().withIdentifier(item.identifiers.first())

                itemLandingPages.add(doiItem)
            }

            // And recurse down.
            for (rel in item.rels) {
                recurse(rel.obj)
            }
        }

        recurse(itemTree.root)

        return itemLandingPages
    }

    /**
     * Remove known ambiguities such as ISSNs and Supplementary IDs for now because they cause a lot of ambiguous
     * Identifiers.
     * This will be fixed in a future Item Tree parser.
     */
    fun removeKnownAmbiguities(itemTree: ItemTree): ItemTree {
        fun recurse(item: Item): Item =
            item
                .withIdentifiers(item.identifiers.filterNot {
                    it.uri.toString().startsWith("http://id.crossref.org/issn/") or
                            it.uri.toString().startsWith("http://id.crossref.org/supp/")
                })
                .withRelationships(item.rels.map { it.withItem(recurse(it.obj)) })

        return ItemTree(recurse(itemTree.root))
    }

}


