package org.crossref.manifold.modules.unixml

import org.crossref.cayenne.boot
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.crossref.manifold.modules.StartupTask
import org.crossref.manifold.modules.unixml.support.MAX_NUM_FILES_TO_INGEST
import org.crossref.manifold.modules.unixml.support.SKIP_FILES
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.services.s3.S3Client
import java.io.File
import kotlin.io.path.Path
import kotlin.system.exitProcess

@Configuration(Module.UNIXML)
class Module(moduleRegistrar: ModuleRegistrar) {
    companion object {
        const val UNIXML = "unixml"
        const val INGEST_XML_SNAPSHOT = "ingest-xml-snapshot"
        const val CONVERT_SNAPSHOT = "convert-snapshot"
        const val CONVERT_SKIP = "convert-skip"
        const val CONVERT_MAX = "convert-max"
        const val CONVERT_ID = "convert-id"
    }

    init {
        moduleRegistrar.register(
            Manifest(
                name = UNIXML,
                description = "UNIXML schema"
            )
        )

        // Always put the Cayenne module in its initial state. This is a simple configuration option, but is needed for
        // all XML ingestion tasks.
        boot()
    }

    @Bean
    @ConditionalOnProperty(prefix = UNIXML, name = [INGEST_XML_SNAPSHOT])
    fun xmlSnapshotIngester(
        itemGraph: ItemGraph,
        @Value("\${$UNIXML.$INGEST_XML_SNAPSHOT}") xmlSnapshotFilename: String
    ): StartupTask = StartupTask({
        val file = File(xmlSnapshotFilename)
        XmlSnapshotIngester.ingestXmlSnapshot(
            itemGraph,
            file
        )
    }, {
        XmlSnapshotIngester.destroy()
    })


    @Bean
    @ConditionalOnProperty(prefix = UNIXML, name = [CONVERT_SNAPSHOT])
    fun xmlSnapshotConverter(
        @Value("\${$UNIXML.$CONVERT_SNAPSHOT}") xmlSnapshotFilename: String,
        @Value("\${$UNIXML.$CONVERT_SKIP:$SKIP_FILES}") skip: Int,
        @Value("\${$UNIXML.$CONVERT_MAX:$MAX_NUM_FILES_TO_INGEST}") take: Int,
        @Value("\${$UNIXML.$CONVERT_ID:}") id: String?
    ): StartupTask = StartupTask({
        val inputFile = File(xmlSnapshotFilename)

        val outputFile = if (id.isNullOrBlank()) {
            Path("$inputFile.envelope-batches.tar.gz")
        } else {
            Path(inputFile.parent, "$id.tar.gz")
        }.toFile()

        UniXMLSnapshotConverter.run(inputFile, outputFile, skip, take)
        exitProcess(0)
    }, {
        XmlSnapshotIngester.destroy()
    })

    @Bean
    @ConditionalOnProperty(prefix = UNIXML, name = [S3_NOTIFICATION_QUEUE])
    fun xmlS3Ingester(
        ingester: XmlSingleIngester,
        s3Client: S3Client
    ): XmlS3Ingester = XmlS3Ingester(
        s3Client,
        ingester
    )
}
