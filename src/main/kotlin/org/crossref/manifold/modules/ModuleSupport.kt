package org.crossref.manifold.modules

import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.ApplicationListener
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

/**
 * A module manifest describes the module and what it does.
 */
class Manifest(
    /**
     * Identifier for this module.
     */
    val name: String,

    /**
     * Human-readable short description.
     */
    val description: String,
)

@Service
class ModuleRegistrar {
    val manifests = mutableSetOf<Manifest>()

    /**
     * Register a module. Every Module should call this on startup.
     */
    fun register(manifest: Manifest) {
        manifests.add(manifest)
    }
}

/**
 * Represents a block of work to be carried out when Manifold starts.
 *
 * New instances of this class should be created by and returned from @Bean annotated functions in @Configuration classes so they are identified and managed by Spring.
 *
 * This ensures they will be run asynchronously and only after the Application Context has been fully populated.
 *
 */
class StartupTask(
    private val listener: () -> Unit,
    private val destructor: () -> Unit = {},
    private val initializer: () -> Unit = {}
) : ApplicationListener<ApplicationStartedEvent>, DisposableBean, InitializingBean {
    override fun afterPropertiesSet() {
        initializer()
    }

    @Async
    override fun onApplicationEvent(event: ApplicationStartedEvent) {
        listener()
    }

    override fun destroy() {
        destructor()
    }
}
