package org.crossref.manifold.modules.crossrefxml

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator
import com.fasterxml.jackson.dataformat.xml.util.DefaultXmlPrettyPrinter
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.crossrefxml.elementRenderers.BookRenderer
import org.crossref.manifold.modules.crossrefxml.elementRenderers.JournalRenderer
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.rendering.RenderingContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory


/**
 * Rendering flow:
 * 1) A renderer is picked based on the desired output format:
 *    DepositXmlRenderer for Deposit XML (<doi_batch>);
 *    UniXsdRenderer for UniXSD (<crossref_result>).
 * 2) That renderer selects an XML element renderer based on the ItemTree type (e.g. Book, Journal, etc.).
 * 3) Element renderer converts ItemTree into an instance of `XmlElement` subclass (with `toXmlElement()`).
 * 4) Renderer from (1) post-processes the returned `XmlElement` and wraps it into an appropriate container,
 *    then serializes it into the final string.
 * */

/**
 * A contract for elementRenderers that handle specific item types.
 */
interface ItemTreeConverter {
    fun toXmlElement(itemTree: ItemTree): XmlElement
}

/**
 * Provides common functionality to render an Item and place it into an XML container.
 * This class itself is abstract and doesn't directly process anything. See inheritors for implementations.
 * */
abstract class XmlRenderer : ContentTypeRenderer {
    protected val logger: Logger = LoggerFactory.getLogger(this::class.java)
    protected val xmlMapper: XmlMapper = XmlMapper.builder()
        .enable(
            ToXmlGenerator.Feature.WRITE_XML_DECLARATION,
            ToXmlGenerator.Feature.UNWRAP_ROOT_OBJECT_NODE
        )
        .enable(SerializationFeature.INDENT_OUTPUT)
        .defaultPrettyPrinter(DefaultXmlPrettyPrinter().apply {
            val indenter = PrettyPrintIndenter()
            indentArraysWith(indenter)
            indentObjectsWith(indenter)
        })
        .build()
    protected val xmlValidator = XmlSchemaValidator()

    private val elementRenderers = hashMapOf(
        "book-component" to BookRenderer,
        "journal-article" to JournalRenderer,
    )

    /**
     * An item-specific method to place an element within a correct container,
     * and output the final XML string.
     * */
    abstract fun finalizeToString(xmlElement: XmlElement, context: RenderingContext?): String

    /**
     * An output-specific method to validate the final XML string.
     * */
    abstract fun validateXmlOutput(xmlString: String): XmlValidationResult

    override fun publicContentType(): ContentType = ContentType.APPLICATION_XML

    override fun offer(type: String?, subtype: String?, itemTree: ItemTree?, context: RenderingContext?): Boolean = type == "work" && elementRenderers.containsKey(subtype)

    override fun render(itemTree: ItemTree, context: RenderingContext?): String {
        logger.debug("Rendering item with `pk={}` to `{}`", itemTree.root.pk, internalContentType())

        val type = itemTree.root.getPropertyText("type")
        val subtype = itemTree.root.getPropertyText("subtype")

        // in case `render` was called without checking `offer`
        if (!offer(type, subtype, itemTree, context)) {
            throw IllegalArgumentException(
                "Renderer ${this::class.java.declaringClass.simpleName} " +
                        "doesn't offer to process Item Tree with type=$type, subtype=$subtype, context=$context"
            )
        }

        val renderer = elementRenderers[subtype]
            ?: throw IllegalArgumentException("Failed to find XML element renderer for Item with `subtype=$subtype`")

        logger.debug("Using `${renderer::class.java.declaringClass.simpleName}` for `subtype=$subtype`")

        val xmlElement = renderer.toXmlElement(itemTree)
        val xmlString = finalizeToString(xmlElement, context)
        val validationResult = validateXmlOutput(xmlString)

        /** Until we are sure that our XML rendering is robust enough, we only log errors and don't throw them */
        if (!validationResult.success) {
            for (msg in validationResult.prettyPrintErrors()) {
                logger.info(msg)
            }
        }

        return xmlString
    }
}
