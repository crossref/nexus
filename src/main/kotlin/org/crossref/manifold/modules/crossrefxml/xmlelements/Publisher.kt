package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("publisher_name", "publisher_place")
data class Publisher(
    @field:JacksonXmlProperty(localName = "publisher_name")
    val publisherName: String? = null,

    @field:JacksonXmlProperty(localName = "publisher_place")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val publisherPlace: String? = null,
)
