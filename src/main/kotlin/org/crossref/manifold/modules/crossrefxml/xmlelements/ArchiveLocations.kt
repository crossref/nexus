package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

@JsonPropertyOrder(*["archive"])
data class ArchiveLocations (
    @field:JacksonXmlElementWrapper(useWrapping = false)
    val archive : List<Archive>?
)