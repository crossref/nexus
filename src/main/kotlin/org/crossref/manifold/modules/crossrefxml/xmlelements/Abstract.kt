package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonRawValue
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

@JsonPropertyOrder(*["value"])
data class Abstract(
    @field:JsonRawValue
    @field:JacksonXmlText
    val value: String?,
)
