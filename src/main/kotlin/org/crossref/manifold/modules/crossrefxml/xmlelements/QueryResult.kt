package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonPropertyOrder(*["head", "body"])
data class QueryResult (
    val head : Head,
    val body : Body
)

