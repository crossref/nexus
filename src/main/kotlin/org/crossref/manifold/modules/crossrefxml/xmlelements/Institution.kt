package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder(
    "institution_name",
    "institution_id",
    "institution_acronym",
    "institution_place",
    "institution_department"
)
data class Institution(
    @field:JacksonXmlProperty(localName = "institution_name")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val institutionName: String? = null,

    @field:JacksonXmlProperty(localName = "institution_id")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val institutionId: InstitutionId? = null,

    @field:JacksonXmlProperty(localName = "institution_acronym")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val institutionAcronym: String? = null,

    @field:JacksonXmlProperty(localName = "institution_place")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val institutionPlace: String? = null,

    @field:JacksonXmlProperty(localName = "institution_department")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val institutionDepartment: String? = null,
)


