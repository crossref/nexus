package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

/**
 * Publication date. Field order in the schema is American: month, day, year.
 * Parts are strings because they contain pre-formatted leading zeroes.
 */
@JsonPropertyOrder("month", "day", "year")
data class AcceptanceDate(
    @field:JacksonXmlProperty(isAttribute = true, localName = "media_type")
    val mediaType: String?,

    val year: String?,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val month: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val day: String? = null,
)