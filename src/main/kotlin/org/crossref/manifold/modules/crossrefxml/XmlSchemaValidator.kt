package org.crossref.manifold.modules.crossrefxml

import org.xml.sax.ErrorHandler
import org.xml.sax.SAXParseException
import java.io.FileNotFoundException
import java.io.StringReader
import java.net.URL
import javax.xml.XMLConstants
import javax.xml.catalog.CatalogFeatures
import javax.xml.catalog.CatalogManager
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory
import javax.xml.validation.Validator


/**
 * Manages XML validation.
 * It uses XML catalog to load schemas from filesystem instead of downloading them from network.
 * A custom `XmlErrorHandler` is used to collect all validation errors instead of failing after the first one.
 * */
class XmlSchemaValidator {
    private val errorHandler = XmlErrorHandler()
    private val uniXsdValidator: Validator
    private val depositXmlValidator: Validator

    init {
        val catalogUri = getXsdResourceURL("catalog.xml").toURI()
        val catalog = CatalogManager.catalog(CatalogFeatures.defaults(), catalogUri)
        val catalogResolver = CatalogManager.catalogResolver(catalog)
        val schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
        schemaFactory.resourceResolver = catalogResolver

        val uniXsdSchemaStream = StreamSource(getXsdResourceURL("unixref_query_combined.xsd").openStream())
        val uniXsdSchema = schemaFactory.newSchema(uniXsdSchemaStream)
        uniXsdValidator = uniXsdSchema.newValidator()
        uniXsdValidator.errorHandler = errorHandler

        val depositXmlSchemaStream = StreamSource(getXsdResourceURL("crossref5.3.1.xsd").openStream())
        val depositXmlSchema = schemaFactory.newSchema(depositXmlSchemaStream)
        depositXmlValidator = depositXmlSchema.newValidator()
        depositXmlValidator.errorHandler = errorHandler
    }

    fun validateUniXsdXml(xmlString: String) = validate(xmlString, uniXsdValidator)

    fun validateDepositXml(xmlString: String) = validate(xmlString, depositXmlValidator)

    private fun validate(xmlString: String, validator: Validator): XmlValidationResult {
        try {
            validator.validate(StreamSource(StringReader(xmlString)))
        } catch (_: SAXParseException) {
            // collect all validation errors
        }
        // clone the error list, so we can reset the error handler
        val exceptions = errorHandler.getExceptions().toList()
        errorHandler.reset()

        return XmlValidationResult(
            success = exceptions.isEmpty(),
            exceptions = exceptions
        )
    }

    private fun getXsdResourceURL(fileName: String): URL {
        val fname = "xsdSchema/$fileName"
        return Thread.currentThread().contextClassLoader.getResource(fname)
            ?: throw FileNotFoundException("File not found: $fname")
    }
}

class XmlErrorHandler : ErrorHandler {
    private val exceptions: MutableList<SAXParseException> = ArrayList()

    fun getExceptions() = exceptions

    fun reset() = exceptions.clear()

    override fun warning(exception: SAXParseException) {
        exceptions.add(exception)
    }

    override fun error(exception: SAXParseException) {
        exceptions.add(exception)
    }

    override fun fatalError(exception: SAXParseException) {
        exceptions.add(exception)
    }
}

data class XmlValidationResult(
    val success: Boolean,
    val exceptions: List<SAXParseException>
) {
    fun prettyPrintErrors() = iterator {
        for (e in exceptions) {
            yield("Line number: ${e.lineNumber}, Column number: ${e.columnNumber}. ${e.message}")
        }
    }
}