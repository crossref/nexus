package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("given_name", "surname", "suffix", "affiliation", "affiliations", "ORCID")
data class PersonName(

    @field:JacksonXmlProperty(isAttribute = true, localName = "sequence")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val sequence: String? = null,

    @field:JacksonXmlProperty(isAttribute = true, localName = "contributor_role")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val contributorRole: String? = null,

    @field:JacksonXmlProperty(localName = "given_name")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val givenName: String? = null,

    @field:JacksonXmlProperty(localName = "surname")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val surname: String? = null,

    @field:JacksonXmlProperty(localName = "suffix")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val suffix: String? = null,

    @field:JacksonXmlProperty(localName = "affiliation")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val affiliation: String? = null,

    @field:JacksonXmlProperty(localName = "affiliations")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val affiliations: Affiliations? = null,

    @field:JacksonXmlProperty(localName = "ORCID")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val orcid: String? = null,
)
