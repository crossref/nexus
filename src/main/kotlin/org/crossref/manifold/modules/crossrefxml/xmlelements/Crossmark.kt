package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("crossmark_policy", "crossmark_domains", "updates", "custom_metadata")
data class Crossmark(

    @field:JacksonXmlProperty(localName = "crossmark_policy")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val crossmarkPolicy: String? = null,

    @field:JacksonXmlProperty(localName = "crossmark_domains")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val crossmarkDomains: List<CrossmarkDomain>? = null,

    @field:JacksonXmlProperty(localName = "update")
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    @field:JacksonXmlElementWrapper(localName = "updates")
    val updates: List<Update>? = null,

    @field:JacksonXmlProperty(localName = "custom_metadata")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val customMetadata: CustomMetadata? = null
)
