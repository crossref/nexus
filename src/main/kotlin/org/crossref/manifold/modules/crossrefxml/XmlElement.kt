package org.crossref.manifold.modules.crossrefxml

import org.crossref.manifold.modules.crossrefxml.xmlelements.Book
import org.crossref.manifold.modules.crossrefxml.xmlelements.Journal

/**
 * Each elementRenderers must return its result as an instance of `XmlElement` subclass
 */
sealed interface XmlElement {
    /**
     * `value` holds the actual object to be rendered, it must be a serializable data-class.
     * */
    val value: Any

    /**
     * These are item properties that can be referenced in the parent container of this XmlElement,
     * so we store them for convenience.
     * */
    val doi: String?
}

class BookElement(override val value: Book, override val doi: String?) : XmlElement

class JournalElement(override val value: Journal, override val doi: String?) : XmlElement
