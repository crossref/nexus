package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class Query(
    @field:JacksonXmlProperty(localName = "status", isAttribute = true)
    val status: String? = "resolved",

    val doi: Doi,

    @field:JacksonXmlProperty(localName = "crm-item")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val crmItem: CrmItem? = null,

    @field:JacksonXmlProperty(localName = "doi_record")
    val doiRecord: DoiRecord
)
