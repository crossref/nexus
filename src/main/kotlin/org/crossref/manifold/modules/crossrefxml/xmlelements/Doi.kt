package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

data class Doi(
    @field:JacksonXmlProperty(isAttribute = true)
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val type: DoiType? = null,

    @field:JacksonXmlText
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val value: String?
)

enum class DoiType(@get:JsonValue val value: String) {
    JOURNAL_TITLE("journal_title"),
    JOURNAL_ISSUE("journal_issue"),
    JOURNAL_VOLUME("journal_volume"),
    JOURNAL_ARTICLE("journal_article"),
    CONFERENCE_TITLE("conference_title"),
    CONFERENCE_SERIES("conference_series"),
    CONFERENCE_PAPER("conference_paper"),
    BOOK_TITLE("book_title"),
    BOOK_SERIES("book_series"),
    BOOK_CONTENT("book_content"),
    COMPONENT("component"),
    DISSERTATION("dissertation"),
    REPORT_PAPER_TITLE("report-paper_title"),
    REPORT_PAPER_SERIES("report-paper_series"),
    REPORT_PAPER_CONTENT("report-paper_content"),
    STANDARD_TITLE("standard_title"),
    STANDARD_SERIES("standard_series"),
    STANDARD_CONTENT("standard_content"),
    PREPUBLICATION("prepublication");
}
