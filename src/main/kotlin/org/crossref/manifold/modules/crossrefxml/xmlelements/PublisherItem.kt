package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class PublisherItem(
    @field:JacksonXmlProperty(localName = "item_number")
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    @field:JacksonXmlElementWrapper(useWrapping = false)
    val itemNumber : List<ItemNumber>? = null
)
