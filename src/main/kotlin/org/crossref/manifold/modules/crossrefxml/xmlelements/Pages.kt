package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("first_page", "last_page", "other_pages")
data class Pages (
    @field:JacksonXmlProperty(localName = "first_page")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val firstPage: String? = null,

    @field:JacksonXmlProperty(localName = "last_page")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val lastPage: String? = null,

    @field:JacksonXmlProperty(localName = "other_pages")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val otherPages: String? = null
)
