package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonPropertyOrder(*["name", "label", "explanation", "href", "order"])
data class Assertion (
    val name: String?,

    val label: String?,

    val explanation: String?,

    val href: String?,

    val order: String?,

)
