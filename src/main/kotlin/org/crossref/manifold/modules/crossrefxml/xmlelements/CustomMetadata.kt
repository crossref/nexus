package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class CustomMetadata(
    val assertion: List<Assertion>? = null,

    /**
     * Contains lists of 'Program', which can be CTN or Access Indicator programs.
     * Using a collection and marker interface as the tags have the same local name (<program>) but are polymorphic.
     */
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JacksonXmlProperty(localName = "program")
    val program : List<Program>? = null
)
