package org.crossref.manifold.modules.crossrefxml.elementRenderers

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.crossrefxml.BookElement
import org.crossref.manifold.modules.crossrefxml.ItemTreeConverter
import org.crossref.manifold.modules.crossrefxml.xmlelements.*


/**
 * Book and Book Chapter rendering.
 */
class BookRenderer {
    companion object : ItemTreeConverter {
        override fun toXmlElement(itemTree: ItemTree): BookElement {
            val chapter = bookChapter(itemTree.root)
            return BookElement(value = chapter, doi = chapter.contentItem.doiData?.doi)
        }

        /**
         * Render a Book tag, which includes a BookChapter.
         */
        private fun bookChapter(item: Item): Book {
            val book = Util.getAncestor(item, "book")
            return Book(
                bookMetadata = getBookMetadata(book),
                bookType = getBookType(book),
                contentItem = ContentItem(
                    componentType = getContentType(item),
                    titles = Titles(
                        title = Util.getFullTitle(item),
                        subtitle = Util.getSubtitle(item),
                        originalLanguageTitle = Util.getOriginalTitle(item)
                    ),
                    pages = Util.getPageNumbers(item),
                    contributors = Util.getContributors(item),
                    abstract = Util.getAbstract(item),
                    publicationDate = Util.getPublicationDates(item),
                    acceptanceDate = Util.getAcceptanceDate(item),
                    publisherItem = Util.getPublisherItems(item),
                    citationList = Util.getCitations(item),
                    crossmark = null,
                    archiveLocations = Util.getArchiveLocations(item),
                    doiData = Util.getDoiData(item),
                    componentNumber = Util.getComponentNumber(item)
                ),
            )
        }

        private fun getBookType(book: Item?): BookType? {
            return BookType::value.fromXmlValue(
                book?.getPropertyText("book-type")
                    // Metadata tree and XML have different spellings
                    ?.replace("edited-book", "edited_book")
            )
        }

        private fun getContentType(chapter: Item?) =
            ComponentType::value.fromXmlValue(chapter?.getPropertyText("book-component-type"))

        private fun getBookMetadata(book: Item?): BookMetadata? {
            return if (book != null) {
                val (isbn, noisbn) = Util.getIsbn(book)
                BookMetadata(
                    language = book.getPropertyText("language"),
                    archiveLocations = Util.getArchiveLocations(book),
                    doiData = Util.getDoiData(book),
                    citationList = Util.getCitations(book),
                    publicationDate = Util.getPublicationDates(book),
                    isbn = isbn,
                    noisbn = noisbn,
                    publisher = Util.getPublisher(book),
                    publisherItem = Util.getPublisherItems(book),
                    abstract = Util.getAbstract(book),
                    titles = Titles(
                        title = Util.getFullTitle(book),
                        subtitle = Util.getSubtitle(book),
                        originalLanguageTitle = Util.getOriginalTitle(book)
                    ),
                )
            } else null
        }
    }
}