package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

data class Affiliations(
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val institution: List<Institution>? = null,
)


