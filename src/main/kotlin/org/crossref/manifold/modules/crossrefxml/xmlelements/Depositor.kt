package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class Depositor (
    @field:JacksonXmlProperty(localName = "depositor_name")
    val depositorName : String,

    @field:JacksonXmlProperty(localName = "email_address")
    val emailAddress : String,
)