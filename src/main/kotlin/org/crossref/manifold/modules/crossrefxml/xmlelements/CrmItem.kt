package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

data class CrmItem(
    @field:JacksonXmlProperty(isAttribute = true)
    val name: CrmItemName,

    @field:JacksonXmlProperty(isAttribute = true)
    val type: CrmItemType,

    @field:JacksonXmlText
    val value: String
)

enum class CrmItemName(@get:JsonValue val value: String) {
    CITATION_ID("citation-id"),
    JOURNAL_TITLE("journal-title"),
    JOURNAL_ID("journal-id"),
    BOOK_ID("book-id"),
    SERIES_ID("series-id"),
    DEPOSIT_TIMESTAMP("deposit-timestamp"),
    OWNER_PREFIX("owner-prefix"),
    PRIME_DOI("prime-doi"),
    OWNER("owner"),
    LAST_UPDATE("last-update"),
    CITEDBY_COUNT("citedby-count"),
    PUBLISHER_NAME("publisher-name"),
    PREFIX_NAME("prefix-name"),
    MEMBER_ID("member-id"),
    CREATED("created"),
    RELATION("relation");
}

enum class CrmItemType(@get:JsonValue val value: String) {
    ELEMENT("element"),
    STRING("string"),
    NUMBER("number"),
    DATE("date"),
    DOI("doi");
}
