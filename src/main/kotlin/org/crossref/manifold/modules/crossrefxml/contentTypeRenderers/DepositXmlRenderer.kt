package org.crossref.manifold.modules.crossrefxml.contentTypeRenderers

import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.crossrefxml.*
import org.crossref.manifold.modules.crossrefxml.xmlelements.*
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.RenderingContext
import org.springframework.stereotype.Component

/**
 * Renders ItemTree into <doi_batch> XML that's suitable for regular deposit.
 */
@Component
class DepositXmlRenderer : XmlRenderer() {
    // This renderer requires proper Context, so don't offer to render if it's missing
    override fun offer(type: String?, subtype: String?, itemTree: ItemTree?, context: RenderingContext?): Boolean {
        return type == "work" && context is DepositContext
    }

    override fun internalContentType(): ContentType = ContentType.DEPOSIT_XML

    override fun validateXmlOutput(xmlString: String) = xmlValidator.validateDepositXml(xmlString)

    override fun finalizeToString(xmlElement: XmlElement, context: RenderingContext?): String {
        val head = contextToHeadDeposit(context)
        val body = when (xmlElement) {
            is BookElement -> BodyDeposit(book = postProcessBook(xmlElement.value))
            is JournalElement -> BodyDeposit(journal = postProcessJournal(xmlElement.value))
        }
        return xmlMapper.writeValueAsString(
            DoiBatch(
                head = head,
                body = body
            )
        )
    }

    fun postProcessBook(book: Book) = book.copy(
        contentItem = book.contentItem.copy(
            contributors = fixContributors(book.contentItem.contributors),
            crossmark = fixCrossmark(book.contentItem.crossmark)
        )
    )

    fun postProcessJournal(journal: Journal) = journal.copy(
        journalArticle = journal.journalArticle?.copy(
            contributors = fixContributors(journal.journalArticle.contributors),
            crossmark = fixCrossmark(journal.journalArticle.crossmark)
        )
    )

    /**
     * <crossref_result> supports both `affiliation` and `affiliations`, but <doi_batch> only supports the latter,
     * so we need to reconstruct it from `affiliation`.
     * */
    private fun fixContributors(contributors: Contributors?): Contributors? {
        val personNames = contributors?.personName
        return if (personNames.isNullOrEmpty()) {
            contributors
        } else {
            val fixedNames = personNames.map {
                if (it.affiliation != null && it.affiliations == null) {
                    it.copy(
                        affiliations = Affiliations(institution = listOf(Institution(institutionName = it.affiliation))),
                        affiliation = null
                    )
                } else it
            }
            contributors.copy(personName = fixedNames)
        }
    }

    /** <crossref_result> requires `label` attribute for the <update>, but <doi_batch> doesn't support it */
    private fun fixCrossmark(crossmark: Crossmark?): Crossmark? {
        val updates = crossmark?.updates
        return if (updates.isNullOrEmpty()) {
            crossmark
        } else {
            val fixedUpdates = updates.map {
                if (it.label != null) {
                    it.copy(label = null)
                } else it
            }
            crossmark.copy(updates = fixedUpdates)
        }
    }

    fun contextToHeadDeposit(context: RenderingContext?): HeadDeposit {
        assert(context is DepositContext)
        val depositContext = context as DepositContext

        return HeadDeposit(
            doiBatchId = depositContext.doiBatchId,
            timestamp = depositContext.timestamp,
            depositor = Depositor(
                depositorName = depositContext.depositorName,
                emailAddress = depositContext.depositorEmail
            ),
            registrant = depositContext.registrant,
        )
    }
}

/**
 * To produce a valid Deposit XML, we need some info that's not available in ItemTree.
 * It should be provided with user's request.
 * */
data class DepositContext(
    val doiBatchId: String,
    val timestamp: Long,
    val depositorName: String,
    val depositorEmail: String,
    val registrant: String,
) : RenderingContext