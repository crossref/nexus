package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder(*["issn","journal_title","author","volume","issue","first_page","elocation_id","cYear", "doi", "isbn","series_title","volume_title","edition_number","component_number","article_title","std_designator","standards_body","unstructured_citation","provider"])
data class Citation (
    @field:JacksonXmlProperty(isAttribute = true)
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val key : String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "issn")
    val issn : Issn? = null,

    @field:JacksonXmlProperty(localName = "journal_title")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val journalTitle : String? = null,


    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "author")
    val author : String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "volume")
    val volume : String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "issue")
    val issue: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "first_page")
    val firstPage: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "elocation_id")
    val elocationId: String? = null,

    /**
     * This field should not be named `cYear` because Kotlin generates `getCYear`, due, to the single lowercase
     * character lower before a capital.
     * Jackson would see these as different, and duplicate the field in the output.
     */
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "cYear")
    val cyear: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "doi")
    val doi: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "isbn")
    val isbn: Isbn? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "series_title")
    val seriesTitle: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "volume_title")
    val volumeTitle: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "edition_number")
    val editionNumber: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "component_number")
    val componentNumber: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "article_title")
    val articleTitle: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "std_designator")
    val stdDesignator: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "standards_body")
    val standardsBody: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    @field:JacksonXmlProperty(localName = "unstructured_citation")
    val unstructuredCitation: String? = null,

    @field:JacksonXmlProperty
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val provider: String? = null
)
