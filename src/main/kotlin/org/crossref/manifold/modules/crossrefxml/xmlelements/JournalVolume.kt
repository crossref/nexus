package org.crossref.manifold.modules.crossrefxml.xmlelements

data class JournalVolume(
    val volume: String? = null
)
