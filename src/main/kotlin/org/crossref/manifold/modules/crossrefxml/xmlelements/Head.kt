package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class Head (
    @field:JacksonXmlProperty(localName = "doi_batch_id")
    val doiBatchId : String? = "none"
)
