package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("title", "subtitle", "original_language_title")
data class Titles(
    @field:JacksonXmlProperty(localName = "title")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val title: String? = null,

    @field:JacksonXmlProperty(localName = "subtitle")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val subtitle: String? = null,

    @field:JacksonXmlProperty(localName = "original_language_title")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val originalLanguageTitle: String? = null,
)