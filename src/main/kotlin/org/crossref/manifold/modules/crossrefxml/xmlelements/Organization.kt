package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

@JsonPropertyOrder(*["sequence", "contributor_role", "value"])
data class Organization(

    @field:JacksonXmlProperty(isAttribute = true, localName = "sequence")
    val sequence: String? = null,

    @field:JacksonXmlProperty(isAttribute = true, localName = "contributor_role")
    val contributorRole: String? = null,


    @field:JacksonXmlText
    val value: String? = null
)
