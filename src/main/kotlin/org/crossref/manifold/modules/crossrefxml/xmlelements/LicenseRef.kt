package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

@JsonPropertyOrder(*["applies_to", "start_date", "value" ])
data class LicenseRef(
    @field:JacksonXmlProperty(localName = "applies_to", isAttribute = true)
    val appliesTo: String?,

    @field:JacksonXmlProperty(localName = "start_date", isAttribute = true)
    val startDate: String?,

    @field:JacksonXmlText
    val value: String?
)
