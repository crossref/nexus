package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText

data class ItemNumber (
    @field:JacksonXmlProperty(localName = "item_number_type", isAttribute = true)
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val itemNumberType : String? = null,

    @field:JacksonXmlText
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val value : String?
)
