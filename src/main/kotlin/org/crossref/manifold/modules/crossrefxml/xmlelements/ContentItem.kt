package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder(
    "contributors", "titles", "jats:abstract", "component_number", "publication_date",
    "acceptance_date", "pages", "publisher_item", "crossmark",
    "fr:program", "ai:program", "ct:program", "archive_locations", "doi_data", "citation_list",
)
data class ContentItem(
    @field:JacksonXmlProperty(isAttribute = true)
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val language: String? = null,

    @field:JacksonXmlProperty(isAttribute = true, localName = "component_type")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val componentType: ComponentType? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val contributors: Contributors? = null,

    val titles: Titles,

    @field:JacksonXmlProperty(localName = "jats:abstract")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val abstract: List<Abstract>?,

    @field:JacksonXmlProperty(localName = "component_number")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val componentNumber: String? = null,

    @field:JacksonXmlProperty(localName = "publication_date")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val publicationDate: List<PublicationDate>,

    @field:JacksonXmlProperty(localName = "acceptance_date")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val acceptanceDate: AcceptanceDate? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val pages: Pages? = null,

    @field:JacksonXmlProperty(localName = "publisher_item")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val publisherItem: PublisherItem? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val crossmark: Crossmark? = null,

    @field:JacksonXmlProperty(localName = "fr:program")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val frProgram: FundrefProgram? = null,

    @field:JacksonXmlProperty(localName = "ai:program")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val aiProgram: AccessIndicatorsProgram? = null,

    @field:JacksonXmlProperty(localName = "archive_locations")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val archiveLocations: ArchiveLocations?,

    @field:JacksonXmlProperty(localName = "doi_data")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val doiData: DoiData?,

    @field:JacksonXmlProperty(localName = "citation")
    @field:JacksonXmlElementWrapper(localName = "citation_list")
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val citationList: List<Citation>
)

enum class ComponentType(@get:JsonValue val value: String) {
    CHAPTER("chapter"),
    SECTION("section"),
    PART("part"),
    TRACK("track"),
    REFERENCE_ENTRY("reference_entry"),
    OTHER("other");
}