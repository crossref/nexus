package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder(*["person_name", "organization"])
data class Contributors(
    @field:JacksonXmlProperty(localName = "person_name")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    val personName: List<PersonName>? = null,

    @field:JacksonXmlProperty(localName = "organization")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    val organization: List<Organization>? = null,
)


