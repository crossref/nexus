package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("clinical-trial-number")
data class CtnProgram(
    @field:JacksonXmlProperty(isAttribute = true, localName = "xmlns")
    val xmlns: String = "http://www.crossref.org/clinicaltrials.xsd",

    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JacksonXmlProperty(localName = "clinical-trial-number")
    val clinicalTrialNumber: List<ClinicalTrialNumber> = emptyList()

) : Program
