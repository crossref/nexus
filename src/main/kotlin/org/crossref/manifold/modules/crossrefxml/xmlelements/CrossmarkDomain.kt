package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

data class CrossmarkDomain (
    @field:JacksonXmlProperty(localName = "crossmark_domain")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val domain : Domain
)
