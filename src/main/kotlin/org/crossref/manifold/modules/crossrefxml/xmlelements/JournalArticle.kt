package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder(*["titles", "contributors", "jats:abstract", "publication_date", "pages", "publisher_item", "crossmark", "archive_locations", "doi_data", "citation_list"])
data class JournalArticle(
    @field:JacksonXmlProperty(isAttribute = true)
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val language: String? = null,

    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    @field:JacksonXmlProperty(localName="title")
    @field:JacksonXmlElementWrapper(localName = "titles")
    val titles: List<String?>? = null,

    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val contributors: Contributors? = null,

    @field:JacksonXmlProperty(localName = "jats:abstract")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val abstract: List<Abstract>? = null,

    @field:JacksonXmlProperty(localName = "publication_date")
    @field:JacksonXmlElementWrapper(useWrapping = false)
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val publicationDate: List<PublicationDate>? = null,

    @field:JacksonXmlProperty(localName = "pages")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val pages: Pages? = null,

    @field:JacksonXmlProperty(localName = "publisher_item")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val publisherItem : PublisherItem? = null,

    @field:JacksonXmlProperty(localName = "crossmark")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val crossmark : Crossmark? = null,

    @field:JacksonXmlProperty(localName = "archive_locations")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val archiveLocations : ArchiveLocations? = null,

    @field:JacksonXmlProperty(localName = "doi_data")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val doiData : DoiData? = null,

    @field:JacksonXmlProperty(localName = "citation")
    @field:JacksonXmlElementWrapper(localName = "citation_list")
    @field:JsonInclude(JsonInclude.Include.NON_EMPTY)
    val citationList : List<Citation>

)
