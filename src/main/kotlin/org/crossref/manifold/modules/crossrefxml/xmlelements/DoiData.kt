package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder

@JsonPropertyOrder(*["doi", "resource"])
data class DoiData(
    val doi: String,
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val resource: String? = null,
)
