package org.crossref.manifold.modules.crossrefxml.xmlelements

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty

@JsonPropertyOrder("book_metadata", "content_item")
data class Book(
    @field:JacksonXmlProperty(localName = "book_metadata")
    @field:JsonInclude(JsonInclude.Include.NON_NULL)
    val bookMetadata: BookMetadata? = null,

    @field:JacksonXmlProperty(isAttribute = true, localName = "book_type")
    val bookType: BookType?,

    @field:JacksonXmlProperty(localName = "content_item")
    val contentItem: ContentItem,
)

enum class BookType(@get:JsonValue val value: String) {
    EDITED_BOOK("edited_book"),
    MONOGRAPH("monograph"),
    REFERENCE("reference"),
    OTHER("other");
}