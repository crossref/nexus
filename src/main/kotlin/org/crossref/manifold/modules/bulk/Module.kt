package org.crossref.manifold.modules.bulk

import org.crossref.manifold.bulk.BulkIngestionService
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.crossref.manifold.modules.StartupTask
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import kotlin.io.path.Path
import kotlin.io.path.isDirectory

@Configuration(Module.BULK_INGESTER)
class Module(moduleRegistrar: ModuleRegistrar) {
    companion object {
        const val BULK_INGESTER = "bulk-ingest"
        const val PATH = "path"
    }

    init {
        moduleRegistrar.register(Manifest(BULK_INGESTER, "Ingest an envelope batch bulk file. "))
    }

    private var logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Bean
    @ConditionalOnProperty(prefix = BULK_INGESTER, name = [PATH])
    fun bulkFileIngester(
        bulkIngestionService: BulkIngestionService,
        @Value("\${${BULK_INGESTER}.${PATH}}") src: String
    ): StartupTask = StartupTask({

        logger.info("Initiating bulk ingestion of $src")

        val srcPath = Path(src).toAbsolutePath()

        if (srcPath.isDirectory()) {

            val files = srcPath.toFile().listFiles()
                ?.filter { it.isFile && it.extension == "gz" }

            logger.info("Total files to process: ${files?.size}")

            files?.let { bulkIngestionService.ingest(it) }
        } else {
            bulkIngestionService.ingest(listOf(srcPath.toFile()))
        }

        logger.info("Finished bulk ingestion of $src")
    })
}
