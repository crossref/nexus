package org.crossref.manifold.modules.envelopebatch

import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.envelopebatch.Module.Companion.ENVELOPE_BATCH
import org.crossref.manifold.modules.envelopebatch.Module.Companion.JSON_QUEUE
import org.crossref.messaging.aws.sqs.SqsListener

class EnvelopeBatchJsonMessageIngester(
    private val itemGraph: ItemGraph
) {
    @SqsListener("$ENVELOPE_BATCH.$JSON_QUEUE")
    fun ingestEnvelopeBatches(envelopeBatches: Collection<EnvelopeBatch>) {
        itemGraph.ingest(envelopeBatches)
    }
}
