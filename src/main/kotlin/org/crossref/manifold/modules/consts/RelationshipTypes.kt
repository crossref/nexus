package org.crossref.manifold.modules.consts

/**
 * Collection of Relationship type constants. These correspond to the content of relationships.schema.json and are
 * included to support Module authors in using a shared vocabulary.
 */
object RelationshipTypes {
    /**
     * Records that the Object Item is the maintainer of the Subject.
     */
    const val STEWARD = "steward"

    const val MEMBER_OF = "member-of"

    const val RESOURCE_RESOLUTION = "resource-resolution"

    const val SPONSOR = "sponsor"

    const val COLLECTION = "collection"

    const val SAME_AS = "same-as"
}

