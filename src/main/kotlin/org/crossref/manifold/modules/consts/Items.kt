package org.crossref.manifold.modules.consts

/** Collection of Items (by their Item Identifier) that we need to exist.
 * Supplied to help Module authors to use shared vocabulary.
 */
object Items {
    /**
     * The Item that represents Crossref.
     * Used as the root of authority assertions for Crossref members, and for assertions that Crossref makes.
     */
    const val CROSSREF_AUTHORITY = "https://ror.org/02twcfp32"
    const val DATACITE_AUTHORITY = "https://ror.org/04wxnsj81"
    const val DOI_FOUNDATION = "https://doi.org"
}

