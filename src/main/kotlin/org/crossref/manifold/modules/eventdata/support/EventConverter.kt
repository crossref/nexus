package org.crossref.manifold.modules.eventdata.support

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.lookup.InMemoryRaLookUp
import java.time.OffsetDateTime

private val inMemoryRaLookUp = InMemoryRaLookUp()

fun parseDataciteEvent(event: Event) =
    runBlocking { async(Dispatchers.IO) { Datacite.itemTreeFrom(event, inMemoryRaLookUp) }.await() }

fun parseEvent(event: Event): ItemTree? {

    val subjectId: Identifier? = listOfNotNull(event.subj?.pid, event.subjId).firstOrNull()
    val objectId: Identifier? = listOfNotNull(event.obj?.pid, event.objId).firstOrNull()

    if (subjectId == null || objectId == null) {
        return null
    }

    val objItem = Item(identifiers = listOf(objectId))

    return ItemTree(
        Item(
            listOf(subjectId),
            rels = listOf(Relationship(event.relationTypeId, objItem, assertedAt = null)),
        )
    )
}

/**
 * Convert an object to an Item
 */
fun Event.toItemTree(): ItemTree? =
    when (sourceId) {
        Constants.DATACITE -> parseDataciteEvent(this)
        else -> parseEvent(this)
    }


/**
 * Convert an event to an envelope batch
 */
fun parseEventToEnvelopeBatch(
    event: Event
): EnvelopeBatch? =
    event.toItemTree()?.let { item ->

        val itemTreeAssertion = ItemTreeAssertion(
            assertedAt = OffsetDateTime.parse(event.occurredAt),
            assertingParty = Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY))
        )

        val envelope = Envelope(
            listOf(item),
            itemTreeAssertion
        )

        EnvelopeBatch(
            listOf(envelope),
            EnvelopeBatchProvenance(UserAgentParser.parse("Crossref ${event.sourceToken}"), event.id)
        )
    }