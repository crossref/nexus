package org.crossref.manifold.modules.eventdata.support

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemtree.Identifier

object Constants {
    const val WIKIPEDIA = "wikipedia"
    const val REDDIT = "reddit"
    const val REDDIT_LINKS = "reddit-links"
    const val STACK_EXCHANGE = "stackexchange"
    const val NEWSFEED = "newsfeed"
    const val WORDPRESS_DOT_COM = "wordpressdotcom"
    const val HYPOTHESIS = "hypothesis"
    const val DATACITE = "datacite"
    const val CROSSREF = "crossref"
}

class IdentifierDeserializer : JsonDeserializer<Identifier>() {
    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): Identifier? {
        val value = p?.valueAsString
        return if (!value.isNullOrBlank()) {
            IdentifierParser.parse(value)
        } else {
            null
        }
    }
}

data class Event(
    val id: String,

    @JsonProperty("source_id")
    val sourceId: String,

    @JsonProperty("subj_id")
    @JsonDeserialize(using = IdentifierDeserializer::class)
    val subjId: Identifier,

    @JsonProperty("obj_id")
    @JsonDeserialize(using = IdentifierDeserializer::class)
    val objId: Identifier,

    @JsonProperty("source_token")
    val sourceToken: String,

    @JsonProperty("occurred_at")
    val occurredAt: String,

    @JsonProperty("evidence_record")
    val evidenceRecord: String?,

    val action: String?,

    val subj: EventSubj?,

    val obj: Obj?,

    @JsonProperty("relation_type_id")
    val relationTypeId: String,
)

data class Author(val url: String)

data class EvidenceRecord(
    val pages: List<Page>,
    @JsonProperty("source-id")
    val sourceId: String,
    val timestamp: String,
)

data class Page(val actions: List<Action> = emptyList())

data class Action(
    val id: String?,
    @JsonProperty("occurred-at")
    val occurredAt: String,
    val matches: List<Match>,
    val url: String,
    val subj: ActionSubject?,
    @JsonProperty("relation-type-id")
    val relationTypeId: String,
    val events: List<Event>
)

data class EventSubj(
    @JsonDeserialize(using = IdentifierDeserializer::class)
    val pid: Identifier?,
    val url: String?,
    val type: String?,
    val title: String?,
    val issued: String?,
    val author: Author?,
    val alternativeId: String?,
    @JsonProperty("work_type_id") val workTypeId: String?
)

data class Obj(
    @JsonDeserialize(using = IdentifierDeserializer::class)
    val pid: Identifier?,
    val url: String?,
    val method: String?,
    val verification: String?,
)

data class Match(
    val type: String,
    val value: String,
    val match: String,
    val method: String?,
    val verification: String?,
)

data class ActionSubject(
    val type: String? = null,
    val title: String? = null,
    val issued: String? = null,
    val author: Author? = null,
    val url: String? = null,
    @JsonProperty("json-url")
    val jsonUrl: String? = null
)

data class LegacyDataciteEvent(
    val id: String,
    @JsonProperty("subj_id")
    @JsonDeserialize(using = IdentifierDeserializer::class)
    val subjId: Identifier,
    @JsonProperty("obj_id")
    @JsonDeserialize(using = IdentifierDeserializer::class)
    val objId: Identifier,
    @JsonProperty("relation_type_id")
    val relationTypeId: String,
    @JsonProperty("occurred_at")
    val occurredAt: String,
    val subj: LegacyDataciteEventSubj?
)

data class LegacyDataciteEventSubj(
    @JsonProperty("@type") val atType: String? = null,
    @JsonProperty("work_type_id") val workTypeId: String? = null
)