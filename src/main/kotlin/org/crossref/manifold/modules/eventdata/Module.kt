package org.crossref.manifold.modules.eventdata

import io.micrometer.core.instrument.MeterRegistry
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.crossref.manifold.modules.StartupTask
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.services.s3.S3Client
import kotlin.system.exitProcess

@Configuration(Module.EVENT_DATA)
class Module(moduleRegistrar: ModuleRegistrar) {
    companion object {
        const val EVENT_DATA = "event-data"
        const val INGEST_JSON_SNAPSHOT = "ingest-json-snapshot"
        const val SOURCES_ALLOW_LIST = "sources-allow-list"
        const val BUS_CONTROLLER = "bus-controller"
        const val JWT_SECRETS = "jwt-secrets"
        const val AGENTS_ALLOW_LIST = "agents-allow-list"
        const val CONVERT_RECORDS_SRC = "convert-records-src"
        const val CONVERT_RECORDS_DST = "convert-records-dst"
        const val CONVERT_DATACITE_SRC = "convert-datacite-src"
        const val CONVERT_DATACITE_DST = "convert-datacite-dst"
        const val S3_EVENTS_QUEUE = "s3-events-queue"
        const val S3_EVIDENCE_QUEUE = "s3-evidence-queue"
    }

    init {
        moduleRegistrar.register(
            Manifest(
                name = EVENT_DATA,
                description = "Crossref Event Data events"
            )
        )
    }

    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [INGEST_JSON_SNAPSHOT])
    fun jsonSnapshotIngester(
        meterRegistry: MeterRegistry?,
        itemGraph: ItemGraph,
        @Value("\${$EVENT_DATA.$INGEST_JSON_SNAPSHOT}") jsonSnapshotFilename: String,
        @Value("\${$EVENT_DATA.$SOURCES_ALLOW_LIST:}") sourceAllowlist: Set<String>
    ): StartupTask = StartupTask({
        EventSnapshotIngester.ingestJsonSnapshot(
            meterRegistry,
            itemGraph,
            jsonSnapshotFilename,
            sourceAllowlist
        )
    }, {
        EventSnapshotIngester.destroy()
    })

    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [BUS_CONTROLLER])
    fun busController(
        meterRegistry: MeterRegistry?,
        itemGraph: ItemGraph,
        @Value("\${$EVENT_DATA.$JWT_SECRETS:}") jwtSecrets: Set<String>,
        @Value("\${$EVENT_DATA.$SOURCES_ALLOW_LIST:}") sourcesAllowlist: Set<String>
    ): BusController = BusController(
        meterRegistry,
        itemGraph,
        jwtSecrets,
        sourcesAllowlist
    )

    /**
     * An ingester for Event Data evidence records.
     * The ingester subscribes to an SQS queue awaiting S3 published notifications.
     */
    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [S3_EVIDENCE_QUEUE])
    fun s3EvidenceRecordIngester(
        itemGraph: ItemGraph,
        s3Client: S3Client,
        @Value("\${$EVENT_DATA.$AGENTS_ALLOW_LIST}") agentsAllowList: Array<String>
    ): S3EvidenceRecordIngester = S3EvidenceRecordIngester(
        itemGraph,
        s3Client,
        agentsAllowList.toSet()
    )

    /**
     * An ingester for Event Data event records.
     * The ingester subscribes to an SQS queue awaiting S3 published notifications.
     */
    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [S3_EVENTS_QUEUE])
    fun s3EventsIngester(
        itemGraph: ItemGraph,
        s3Client: S3Client,
        @Value("\${$EVENT_DATA.$AGENTS_ALLOW_LIST}") agentsAllowList: Array<String>
    ): S3EventsIngester = S3EventsIngester(
        itemGraph,
        s3Client,
        agentsAllowList.toSet()
    )

    /**
     * Read one or many evidence record archives from src and produce item tree archives
     * stored at dst.
     *
     * @param src Absolute path to file or directory
     * @param dst Absolute path to directory
     */
    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [CONVERT_RECORDS_SRC])
    fun evidenceRecordSnapshotConverter(
        @Value("\${$EVENT_DATA.$CONVERT_RECORDS_SRC}") src: String,
        @Value("\${$EVENT_DATA.$CONVERT_RECORDS_DST}") dst: String,
        @Value("\${$EVENT_DATA.$AGENTS_ALLOW_LIST}") agentsAllowList: Array<String>
    ): StartupTask = StartupTask({
        EvidenceRecordSnapshotConverter.run(src, dst, agentsAllowList.toSet())
    })

    /**
     * Read one or many evidence record archives from src and produce item tree archives
     * stored at dst.
     *
     * @param src Absolute path to file or directory
     * @param dst Absolute path to directory
     */
    @Bean
    @ConditionalOnProperty(prefix = EVENT_DATA, name = [CONVERT_DATACITE_SRC])
    fun dataciteSnapshotConverter(
        @Value("\${$EVENT_DATA.$CONVERT_DATACITE_SRC}") src: String,
        @Value("\${$EVENT_DATA.$CONVERT_DATACITE_DST}") dst: String
    ): StartupTask = StartupTask({
        DataciteSnapshotConverter.run(src, dst)
        exitProcess(0)
    })
}
