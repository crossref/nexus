package org.crossref.manifold.modules.eventdata.support

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.*
import org.crossref.manifold.modules.consts.Items
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.OffsetDateTime

/**
 * This package contains a collection of functions dedicated to the mapping
 * of Event Data evidence records to manifold Item Trees.
 *
 * The code is highly domain specific, containing assumptions and specificities
 * that originate from Eventdata.
 *
 * The evidence records are produced by the Event Data Percolator and they are stored
 * in JSON format. The main path to reach the object of an event is page -> action -> match.
 *
 * In Event Data, observations made by the various agents are attempted to be matched to DOIs.
 * A successful match produces an event. An action contains both the observations and
 * the matches as well as the resulting events. As the events copy their information from the observations
 * and the matches we can ignore them during the mapping and gather all needed data from the former entities.
 */

private const val RELATIONSHIP_OBJECT = "object"
private const val RELATIONSHIP_SOURCE = "source"

private val assertingParty = Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY))

object EvidenceRecordConverter {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)
}

/**
 * Get the subj_id from the first available [Event] or default to the [Action] url otherwise.
 */
private fun getActionId(action: Action): String = if (action.events.isEmpty()) {
    action.url
} else {
    val id = action.events[0].subjId.uri.toASCIIString()

    if (id.isNullOrBlank()) {
        action.url
    } else {
        id
    }
}


/**
 * Creates a map of properties for the subject of the reified relationship.
 *
 * The properties map needs to match a certain item type.
 * In Eventdata the subject is always a url, therefore the type is 'web-resource'.
 * The subtype is 'annotation' or 'web-content' depending on the agent's relationship type.
 *
 * Further properties are included if present and depending on the agent.
 * The item type and subtype are always included to the map.
 */
fun getSubjectProperties(action: Action, sourceId: String): Map<String, String> {

    val m = mutableMapOf<String, String>()

    m["type"] = "web-resource"

    m["subtype"] = when (sourceId) {
        Constants.HYPOTHESIS -> if (action.relationTypeId == "annotates") "annotation" else "web-content"
        else -> "web-content"
    }

    action.subj?.issued?.let { m["issued"] = it }

    if (sourceId != Constants.HYPOTHESIS) {
        action.subj?.title?.let { m["title"] = it }
    }

    if (sourceId == Constants.STACK_EXCHANGE) {
        action.subj?.author?.url?.let { m["username"] = it }
    }

    return m
}

/**
 * Creates a map of properties for the "observation" part of the reified relationship.
 *
 * The properties map needs to match a certain item type.
 * For Eventdata the intermediate object of a reified relationship always points to an
 * already known or possibly existing DOI. Therefore, the type of the item is 'reference'
 * and the subtype 'work'.
 *
 * The 'text' property is mandatory.
 * It holds the value of the observation in the form of unstructured text.
 */
fun getObservationProperties(match: Match): Map<String, String> {

    val m = mutableMapOf<String, String>()

    m["type"] = "reference"

    m["subtype"] = "work"

    m["text"] = match.value

    return m
}

/**
 * Collapse all envelopes into a single one.
 * The produced envelope will contain all original relationships collapsed into a single item tree.
 */
private fun collapseDuplicates(identifier: Identifier, envelopes: List<Envelope>): Envelope {

    // Grab all relationships from within the existing item trees
    val allRelationships = envelopes.flatMap { it.itemTrees }.flatMap { itemTree -> itemTree.root.rels }

    val properties = envelopes.sortedBy { it.assertion.assertedAt }.first().itemTrees.first().root.properties

    val root = Item()
        .withIdentifier(identifier)
        .withRelationships(allRelationships)
        .withProperties(properties)

    // Return a new envelope using the assertion of the first envelope
    return Envelope(listOf(ItemTree(root)), envelopes.first().assertion)
}

/**
 * Convert a list of actions using the provided function.
 * Most importantly, in the end merge any ambiguous item trees.
 */
private fun List<Action>.convertUsing(sourceId: String, filename: String, fn: (Action) -> Envelope): List<Envelope> =
    this
        .mapNotNull { action ->
            try {
                fn(action)
            } catch (e: Exception) {
                EvidenceRecordConverter.logger.error("Exception while processing $sourceId action ${action.id} in evidence record $filename")
                e.printStackTrace()
                null
            }
        }
        .filter { envelope -> envelope.itemTrees.isNotEmpty() }

/**
 * Converts actions to envelopes.
 * For more details on the returned results see [envelopeFrom].
 */
private fun envelopesFrom(
    actions: List<Action>,
    sourceId: String,
    filename: String,
): List<Envelope> =
    actions.convertUsing(sourceId, filename) { action ->
        envelopeFrom(
            subj = getActionId(action),
            relType = action.relationTypeId,
            observedAt = action.occurredAt,
            matches = action.matches,
            subjectProperties = getSubjectProperties(action, sourceId),
        )
    }

/**
 * Creates one or more reified relationships between a subject and one or more objects.
 *
 * The importance of this function is to guarantee the structure of the reified
 * model. We want to create item trees with a structure of:
 *
 *     subject -reference-> referenceItem -object-> work
 *
 * In event data agents make observations, where a URL subject observes a possible mention
 * to a DOI on a certain resource on the internet, for example Wikipedia, blogs etc.
 *
 * The observations are verified by the Event Data Percolator, that either confirms discovered DOIs
 * or attempts to discover possible ones based on the observation. The matches are used to construct
 * events. The event represents the relationship between the subject and each of the discovered DOIs.
 * The discovered DOIs are the object of this relationship.
 */
private fun envelopeFrom(
    subj: String,
    relType: String,
    observedAt: String,
    matches: List<Match>,
    subjectProperties: Map<String, String?> = emptyMap(),
    additionalObservationRelationships: List<Relationship> = emptyList(),
): Envelope {
    // An Eventdata relationship is an observation made by an agent matched to a target DOI.
    // The relationship type depends on the agent that made the observation.
    // The subject is an observation with properties and the target is a DOI without any properties.
    // The actual match of the observation to the DOI is performed by the percolator but this is something
    // we will not be documenting in the item tree.

    val relationshipsFromMatches = matches
        .groupBy { it.match }.values
        .map { matchList ->

            // We may have relationships where the only distinction between them is
            // the properties of the reified object. In this case we will attach their properties
            // to a single relationship.

            val properties = matchList.map { match -> propertiesFromSimpleMap(getObservationProperties(match)) }

            Relationship(
                relTyp = relType,
                obj = Item().withRelationships(
                    additionalObservationRelationships + Relationship(
                        relTyp = RELATIONSHIP_OBJECT,
                        obj = Item()
                            .withIdentifier(IdentifierParser.parse(matchList.first().match)),
                        assertedAt = null
                    )
                ).withProperties(properties)
            )
        }

    val itemTrees = Item()
        .withIdentifier(IdentifierParser.parse(subj))
        .withPropertiesFromMap(subjectProperties)
        .withRelationships(relationshipsFromMatches)
        .let { listOf(ItemTree(it)) }

    return Envelope(
        itemTrees,
        ItemTreeAssertion(
            assertedAt = OffsetDateTime.parse(observedAt),
            assertingParty = assertingParty
        )
    )
}

/**
 * Converts a list of Wikipedia actions to a list of envelopes.
 * Takes care of attaching a 'source' relationship to the edit url of the wikipedia page.
 *
 * @see [envelopeFrom]
 */
private fun toWikipediaEnvelopes(actions: List<Action>, filename: String): List<Envelope> {

    val (valid, rest) = actions.filter { action -> action.matches.isNotEmpty() }
        .partition { action -> !action.subj?.url.isNullOrBlank() }

    rest.forEach {
        EvidenceRecordConverter.logger.error("Missing attribute action.subj.url for ${Constants.WIKIPEDIA} action ${it.id} in evidence record $filename")
    }

    return valid.convertUsing(Constants.WIKIPEDIA, filename) { action ->

        // This is an item holding the edit URL for the current Wikipedia page.
        // We filter out the tittle because it is not needed in this instance
        // (it will be included in the subject properties).
        val additionalObservationRelationships = if (action.subj == null || action.subj.url.isNullOrBlank()) {
            emptyList()
        } else {
            listOf(
                Relationship(
                    relTyp = RELATIONSHIP_SOURCE,
                    Item(listOf(IdentifierParser.parse(action.subj.url)))
                        .withPropertiesFromMap(
                            getSubjectProperties(action, Constants.WIKIPEDIA)
                                .minus("title")
                        ),
                    assertedAt = null
                )
            )
        }

        envelopeFrom(
            subj = getActionId(action),
            relType = action.relationTypeId,
            observedAt = action.occurredAt,
            matches = action.matches,
            subjectProperties = getSubjectProperties(action, Constants.WIKIPEDIA),
            additionalObservationRelationships = additionalObservationRelationships,
        )
    }
}

/**
 * Converts a list of hypothes.is actions to a list of envelopes.
 * Takes care of attaching a 'source' relationship to the json url for the hypothes.is pid.
 *
 * @see [envelopeFrom]
 */
private fun toHypothesisEnvelopes(actions: List<Action>, filename: String): List<Envelope> {

    val (valid, rest) = actions.filter { action -> action.matches.isNotEmpty() }
        .partition { action -> !action.subj?.jsonUrl.isNullOrBlank() }

    rest.forEach {
        EvidenceRecordConverter.logger.error("Missing attribute action.subj.jsonUrl for $Constants.HYPOTHESIS action ${it.id} in evidence record $filename")
    }

    return valid.convertUsing(Constants.HYPOTHESIS, filename) { action ->

        // Represent the json URL as a source relationship.
        val additionalObservationRelationships = if (action.subj == null || action.subj.jsonUrl.isNullOrBlank()) {
            emptyList()
        } else {
            listOf(
                Relationship(
                    relTyp = RELATIONSHIP_SOURCE,
                    Item(listOf(IdentifierParser.parse(action.subj.jsonUrl)))
                        .withPropertiesFromMap(
                            getSubjectProperties(action, Constants.HYPOTHESIS)
                                .plus("subtype" to "web-content")
                        ),
                    assertedAt = null
                )
            )
        }

        envelopeFrom(
            subj = getActionId(action),
            relType = action.relationTypeId,
            observedAt = action.occurredAt,
            matches = action.matches,
            subjectProperties = getSubjectProperties(action, Constants.HYPOTHESIS),
            additionalObservationRelationships = additionalObservationRelationships,
        )
    }
}


/**
 * Dispatch processing to the appropriate extension.
 *
 * @see [toHypothesisEnvelopes], [toWikipediaEnvelopes], [envelopesFrom]
 */
private fun convert(
    actions: List<Action>,
    sourceId: String,
    filename: String,
): List<Envelope> =
    when (sourceId) {
        Constants.REDDIT,
        Constants.REDDIT_LINKS,
        Constants.STACK_EXCHANGE,
        Constants.NEWSFEED,
        Constants.WORDPRESS_DOT_COM -> envelopesFrom(actions, sourceId, filename)

        Constants.HYPOTHESIS -> toHypothesisEnvelopes(actions, filename)
        Constants.WIKIPEDIA -> toWikipediaEnvelopes(actions, filename)
        else -> {
            EvidenceRecordConverter.logger.warn("Skipping $filename due to unknown sourceId: $sourceId")
            emptyList()
        }
    }

/**
 * Accepts an Eventdata [EvidenceRecord] object and converts it into a List<EnvelopeBatch>
 * containing a single [EnvelopeBatch].
 *
 * @see [convert]
 */
fun convertEvidenceRecordToEnvelopeBatch(
    filename: String,
    evidenceRecord: EvidenceRecord,
): List<EnvelopeBatch> {


    // 1. Evidence records may contain multiple pages, extract all actions from them
    // 2. Filter out any actions without matches
    // 3. Convert actions to envelopes
    // 4. Group envelopes by root uri to identify duplicates
    //      - based on the current implementation each envelope contains a single item tree
    // 5. Collapse any duplicates
    val envelopes = evidenceRecord.pages
        .flatMap { page -> page.actions }
        .filter { action -> action.matches.isNotEmpty() }
        .let { actions -> convert(actions, evidenceRecord.sourceId, filename) }
        .groupBy { envelope -> envelope.itemTrees.first().root.identifiers.first() }
        .map { (identifier, envelopes) -> collapseDuplicates(identifier, envelopes)}

    val envelopeBatch = envelopes
        .filter { envelope -> envelope.itemTrees.isNotEmpty() }
        .let { envs ->
            if (envs.isEmpty()) {
                emptyList()
            } else {
                EnvelopeBatch(
                    envs,
                    EnvelopeBatchProvenance(
                        UserAgentParser.parse("Crossref ${evidenceRecord.sourceId} agent/1.0"),
                        filename
                    )
                ).let { listOf(it) }
            }
        }

    return envelopeBatch
}