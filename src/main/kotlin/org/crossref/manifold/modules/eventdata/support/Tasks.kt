package org.crossref.manifold.modules.eventdata.support

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.micrometer.core.instrument.MeterRegistry
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import org.crossref.manifold.itemgraph.ItemGraph
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.InputStreamReader
import java.util.concurrent.atomic.AtomicInteger


const val PARSE_PARALLELISM = 20

class EventData

var logger: Logger = LoggerFactory.getLogger(EventData::class.java)

/**
 * Validate event and send to channel if valid, ignore otherwise.
 */
suspend fun readEntryToChannel(event: Event, channel: Channel<Event>, allowList: Set<String>) {
    try {
        if (allowList.contains(event.sourceId)) {
            channel.send(event)
        }
    } catch (e: Exception) {
        logger.warn("Failed: $e")
    }
}

/**
 * If path exists and is a directory return all .tar.gz files in it, return an emptySequence() otherwise.
 */
fun filesInPath(path: String): Sequence<File> = File(path).let {
    if (it.exists() && it.isDirectory) {
        it.walk()
            .filter { file -> file.name.endsWith(".tar.gz") }
    } else {
        logger.error("The provided path is not a directory")
        emptySequence()
    }
}


/**
 * In a given directory, read all files ending in .tar.gz.
 * Assuming the archives contain only JSON files try to stream each one of them
 * and convert their entries to Event Data events. The extraction will continue despite any exceptions
 * for individual archives or files contained within them.
 */
suspend fun extractEventsToChannel(path: String, channel: Channel<Event>, allowList: Set<String>) {
    try {

        val mapper = ObjectMapper()
            .registerModule(JavaTimeModule())
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .registerModule(KotlinModule())

        filesInPath(path).forEach { file ->
            try {
                file.inputStream().use { reader ->
                    val gz = GzipCompressorInputStream(reader)
                    val input = TarArchiveInputStream(gz)
                    if (input.nextTarEntry != null) {
                        InputStreamReader(input).use { entryReader ->
                            val jFactory = JsonFactory()
                            val jParser: JsonParser = jFactory.createParser(entryReader)

                            // Check the first token
                            if (jParser.nextToken() != JsonToken.START_ARRAY) {
                                throw IllegalStateException("Expected content to be an array")
                            }

                            // Iterate over the tokens until the end of the array
                            while (jParser.nextToken() != JsonToken.END_ARRAY) {
                                val event = mapper.readValue(jParser, Event::class.java)
                                readEntryToChannel(event, channel, allowList)
                            }
                            logger.info("Finished reading JSON .tar.tz")
                        }
                    }
                }
            } catch (e: Exception) {
                logger.warn("Error! $e")
                e.printStackTrace()
            }
        }
    } catch (e: Exception) {
        logger.warn("Error! $e")
        e.printStackTrace()
    } finally {
        channel.close()
    }
}

suspend fun ingestEventFromChannel(
    routineId: Int,
    eventChannel: Channel<Event>,
    itemGraph: ItemGraph,
    itemTreesParsed: AtomicInteger,
    eventsRead: AtomicInteger,
    eventsProcessed: TaggedCounter?
) {
    logger.info("ingestEventsFromChannel... $routineId")
    for (event in eventChannel) {
        try {
            val envelopeBatch = parseEventToEnvelopeBatch(event)

            if (envelopeBatch != null) {
                logger.debug("Item count ${itemTreesParsed.incrementAndGet()}")
                logger.info("EVENT ${envelopeBatch.provenance.externalTrace}")
                itemGraph.ingest(listOf(envelopeBatch))
            }
        } catch (e: Exception) {
            logger.warn("Error! $e")
            e.printStackTrace()
        }

        eventsProcessed?.increment(event.sourceId)
        eventsRead.incrementAndGet()
    }
    logger.info("ingestEventFromChannel... $routineId")
}

fun scanDirectory(
    pathToDirectory: String,
    itemGraph: ItemGraph,
    allowList: Set<String>,
    meterRegistry: MeterRegistry?
) {
    val eventChannel = Channel<Event>(PARSE_PARALLELISM)

    val eventsProcessed: TaggedCounter? = meterRegistry?.run {
        TaggedCounter("manifold.ingest.events.processed", "source", this)
    }

    val startTime = System.currentTimeMillis()

    // Number of events read
    val eventsRead = AtomicInteger()

    // Number of Item Trees parsed
    val itemTreesParsed = AtomicInteger()

    runBlocking {

        val gzTask = launch { extractEventsToChannel(pathToDirectory, eventChannel, allowList) }

        val parseTasks = (0..PARSE_PARALLELISM).map<Int, Job> {
            launch(CoroutineName("parse-events-$it") + Dispatchers.IO) {
                logger.info("Start Event ingester on ${Thread.currentThread().name}")
                ingestEventFromChannel(
                    it,
                    eventChannel,
                    itemGraph,
                    itemTreesParsed,
                    eventsRead,
                    eventsProcessed
                )
            }
        }

        parseTasks.forEach { it.join() }
        gzTask.join()
        val duration = (System.currentTimeMillis() - startTime) / 1000
        logger.info("Took $duration seconds.")
    }
}
