package org.crossref.manifold.modules.eventdata

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.eventdata.Module.Companion.EVENT_DATA
import org.crossref.manifold.modules.eventdata.Module.Companion.S3_EVENTS_QUEUE
import org.crossref.manifold.modules.eventdata.support.Constants
import org.crossref.manifold.modules.eventdata.support.Event
import org.crossref.manifold.modules.eventdata.support.toItemTree
import org.crossref.messaging.aws.s3.S3EventNotification
import org.crossref.messaging.aws.sqs.SqsListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetObjectRequest
import java.time.OffsetDateTime

class S3EventsIngester(
    private val itemGraphIngester: ItemGraph,
    private val s3Client: S3Client,
    private val agentsAllowList: Set<String>
) {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val mapper: ObjectMapper = ObjectMapper()
        .registerModule(JavaTimeModule())
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .setDateFormat(StdDateFormat().withColonInTimeZone(true))
        .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
        .registerModule(KotlinModule())

    private val crossrefAssertingParty = Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY))

    private val assertingParties = mapOf(
        Constants.CROSSREF to crossrefAssertingParty,
        Constants.DATACITE to Item().withIdentifier(IdentifierParser.parse(Items.DATACITE_AUTHORITY))
    )

    private fun Event.assertingParty(): Item = assertingParties.getOrDefault(sourceId, crossrefAssertingParty)

    private fun String.toUserAgent() = when (this) {
        Constants.DATACITE -> "Datacite agent/1.0"
        else -> "Crossref $this agent/1.0"

    }

    @SqsListener("$EVENT_DATA.$S3_EVENTS_QUEUE")
    fun onMessageReceived(
        s3EventNotification: S3EventNotification
    ) {
        try {
            val events = s3EventNotification.records
                .map { (objectKey, bucketName) ->
                    s3Client.getObject(
                        GetObjectRequest.builder()
                            .bucket(bucketName)
                            .key(objectKey)
                            .build()
                    ).use {
                        val content = it.buffered().reader().readText()
                        mapper.readValue<Event>(content)
                    }
                }

                events.filter { event -> event.sourceId in agentsAllowList }
                .forEach { event ->
                    val itemTree = event.toItemTree()
                    itemTree?.let {
                        val envelope = Envelope(
                            listOf(it),
                            ItemTreeAssertion(
                                OffsetDateTime.parse(event.occurredAt),
                                event.assertingParty()
                            )
                        )

                        val envelopeBatch = EnvelopeBatch(
                            listOf(envelope),
                            EnvelopeBatchProvenance(
                                UserAgentParser.parse(event.sourceId.toUserAgent()),
                                event.id
                            )
                        )
                        logger.info("Ingested an event ${event.id}")
                        itemGraphIngester.ingest(listOf(envelopeBatch))
                    }
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}