package org.crossref.manifold.modules.eventdata

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.micrometer.core.instrument.MeterRegistry
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.eventdata.support.Event
import org.crossref.manifold.modules.eventdata.support.TaggedCounter
import org.crossref.manifold.modules.eventdata.support.bus.MultiJwtVerifier
import org.crossref.manifold.modules.eventdata.support.bus.postEvent
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*

@ResponseBody
@RequestMapping(path = ["/v3/event-data/bus"])
class BusController(
    meterRegistry: MeterRegistry?,
    private val itemGraph: ItemGraph,
    jwtSecrets: Set<String>,
    private val sourcessAllowList: Set<String>
) {
    private val multiJwtVerifier = MultiJwtVerifier(jwtSecrets)

    private val mapper: ObjectMapper = ObjectMapper().registerKotlinModule()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

    private val eventsSuccessfullyProcessed: TaggedCounter? = meterRegistry?.run {
        TaggedCounter("manifold.ingest.events.processed", "source", this)
    }

    @PostMapping(value = ["events"], consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun post(@RequestHeader headers: HttpHeaders, @RequestBody body: String) {
        val event: Event = mapper.readValue(body, Event::class.java)
        // Post the event and log metric if success
        if (postEvent(multiJwtVerifier, headers, itemGraph, sourcessAllowList, event)) {
            eventsSuccessfullyProcessed?.increment(event.sourceId)
        }
    }

    @GetMapping(value = ["ping"])
    fun ping() = HttpStatus.OK
}
