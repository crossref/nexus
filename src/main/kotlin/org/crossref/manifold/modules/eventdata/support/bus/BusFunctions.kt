package org.crossref.manifold.modules.eventdata.support.bus

import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.eventdata.support.Event
import org.crossref.manifold.modules.eventdata.support.logger
import org.crossref.manifold.modules.eventdata.support.parseEventToEnvelopeBatch
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

fun extractToken(headers: HttpHeaders): String? {

    val token = headers["authorization"]
        ?.firstOrNull { it.startsWith("Bearer") }
        ?.substringAfter("Bearer ", "error")
        .takeIf { it != "error" }

    return token
}

fun postEvent(
    multiJwtVerifier: MultiJwtVerifier,
    headers: HttpHeaders,
    itemGraph: ItemGraph,
    sourcesAllowList: Set<String>,
    event: Event
): Boolean =
    multiJwtVerifier
        .verify(extractToken(headers))
        .let { jwt ->
            if (jwt != null && jwt.subject.equals(event.sourceId)) {
                if (sourcesAllowList.contains(event.sourceId)) {
                    try {
                        val envelopeBatch = parseEventToEnvelopeBatch(event)

                        if (envelopeBatch != null) {
                            itemGraph.ingest(listOf(envelopeBatch))
                            logger.info("EVENT ${envelopeBatch.provenance.externalTrace}")
                        }
                        true
                    } catch (e: Exception) {
                        e.printStackTrace()
                        false
                    }
                } else {
                    logger.warn("Ignoring event ${event.id} of non allowed source: ${event.sourceId}")
                    throw ResponseStatusException(HttpStatus.FORBIDDEN, "Source '${event.sourceId}' not allowed")
                }
            } else {
                throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid or missing authentication token")
            }
        }
