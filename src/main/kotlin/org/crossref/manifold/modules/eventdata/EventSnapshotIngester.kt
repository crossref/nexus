package org.crossref.manifold.modules.eventdata

import io.micrometer.core.instrument.MeterRegistry
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.eventdata.support.scanDirectory
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * If the user supplies EVENT_DATA_INGEST_JSON_SNAPSHOT with a .tgz file path in the environment, load that file into the database.
 */
object EventSnapshotIngester {
    private var logger: Logger = LoggerFactory.getLogger(this::class.java)
    private var scope = CoroutineScope(Dispatchers.IO)

    fun ingestJsonSnapshot(
        /** Meter registry for metrics. These are all nullable, so it can be disabled for simpler testing.
         */
        meterRegistry: MeterRegistry?,
        itemGraph: ItemGraph,
        snapshotsDirectory: String,
        sourceAllowlist: Set<String>
    ) {
        try {
            logger.info("JSON ingestion starting in background...")
            logger.info("Scanning : $snapshotsDirectory")
            scanDirectory(snapshotsDirectory, itemGraph, sourceAllowlist, meterRegistry)
            logger.info("JSON ingestion continuing in background!")
        } catch (e: Exception) {
            logger.warn("Error! $e")
            e.printStackTrace()
            throw (e)
        }
    }

    fun destroy() {
        scope.cancel()
    }
}
