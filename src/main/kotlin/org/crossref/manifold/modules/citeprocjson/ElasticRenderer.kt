package org.crossref.manifold.modules.citeprocjson

import clojure.lang.Keyword
import clojure.lang.RT
import org.crossref.cayenne
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.rendering.RenderingContext
import org.crossref.manifold.util.clojure.toClj
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class ElasticRenderer : ContentTypeRenderer {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    init {
        cayenne.boot()
    }

    override fun internalContentType(): ContentType = ContentType.CITEPROC_JSON_ELASTIC
    override fun publicContentType(): ContentType = ContentType.APPLICATION_JSON


    override fun offer(type: String?, subtype: String?, itemTree: ItemTree?, context: RenderingContext?): Boolean = type == "work"

    override fun render(itemTree: ItemTree, context: RenderingContext?): String {
        logger.debug("Rendering ${itemTree.root.pk} using ${internalContentType()}")

        val fieldsToMakeValuesKeywords = setOf("type", "subtype", "kind")
        val converted = itemTree.root.toClj(fieldsToMakeValuesKeywords)
        val esDoc = cayenne.itemToEsDoc(converted)
        val cleanedEsDoc = RT.dissoc(esDoc, Keyword.intern("indexed"))

        return cayenne.writeJsonString(cleanedEsDoc)
    }
}
