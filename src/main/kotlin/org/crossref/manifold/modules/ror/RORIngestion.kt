package org.crossref.manifold.modules.ror

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.*
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.io.File
import java.io.InputStream
import java.time.OffsetDateTime

data class RORFundRef(
    val preferred: String?,
    val all: Array<String>?,
) {
    val distinct: Set<String> get() {
        val preferredSet = if (preferred.isNullOrEmpty()) emptySet() else setOf(preferred)
        return (all?.toSet() ?: emptySet()).plus(preferredSet).toSet()
    }
}

data class RORExternalIDs(
    @field:JsonProperty("FundRef")
    val fundRef: RORFundRef?,
)

data class ROROrganization(
    val id: String,
    val status: String,
    val name: String,
    val externalIds: RORExternalIDs,
) {
    val isWithdrawn: Boolean get() = when (status.lowercase()) {
        "withdrawn",
        -> true
        else -> false
    }
}

@Service
class RORIngestion(
    private val itemGraph: ItemGraph,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val mapper = jacksonObjectMapper()
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .setPropertyNamingStrategy(PropertyNamingStrategies.SnakeCaseStrategy())

    fun ingest(json: String) {
        val organizations = mapper.readValue(json, Array<ROROrganization>::class.java)
        ingestOrganizations(organizations)
    }

    fun ingestFile(path: String) {
        logger.info("Starting ROR ingestion using path $path")

        val inputStream: InputStream = File(path).inputStream()
        val inputString = inputStream.bufferedReader().use { it.readText() }

        val organizations = mapper.readValue(inputString, Array<ROROrganization>::class.java)
        ingestOrganizations(organizations)
    }

    private fun ingestOrganizations(organizations: Array<ROROrganization>) {
        val envelopeBatch = EnvelopeBatch(
            organizations.filter { o -> !o.isWithdrawn }.map { o ->
                var item = Item()
                    .withIdentifier(IdentifierParser.parse(o.id))
                    .withProperties(
                        listOf(
                            Properties(
                                jsonObjectFromMap(
                                    mapOf(
                                        "name" to o.name,
                                        "type" to "org",
                                    ),
                                ),
                            ),
                        ),
                    )
                    .withRelationship(
                        Relationship(
                            RelationshipTypes.COLLECTION,
                            Item().withIdentifier(IdentifierParser.parse("https://id.crossref.org/collections/organization")),
                            assertedAt = null,
                        ),
                    )
                if (o.externalIds.fundRef != null) {
                    item = item.withRelationships(
                        o.externalIds.fundRef.distinct.map {
                            Relationship(
                                RelationshipTypes.SAME_AS,
                                Item().withIdentifier(IdentifierParser.parse("https://doi.org/10.13039/$it")),
                                assertedAt = null,
                            )
                        },
                    )
                }

                Envelope(
                    listOf(ItemTree(item)),
                    ItemTreeAssertion(
                        OffsetDateTime.now(),
                        Item().withIdentifier(IdentifierParser.parse(Items.CROSSREF_AUTHORITY)),
                    ),
                )
            },
            EnvelopeBatchProvenance(
                UserAgentParser.parse("Crossref ROR Import/1.0"),
                OffsetDateTime.now().toString()),
        )

        logger.info("Asserting ROR organizations into the item graph")
        itemGraph.ingest(listOf(envelopeBatch))

        logger.info("Asserted ROR organizations into the item graph")
    }
}
