package org.crossref.manifold.modules.ror

import org.crossref.cayenne.boot
import org.crossref.manifold.modules.Manifest
import org.crossref.manifold.modules.ModuleRegistrar
import org.crossref.manifold.modules.StartupTask
import org.crossref.messaging.aws.autoconfig.S3AutoConfig.Companion.S3_NOTIFICATION_QUEUE
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import software.amazon.awssdk.services.s3.S3Client

@Configuration(Module.ROR)
class Module(moduleRegistrar: ModuleRegistrar) {
    companion object {
        const val ROR = "ror"
        const val JSON_FILE = "json-file"
    }

    init {
        moduleRegistrar.register(
            Manifest(
                name = ROR,
                description = "Research Organization Registry",
            ),
        )

        // Always put the Cayenne module in its initial state. This is a simple configuration option, but is needed for
        // all XML ingestion tasks.
        boot()
    }

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Bean
    @ConditionalOnProperty(prefix = ROR, name = [S3_NOTIFICATION_QUEUE])
    fun IngestS3(
        s3Client: S3Client,
        rorIngestion: RORIngestion,
    ): S3Ingester = S3Ingester(
        s3Client,
        rorIngestion,
    )

    @Bean
    @ConditionalOnProperty(prefix = ROR, name = [JSON_FILE])
    fun IngestFile(
        @Value("\${$ROR.$JSON_FILE}") path: String,
        rorIngestion: RORIngestion,
    ): StartupTask = StartupTask(
        {
            rorIngestion.ingestFile(path)
        },
    )
}
