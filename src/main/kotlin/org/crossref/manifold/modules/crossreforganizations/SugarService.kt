package org.crossref.manifold.modules.crossreforganizations

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class SugarService(
    private val url: String,
) {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private val mapper = jacksonObjectMapper()
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .setPropertyNamingStrategy(PropertyNamingStrategies.SnakeCaseStrategy())

    private inline fun <reified T>tryGetResponse(request: HttpRequest.Builder): T {
        val client = HttpClient.newBuilder().build()

        request.setHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)

        val response = client.send(request.build(), HttpResponse.BodyHandlers.ofString())

        if (response.statusCode() == 200) {
            return mapper.readValue(response.body(), T::class.java)
        }

        throw Exception(
            "There was a problem calling the Sugar API: ${response.statusCode()} ${response.body()}",
        )
    }

    private fun getUrl(rootUrl: String): UriComponentsBuilder {
        return ServletUriComponentsBuilder.fromUriString(rootUrl)
    }

    /**
     * Takes @param[username] and @param[password] and returns a token
     * to be used when making other sugar requests
     */
    fun authenticate(username: String, password: String): String {
        val requestUrl = getUrl(url).pathSegment("oauth2", "token").build().toString()
        val body = mapper.writeValueAsString(
            AuthRequest(
                username = username,
                password = password,
            ),
        )

        val request = HttpRequest.newBuilder()
            .uri(URI.create(requestUrl))
            .POST(HttpRequest.BodyPublishers.ofString(body))

        return tryGetResponse<AuthResponse>(request).accessToken
    }

    /**
     * Get a page of organizations from the Sugar API, starting at @param[offset]
     *
     * Requires @param[token] which can be otained via the authenticate function
     */
    fun getOrganizations(token: String, offset: Int): PagedSugarOrganizations {
        val requestUrl = getUrl(url)
            .pathSegment("Accounts")
            .queryParam("offset", offset)
            .queryParam("max_num", 100)
            .queryParam("fields", "id,name,sponsor_id,crossref_member_id_c,account_type,active_c")
            .build().toString()

        val request = HttpRequest.newBuilder()
            .uri(URI.create(requestUrl))
            .setHeader("OAuth-Token", token)

        return tryGetResponse<PagedSugarOrganizations>(request)
    }
}
