package org.crossref.manifold.modules.crossreforganizations

import com.fasterxml.jackson.annotation.JsonProperty

data class AuthRequest(
    val grantType: String = "password",
    val clientId: String = "sugar",
    val clientSecret: String = "",
    val username: String,
    val password: String,
    val platform: String = "custom",
)

data class AuthResponse(
    val accessToken: String,
    val expiresIn: Int,
    val tokenType: String,
    val scope: String,
)

data class PagedSugarOrganizations(
    val nextOffset: Int,
    val records: Array<SugarOrganization>,
)

enum class AccountTypes(val value: String) {
    MEMBER_PUBLISHER("Member Publisher"),
    REPRESENTED_MEMBER("REPRESENTED_MEMBER"),
    SPONSORING_PUBLISHERS("Sponsoring Publishers"),
    SPONSORING_ENTITIES("Sponsoring Entities"),
    SPONSORED_PUBLISHER("Sponsored Publisher")
}

data class SugarOrganization(
    val id: String,
    val name: String,
    val sponsorId: String?,
    @JsonProperty("crossref_member_id_c")
    val crossrefMemberId: Int?,
    val accountType: String,
    val identifier: String = "https://id.crossref.org/org/$crossrefMemberId",

    /**
     * This field records a member as being inactive, despite its name.
     */
    @JsonProperty("active_c")
    val inactive: Boolean,
) {
    val isMember: Boolean get() = when (accountType) {
        AccountTypes.MEMBER_PUBLISHER.value,
        AccountTypes.REPRESENTED_MEMBER.value,
        AccountTypes.SPONSORING_PUBLISHERS.value,
        -> true
        else -> false
    }

    val isSponsor: Boolean get() = when (accountType) {
        AccountTypes.SPONSORING_ENTITIES.value,
        AccountTypes.SPONSORING_PUBLISHERS.value ,
        -> true
        else -> false
    }

    val isSponsored: Boolean get() = when (accountType) {
        AccountTypes.SPONSORED_PUBLISHER.value,
        AccountTypes.REPRESENTED_MEMBER.value,
        -> true
        else -> false
    }
}
