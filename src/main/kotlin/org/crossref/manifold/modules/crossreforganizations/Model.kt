package org.crossref.manifold.modules.crossreforganizations

import com.fasterxml.jackson.annotation.JsonInclude
import java.net.URI

data class OrganizationIdentifier(
    val uri: URI,
)

data class NamedOrganizationIdentifiers(
    val identifiers: List<OrganizationIdentifier>,
    val name: String?,
)

data class OrganizationRelationship(
    @JsonInclude(JsonInclude.Include.NON_NULL)
    val memberOf: NamedOrganizationIdentifiers? = null,
    @JsonInclude(JsonInclude.Include.NON_NULL)
    val sponsoredBy: NamedOrganizationIdentifiers?,
)

data class Organization(
    val primaryName: String,
    val type: String,
    val identifiers: List<OrganizationIdentifier>,
    val relationships: OrganizationRelationship,
)

data class Member(
    val id: Int?,
    val primaryName: String,
    val type: String,
    val identifiers: List<OrganizationIdentifier>,
    val relationships: OrganizationRelationship,
)
