package org.crossref.manifold.modules.crossreforganizations

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.consts.RelationshipTypes
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.rendering.RenderingContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.net.URI

@Service
class MemberRenderer() : ContentTypeRenderer {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    private val mapper = jacksonObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategies.KEBAB_CASE)

    override fun internalContentType(): ContentType = ContentType.MEMBER_JSON

    override fun publicContentType(): ContentType = ContentType.APPLICATION_JSON

    override fun offer(type: String?, subtype: String?, itemTree: ItemTree?, context: RenderingContext?): Boolean = type == "org"

    override fun render(itemTree: ItemTree, context: RenderingContext?): String? {
        logger.debug("Rendering ${itemTree.root.pk} using ${internalContentType()}")

        val memberOf = itemTree.root.rels.firstOrNull { it.relTyp == RelationshipTypes.MEMBER_OF }?.obj
        if (memberOf?.identifiers?.none { it.uri == URI(Items.CROSSREF_AUTHORITY) } ?: true) return null

        val property = itemTree.root.properties.firstOrNull()
        if (property == null) {
            logger.error("Tried to render ${itemTree.root.pk} as member JSON but the expected property assertions cannot be found")
            return null
        }

        val primaryName = property.values.get("name").textValue()
        val type = property.values.get("type").textValue()
        val sponsoredBy = itemTree.root.rels.firstOrNull { it.relTyp == RelationshipTypes.SPONSOR }?.obj

        var memberPath = itemTree.root.identifiers.firstOrNull()?.uri?.path

        val crossrefMemberId = try
        {
            var memberPattern = """/(\d*)$""".toRegex()
            if (memberPath != null) {
                memberPattern.findAll(memberPath).firstOrNull()?.groups?.get(1)?.value?.toInt()
            } else {
                null
            }
        } catch (e: Exception) {
            logger.error("Error parsing member id: $memberPath error: $e")
            null
        }

        val member = Member(
            id = crossrefMemberId,
            type = type,
            primaryName = primaryName,
            identifiers = itemTree.root.identifiers.map { OrganizationIdentifier(it.uri) },
            relationships = OrganizationRelationship(
                memberOf = memberOf?.let {
                    NamedOrganizationIdentifiers(
                        memberOf.identifiers.map { OrganizationIdentifier(it.uri) },
                        "Crossref",
                    )
                },
                sponsoredBy = sponsoredBy?.let {
                    NamedOrganizationIdentifiers(
                        sponsoredBy.identifiers.map { OrganizationIdentifier(it.uri) },
                        sponsoredBy.properties.firstOrNull()?.values?.get("name")?.textValue(),
                    )
                },
            ),
        )

        return mapper.writeValueAsString(member)
    }
}
