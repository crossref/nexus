package org.crossref.manifold.modules.itemTree

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTreeService
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

/**
 * Note that these endpoints take a full Identifier URL as an endpoint.
 * We also accept the non-URL DOI form as special case, and redirect back to the same endpoint with the full-URL DOI.
 */
@RestController
class ItemTreeController(
    val itemTreeService: ItemTreeService,
    val itemResolver: Resolver,
) {
    /**
     * Get an Item Tree with the default perspective.
     */
    @GetMapping("/beta/item-trees/{*identifier}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getItemTree(@PathVariable identifier: String): Item = itemTree(identifier)

    private fun itemTree(
        identifierStr: String
    ): Item {
        // Need to trim the leading slash.
        val identifier = itemResolver.resolveRO(IdentifierParser.parse(identifierStr.substring(1)))
        if (identifier.pk == null) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't find that item.")
        }

        return itemTreeService.getCanonicalItemTree(identifier.pk)?.second?.root
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Couldn't retrieve data for that item.")
    }
}
