package org.crossref.manifold.modules.itemTree

import com.fasterxml.jackson.databind.ObjectMapper
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.rendering.RenderingContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class ItemTreeJsonRenderer(
    val mapper: ObjectMapper
): ContentTypeRenderer {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun internalContentType(): ContentType = ContentType.ITEM_TREE_JSON
    override fun publicContentType(): ContentType = ContentType.APPLICATION_JSON

    override fun offer(type: String?, subtype: String?, itemTree: ItemTree?, context: RenderingContext?): Boolean = true

    override fun render(itemTree: ItemTree, context: RenderingContext?): String {
        logger.debug("Rendering ${itemTree.root.pk} using ${internalContentType()}")
        return mapper.writeValueAsString(itemTree.root)
    }
}
