package org.crossref.manifold.modules.preprints

import org.crossref.manifold.identifiers.parsers.Doi
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.rendering.ContentType
import org.crossref.manifold.rendering.ContentTypeRenderer
import org.crossref.manifold.rendering.RenderingContext
import org.crossref.manifold.util.BasicJsonMapper
import org.springframework.stereotype.Component

/**
 * This Renderer is used for preprint matching and currently it is implemented
 * according to the specifications set by Marple.
 * Marple is a Crossref labs project that provides a preprint matching API.
 * Its API runs against an Opensearch cluster and this renderer is meant
 * to produce output suitable for populating Marple's preprint index.
 */
@Component
class PreprintRenderer : ContentTypeRenderer {

    /**
     * Returns a metadata string.
     * The string is a concatenation of all the titles, authors’ family names (for all human authors),
     * and authors’ names (for all organisation authors).
     * The order does not matter and no preprocessing is needed.
     * The values are joined with a single space.
     */
    fun metadata(titles: List<String>, authors: List<Author>) =
        (titles + authors.mapNotNull { it.family } + authors.mapNotNull { it.name }).joinToString(" ")

    override fun internalContentType(): ContentType = ContentType.PREPRINT_MARPLE

    override fun publicContentType(): ContentType = ContentType.APPLICATION_JSON

    override fun offer(type: String?, subtype: String?, itemTree: ItemTree?, context: RenderingContext?): Boolean =
        type == "work" &&
                subtype == "posted-content" &&
                itemTree?.root?.properties?.any {
                    it.values.get("content-type")?.textValue().equals("preprint")
                } ?: false

    override fun render(itemTree: ItemTree, context: RenderingContext?): String? {

        val doi = Doi.withoutResolver(itemTree.root.identifiers.first())

        val titles = getTitles(itemTree)

        val (created, issued) = getEarliestDate(itemTree)

        val authors = getAuthors(itemTree)

        val metadata = metadata(titles, authors)

        return if (doi.isNullOrBlank()) {
            null
        } else {
            val response = Preprint(
                doi,
                titles,
                created,
                issued,
                authors,
                metadata
            )

            return BasicJsonMapper.mapper.writeValueAsString(response)
        }
    }
}