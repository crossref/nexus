package org.crossref.manifold.modules.preprints

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.JsonNode
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship
import java.time.OffsetDateTime

/**
 * All optional values in the following data classes should default to null
 * and any data classes containing such null values should be annotated with
 * Include.NON_NULL in order to avoid rendering any null values.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Author(
    val given: String? = null,
    val family: String? = null,
    @JsonProperty(value = "ORCID") val orcid: String? = null,
    val name: String? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Preprint(
    @JsonProperty(value = "DOI") val doi: String,
    val title: List<String>,
    val created: OffsetDateTime? = null,
    val issued: JsonNode? = null,
    val author: List<Author>,
    val metadata: String
)

data class Issued(
    @JsonProperty(value = "date-parts") val dateParts: List<JsonNode?> = emptyList()
)

data class MarpleInput(
    val title: List<String> = emptyList(),
    val issued: Issued,
    val author: List<Author> = emptyList()
)

data class MarpleResponse(
    val status: String,
    @JsonProperty("message-version") val messageVersion: String,
    @JsonProperty("message-type") val messageType: String,
    val message: Message
)

data class Message(
    val items: List<Item>
)

data class Item(
    val id: String,
    val confidence: Double,
    val strategies: List<String>
)

fun getOrcid(relationship: Relationship) = relationship.obj
    .childrenWithRel("object")
    .flatMap { it.identifiers }
    .firstOrNull { it.type == IdentifierType.ORCID }
    ?.uri?.toString()

/**
 * Returns list of [Author] using the properties 'author'->'first-name' and 'author'->'lastname'.
 */
fun getAuthors(itemTree: ItemTree) = itemTree.root.rels
    .filter { it.relTyp == "author" }
    .map {
        Author(
            it.obj.getPropertyText("first-name"),
            it.obj.getPropertyText("last-name"),
            getOrcid(it),
            it.obj.getPropertyText("name"),
        )
    }

/**
 * Returns all titles under the property 'title-long' as a list of strings.
 */
fun getTitles(itemTree: ItemTree): List<String> {
    val titles = itemTree.root.getPropertyArray("title-long")

    return if (titles.isNullOrEmpty()) {
        emptyList()
    } else {
        titles.map { it.get("value").asText() }
    }
}

/**
 * Choose the first (earliest) date object available in ascending order
 * and return its date-time and date-parts as a Pair of [OffsetDateTime] to [JsonNode].
 */
fun getEarliestDate(itemTree: ItemTree): Pair<OffsetDateTime?, JsonNode?> = listOf(
    "posted",
    "published-print",
    "published-online",
    "published-other",
    "published",
    "content-created"
)
    // Get as many dates as we can find
    .mapNotNull { itemTree.root.getProperty(it) }
    // Sort them in ascending order
    .sortedBy { it.get("date-time")?.asText() }
    // Get the first one (the earliest)
    .firstOrNull()
    // Unlikely to happen but ensure we return a null pair if we fail to find any date at all
    ?.let {

        val dateTime = it.get("date-time")?.asText()?.let { dt -> OffsetDateTime.parse(dt) }

        dateTime to it.get("date-parts")
        
    } ?: (null to null)