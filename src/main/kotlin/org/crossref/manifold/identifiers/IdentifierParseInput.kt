package org.crossref.manifold.identifiers

import java.net.URI

/**
 * Input for a parse function with some common preliminary data conversions.
 */
data class IdentifierParseInput(
    val raw: String,
    val uri: URI?,
    val lowercaseUri: URI?,
    val uppercaseUri: URI?) {

    /**
     * Path or empty string.
     */
    fun path() : String = uri?.path ?: ""

    /**
     * Lower-case path or empty string.
     */
    fun lowercasePath() : String = lowercaseUri?.path ?: ""

    /**
     * Check if the lower-cased host equals the input.
     */
    fun hostIs(input: String) : Boolean = (input == lowercaseUri?.host)

    /**
     * Lower-case path or empty string.
     */
    fun uppercasePath(): String = uppercaseUri?.path ?: ""

    fun lowercasePathStartsWith(input: String) : Boolean =
        lowercaseUri?.path?.startsWith(input) ?: false

    fun hostIsOneOf(hosts: Set<String>): Boolean =
        hosts.contains(lowercaseUri?.host)

    fun lowercasePathNoSlash() = lowercasePath().removeSuffix("/")
    fun uppercasePathNoSlash() = uppercasePath().removeSuffix("/")

    companion object {
        fun build(input: String) : IdentifierParseInput {
            // Nearly all identifier types want the input parsed to a URI.
            val uri = try {
                URI(input)
            } catch (exception: Exception) {
                null
            }

            // They also want components of the URI lower-cased, e.g. for comparing hostnames.
            val lowercaseUri = try {
                URI(input.lowercase())
            } catch (exception: Exception) {
                null
            }

            // They also want components of the URI lower-cased, e.g. for comparing hostnames.
            val uppercaseUri = try {
                URI(input.uppercase())
            } catch (exception: Exception) {
                null
            }

            return IdentifierParseInput(input, uri, lowercaseUri, uppercaseUri)
        }
    }

}