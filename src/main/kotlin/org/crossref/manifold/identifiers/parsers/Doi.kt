package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.removePrefixes
import org.crossref.manifold.itemtree.Identifier
import java.net.URI
import java.net.URISyntaxException
import java.net.URLDecoder
import java.nio.charset.StandardCharsets


/**
 *
 * Danger! Do not change any behaviour or edit any test cases!
 *
 * The behaviour of the DOI identifier type and how we encode them in the database are of critical importance. The
 * behaviour of this code must remain stable.
 *
 * Once an identifier is in the database, we expect it to match when queried. Changing the encoding of a DOI would mean
 * a failure to match. This applies to all identifier types, but DOIs are especially important.
 *
 * DOIs are URI-encoded according to the "DOI Name Encoding Rules for URL Presentation" in the DOI handbook.
 * https://www.doi.org/doi-handbook/HTML/encoding-rules-for-urls.html
 *
 * When we encode a DOI:
 *  - we do not encode RFC 3986 "unreserved characters"
 *  - we encode all characters that are "mandatory" and "recommended" according to the DOI handbook
 *  - we do not encode any RFC 3986 "reserved" character that fall outside these ranges
 *  - we encode all other characters
 *
 *  The parser recognises DOIs in two formats:
 *  - plain DOIs, which start with "10." and should be Unicode strings, e.g. "10.1000/456#789"
 *  - URIs, which should be valid URIs, correctly encoded.
 *
 *  In both cases the resultant Identifier URI is encoded (or re-encoded) according to the rules above.
 */
object Doi : IdentifierTypeParser {

    // A sequence of prefixes that we may need to remove to get to a DOI from a URI.
    // Note these regexes are anchored to the start of the string.
    // These are not 'prefixes' in the "DOI prefix" sense (see the doi.org documentation).
    private val URI_PREFIXES_SCHEME = """^(https://|http://|doi:|urn:doi:|info:doi/)""".toRegex()
    private val URI_PREFIXES_HOST = "^(dx.doi.org/|doi.org/)".toRegex()
    private val URI_PREFIX_STRATEGY = listOf(
        URI_PREFIXES_SCHEME,
        URI_PREFIXES_HOST,
        URI_PREFIXES_SCHEME
    )

    // Regular expression to match a DOI. Must be found at the start of the string.
    // Match the slash encoded (lowercase). It will be normalized later.
    private val DOI_RE = """^10\.\d+(/|%2f).*""".toRegex()

    private val DOI_STRICT_RE = """^(10\.\d+)/(.+)$""".toRegex()

    // From RFC 3986 section 2.3 Unreserved Characters
    private val unreserved = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~".map{it}.toSet()

    // From RFC 3986 section 2.2 Reserved Characters
    private val reserved = "!$&'()*,/:;=@".map{it}.toSet()

    // These characters should not be encoded in a DOI. All others must be.
    private val doNotEncode = unreserved.union(reserved)

    init {
        // The DOI handbook specifies which characters are 'mandatory' and 'recommended' to be encoded. We encode
        // all of those.
        // These are an important part of the specification of the encoder, so they are asserted here.
        val shouldEncode = """%"# ?<>{}^[]`|\+""".map { it }.toSet()

        shouldEncode.forEach {
            check(!doNotEncode.contains(it)) {
                "Should not encode character '$it' according to DOI handbook."
            }
        }
    }

    /**
     * Encode characters in a DOI that need to be encoded according to the rules.
     * Handles multibyte UTF-8 sequences.
     * This is not a general purpose encoding function, as it selectively encodes for DOIs.
     */
    private fun percentEncode(input: String) = input.map { c ->
        val asString = c.toString()
        if (doNotEncode.contains(c)) {
            asString
        } else {
            // Handle multibyte UTF-8 sequences.
            // Although [java.net.URLDecoder] correctly decodes all escape sequences, we can't use
            // [java.net.URLEncoder], as it's designed for HTML form encoding, not URL encoding. As such, it doesn't
            // handle the space character.
            asString.toByteArray(StandardCharsets.UTF_8).joinToString("") { "%%%02X".format(it) }
        }
    }.joinToString("")

    private fun decodeUriComponent(input: String) : String? = try {
        URLDecoder.decode(input, StandardCharsets.UTF_8)
    } catch (_: IllegalArgumentException) {
        null
    }

    /**
     * Construct a correctly escaped DOI Identifier from a plain Unicode DOI.
     */
    fun construct(decodedRawDoi: String) : Identifier? =
            DOI_STRICT_RE.matchEntire(decodedRawDoi)?.groupValues?.let {
            val prefix = it[1]
            val suffix = it[2]

            val lowercase = suffix.lowercase()

            val encodedSuffix = percentEncode(lowercase)
            Identifier(URI("https://doi.org/$prefix/$encodedSuffix"), IdentifierType.DOI)
        }


    /**
     * Attempt to parse a string into a DOI URI. Do this based on its structure, so there's no guarantee
     * that the DOI exists.
     *
     * There are inputs we may expect:
     *  - 'raw' DOIs, which are the pure Unicode string. No URI encoding.
     *  - URI DOIs, which must be correctly encoded.
     *
     *  These are handled differently.
     *
     *  Raw DOIs are encoded (every character except 'unreserved characters') and packaged into a URI.
     *
     *  URI DOIs must be valid URIs, so we expect them to be correctly encoded. If they don't parse, we don't match them
     *  as a DOI.
     *
     */
    override fun tryParse(input: IdentifierParseInput): Identifier? {
        // Always treat DOIs as lower case.
        // We rely on this for matching the host too.
        val lowercase = input.raw.lowercase()

        // Raw DOIs can be encoded and put into a URI.
        return if (DOI_STRICT_RE.matches(lowercase)) {
            construct(lowercase)
        } else {

            // Otherwise treat this as a URI DOI, and attempt to parse.
            val lessPrefixes = removePrefixes(URI_PREFIX_STRATEGY, lowercase)

            if (DOI_RE.matches(lessPrefixes)) {
                 try {
                     decodeUriComponent(lessPrefixes)?.let { decoded ->
                         construct(decoded)
                     }
                } catch (_: URISyntaxException) {
                    null
                }
            } else null
        }
    }

    /**
     * The prefix assigned to Crossref Funder DOIs.
     */
    private const val FUNDER_DOI_PREFIX = "10.13039"

    fun isFunderDoi(identifier: Identifier) = doiPrefix(identifier) == FUNDER_DOI_PREFIX

    /**
     * Match a DOI prefix, e.g. "10.5555" from a DOI URI path.
     */
    private val DOI_PREFIX_RE = """^/(10\.\d+)/""".toRegex()

    /**
     * Find the DOI prefix, e.g. "10.5555".
     */
    fun doiPrefix(identifier: Identifier): String? =
        DOI_PREFIX_RE.find(identifier.uri.path)?.groups?.get(1)?.value

    /**
     * Translate to "10.5555/123456789" format for display.
     */
    fun withoutResolver(identifier: Identifier): String? =
        if (identifier.type == IdentifierType.DOI) {
            decodeUriComponent(identifier.uri.path.removePrefix("/"))
        } else null
}