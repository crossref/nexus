package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object Wikidata : IdentifierTypeParser {
    const val CANONICAL_HOST = "www.wikidata.org"
    private val PATH_REGEX = """^/(wiki|entity)/(Q\d*.)$""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? {
        return if (input.hostIs(CANONICAL_HOST) &&
            PATH_REGEX.matches(input.path())
        ) {
            getInternalIdFromPath(input)?.let {
                construct(it)
            }
        } else null
    }

    /**
     * Extract the Wikidata entity, or null.
     */
    private fun getInternalIdFromPath(input: IdentifierParseInput): String? =
        PATH_REGEX.find(input.path().removeSuffix("/"))?.groups?.get(2)?.value

    private fun construct(entityId : String) : Identifier =
        Identifier(URI(Util.HTTPS_SCHEME, CANONICAL_HOST, "/entity/$entityId", null), IdentifierType.WIKIDATA)
}