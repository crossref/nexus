package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object Arxiv : IdentifierTypeParser {

    private const val MATCH_SCHEME = "arxiv"

    const val CANONICAL_HOST = "arxiv.org"

    // Old and new identifier styles.
    // Matchers for URI scheme-specific-parts and paths.
    private const val NEW_RE = """(\d+.*\d+([vV\d])?)"""
    private const val OLD_RE = """([a-zA-Z.-]+/\d+([vV]\d+)?)"""
    private val NEW_PATH_RE = "^/abs/$NEW_RE/?\$".toRegex()
    private val OLD_PATH_RE = "^/abs/$OLD_RE/?\$".toRegex()
    private val NEW_SSP_RE = "^$NEW_RE\$".toRegex()
    private val OLD_SSP_RE = "^$OLD_RE\$".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? =
        getInternalIdFromUri(input)?.let {
            construct(it)
        }


    private fun getInternalIdFromUri(input: IdentifierParseInput): String? =
        if (input.lowercaseUri?.scheme == MATCH_SCHEME) {
            val ssp = input.uri?.schemeSpecificPart ?: ""
            if (NEW_SSP_RE.matches(ssp)) {
                ssp
            } else if (OLD_SSP_RE.matches(ssp)) {
                ssp
            } else null
        } else if (input.hostIs(CANONICAL_HOST)) {
            NEW_PATH_RE.find(input.path())?.groups?.get(1)?.value ?: OLD_PATH_RE.find(input.path())?.groups?.get(1)?.value
        } else null

    fun construct(arxivId: String): Identifier? =
        Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, "/abs/$arxivId", null), IdentifierType.ARXIV)


}