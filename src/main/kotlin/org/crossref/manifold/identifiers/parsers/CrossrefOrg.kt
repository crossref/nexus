package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

/**
 * An organization identifier assigned by Crossref to its members and subscribers.
 */
object CrossrefOrg : IdentifierTypeParser {

    private const val CANONICAL_HOST = "id.crossref.org"
    private const val CANONICAL_PATH = "/org/"
    private val LAST_INT_REGEX = """/(\d+)$""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? =
        if (input.lowercaseUri != null && input.hostIs(CANONICAL_HOST)) {
            val cleanedPath = input.lowercasePathNoSlash()

            // Migrate member -> organization and recognise.
            Util.replaceIfStartsWith(cleanedPath, "/member/", CANONICAL_PATH)?.let {
                return construct(it)
            }

            // Recognise organization.
            if (cleanedPath.startsWith(CANONICAL_PATH)) {
                Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, cleanedPath, null), IdentifierType.CROSSREF_ORG)
            } else null
        } else null


    /**
     * Get the numerical ID that Crossref assigned to this member or subscriber.
     */
    fun getOrganizationId(identifier: Identifier): Int? =
        if (identifier.type == IdentifierType.CROSSREF_ORG) {
            LAST_INT_REGEX.find(identifier.uri.path ?: "")?.groups?.get(1)?.value?.toInt()
        } else null

    fun construct(internalId: String): Identifier =
        Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, internalId, null), IdentifierType.CROSSREF_ORG)

    fun toLegacyMemberId(identifier: Identifier) : Identifier? =
        if (identifier.type == IdentifierType.CROSSREF_ORG) {
            val id = getOrganizationId(identifier)
            Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, "/member/$id", null), IdentifierType.URI)
        } else null

}

