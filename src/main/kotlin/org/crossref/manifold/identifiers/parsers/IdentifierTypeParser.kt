package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.itemtree.Identifier

/**
 * All identifiers are stored the same way, as URIs, but they have polymorphic behaviour. The [IdentifierTypeParser] interface
 * is implemented by a family of objects, one for each type of Identifier. Each of these provides a standard set of
 * behaviour for each identifier (recognising, normalizing). They are also markers to indicate identifier type.
 *
 * Implementing objects may have their own specialized type-specific behaviour (e.g. DOI may have prefix-specific).
 *
 * The [pk] function plays a role similar to an Enum type. This implementation was chosen because it allows for extra
 * specialized functions for each type, which Enum doesn't.
 *
 * Another approach that was considered was making Identifier abstract and extending with subclasses. This was rejected
 * because it prevents the use of a data class and required a lot of extra boilerplate.
 */
sealed interface IdentifierTypeParser {

    /**
     * Try to parse the given input to produce a URI.
     * Accepts an arbitrary untrusted string.
     *
     * If the input matches this type, normalize and return, along with recognised type.
     * If not recognised, return null.
     */
    fun tryParse(input: IdentifierParseInput): Identifier?
}