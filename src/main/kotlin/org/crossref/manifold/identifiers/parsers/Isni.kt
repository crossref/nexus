package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

/**
 * ISNI
 * Expressed as URIs in the "linked data URI" form from https://isni.org/page/linked-data/ .
 */
object Isni : IdentifierTypeParser {

    const val CANONICAL_HOST = "isni.org"

    override fun tryParse(input: IdentifierParseInput): Identifier? = try {
        val cleanedPath = input.path().removeSuffix("/")
        if (input.hostIs(CANONICAL_HOST) &&
            validateCheckDigit(cleanedPath)
        ) {
            Identifier(URI(Util.HTTPS_SCHEME, CANONICAL_HOST, cleanedPath, null, null), IdentifierType.ISNI)
        } else null
    } catch (exception: Exception) {
        null
    }

    /**
     * Find digits from a path. Last digit is checksum, broken out separately.
     * Unlike ORCID, we don't expect hyphens.
     */
    private val PATH_NUMBERS_RE = """^/isni/(\d{15})([\dX])$""".toRegex()

    private fun validateCheckDigit(path: String) =
        PATH_NUMBERS_RE.find(path)?.groupValues?.let { groups ->
            val checkDigit = groups.get(2).uppercase()
            val numbers = groups[1]
            val expectedCheckDigit = Util.generateCheckDigitIso27729(numbers)

            checkDigit == expectedCheckDigit
        } ?: false
}
