package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.identifiers.parsers.Util.decodeUriComponent
import org.crossref.manifold.identifiers.parsers.Util.parseChecksumDigit
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object Issn : IdentifierTypeParser {
    const val CANONICAL_HOST = "id.crossref.org"
    private const val CANONICAL_PATH_PREFIX = "/issn/"

    private val RESOLVER_HOSTS = setOf("id.crossref.org", "urn.issn.org", "issn.org")

    // Anchor to end to ensure no extra suffix digits are ignored.
    private val PATH_FIND_RE = """(/issn/|/urn:issn:|/issn:|/resource/ISSN/)([0-9]{4}-?[0-9]{3}[0-9x])$""".toRegex()

    val URN_SCHEME = "urn"

    // Anchor to end to ensure no extra suffix digits are ignored.
    private val URN_FIND_RE = """issn:([0-9]{4}-?[0-9]{3}[0-9x])$""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? {
        val maybeDigits = if (input.lowercaseUri != null) {
            if (input.hostIsOneOf(RESOLVER_HOSTS)) {
                PATH_FIND_RE.find(input.lowercasePathNoSlash())?.groups?.get(2)?.let {
                    it.value.replace("-", "").replace("/", "")
                }
            } else if (input.lowercaseUri.scheme == URN_SCHEME) {
                URN_FIND_RE.find(input.lowercaseUri.schemeSpecificPart)?.groups?.get(1)?.let {
                    it.value.replace("-", "")
                }
            } else null
        } else null

        return if (maybeDigits != null && validate(maybeDigits)) {
            // Uppercase the last bit for the 'X' checksum.
            Identifier(URI(
                HTTPS_SCHEME, CANONICAL_HOST, CANONICAL_PATH_PREFIX +
                        maybeDigits.substring(0, 4) + "-" + maybeDigits.substring(4, 8).uppercase(), null, null
            ), IdentifierType.ISSN
            )
        } else null

    }

    private val DIGITS_RE = """^[0-9]{4}[0-9]{3}[0-9x]$""".toRegex()
    private val WEIGHTS = listOf(8, 7, 6, 5, 4, 3, 2)

    /**
     * Validate ISSN as string of digits with no hyphens.
     *
     * @param[input] string of digits.
     */
    private fun validate(input: String): Boolean =
        // Belt and braces check to guard parseInt.
        if (DIGITS_RE.matches(input)) {
            val digits = input.substring(0, 7)
            val check = input.substring(7, 8)
            val checkInt = parseChecksumDigit(check)

            val expectedCheck = (11 - digits.chunked(1)
                .map(Integer::parseInt)
                .zip(WEIGHTS)
                .sumOf { (digit, weight) -> digit * weight }
                .mod(11)).mod(11)

            checkInt == expectedCheck
        } else false

    fun withoutResolver(identifier: Identifier?): String? =
        if (identifier?.type == IdentifierType.ISSN) {
            decodeUriComponent(identifier.uri.path.removePrefix(CANONICAL_PATH_PREFIX))
        } else null

}