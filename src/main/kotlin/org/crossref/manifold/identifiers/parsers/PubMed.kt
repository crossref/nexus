package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object PubMed : IdentifierTypeParser {
    const val CANONICAL_HOST = "pubmed.ncbi.nlm.nih.gov"

    private val PATH_RE = """^/(\d+)/?$""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? {
        return if (input.hostIs(CANONICAL_HOST)) {
            getInternalIdFromPath(input)?.let {
                construct(it)
            }
        } else null
    }

    /**
     * Extract the Wikidata entity, or null.
     */
    private fun getInternalIdFromPath(input: IdentifierParseInput): String? {
        return PATH_RE.find(input.lowercasePath())?.groups?.get(1)?.value
    }

    fun construct(pubMedId: String): Identifier =
        Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, "/$pubMedId", null), IdentifierType.PUB_MED)

}