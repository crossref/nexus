package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.identifiers.parsers.Util.decodeUriComponent
import org.crossref.manifold.identifiers.parsers.Util.parseChecksumDigit
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object Isbn : IdentifierTypeParser {

    private const val CANONICAL_HOST = "id.crossref.org"
    private const val PATH_PREFIX = "/isbn/"


    override fun tryParse(input: IdentifierParseInput): Identifier? {
        val maybeDigits = normalizeMaybeDigits(input)
        // Find the ISBN string from the recognised URI.
        return validate10Digit(maybeDigits)?.let {
            // If it's a 10 digit, convert to a valid 13 digit one.
            Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, PATH_PREFIX + tenToThirteen(it), null, null), IdentifierType.ISBN)
        } ?: validate13Digit(maybeDigits)?.let {

            Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, PATH_PREFIX + it, null, null), IdentifierType.ISBN)
        }
    }

    /**
     * From a well formatted URI, or direct input, return the part that might be digits.
     */
    fun normalizeMaybeDigits(input: IdentifierParseInput): String =
        if (input.lowercaseUri != null &&
            input.hostIs(CANONICAL_HOST) &&
            input.lowercasePathStartsWith(PATH_PREFIX)
        ) {
            input.lowercaseUri.path.replaceFirst(PATH_PREFIX, "").replace("-", "").removeSuffix("/")
        } else {
            // If it's a simple string remove hyphens.
            input.raw.replace("-", "")
        }.uppercase()

    private val TEN_DIGITS_RE = """\d{9}[0-9X]""".toRegex()

    // Weights for checksum of 10-digit ISBN.
    private val TEN_WEIGHTS = listOf(10, 9, 8, 7, 6, 5, 4, 3, 2, 1)


    /**
     * If the input is a 10-digit ISBN with a valid checksum then normalize, remove punctuation and checksum.
     * If it isn't return null.
     */
    fun validate10Digit(input: String): String? =

        if (TEN_DIGITS_RE.matches(input)) {
            val digits = input.substring(0, 9)
            val check = input.substring(9, 10)
            val checkInt = parseChecksumDigit(check)

            val expectedCheck = digits.chunked(1)
                .map(Integer::parseInt)
                .zip(TEN_WEIGHTS)
                .sumOf { (digit, weight) -> digit * weight }
                .mod(11)

            if (checkInt == expectedCheck) {
                input
            } else null

        } else null

    private val THIRTEEN_DIGITS_RE = """\d{13}""".toRegex()

    private val THIRTEEN_WEIGHTS = listOf(1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1)

    private fun create13Digit(digits: String): Int =
        10 - digits.chunked(1)
            .map(Integer::parseInt).zip(THIRTEEN_WEIGHTS)
            .sumOf { (digit, weight) -> digit * weight }
            .mod(10)


    /**
     * If the input is a 13-digit ISBN with a valid checksum then normalize, remove punctuation and checksum.
     * If it isn't return null.
     */
    private fun validate13Digit(input: String): String? = if (THIRTEEN_DIGITS_RE.matches(input)) {
        val digits = input.substring(0, 12)
        val check = input.substring(12, 13)
        val checkInt = Integer.parseInt(check)

        if (checkInt == create13Digit(digits)) {
            input
        } else null

    } else null

    /**
     * Convert a 10 digit ISBN to a 13 digit ISBN, including the checksum.
     * Don't check the checksum for the input.
     */
    private fun tenToThirteen(input: String): String {
        val digits = "978" + input.substring(0, 9)
        val check = create13Digit(digits)
        return "$digits$check"
    }

    fun withoutResolver(identifier: Identifier?): String? =
        if (identifier?.type == IdentifierType.ISBN) {
            decodeUriComponent(identifier.uri.path.removePrefix(PATH_PREFIX))
        } else null

}
