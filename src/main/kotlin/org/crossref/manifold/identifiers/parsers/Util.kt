package org.crossref.manifold.identifiers.parsers

import java.nio.charset.StandardCharsets

object Util {

    /**
     * Different URI constructors need different styles.
     */
    const val HTTPS_SCHEME = "https"


    /**
     * If the input string starts with the given prefix, return it with the prefix removed.
     * If not, return null.
     */
    private fun removeIfStartsWith(input: String, prefix: String): String? {
        if (input.startsWith(prefix)) {
            return input.drop(prefix.length)
        }
        return null
    }

    /**
     * If the input string starts with the given prefix, return it with the prefix removed.
     * If not, return null.
     */
    fun replaceIfStartsWith(input: String, oldPrefix: String, newPrefix: String): String? {
        return removeIfStartsWith(input, oldPrefix)?.let { newPrefix + it }
    }

    // From RFC 3986 section 2.2 Reserved Characters
    private val unreserved = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~".map{it}.toSet()

    // From RFC 3986 section 2.2 Reserved Characters
    private val reserved = """!#$&'()*+,/:;=?@""".map{it}.toSet()

    private val doNotEncode = unreserved + reserved

    /**
     * Encode all characters except unreserved.
     */
    // Encode any characters that aren't deemed reserved.
    fun percentEncode(input: String, encodePercent: Boolean = false) = input.map { c ->
        if (c == '%') {
            if (encodePercent) {
                "%%%02X".format(c.code)
            } else {
                c.toString()
            }
        } else if (doNotEncode.contains(c)) {
            c.toString()
        } else {
            "%%%02X".format(c.code)
        }
    }.joinToString("")

    fun decodeUriComponent(input: String) : String? = try {
        java.net.URLDecoder.decode(input, StandardCharsets.UTF_8)
    } catch (_: IllegalArgumentException) {
        null
    }

    fun buildEncoded(
        scheme: String?,
        host: String?,
        port: Int,
        path: String?,
        query: String?,
        fragment: String?,
    ): String =
        build(
            scheme,
            host,
            port,
            path?.let { percentEncode(it) },
            query?.let { percentEncode(it) },
            fragment?.let { percentEncode(it) })

    fun build(scheme: String?, host: String?, port: Int, path: String?, query: String?, fragment: String?): String =
        (scheme?.let { "$it:" } ?: "") +
                (host?.let { "//$it" } ?: "") +
                (if (port != -1) {
                    ":${port}"
                } else "") +
                (path?.let { (it) } ?: "") +
                (query?.let { "?$it" } ?: "") +
                (fragment?.let { "#$it" } ?: "")

    fun build(scheme: String?, host: String?, port: Int, rest: String?): String =
        (scheme?.let { "$it:" } ?: "") +
                (host?.let { "//$it" } ?: "") +
                (if (port != -1) {
                    ":${port}"
                } else "") +
                (rest?.let { (it) } ?: "")

    /**
     * See <https://support.orcid.org/hc/en-us/articles/360006897674-Structure-of-the-ORCID-Identifier>
     */
    fun generateCheckDigitIso27729(baseDigits: String): String {
        var total = 0
        baseDigits.forEach {
            val digit = Character.getNumericValue(it)
            total = (total + digit) * 2
        }

        val remainder = total % 11
        val result = (12 - remainder) % 11

        return if (result == 10) "X" else result.toString()
    }

    /**
     * Apply prefix removal in turn.
     */
    fun removePrefixes(regexes: List<Regex>, input: String) =
        regexes.foldRight(input) { prefix, x -> prefix.replaceFirst(x, "") }

    fun parseChecksumDigit(input: String) =
        if (input.uppercase() == "X") {
            10
        } else Integer.parseInt(input)


}