package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.Identifier
import java.net.URI
import java.net.URL


object Uri : IdentifierTypeParser {
    /**
     * Parse a found URI.
     * See note on [recodeUri].
     */
    override fun tryParse(input: IdentifierParseInput): Identifier? =
        // If the original URI parse succeeded, return that.
        if (input.uri != null) {
            Identifier(URI(Util.percentEncode(input.uri.toString())), IdentifierType.URI)
        } else {
            // The URL parser is a bit more tolerant in how it parses odd structures, so try to interpret as a URL and re-code.
            try {
                val url = URL(input.raw)

                Identifier(URI(Util.buildEncoded(url.protocol, url.host, url.port, url.path, url.query, url.ref)), IdentifierType.URI)
            } catch (malformedUrl: Exception) {
                // If there's no protocol then the URL won't parse. Pass.
                null
            }
        }
}


