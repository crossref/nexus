package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI


object Grid : IdentifierTypeParser {

    private val GRID_PATH_RE = """^(/institutes/)?(grid\.\d{4,6}\.[0-9a-f]{1,2})""".toRegex()

    private const val CANONICAL_HOST = "grid.ac"

    override fun tryParse(input: IdentifierParseInput): Identifier? =
        GRID_PATH_RE.find(input.lowercaseUri?.path ?: "")?.groups?.get(2)?.value?.let {
            Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, "/institutes/$it", null), IdentifierType.GRID)
        }


}
