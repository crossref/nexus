package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.identifiers.parsers.Util.HTTPS_SCHEME
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

/**
 * Identifier for container items, such as books, journals, series.
 * This is the union of the "Journal ID", "Book ID" and "Series ID" but may expand in the future.
 * The character 'S', 'B', and 'J' namespace the numerical ids but,
 * as they are a function of the legacy feature, should be treated as opaque.
 */
object CrossrefContainer : IdentifierTypeParser {
    private const val CANONICAL_HOST = "id.crossref.org"

    private val PATH_UPPER_REGEX = """^/CONTAINER/[JBS]\d+$""".toRegex()
    private val NUMBER_REGEX = """([JBS]\d+$)""".toRegex()

    override fun tryParse(input: IdentifierParseInput): Identifier? {
        val cleanedInput = input.uppercasePathNoSlash()
        return if (input.hostIs(CANONICAL_HOST) &&
            PATH_UPPER_REGEX.matches(cleanedInput)
        ) {
            // Uppercase the internal type "S", "B" or "J".
            NUMBER_REGEX.find(cleanedInput)?.value?.let {
                construct(it)
            }
        } else null
    }

    /**
     * Construct from the number including the leading 'S', 'B' or 'J'.
     */
    private fun construct(number: String): Identifier {
        return Identifier(URI(HTTPS_SCHEME, CANONICAL_HOST, "/container/$number", null), IdentifierType.CROSSREF_CONTAINER)
    }

    /**
     * Construct from a numerical BookCite number.
     */
    fun fromBookId(citeId: String) = construct("B$citeId")

    /**
     * Construct from a numerical SeriesCite ID.
     */
    fun fromSeriesId(citeId: String) = construct("S$citeId")

    /**
     * Construct from a numerical JournalCite ID.
     */
    fun fromJournalId(citeId: String) = construct("J$citeId")
}
