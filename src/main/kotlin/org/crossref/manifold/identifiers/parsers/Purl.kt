package org.crossref.manifold.identifiers.parsers

import org.crossref.manifold.identifiers.IdentifierParseInput
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.Identifier
import java.net.URI

object Purl : IdentifierTypeParser {
    private val RECOGNISED_HOSTS = setOf("purl.org", "purl.oclc.org")

    private const val CANONICAL_HOST = "purl.org"

    override fun tryParse(input: IdentifierParseInput): Identifier? =
            if (input.hostIsOneOf(RECOGNISED_HOSTS))
            {
                Identifier(URI(Util.HTTPS_SCHEME, CANONICAL_HOST, input.uri?.path, null, null), IdentifierType.PURL)
            } else null
}
