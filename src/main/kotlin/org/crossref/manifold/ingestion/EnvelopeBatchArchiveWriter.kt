package org.crossref.manifold.ingestion

import org.crossref.manifold.util.JsonItemTreeMapper
import org.crossref.manifold.util.TarGzWriter
import java.io.Closeable
import java.io.File
import java.io.Flushable

/**
 * Writer for an EnvelopeBatch Archive, which is a GZipped Tar file containing a sequence of Envelope Batch files.
 * This must be used in a managed context such as Kotlin's `use`, and is not thread safe.
 */
class EnvelopeBatchArchiveWriter(file: File) : Closeable, Flushable {

    private val writer = TarGzWriter(file)

    /**
     * Add a file containing @param[envelopeBatches].
     * The filename does not need to be unique to be written, but it's useful for traceability.
     */
    fun add(recordFilename: String, envelopeBatches: Collection<EnvelopeBatch>) {
        writer.add(recordFilename) {
            JsonItemTreeMapper.mapper.writeValue(it, envelopeBatches)
        }
    }

    override fun close() {
        writer.close()
    }

    override fun flush() {
        writer.flush()
    }
}