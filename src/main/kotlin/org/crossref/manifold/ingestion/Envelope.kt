package org.crossref.manifold.ingestion

import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.removeAmbiguousItems

/**
 * One or more [ItemTree] packaged up with the [ItemTreeAssertion] that describes what to do with them.
 * @param[itemTrees] one or more item trees. Each Item is recursive via the assertions, so a tree looks like a node.
 */
data class Envelope(
    val itemTrees: List<ItemTree>,
    val assertion: ItemTreeAssertion,
    val pk: Long? = null
) {
    fun withAssertion(newAssertion: ItemTreeAssertion): Envelope = Envelope(itemTrees, newAssertion, pk)
    fun withItemTrees(newItemTrees: List<ItemTree>) =
        Envelope(newItemTrees, assertion, pk)

    fun withPk(newPk: Long) = Envelope(itemTrees, assertion, newPk)
    fun countItems() : Int = itemTrees.sumOf { it.countItems() }
}

/**
 * Remove ambiguously identified Items form the [EnvelopeBatch] and return.
 */
fun removeAmbiguousItems(envelope: Envelope): Envelope =
    envelope.withItemTrees(
        envelope.itemTrees.mapNotNull {
            // The Item Tree is immutable. It needs rebuilding if the structure changed.
            removeAmbiguousItems(it.root)?.let { ItemTree(it) }
        }
    )
