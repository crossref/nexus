package org.crossref.manifold.ingestion

/**
 * Provenance for an [EnvelopeBatch]: the User Agent asserts it, and the trace ID that they supply.
 */
data class EnvelopeBatchProvenance(

    /**
     * User Agent string, supplied by submitters. Should uses the HTTP
     * [User Agent](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/User-Agent) format.
     */
    val userAgent: UserAgent,

    /**
     * External trace string, supplied by submitters to connect to their own processes.
     */
    val externalTrace: String? = null,

    /**
     * Batch ID of the Envelope, known after insertion.
     */
    val envelopeBatchPk: Long? = null
)