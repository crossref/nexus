package org.crossref.manifold.ingestion

import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional


/**
 * Data access for book-keeping around ingestion processes.
 */
@Repository
class IngestionDao(
    private val jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun createEnvelopeBatches(
        batches: Collection<EnvelopeBatch>,
    ): Collection<EnvelopeBatch> =
        if (batches.isEmpty()) {
            emptyList()
        } else {
            // Order the collection so we can map PKs to indexes since insertion order is not guaranteed in batch updates.
            val orderedBatches = batches.toList()

            jdbcTemplate.execute(
                """
                CREATE TEMP TABLE new_envelope_batches (
                    index INT NOT NULL,
                    ext_trace VARCHAR NULL,
                    user_agent_pk INT NOT NULL,
                    envelope_batch_pk BIGINT NOT NULL DEFAULT NEXTVAL('envelope_batch_pk_seq')
                ) ON COMMIT DROP
                """
            )

            npTemplate.batchUpdate(
                "INSERT INTO new_envelope_batches (index, ext_trace, user_agent_pk) VALUES (:index, :ext_trace, :user_agent_pk)",
                orderedBatches.mapIndexed { index, batch ->
                    mapOf(
                        "index" to index,
                        "ext_trace" to batch.provenance.externalTrace,
                        "user_agent_pk" to batch.provenance.userAgent.pk
                    )
                }.toTypedArray()
            )

            jdbcTemplate.execute(
                """
                INSERT INTO envelope_batch (
                    pk,
                    ext_trace,
                    user_agent_pk,
                    created_at)
                SELECT envelope_batch_pk, ext_trace, user_agent_pk, NOW()
                FROM new_envelope_batches
                """
            )

            jdbcTemplate.query(
                "SELECT envelope_batch_pk, index FROM new_envelope_batches ORDER by index"
            ) { rs, _ ->
                orderedBatches[rs.getInt("index")].copy(pk = rs.getLong("envelope_batch_pk"))
            }
        }

    /**
     * For a collection of unresolved EnvelopeBatches, create corresponding Envelopes in the database and return with
     * those PKs attached.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun createEnvelopes(
        batches: Collection<EnvelopeBatch>,
    ): Collection<EnvelopeBatch> =
        if (batches.isEmpty()) {
            emptyList()
        } else {
            // Order the envelopes so we can map PKs to indexes since insertion order is not guaranteed in batch updates.
            val orderedEnvelopes = batches.associate { batch ->
                batch.pk!! to batch.envelopes.toMutableList()
            }

            jdbcTemplate.execute(
                """
                CREATE TEMP TABLE new_envelopes (
                    index INT NOT NULL,
                    envelope_batch_pk BIGINT NOT NULL,
                    envelope_pk BIGINT NOT NULL DEFAULT NEXTVAL('envelope_pk_seq')
                ) ON COMMIT DROP
                """
            )

            npTemplate.batchUpdate(
                """
                INSERT INTO new_envelopes (index, envelope_batch_pk)
                VALUES (:index, :envelope_batch_pk)
                """,
                orderedEnvelopes.flatMap { (batchPk, envelopes) ->
                    List(envelopes.size) { index ->
                        mapOf(
                            "index" to index,
                            "envelope_batch_pk" to batchPk,
                        )
                    }
                }.toTypedArray()
            )

            jdbcTemplate.execute(
                """
                INSERT INTO envelope (
                    pk,
                    envelope_batch_pk,
                    created_at
                )
                SELECT envelope_pk, envelope_batch_pk, NOW()
                FROM new_envelopes
                """
            )

            jdbcTemplate.query(
                "SELECT envelope_pk, envelope_batch_pk, index FROM new_envelopes ORDER BY envelope_batch_pk, index"
            ) { rs ->
                orderedEnvelopes[rs.getLong("envelope_batch_pk")]!!.apply {
                    val envelopeIndex = rs.getInt("index")
                    add(envelopeIndex, removeAt(envelopeIndex).copy(pk = rs.getLong("envelope_pk")))
                }
            }

            batches.map { batch ->
                batch.copy(envelopes = orderedEnvelopes[batch.pk]!!)
            }
        }

    /**
     * For a collection of unresolved EnvelopeBatches, create corresponding Envelopes in the database and return with
     * those PKs attached.
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun createUserAgents(
        userAgents: Collection<UserAgent>,
    ) = userAgents.sortedBy { "${it.product}/${it.version}" }
        .map {
            mapOf(
                "product" to it.product,
                "version" to it.version
            )
        }.let {
            npTemplate.batchUpdate(
                """
                INSERT INTO user_agent (product, version, created_at)
                VALUES (:product, :version, NOW())
                ON CONFLICT (product, version) DO NOTHING
                """,
                it.toTypedArray()
            )
        }


    /**
     * Fetch the PK of a user agent matching both product and version.
     * @return Int?
     */
    @Transactional(propagation = Propagation.SUPPORTS)
    fun fetchUserAgentPK(
        product: String,
        version: String?,
    ): Int? {
        return npTemplate.queryForList(
            "SELECT pk FROM user_agent " +
                    "WHERE product = :product " +
                    "AND version = :version",
            mapOf(
                "product" to product,
                "version" to version
            ),
            Integer::class.java
        ).firstOrNull()?.toInt()
    }
}
