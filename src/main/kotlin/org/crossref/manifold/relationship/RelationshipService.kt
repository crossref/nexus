package org.crossref.manifold.relationship

import org.crossref.manifold.api.ItemView
import org.crossref.manifold.api.RelationshipView
import org.crossref.manifold.api.relationship.RelationshipsMessage
import org.crossref.manifold.api.relationship.RelationshipsRequest
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemgraph.ItemDao
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.ItemInfoDao
import org.crossref.manifold.lookup.RALookupDao
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.crossref.manifold.relationship.Constants.API
import org.crossref.manifold.relationship.Constants.RELATIONSHIPS
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service

@Service
@ConditionalOnProperty(prefix = RELATIONSHIPS, name = [API])
class RelationshipService(
    private val relationshipTypeRegistry: RelationshipTypeRegistry,
    private val resolver: Resolver,
    private val itemDao: ItemDao,
    private val relationshipDao: RelationshipDao,
    private val ralLookupDao: RALookupDao,
    private val itemInfoDao: ItemInfoDao
) {
    fun getRelationships(relationshipsRequest: RelationshipsRequest): RelationshipsMessage {
        var resolvedSubjectPk: Long? = null
        if (relationshipsRequest.subjectId != null) {
            resolvedSubjectPk = resolver.resolveRO(IdentifierParser.parse(relationshipsRequest.subjectId)).pk
            if (resolvedSubjectPk == null) {
                return RelationshipsMessage()
            }
        }

        var resolvedObjectPk: Long? = null
        if (relationshipsRequest.objectId != null) {
            resolvedObjectPk = resolver.resolveRO(IdentifierParser.parse(relationshipsRequest.objectId)).pk
            if (resolvedObjectPk == null) {
                return RelationshipsMessage()
            }
        }

        var resolvedRelationshipTypePk: Int? = null
        if (relationshipsRequest.relationshipType != null) {
            resolvedRelationshipTypePk = relationshipTypeRegistry.resolve(relationshipsRequest.relationshipType)
            if (resolvedRelationshipTypePk == null) {
                return RelationshipsMessage()
            }
        }

        var resolvedAssertedByPk: Long? = null
        if (relationshipsRequest.assertedBy != null) {
            resolvedAssertedByPk = resolver.resolveRO(IdentifierParser.parse(relationshipsRequest.assertedBy)).pk
            if (resolvedAssertedByPk == null) {
                return RelationshipsMessage()
            }
        }


        var resolvedNotAssertedByPk: Long? = null
        if (relationshipsRequest.notAssertedBy != null) {
            resolvedNotAssertedByPk = resolver.resolveRO(IdentifierParser.parse(relationshipsRequest.notAssertedBy)).pk
            if (resolvedNotAssertedByPk == null) {
                return RelationshipsMessage()
            }
        }


        var resolvedLinkedByPk: Long? = null
        if (relationshipsRequest.linkedBy != null) {
            resolvedLinkedByPk = resolver.resolveRO(IdentifierParser.parse(relationshipsRequest.linkedBy)).pk
            if (resolvedLinkedByPk == null) {
                return RelationshipsMessage()
            }
        }


        var resolvedNotLinkedByPk: Long? = null
        if (relationshipsRequest.notLinkedBy != null) {
            resolvedNotLinkedByPk = resolver.resolveRO(IdentifierParser.parse(relationshipsRequest.notLinkedBy)).pk
            if (resolvedNotLinkedByPk == null) {
                return RelationshipsMessage()
            }
        }

        var subjectRegistrationAgencyPk: Int? = null
        if (relationshipsRequest.subjectRegistrationAgency != null) {
            subjectRegistrationAgencyPk =
                ralLookupDao.getRegistrationAgencyPkFromName(relationshipsRequest.subjectRegistrationAgency)
            if (subjectRegistrationAgencyPk == null) {
                return RelationshipsMessage()
            }
        }

        var objectRegistrationAgencyPk: Int? = null
        if (relationshipsRequest.objectRegistrationAgency != null) {
            objectRegistrationAgencyPk =
                ralLookupDao.getRegistrationAgencyPkFromName(relationshipsRequest.objectRegistrationAgency)
            if (objectRegistrationAgencyPk == null) {
                return RelationshipsMessage()
            }
        }

        var subjectTypePk: Int? = null
        if (relationshipsRequest.subjectCollection != null) {
            subjectTypePk = itemInfoDao.getItemTypePkFromName(relationshipsRequest.subjectCollection)
            if (subjectTypePk == null) {
                return RelationshipsMessage()
            }
        }

        var subjectSubtypePk: Int? = null
        if (relationshipsRequest.subjectType != null) {
            subjectSubtypePk = itemInfoDao.getItemSubtypePkFromName(relationshipsRequest.subjectType)
            if (subjectSubtypePk == null) {
                return RelationshipsMessage()
            }
        }

        var objectTypePk: Int? = null
        if (relationshipsRequest.objectCollection != null) {
            objectTypePk = itemInfoDao.getItemTypePkFromName(relationshipsRequest.objectCollection)
            if (objectTypePk == null) {
                return RelationshipsMessage()
            }
        }

        var objectSubtypePk: Int? = null
        if (relationshipsRequest.objectType != null) {
            objectSubtypePk = itemInfoDao.getItemSubtypePkFromName(relationshipsRequest.objectType)
            if (objectSubtypePk == null) {
                return RelationshipsMessage()
            }
        }

        var resolvedSubjectStewardPk: Long? = null
        if (relationshipsRequest.subjectSteward != null) {
            resolvedSubjectStewardPk =
                resolver.resolveRO(IdentifierParser.parse(relationshipsRequest.subjectSteward)).pk
            if (resolvedSubjectStewardPk == null) {
                return RelationshipsMessage()
            }
        }

        var resolvedObjectStewardPk: Long? = null
        if (relationshipsRequest.objectSteward != null) {
            resolvedObjectStewardPk =
                resolver.resolveRO(IdentifierParser.parse(relationshipsRequest.objectSteward)).pk
            if (resolvedObjectStewardPk == null) {
                return RelationshipsMessage()
            }
        }

        val fetchedRelationships =
            relationshipDao.fetchRelationships(
                relationshipsRequest.cursor,
                resolvedSubjectPk,
                subjectRegistrationAgencyPk,
                resolvedSubjectStewardPk,
                resolvedObjectPk,
                objectRegistrationAgencyPk,
                resolvedObjectStewardPk,
                resolvedRelationshipTypePk,
                relationshipsRequest.fromUpdatedTime,
                relationshipsRequest.untilUpdatedTime,
                relationshipsRequest.rows!!,
                subjectTypePk,
                subjectSubtypePk,
                objectTypePk,
                objectSubtypePk,
                resolvedAssertedByPk,
                resolvedNotAssertedByPk,
                resolvedLinkedByPk,
                resolvedNotLinkedByPk,
            )

        val itemsByItemPk: Map<Long, List<String?>> =
            itemDao.fetchItemIdentifierPairsForItemPks(fetchedRelationships.rows.flatMap {
                setOf(
                    it.subjectItemPk,
                    it.objectItemPk,
                    it.assertedByPk,
                    it.linkedByPk
                )
            }.toSet())
                .groupBy({ it.first },
                    { it.second.uri.toString() })

        val typesByPk = itemInfoDao.getItemTypes()

        val subtypesByPk = itemInfoDao.getItemSubtypes()

        // Return the first identifier found.
        // When we allow multiple identifiers to be present, we must ensure that only the primary one is returned.
        return RelationshipsMessage(
            fetchedRelationships.nextCursor,
            fetchedRelationships.fetchSize,
            fetchedRelationships.rows.map { relStmt ->
                RelationshipView(
                    ItemView(
                        itemsByItemPk[relStmt.subjectItemPk]?.firstOrNull(),
                        relStmt.subj.collectionPk?.let { typesByPk[it] },
                        relStmt.subj.typePk?.let { subtypesByPk[it] }
                    ),
                    ItemView(
                        itemsByItemPk[relStmt.objectItemPk]?.firstOrNull(),
                        relStmt.obj.collectionPk?.let { typesByPk[it] },
                        relStmt.obj.typePk?.let { subtypesByPk[it] }
                    ),
                    relationshipTypeRegistry.reverse(relStmt.relationshipTypePk),
                    relStmt.updatedAt,
                    itemsByItemPk[relStmt.assertedByPk]?.firstOrNull(),
                    itemsByItemPk[relStmt.linkedByPk]?.firstOrNull(),
                )
            })
    }
}
