package org.crossref.manifold.relationship

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.ExampleObject
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.tags.Tag
import org.crossref.manifold.api.relationship.RelationshipsRequest
import org.crossref.manifold.api.relationship.RelationshipsResponse
import org.crossref.manifold.itemgraph.Configuration
import org.crossref.manifold.relationship.Constants.API
import org.crossref.manifold.relationship.Constants.RELATIONSHIPS
import org.crossref.manifold.util.InvalidRequestParamException
import org.crossref.manifold.util.UNKNOWN_REQUEST_PARAMS
import org.crossref.manifold.util.UnknownRequestParamException
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.time.OffsetDateTime
import kotlin.math.max
import kotlin.math.min

/**
 * Parameters are longhand as we mix kebab case with dots, which can't be automatically resolved.
 */
@Tag(name = "Relationships")
@RestController
@RequestMapping("/beta/relationships")
@ConditionalOnProperty(prefix = RELATIONSHIPS, name = [API])
class RelationshipController(
    private val relationshipService: RelationshipService,
    @Value("\${${Configuration.MAX_RETURNED_ROWS}:1000}") private val maxReturnedRows: Int,
    @Value("\${${Configuration.DEFAULT_RETURNED_ROWS}:100}") private val defaultReturnedRows: Int
) {
    @Operation(
        summary = "Retrieve the set of asserted relationships that match the given criteria",
        description = "Retrieve relationships between items. The subject, relationship type and object, along with metadata are returned. " +
                "The relationships endpoint is optimized for use cases that use certain combinations of query filters, " +
                "see [here](https://www.crossref.org/documentation/retrieve-metadata/relationships/#00152) for more details.",
        parameters = [Parameter(
            name = "cursor",
            description = "Positional marker that allows large result sets to be scrolled a page at a time. " +
                    "If none is provided then the first page of results is returned with no cursor pointing to the next page. " +
                    "Use * as the value to return `next-cursor` in the results."
        ),
            Parameter(
                name = "rows",
                description = "The number of rows to be returned in the next page, defaults to 100. Max value is 1000. Min value is 0."
            ),
            Parameter(
                name = "subject.id",
                description = "An identifier for the subject of the relationships. " +
                        "If none is provided then all identifiers are searched."
            ), Parameter(
                name = "subject.collection",
                description = "The collection of the subject item (e.g. `work`, `org`) " +
                        "If none is provided then relationships for all subject items are searched."
            ), Parameter(
                name = "subject.type",
                description = "The type of the subject item, within the collection (e.g. for the `works` collection, type could be `journal-article`, or `book`) " +
                        "If none is provided then relationships for all subject items are searched."
            ), Parameter(
                name = "subject.registration-agency",
                description = "An identifier for the registration agency of the subject of the relationship (according to DOI). " +
                        "Possible values include `Crossref` and `DataCite`. " +
                        "If none is provided then relationships for items registered by any registration agency are searched."
            ), Parameter(
                name = "subject.steward",
                description = "Steward of the subject item. For Crossref DOIs, the steward is the Crossref member who maintains the metadata record."
            ), Parameter(
                name = "object.id",
                description = "An identifier for the object of the relationships. " +
                        "If none is provided then all identifiers are searched."
            ), Parameter(
                name = "object.collection",
                description = "The collection of the object item (e.g. 'work', 'org')" +
                        "If none is provided then relationships for all object items are searched."
            ), Parameter(
                name = "object.type",
                description = "The type of the object item, within the collection (e.g. for the `works` collection, type could be `journal-article`, or `book`)" +
                        "If none is provided then relationships for all object items are searched."
            ), Parameter(
                name = "object.registration-agency",
                description = "An identifier for the registration agency of the object of the relationship (according to DOI). " +
                        "Possible values include `Crossref` and `DataCite`. " +
                        "If none is provided then relationships for items registered by any registration agency are searched."
            ), Parameter(
                name = "object.steward",
                description = "Steward of the object item. For Crossref DOIs, the steward is the Crossref member who maintains the metadata record."
            ), Parameter(
                name = "relationship-type",
                description = "The type of relationship to be searched, e.g. `cites`, `is-supplemented-by`. " +
                        "If none is provided then all relationships are searched."
            ),
            Parameter(
                name = "linked-by",
                description = "Filter for relationships linked by the given party. " +
                        "The linking party is the one that provided the object ID. " +
                        "For example, in some cases a Crossref member deposits reference metadata without a DOI and it is added later by Crossref. " +
                        "Possible values include ROR IDs (e.g. `https://ror.org/02twcfp32` for Crossref) or organization IDs, (e.g. `https://id.crossref.org/org/340` for Plos)"
            ),
            Parameter(
                name = "not-linked-by",
                description = "Filter for relationships not linked by the given party, using an organization ID or ROR ID."
            ),
            Parameter(
                name = "asserted-by",
                description = "Filter for relationships asserted by the given party, using an organization ID, e.g. Crossref member or ROR ID. " +
                        "The asserting party is the one that provided metadata in the subject about the object. If that included the ID they are also the linking party. " +
                        "If another organisation inferred the object ID from the subject metadata, they would be the linking party. " +
                        "Possible values include ROR IDs (e.g. `https://ror.org/02twcfp32` for Crossref) or organization IDs, (e.g. `https://id.crossref.org/org/340` for Plos)",
            ),
            Parameter(
                name = "not-asserted-by",
                description = "Filter for relationships not asserted by the given party, using an organization ID, e.g. Crossref member or ROR id."
            ),
            Parameter(
                schema = Schema(type = "string", format = "date-time"),
                name = "from-updated-time",
                description = "Return relationships asserted after the given ISO-8601 timestamp. Time is optional, assumed to be start-of-day if missing. Interpreted as UTC-Z.",
                examples = [ExampleObject(
                    value = "",
                    summary = "None",
                    name = "Empty string",
                ),
                    ExampleObject(
                        value = "2020-01-01",
                        summary = "Day",
                        name = "January 1st 2020",
                    ),
                    ExampleObject(
                        value = "2020-01-01T23:59:59.999999Z",
                        summary = "Day and time",
                        name = "End of day January 1st 2020",
                    )]
            ),
            Parameter(
                schema = Schema(type = "string", format = "date-time"),
                name = "until-updated-time",
                description = "Return relationships asserted before the given ISO-8601 timestamp. Time is optional, assumed to be start-of-day if missing. Interpreted as UTC-Z.",
                examples = [ExampleObject(
                    value = "",
                    summary = "None",
                    name = "Empty string",
                ),
                    ExampleObject(
                        value = "2030-01-01",
                        summary = "Day",
                        name = "January 1st 2030",
                    ),
                    ExampleObject(
                        value = "2030-01-01T23:59:59.999999Z",
                        summary = "Day and time",
                        name = "End of day January 1st 2030",
                    )]
            )]
    )
    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getRelationships(
        @RequestParam(name = "cursor")
        cursor: String?,

        @RequestParam(name = "rows")
        rows: Int?,

        @RequestParam(name = "subject.id")
        subjectId: String?,

        @RequestParam(name = "subject.collection")
        subjectCollection: String?,

        @RequestParam(name = "subject.type")
        subjectType: String?,

        @RequestParam(name = "subject.steward")
        subjectSteward: String?,

        @RequestParam(name = "subject.registration-agency")
        subjectRegistrationAgency: String?,

        @RequestParam(name = "object.id")
        objectId: String?,

        @RequestParam(name = "object.collection")
        objectCollection: String?,

        @RequestParam(name = "object.type")
        objectType: String?,

        @RequestParam(name = "object.steward")
        objectSteward: String?,

        @RequestParam(name = "object.registration-agency")
        objectRegistrationAgency: String?,

        @RequestParam(name = "relationship-type")
        relationshipType: String?,

        @RequestParam(name = "from-updated-time")
        fromUpdatedTime: OffsetDateTime?,

        @RequestParam(name = "until-updated-time")
        untilUpdatedTime: OffsetDateTime?,

        @RequestParam(name = "asserted-by")
        assertedBy: String?,

        @RequestParam(name = "not-asserted-by")
        notAssertedBy: String?,

        @RequestParam(name = "linked-by")
        linkedBy: String?,

        @RequestParam(name = "not-linked-by")
        notLinkedBy: String?,

        @RequestAttribute(UNKNOWN_REQUEST_PARAMS) unknownRequestParams: Collection<String>
    ): RelationshipsResponse {
        if (unknownRequestParams.isNotEmpty()) {
            throw ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                null,
                UnknownRequestParamException(unknownRequestParams.first())
            )
        }
        val rowsToFetch = rows?.let {
            if (it < 0 || it > maxReturnedRows) {
                throw ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    null,
                    InvalidRequestParamException("rows", it)
                )
            }
            it
        } ?: max(min(defaultReturnedRows, maxReturnedRows), 0)

        val request = RelationshipsRequest(
            cursor,
            rowsToFetch,
            subjectId,
            subjectCollection,
            subjectType,
            subjectSteward,
            subjectRegistrationAgency,
            objectId,
            objectCollection,
            objectType,
            objectSteward,
            objectRegistrationAgency,
            relationshipType,
            fromUpdatedTime,
            untilUpdatedTime,
            assertedBy,
            notAssertedBy,
            linkedBy,
            notLinkedBy
        )

        return RelationshipsResponse(relationshipService.getRelationships(request))
    }
}
