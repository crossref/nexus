package org.crossref.manifold.relationship

object Constants {
    const val RELATIONSHIPS = "relationships"
    const val API = "api"
}
