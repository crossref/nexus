package org.crossref.manifold

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.PropertyNamingStrategies
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import io.swagger.v3.core.jackson.ModelResolver
import org.crossref.manifold.util.ApiParameterArgumentResolver
import org.crossref.manifold.util.NonUrlDoiInterceptor
import org.crossref.manifold.util.UnknownRequestParamInterceptor
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.boot.web.servlet.server.ServletWebServerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource
import org.springframework.messaging.converter.MappingJackson2MessageConverter
import org.springframework.messaging.converter.MessageConverter
import org.springframework.retry.annotation.EnableRetry
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.transaction.support.TransactionSynchronizationManager
import org.springframework.web.method.support.HandlerMethodArgumentResolver
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.util.pattern.PathPatternParser
import java.time.InstantSource
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.sql.DataSource


/**
 * Spring Boot configuration.
 */
@Configuration
@EnableRetry
@EnableAsync
@EnableScheduling
class AppConfiguration : WebMvcConfigurer, AsyncConfigurer {
    /**
     * Servlet web factory, required to smooth interop with transitive dependencies of Cayenne.
     */
    @Bean
    fun servletWebServerFactory(): ServletWebServerFactory? {
        return TomcatServletWebServerFactory()
    }

    @Bean
    fun modelResolver(objectMapper: ObjectMapper): ModelResolver = ModelResolver(objectMapper)

    @Bean
    fun instantSource(): InstantSource = InstantSource.system()

    override fun configureContentNegotiation(configurer: ContentNegotiationConfigurer) {
        configurer.favorParameter(true)
            .parameterName("format")
            .ignoreAcceptHeader(false)
            .defaultContentType(MediaType.APPLICATION_JSON)
            .mediaType("application/json", MediaType.APPLICATION_JSON)
            .mediaType("application/vnd.scholix+json", MediaType("application", "vnd.scholix+json"))
            .mediaType("application/rdf+xml", MediaType("application", "rdf+xml"))
            .mediaType("text/turtle", MediaType("text", "turtle"))
            .mediaType("application/ld+json", MediaType("application", "ld+json"))
            .mediaType("json", MediaType.APPLICATION_JSON)
            .mediaType("scholix", MediaType("application", "vnd.scholix+json"))
            .mediaType("rdf-xml", MediaType("application", "rdf+xml"))
            .mediaType("rdf-turtle", MediaType("text", "turtle"))
            .mediaType("json-ld", MediaType("application", "ld+json"))
    }

    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        configurer.setPatternParser(PathPatternParser())
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(NonUrlDoiInterceptor())
        registry.addInterceptor(UnknownRequestParamInterceptor())
    }

    override fun addArgumentResolvers(resolvers: MutableList<HandlerMethodArgumentResolver>) {
        resolvers.add(ApiParameterArgumentResolver())
    }

    override fun extendMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        for (converter in converters) {
            if (converter is MappingJackson2HttpMessageConverter) {
                converter.objectMapper
                    .registerModule(JavaTimeModule())
                    .registerModule(KotlinModule())
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                    .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

                converter.objectMapper.propertyNamingStrategy = PropertyNamingStrategies.KEBAB_CASE
                converter.objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
                break
            }
        }
    }

    @Bean
    fun objectMapper(messageConverter: MessageConverter): ObjectMapper {
        return if (messageConverter is MappingJackson2MessageConverter) {
            messageConverter.objectMapper
        } else {
            ObjectMapper()
        }
    }

    @Bean
    fun messageConverter(): MessageConverter =
        MappingJackson2MessageConverter()

    override fun getAsyncExecutor(): Executor = Executors.newCachedThreadPool()

    @Bean("rwDataSourceProperties")
    @ConfigurationProperties("spring.datasource.rw")
    fun rwDataSourceProperties(): DataSourceProperties = DataSourceProperties()

    @Bean("roDataSourceProperties")
    @ConfigurationProperties("spring.datasource.ro")
    fun roDataSourceProperties(): DataSourceProperties = DataSourceProperties()

    /*
     * This is an implementation of AbstractRoutingDataSource that routes to a read-only data source if it is used
     * within a read-only transaction, e.g. @Transactional(readOnly = true). Typically the read-only data source will be
     * backed by a read-replica in AWS, allowing read heavy operations to be scaled.
     *
     * The AbstractRoutingDataSource is also wrapped in a LazyConnectionDataSourceProxy. This ensures the jdbc
     * connection is not eagerly fetched at transaction creation time, before its read/write state is known.
     */
    @Bean
    fun dataSource(
        @Qualifier("rwDataSourceProperties") rwDataSourceProperties: DataSourceProperties,
        @Qualifier("roDataSourceProperties") roDataSourceProperties: DataSourceProperties
    ): DataSource = LazyConnectionDataSourceProxy(object : AbstractRoutingDataSource() {
        init {
            setTargetDataSources(
                mapOf(
                    true to roDataSourceProperties
                        .initializeDataSourceBuilder()
                        .build(),
                    false to rwDataSourceProperties
                        .initializeDataSourceBuilder()
                        .build()
                )
            )
            afterPropertiesSet()
        }

        override fun determineCurrentLookupKey(): Any =
            TransactionSynchronizationManager.isCurrentTransactionReadOnly()
    })
}
