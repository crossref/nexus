package org.crossref.manifold.util

import com.fasterxml.jackson.databind.JsonNode
import com.networknt.schema.JsonSchemaFactory
import com.networknt.schema.OutputFormat
import com.networknt.schema.PathType
import com.networknt.schema.SchemaValidatorsConfig
import com.networknt.schema.SpecVersion.VersionFlag
import com.networknt.schema.output.OutputUnit
import com.networknt.schema.resource.SchemaMappers


object JsonValidator {
    // load JSON schemas from local file system, not over the network (including schemas from $ref's)
    private val jsonSchemaFactory =
        JsonSchemaFactory.getInstance(VersionFlag.V7) { builder: JsonSchemaFactory.Builder ->
            builder.schemaMappers { schemaMappers: SchemaMappers.Builder ->
                schemaMappers.mapPrefix(
                    "https://crossref.org/item-graph/schemas/",
                    "classpath:schema/"
                )
            }
        }
    private val config = SchemaValidatorsConfig().apply {
        pathType = PathType.JSON_PATH
        isFailFast = false
    }
    private val convertRequestSchema = jsonSchemaFactory.getSchema(
        this::class.java.getResourceAsStream("/schema/api-convert-request.schema.json"),
        config
    )
    private val itemSchema = jsonSchemaFactory.getSchema(
        this::class.java.getResourceAsStream("/schema/item-types.schema.json"),
        config
    )

    init {
        // by default all schemas are preloaded eagerly but $ref resolve failures are not thrown, so check them
        convertRequestSchema.initializeValidators()
        itemSchema.initializeValidators()
    }

    fun validateConvertRequest(input: JsonNode): OutputUnit {
        return convertRequestSchema.validate(input, OutputFormat.HIERARCHICAL)
    }

    fun validateItem(input: JsonNode): OutputUnit {
        return itemSchema.validate(input, OutputFormat.HIERARCHICAL)
    }
}