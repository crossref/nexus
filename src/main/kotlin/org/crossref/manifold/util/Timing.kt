package org.crossref.manifold.util

import org.slf4j.Logger

/**
 * Run a function and log the duration it took.
 * Useful for routines we may want to performance tweak.
 */
fun <T> logDuration(logger: Logger, name: String, f: (() -> T)) : T {
    val startTime = System.currentTimeMillis()
    logger.debug("Start $name...")
    val result = f()
    val duration = (System.currentTimeMillis() - startTime) / 1000
    logger.debug("Finished $name, took $duration seconds.")
    return result
}
