package org.crossref.manifold.util

import com.fasterxml.jackson.databind.PropertyNamingStrategies
import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.core.MethodParameter
import org.springframework.core.convert.converter.Converter
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.context.request.NativeWebRequest
import org.springframework.web.method.HandlerMethod
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.DispatcherServlet
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.HandlerMapping
import org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor
import org.springframework.web.servlet.view.RedirectView
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException
import java.time.temporal.TemporalQueries
import javax.servlet.RequestDispatcher
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * When a URL path is supplied with a non-URL DOI, redirect back to the same path but with the full DOI URL.
 */
class NonUrlDoiInterceptor : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE)?.let {
            val maybeDoi = ((it as Map<*, *>).getOrDefault("identifier", "~") as String).substring(1)
            return if (maybeDoi.startsWith("10.")) {
                response.status = HttpServletResponse.SC_MOVED_PERMANENTLY
                response.setHeader("Location", "${request.requestURI.removeSuffix(maybeDoi)}https://doi.org/$maybeDoi")
                false
            } else {
                true
            }
        }
        return true
    }
}

const val UNKNOWN_REQUEST_PARAMS = "UNKNOWN_REQUEST_PARAMS"

/**
 * Collects all request parameter names that are not explicitly annotated with @RequestParam in the Handler method and
 * adds them to the request as an attribute. The Handler method can then decide what to do with them.
 */
class UnknownRequestParamInterceptor : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        if (handler is HandlerMethod) {
            val knownParams =
                handler.methodParameters.mapNotNull { it.getParameterAnnotation(RequestParam::class.java) }
                    .map { it.name }.toSet()
            request.setAttribute(UNKNOWN_REQUEST_PARAMS, request.parameterNames.toList().subtract(knownParams))
        }
        return true
    }
}

class UnknownRequestParamException(val name: String) : Exception()

class InvalidRequestParamException(val name: String, val value: Any) : Exception()

/**
 * Our REST API uses both kebab case request parameters and dots, but Spring MVC does not support these out of the box
 * when instantiating [org.springframework.web.bind.annotation.ModelAttribute]s.
 *
 * For example "full-text.type" should be resolved to fullTextType.
 *
 * In conjunction with [ApiParameter] this argument resolver adds that support.
 */
class ApiParameterArgumentResolver : ServletModelAttributeMethodProcessor(false) {
    override fun supportsParameter(parameter: MethodParameter): Boolean =
        parameter.hasParameterAnnotation(ApiParameter::class.java)

    override fun resolveConstructorArgument(paramName: String, paramType: Class<*>, request: NativeWebRequest): Any? =

        // If needed, apply both strategies, passing the result through.
        super.resolveConstructorArgument(paramName, paramType, request)
            ?: request.getParameter(PropertyNamingStrategies.KebabCaseStrategy().translate(paramName))
}

/**
 * To be used in place of [org.springframework.web.bind.annotation.ModelAttribute] when support for mixed kebab case
 * and dotted request parameters, or dots in parameter names, is required.
 */
@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
@MustBeDocumented
annotation class ApiParameter

@Component
class StringToOffsetDateTimeConverter : Converter<String, OffsetDateTime> {
    override fun convert(source: String): OffsetDateTime = parseToOffsetDateTime(
        source,
        DateTimeFormatter.ISO_LOCAL_DATE
    ) ?: parseToOffsetDateTime(
        source,
        DateTimeFormatter.ISO_LOCAL_DATE_TIME
    ) ?: OffsetDateTime.parse(source)

    private fun parseToOffsetDateTime(source: String, formatter: DateTimeFormatter): OffsetDateTime? =
        try {
            formatter.parse(source).let { temporal ->
                temporal.query(TemporalQueries.localDate()).let { date ->
                    (temporal.query(TemporalQueries.localTime()) ?: LocalTime.MIDNIGHT).let { time ->
                        OffsetDateTime.of(date, time, ZoneOffset.UTC)
                    }
                }
            }
        } catch (e: DateTimeParseException) {
            null
        }
}

/*
 * Controller that handles requests to the /beta URL (which isn't the application root).
 */
@RestController
@RequestMapping("/beta")
class BetaUrlController {
    @RequestMapping
    fun redirectToSwaggerUI(): RedirectView = RedirectView("/beta/swagger-ui/index.html")
}

@RestController
@RequestMapping(path = ["\${server.error.path:\${error.path:/error}}"], produces = [MediaType.APPLICATION_JSON_VALUE])
class ManifoldErrorController : ErrorController {
    @RequestMapping
    fun error(request: HttpServletRequest): ResponseEntity<Map<String, Any>> =
        (request.getAttribute(DispatcherServlet.EXCEPTION_ATTRIBUTE) as Exception?)?.let {
            // If an exception was thrown by a controller, process it.
            getResponseEntity(it)
        } ?: getStatus(request).let {
            // Fallback error response based on HTTP status code.
            ResponseEntity(
                mapOf(
                    "status" to "error",
                    "message-type" to it.name.lowercase().replace('_', '-'),
                    "message-version" to "1.0.0",
                    "message" to it.name.lowercase().replace('_', ' ').replaceFirstChar(Char::titlecase)
                ),
                it
            )
        }

    private fun getStatus(request: HttpServletRequest): HttpStatus =
        (request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE) as Int?)?.let { HttpStatus.resolve(it) }
            ?: HttpStatus.INTERNAL_SERVER_ERROR

    private fun getResponseEntity(ex: Exception): ResponseEntity<Map<String, Any>> =
        ResponseEntity(
            mapOf(
                "status" to "failed",
                "message-type" to "validation-failure",
                "message-version" to "1.0.0",
                "message" to when (ex) {
                    // Thrown when a param value cannot be deserialized to the correct type.
                    is MethodArgumentTypeMismatchException -> mapOf(
                        "type" to "value-not-valid",
                        "parameter" to ex.name,
                        "value" to ex.value
                    )

                    // Can be thrown for a number of reasons.
                    is ResponseStatusException -> when (val cause = ex.cause) {
                        is UnknownRequestParamException -> mapOf(
                            "type" to "unknown-parameter",
                            "value" to cause.name
                        )

                        is InvalidRequestParamException -> mapOf(
                            "type" to "value-not-valid",
                            "parameter" to cause.name,
                            "value" to cause.value
                        )

                        // Something else.
                        else -> mapOf("type" to ex.status, "message" to ex.reason)
                    }

                    // A controller threw an unhandled exception.
                    else -> mapOf("type" to "unknown-error", "message" to ex.message)
                }
            ), if (ex is ResponseStatusException) ex.status else HttpStatus.BAD_REQUEST
        )
}
