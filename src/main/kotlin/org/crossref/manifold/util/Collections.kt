package org.crossref.manifold.util

/**
 * From this iterable, subtract all items from this set that are present in the other set, produced by the selector
 * function applied ot each entry.
 */
fun <T, K> Iterable<T>.subtractBy(other: Set<T>, selector: (T) -> K): Set<T> {
    val visited = other.mapTo(HashSet(), selector)
    val result = mutableSetOf<T>()
    for (e in this) {
        val key = selector(e)
        if (visited.add(key)) {
            result.add(e)
        }
    }

    return result
}
