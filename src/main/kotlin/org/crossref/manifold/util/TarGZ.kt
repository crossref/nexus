package org.crossref.manifold.util

import org.apache.commons.compress.archivers.ArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import org.crossref.manifold.modules.unixml.support.MAX_NUM_FILES_TO_INGEST
import org.crossref.manifold.modules.unixml.support.SKIP_FILES
import org.crossref.manifold.modules.unixml.support.logger
import java.io.BufferedInputStream
import java.io.File
import java.io.InputStreamReader
import java.io.Reader

suspend fun scanTarGz(archiveFile: File, f: suspend (filename: String, ir: Reader) -> Unit){
    scanTarGz(archiveFile, SKIP_FILES, MAX_NUM_FILES_TO_INGEST, f)
}

suspend fun scanTarGz(archiveFile: File, skipFiles: Int, maxNumFilesToProcess: Int, f: suspend (filename: String, ir: Reader) -> Unit) {
    try {

        val startTime = System.currentTimeMillis()

        archiveFile.inputStream().use { reader ->
            val gzStream = GzipCompressorInputStream(BufferedInputStream(reader))

            gzStream.use { gz ->

                val input = TarArchiveInputStream(gz)
                var entry: ArchiveEntry?
                var stop = false
                var count = 0

                logger.info("TGZ: Start reading file: ${archiveFile.name}")
                InputStreamReader(input).use { entryReader ->
                    while (run {
                            entry = input.nextTarEntry
                            entry
                        } != null && !stop) {
                        if (entry != null) {
                            val theEntry = entry!!

                            // Ignore directories. Every other file is considered to be relevant.
                            if (!theEntry.isDirectory) {

                                // This is used for debugging. Usually set to 0.
                                if (count >= skipFiles) {
                                    if (count % 10000 == 0) {

                                        val duration = (System.currentTimeMillis() - startTime) / 1000
                                        val averageRate = (count.toFloat() / duration.toFloat()).toInt()
                                        logger.info("Read $count files from ${archiveFile.name} in $duration seconds, $averageRate per second")
                                    }
                                    if (count > maxNumFilesToProcess) {
                                        stop = true
                                        logger.info("TGZ Stopping, reached $maxNumFilesToProcess entries.")
                                    } else {
                                        f(theEntry.name, entryReader)
                                    }
                                }
                                count++
                            }
                        }

                    }
                }

            }
        }
        logger.info("TGZ: Finished reading file: ${archiveFile.name}")
    } catch (e: Exception) {
        logger.warn("TGZ: Error! $e")
        e.printStackTrace()
    } finally {
        logger.info("TGZ: Finished.")
    }
}
