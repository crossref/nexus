package org.crossref.manifold.matching.strategies

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.matching.Configuration
import org.crossref.manifold.matching.MatcherContext
import org.crossref.manifold.matching.MatcherContext.Companion.DOI_ASSERTED_BY_KEY
import org.crossref.manifold.matching.MatcherContext.Companion.DOI_ASSERTED_BY_VALUE_CROSSREF
import org.crossref.manifold.matching.MatcherContext.Companion.OBJECT_REL_TYPE
import org.crossref.manifold.matching.MatchingStrategy
import org.crossref.manifold.matching.RelationshipMatch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.OffsetDateTime



/**
 * Find relationships represented as reified relationships.
 * Where we find(subj-item, relationship-type, reified-item, "object", obj-item), we assert (subj-item, relationship-type, obj-item).
 * Assertions are made in matching scope 2.
 */
@Component
class ReifiedMatcher(
) : MatchingStrategy {
    companion object {
        const val SCOPE = 2
    }

    override fun partition(): Configuration.QueuePartition = Configuration.QueuePartition.BASIC

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun match(
        itemTree: ItemTree,
        assertionPk: Long,
        assertedAt: OffsetDateTime,
        assertingPartyPk: Long,
        context: MatcherContext,
    ): List<RelationshipMatch> =
        if (itemTree.root.pk != null) {

            val rootNodePk = itemTree.root.pk

            itemTree.getNodes().flatMap { (subjItem, _) ->
                if (subjItem.pk != null) {
                    val subjectPk = subjItem.pk

                    subjItem.rels.flatMap { relationship ->
                        val relType = relationship.relTypePk
                        val maybeReified = relationship.obj
                        val objectLinks = maybeReified.rels.filter { it.relTyp == OBJECT_REL_TYPE }.map { it.obj }
                        val linkingParty = getLinkingParty(maybeReified, assertingPartyPk, context)

                        if (linkingParty == null) {
                            logger.error("Can't build linking party")
                            emptyList()
                        } else {
                        objectLinks.map {
                            val position = itemTree.getPosition(it)

                            if (it.pk != null && position != null && relType != null) {
                                RelationshipMatch(
                                    assertingPartyPk,
                                    assertedAt,
                                    rootNodePk,
                                    assertionPk,
                                    position,
                                    linkingParty,
                                    SCOPE,
                                    subjectPk,
                                    relType,
                                    it.pk,
                                    true
                                )
                            } else null
                        }
                    }
                    }
                } else emptyList()
            }.filterNotNull()
        } else emptyList()

    /** The 'doi-asserted-by' is how the Crossref matcher in the legacy content registration system
     * indicates reference that it matched (via the <doi provider='crossref'> attribute).
     * If this is set, record that it was Crossref that linked it.
     */
    private fun getLinkingParty(
        reifiedRelationshipItem: Item,
        assertingPartyPk: Long,
        context: MatcherContext
    ): Long? {
        // This is a critical error. We rely on this not being null.
        val assertedBy =
            reifiedRelationshipItem.properties.firstNotNullOfOrNull { it.values[DOI_ASSERTED_BY_KEY]?.textValue() }

        return if (assertedBy == DOI_ASSERTED_BY_VALUE_CROSSREF) {
            context.crossrefPk
        } else {
            assertingPartyPk
        }
    }
}