package org.crossref.manifold.matching.strategies

import org.crossref.manifold.identifiers.parsers.Doi
import org.crossref.manifold.itemgraph.RelationshipDao
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.matching.Configuration
import org.crossref.manifold.matching.MatcherContext
import org.crossref.manifold.matching.MatcherContext.Companion.FUNDER_REL
import org.crossref.manifold.matching.MatchingStrategy
import org.crossref.manifold.matching.RelationshipMatch
import org.crossref.manifold.registries.RelationshipTypeRegistry
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

/**
 * Funding relationships are asserted using Funder IDs. The ROR registry file includes mappings.
 * Use these to additionally assert funding relationships using the ROR id where found.
 *
 * Funding relationships are currently simple, not reified.
 * They will be refactored to be reified at some point in the future, and this code will need to change.
 * See note in funding-relationships.feature
 *
 * Assertions are made in matching scope 3.
 */
@Component
class RorFunderMigration(
    val resolver: Resolver,
    val relationshipDao: RelationshipDao,
    val relationshipTypeRegistry: RelationshipTypeRegistry,
) : MatchingStrategy {
    companion object {
        const val SCOPE = 3
    }

    override fun partition(): Configuration.QueuePartition = Configuration.QueuePartition.ROR

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    override fun match(
        itemTree: ItemTree,
        assertionPk: Long,
        assertedAt: OffsetDateTime,
        assertingPartyPk: Long,
        context: MatcherContext,
    ): List<RelationshipMatch> =

        if (itemTree.root.pk != null) {

            val sameAs = relationshipTypeRegistry.resolve("same-as")
            val funderRelId = relationshipTypeRegistry.resolve(FUNDER_REL)

            if (sameAs == null) {
                logger.error("Can't resolve 'same-as' relationship.")
            }

            if (funderRelId == null) {
                logger.error("Can't resolve 'funder' relationship.")
            }

            if (funderRelId == null) {
                logger.error("Can't resolve 'Crossref' item.")
            }

            if (sameAs != null && funderRelId != null) {

                itemTree.getNodes().flatMap { (subject, nodePosition) ->

                    val fundingRelationships = subject.rels.filter { rel ->
                        rel.relTyp == FUNDER_REL && rel.obj.identifiers.firstOrNull()?.let { Doi.isFunderDoi(it) } ?: false
                    }

                    val fundrefPks =
                        fundingRelationships.mapNotNull { resolver.resolveRO(it.obj.identifiers.first()).pk }

                    val equivalents = fundrefPks.flatMap {
                        relationshipDao.fetchSimpleRelationships(
                            objectItemPk = it,
                            relationshipTypePk = sameAs,
                            assertedByPk = context.crossrefPk).map { it.subjectItemPk }
                    }

                    equivalents.mapNotNull {
                        // Subject might have no PK, e.g. for reified relationships that don't have a PK.
                        // These shouldn't (and can't) be recorded as relationships.
                        if (subject.pk != null) {
                            RelationshipMatch(
                                assertingPartyPk,
                                assertedAt,
                                itemTree.root.pk,
                                assertionPk,
                                nodePosition,
                                context.crossrefPk,
                                SCOPE,
                                subject.pk,
                                funderRelId,
                                it,
                                true
                            )
                        } else null

                    }
                }
            } else emptyList()

        } else emptyList()
}