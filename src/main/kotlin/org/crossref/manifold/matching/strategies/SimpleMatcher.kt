package org.crossref.manifold.matching.strategies

import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.matching.Configuration
import org.crossref.manifold.matching.MatcherContext
import org.crossref.manifold.matching.MatchingStrategy
import org.crossref.manifold.matching.RelationshipMatch
import org.springframework.stereotype.Component
import java.time.OffsetDateTime

/**
 * Return all simple relationships found in the item tree.
 * Matches are made in matching scope 1.
 */
@Component
class SimpleMatcher : MatchingStrategy {
    companion object {
        const val SCOPE = 1
    }

    override fun partition(): Configuration.QueuePartition = Configuration.QueuePartition.BASIC

    override fun match(
        itemTree: ItemTree,
        assertionPk: Long,
        assertedAt: OffsetDateTime,
        assertingPartyPk: Long,
        context: MatcherContext,
    ): List<RelationshipMatch> =
        (if (itemTree.root.pk != null) {
            val rootNodePk = itemTree.root.pk

            itemTree.getNodes().flatMap { (node, position) ->
                if (node.pk != null) {
                    val subjectPk = node.pk

                    node.rels.map { rel ->
                        if (rel.obj.pk != null && rel.relTypePk != null) {
                            val objectPk = rel.obj.pk

                            RelationshipMatch(
                                assertingPartyPk,
                                assertedAt,
                                rootNodePk,
                                assertionPk,
                                position,

                                // Match asserting party is the same as the item tree asserting party.
                                assertingPartyPk,
                                SCOPE,
                                subjectPk,
                                rel.relTypePk,
                                objectPk,
                                true
                            )
                        } else null
                    }
                } else emptyList()
            }
        } else emptyList()).filterNotNull()
}