package org.crossref.manifold.matching


import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.crossref.manifold.itemgraph.ItemTreeAssertionDao
import org.crossref.manifold.itemgraph.ItemTreeUpdateQueueDao
import org.crossref.manifold.itemgraph.RelationshipMatchDao
import org.crossref.manifold.itemtree.ItemTreeService
import org.crossref.manifold.matching.Configuration.MATCHING
import org.crossref.manifold.matching.Configuration.PARTITIONS
import org.crossref.manifold.relationship.RelationshipSyncDao
import org.crossref.manifold.util.Constants.BATCH_SIZE
import org.crossref.manifold.util.Constants.ENABLED
import org.crossref.manifold.util.Constants.FIXED_DELAY
import org.crossref.manifold.util.Constants.INITIAL_DELAY
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit

@Service
@ConditionalOnProperty(prefix = MATCHING, name = [ENABLED])
class MatcherService(
    @Value("\${$MATCHING.$BATCH_SIZE:100}") private val batchSize: Int,

    @Value("\${$MATCHING.$PARTITIONS:}") private val partitions: List<String>?,

    private val queueDao: ItemTreeMatchQueueDao,
    private val itemTreeAssertionDao: ItemTreeAssertionDao,
    private val relationshipMatchDao: RelationshipMatchDao,
    private val matcher: Matcher,
    private val itemTreeService: ItemTreeService,
    private val itemTreeUpdateQueueDao: ItemTreeUpdateQueueDao,
    private val relationshipSyncDao: RelationshipSyncDao
    ) {
    var logger: Logger = LoggerFactory.getLogger(this::class.java)
    val mapper = jacksonObjectMapper()

    @Scheduled(
        initialDelayString = "\${$MATCHING.$INITIAL_DELAY:0}",
        fixedDelayString = "\${$MATCHING.$FIXED_DELAY:1}",
        timeUnit = TimeUnit.SECONDS
    )
    fun run() {
        if (batchSize > 200) {
            logger.error("MATCHING_BATCH_SIZE too large could cause 'out of range integer' SQL errors.")
        }

        if (partitions == null) {
            // This check also guarantees that `partitions` is non-null later on.
            logger.error("Supplied MATCHING_ENABLED but didn't supply MATCHING_PARTITIONS, so no matching will run.")
            return
        }

        logger.debug("Checking for item tree assertions to be matched")

        // Build a context of common values for each run.
        // These are not hard-coded at bean startup in case the value changes during runtime.
        // Unlikely, but as the context contains references to items, make sure we look up the most up to date
        // throughout the operation.
        val context = matcher.buildContext()

        // Round-robin each partition that this instance has been assigned.
        for (partitionName in partitions) {
            Configuration.QueuePartition.forName(partitionName)?.let {
                partition ->

                try {
                    var lastBatchCount: Int? = null
                    while (lastBatchCount == null || lastBatchCount > 0) {

                        // Make sure we terminate the loop if there's a failure polling.
                        lastBatchCount = 0

                        // This code block is run in a transaction by the DAO code, so it can only see database state that
                        // exists before each queue batch is processed.
                        queueDao.getFromQueue(batchSize, partition.partitionId) { itemTreeAssertionPks ->

                            // Set the count, so we know whether to loop next time.
                            lastBatchCount = itemTreeAssertionPks.count()

                            logger.debug("Got ${itemTreeAssertionPks.count()} from matcher queue. Of which ${itemTreeAssertionPks.count()} distinct.")

                            if (itemTreeAssertionPks.isNotEmpty()) {
                                // It's possible for property assertions to be replaced (and deleted from 'current' table) before the queue processor got to them.
                                // So be tolerant of getting fewer back than we requested.
                                val compactTrees = itemTreeAssertionDao.getByAssertionPks(itemTreeAssertionPks)

                                logger.debug("From ${itemTreeAssertionPks.count()} assertion pks, found ${compactTrees.count()} assertions still current.")

                                val fullTrees = itemTreeService.fromCompact(compactTrees.map { it.first.tree })
                                val entries = compactTrees.zip(fullTrees)

                                val matches = if (context == null) {
                                    // This won't happen unless Crossref is deleted from the database, or we fail to resolve it
                                    // somehow. Don't attempt to re-process matching.
                                    logger.error("Couldn't build match context, skipping matching!")
                                    emptyList()
                                } else {
                                    entries.flatMap {
                                        val itemTree = it.second
                                        val (assertion, assertionPk) = it.first

                                        matcher.extractAllMatches(
                                            partition,
                                            itemTree,
                                            assertionPk,
                                            assertion.assertedAt,
                                            assertion.assertingPartyPk,
                                            context
                                        )
                                    }
                                }

                                val syncId = relationshipSyncDao.newRelationshipSync()
                                relationshipMatchDao.assertMatches(matches, syncId)
                                relationshipSyncDao.relationshipSyncReady(syncId)

                                // The matcher is responsible for enqueueing updates. Both for Item Trees where there was a
                                // match, but also if there wasn't.
                                val itemTreesToUpdate = (
                                        entries.mapNotNull { it.second.root.pk } + matches.map { it.subjectPk }).toSet()
                                itemTreeUpdateQueueDao.enqueueAffectedTrees(itemTreesToUpdate)
                            }
                        }
                    }
                } catch (e: Exception) {
                    logger.error("Error matching: $e", e)
                }
            }
        }
    }
}
