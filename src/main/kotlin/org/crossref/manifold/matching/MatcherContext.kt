package org.crossref.manifold.matching

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.modules.consts.Items

/**
 * Immutable data that's useful to all matching strategies.
 * Must be constructed each run of matches.
 */
data class MatcherContext (
    val crossrefPk : Long
) {
    companion object {
        val CROSSREF_ID = IdentifierParser.parse(Items.CROSSREF_AUTHORITY)

        /**
         * This key and value is found in the item tree as asserted by the parser in Cayenne.
         */
        const val DOI_ASSERTED_BY_KEY = "doi-asserted-by"
        const val DOI_ASSERTED_BY_VALUE_CROSSREF = "crossref"

        const val FUNDER_REL = "funder"
        const val OBJECT_REL_TYPE = "object"
    }
}
