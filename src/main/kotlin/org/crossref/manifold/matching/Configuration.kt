package org.crossref.manifold.matching

object Configuration {
    const val MATCHING = "matching"
    const val PARTITIONS = "partitions"
    // For enabling and pointing to a preprint matching API
    const val PREPRINT_API = "preprint-api"

    /**
     * Partitions for the matching queue.
     *
     * Whenever a new partition is added, be sure to add its ID to the `item_tree_assertion_trigger_f` function.
     *
     * @param[partitionName] name of the partition name, used by configuration.
     * @param[partitionId] ID in the database.
     */
    enum class QueuePartition(val partitionName : String, val partitionId: Int) {
        /**
         * Matching strategies where minimal computation and no external data is required to match, so these are fast.
         */
        BASIC("basic", 1),

        /**
         * For ROR matching.
         */
        ROR("ror", 2),

        PREPRINT("preprint", 3);

        companion object {
            fun forName(partitionName: String): QueuePartition? =
                QueuePartition.values().firstOrNull { it.partitionName == partitionName }
        }
    }
}