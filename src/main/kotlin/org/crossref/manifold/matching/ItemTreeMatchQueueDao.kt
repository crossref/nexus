package org.crossref.manifold.matching

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import java.sql.ResultSet

/**
 * Queue for matching item tree assertions.
 * Currently, the queue is populated by a SQL trigger on item_tree_assertion
 */
@Repository
class ItemTreeMatchQueueDao(
    jdbcTemplate: JdbcTemplate,
) {
    private val npTemplate: NamedParameterJdbcTemplate = NamedParameterJdbcTemplate(jdbcTemplate)
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    /**
     * Get current length of queue.
     */
    fun queueLength(): Long =
        npTemplate.query("SELECT count(item_tree_assertion_pk) AS count FROM item_tree_match_queue") { rs: ResultSet, _: Int ->
            rs.getLong(
                "count"
            )
        }.first()

    /**
     * Get from the queue and call callback function with PKs.
     * Always call callback, even if there are no results.
     * This enables the callback to notice and stop polling if it wants.
     */
    @Transactional
    fun getFromQueue(count: Int, partition: Int, f: ((itemTreeAssertionPkBatch: Set<Long>) -> Unit)) {
        val params = mapOf("count" to count, "partition" to partition)

        val itemPks = npTemplate.query(
            """            
            DELETE FROM item_tree_match_queue
            WHERE pk IN (
              SELECT pk
              FROM item_tree_match_queue
              WHERE partition = :partition
              FOR UPDATE SKIP LOCKED
              LIMIT :count
            )
            RETURNING item_tree_assertion_pk;
        """.trimIndent(),
            params
        ) { rs: ResultSet, _: Int -> rs.getLong("item_tree_assertion_pk") }

        val distinct = itemPks.toSet()
        logger.debug("Dequeue ${itemPks.count()} of which ${distinct.count()} distinct.")

        f(distinct)
    }

}