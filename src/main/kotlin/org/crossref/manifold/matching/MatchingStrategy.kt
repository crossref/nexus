package org.crossref.manifold.matching

import org.crossref.manifold.itemtree.ItemTree
import java.time.OffsetDateTime

interface MatchingStrategy {
    /**
     * Perform a match on the given Item Tree, returning a list of matches.
     */
    fun match(
        itemTree: ItemTree,
        assertionPk: Long,
        assertedAt: OffsetDateTime,
        assertingPartyPk: Long,
        context: MatcherContext,
    ): List<RelationshipMatch>

    /**
     * Which partition this matching strategy should be included in.
     */
    fun partition(): Configuration.QueuePartition
}