package org.crossref.manifold

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.servers.Server
import kotlinx.coroutines.DEBUG_PROPERTY_NAME
import kotlinx.coroutines.DEBUG_PROPERTY_VALUE_ON
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan

/**
 * Spring Boot application for Manifold.
 */
@OpenAPIDefinition(
    servers = [Server(url = "/")]
)@SpringBootApplication(
    // Required to exclude Cayenne's incompatible transitive dependencies, for now.
    exclude = [MongoAutoConfiguration::class, MongoDataAutoConfiguration::class]
)
// @ConfigurationPropertiesScan is required
// to enable us to use values instead of variables
// in configuration data classes
@ConfigurationPropertiesScan
class CrossrefManifoldApplication

/**
 * Main method to run the app.
 */
fun main(args: Array<String>) {
    // Keep these installed for debugging coroutines.
    System.setProperty(DEBUG_PROPERTY_NAME, DEBUG_PROPERTY_VALUE_ON)

    try {
        // https://vividcode.io/fixing-spring-boot-error-unable-to-start-servletwebserverapplicationcontext-due-to-missing-servletwebserverfactory-bean/
        val app = SpringApplication(CrossrefManifoldApplication::class.java)
        app.run(*args)
    } catch (e: java.lang.Exception) {
        e.printStackTrace()
    }
}
