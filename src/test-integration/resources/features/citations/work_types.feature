Feature: For DOIs where the RA is DataCite, the Item Graph type is set to ‘work’ and ‘subtype’ is found by querying the DataCite API. These will appear in the relationships API endpoint.  The subtype is looked up in the DataCite API, selecting the value of the field types;resourceTypeGeneral.

  Scenario: Crossref asserts a relationship to a DataCite DOI with no previous type assertion.

    Given "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.5555"
    And "Dataset" is the type for DOI "10.6084/m9.figshare.21716298.v1"
    And UniXSD file "citations/work_types/org-7822-citation.xml" has been ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                                       | Obj Collection | Obj Type | Asserted By                      |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.6084/m9.figshare.21716298.v1 | work           | Dataset  | https://id.crossref.org/org/7822 |

  Scenario: Crossref asserts a relationship to a Datacite DOI that has a previous type assertion.

    Given "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.5555"
    And "Dataset" is the type for DOI "10.6084/m9.figshare.21716298.v1"
    And UniXSD file "citations/work_types/org-7822-citation.xml" has been ingested
    And UniXSD file "citations/work_types/org-7823-citation.xml" has been ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced


    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                                       | Obj Collection | Obj Type | Asserted By                      |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.6084/m9.figshare.21716298.v1 | work           | Dataset  | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.6084/m9.figshare.21716298.v1 | work           | Dataset  | https://id.crossref.org/org/7823 |

  Scenario: DataCite asserts a relationship from a DataCite DOI without a type to a Crossref DOI.

    Given "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.5555"
    And "Software" is the type for DOI "10.6084/m9.figshare.21716298.v1"
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/work_types/citation-without-type.json"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested

    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                      | Subj Collection | Subj Type | Relationship Type | Object ID                        | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.21716298.v1 | work            | Software  | cites             | https://doi.org/10.5555/12345678 | https://ror.org/04wxnsj81 |

  Scenario: DataCite asserts a relationship from a DataCite DOI with a type to a Crossref DOI.

    Given "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.5555"
    And "Software" is the type for DOI "10.6084/m9.figshare.21716298.v1"
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/work_types/citation-with-type.json"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested

    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced


    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                      | Subj Collection | Subj Type | Relationship Type | Object ID                        | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.21716298.v1 | work            | Dataset   | cites             | https://doi.org/10.5555/12345678 | https://ror.org/04wxnsj81 |
