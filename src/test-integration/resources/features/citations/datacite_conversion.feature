Feature: Datacite events archive conversion

  We want to be able to convert archives of Datacite events.
  For now the archives contain one JSON file per event.

  We also want to be able to filter out Datacite to Datacite events.

  Rule We will only process Datacite to Crossref events and reject everything else


  Scenario: Historical format 1
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-f.json"
    And "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.1186"
    And "Software" is the type for DOI "10.6084/m9.figshare.c.3705574"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced
    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                    | Subj Collection | Subj Type | Relationship Type | Object ID                                 | Obj Collection | Obj Type | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.c.3705574 | work            | Software  | is_supplement_to  | https://doi.org/10.1186/s12882-017-0486-9 |                |          | https://ror.org/04wxnsj81 |

  Scenario: Historical format 2
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-a.json"
    And "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.1080"
    And "Software" is the type for DOI "10.6084/m9.figshare.c.3705574"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                    | Subj Collection | Subj Type | Relationship Type | Object ID                                    | Obj Collection | Obj Type | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.c.3705574 | work            | Software  | is_supplement_to  | https://doi.org/10.1080/15384101.2014.998047 |                |          | https://ror.org/04wxnsj81 |

  Scenario: Historical format 3
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-b.json"
    And "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.5194"
    And "Software" is the type for DOI "10.6084/m9.figshare.c.3705574"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                    | Subj Collection | Subj Type | Relationship Type | Object ID                               | Obj Collection | Obj Type | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.c.3705574 | work            | Software  | is_supplement_to  | https://doi.org/10.5194/essd-8-663-2016 |                |          | https://ror.org/04wxnsj81 |

  Scenario: Historical format 4
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-c.json"
    And "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.5281"
    And "Software" is the type for DOI "10.6084/m9.figshare.c.3705574"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                    | Subj Collection | Subj Type   | Relationship Type | Object ID                              | Obj Collection | Obj Type | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.c.3705574 | work            | ImageObject | has_version       | https://doi.org/10.5281/zenodo.2529064 |                |          | https://ror.org/04wxnsj81 |

  Scenario: Historical format 5
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-d.json"
    And "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.5281"
    And "Software" is the type for DOI "10.6084/m9.figshare.c.3705574"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                    | Subj Collection | Subj Type | Relationship Type | Object ID                              | Obj Collection | Obj Type | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.c.3705574 | work            | Software  | has_version       | https://doi.org/10.5281/zenodo.8231448 |                |          | https://ror.org/04wxnsj81 |

  Scenario: Historical format 6
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-e.json"
    And "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.5281"
    And "Software" is the type for DOI "10.6084/m9.figshare.c.3705574"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                    | Subj Collection | Subj Type | Relationship Type | Object ID                              | Obj Collection | Obj Type | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.c.3705574 | work            | Dataset   | has_version       | https://doi.org/10.5281/zenodo.3738980 |                |          | https://ror.org/04wxnsj81 |

  Scenario: Empty object format
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-empty-obj.json"
    And "DataCite" is the RA for DOI prefix "10.6084"
    And "Crossref" is the RA for DOI prefix "10.25798"
    And "Software" is the type for DOI "10.6084/m9.figshare.c.3705574"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                    | Subj Collection | Subj Type | Relationship Type | Object ID                          | Obj Collection | Obj Type | Asserted By               |
      | https://doi.org/10.6084/m9.figshare.c.3705574 | work            | Event     | is_part_of        | https://doi.org/10.25798/5p6r-4v68 |                |          | https://ror.org/04wxnsj81 |

  Example: Datacite to Datacite events should be ignored
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-f.json"
    And "DataCite" is the RA for DOI prefix "10.1186"
    Given the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships for subject "https://doi.org/10.6084/m9.figshare.c.3705574" are requested
    Then 0 relationships are returned from the item graph


  Example: Datacite to non Crossref should be ignored
    Given an empty Item Graph database
    And an Event Data Snapshot file named "datacite-events.tar.gz" containing JSON file "citations/datacite_conversion/datacite-f.json"
    And "JaLC" is the RA for DOI prefix "10.1186"
    And the Event Data snapshot file "datacite-events.tar.gz" has been converted and ingested
    And the registration agency lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the DataCite lookup has run
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships for subject "https://doi.org/10.6084/m9.figshare.c.3705574" are requested
    Then 0 relationships are returned from the item graph