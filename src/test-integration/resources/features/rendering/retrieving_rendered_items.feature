Feature: Retrieving rendered items

  Scenario Outline: Articles can be rendered, and are available in expected types

  The items endpoint should allow specific content types to be requested (e.g. application/citeproc-json).
  The correct type should be returned, using its public content type (e.g. application/json). This internal / public
  difference allows for users to request specific formats, but have it returned in a format that browsers can render.

  The link header to the Atom feed should also indicate the correct type. Note that the URL is correctly encoded
  (e.g. '+' is encoded as '%2B') in the API, but tests deal with the path in un-encoded form.

    Given UniXSD file "silly-string-moved.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "silly-string-moved.xml" is ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the render queue has emptied

    When <Identifier> is retrieved from the "/beta/items/" endpoint with content type <Internal Content Type>
    Then the Content-Type header should be <Public Content Type>
    And the Link header should be <Link>

    Examples:
      | Identifier                         | Internal Content Type                 | Public Content Type | Link                                                                                                                                             |
      | "https://doi.org/10.5555/12345678" | "application/citeproc+json"           | "application/json"  | "<http://localhost:1234/beta/items/feed.xml?content-type=application/citeproc+json&item=https://doi.org/10.5555/12345678>, rel=\"feed\""           |
      | "https://doi.org/10.5555/12345678" | "application/vnd.crossref.unixsd+xml" | "application/xml"   | "<http://localhost:1234/beta/items/feed.xml?content-type=application/vnd.crossref.unixsd+xml&item=https://doi.org/10.5555/12345678>, rel=\"feed\"" |

  Scenario Outline: Articles are served in citeproc-json preferentially

    Given UniXSD file "silly-string-moved.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "silly-string-moved.xml" is ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the render queue has emptied

    When <Identifier> is retrieved from the "/beta/items/" endpoint
    Then the Content-Type header should be <Public Content Type>

    Examples:
      | Identifier                         | Public Content Type |
      | "https://doi.org/10.5555/12345678" | "application/json"  |

