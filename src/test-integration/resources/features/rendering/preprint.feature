Feature: Render preprint metadata

  Scenario Outline: Preprints can be rendered, and are available in expected types

    Given an empty Item Graph database
    And UniXSD file "preprint-10.1038-s41598-022-12862-6.xml" registers posted content DOI "10.21203/rs.3.rs-1228934/v1"
    And UniXSD file "preprint-10.1038-s41598-022-12862-6.xml" is ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the render queue has emptied

    And the render for "https://doi.org/10.21203/rs.3.rs-1228934/v1" in <Internal Content Type> has become available

    When <Identifier> is retrieved from the "/beta/items/" endpoint with content type <Internal Content Type>
    Then the Content-Type header should be <Public Content Type>

    Examples:
      | Identifier                                    | Internal Content Type                  | Public Content Type |
      | "https://doi.org/10.21203/rs.3.rs-1228934/v1" | "application/vnd.marple.preprint+json" | "application/json"  |