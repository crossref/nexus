Feature: Filter by relationship type and specify whether to search by object or subject.

  Supports use cases like finding citations to/from a work, works funded by a funder, or works authored by a scholar.

  Scenario: Filter by relationship type

    Given the following relationship assertions in the item graph
      | Subject ID                                  | Relationship Type | Object ID                                    | Asserted At          | Asserted By                 |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | funder            | https://doi.org/10.21203/rs.3.rs-2279577/v1  | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3    |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.1016/j.patcog.2004.02.003 | 2021-11-10T12:00:00Z | https://ror.org/02twcfp3    |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | author            | https://orcid.org/0000-0001-7648-6447        | 2021-11-11T12:00:00Z | https://id.crossref.org/org/100 |
    And the item tree match queue has emptied
    And the relationships have been synced

    When relationships for relationship type "references" are requested

    Then all of the following relationships are returned from the item graph
      | Subject ID                                  | Relationship Type | Object ID                                    |
      | https://doi.org/10.21203/rs.3.rs-2279577/v2 | references        | https://doi.org/10.1016/j.patcog.2004.02.003 |
