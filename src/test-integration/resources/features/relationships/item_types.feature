Feature: Item Types in the Relationship endpoint

  The type and subtype of an Item should be present in the Relationships API response, and it should be possible to
  filter by them.

  In these scenarios, "NULL" means we specify the value is missing (not that we don't know).

  Scenario: Citation between two known Crossref XML documents.

    Given UniXSD file "relationships/article-cites-proceedings.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "relationships/cited-proceedings.xml" registers Conference DOI "10.1109/elinsl.1996.549297"
    And UniXSD file "relationships/article-cites-proceedings.xml" has been ingested
    And UniXSD file "relationships/cited-proceedings.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                                  | Obj Collection | Object Type         |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.1109/elinsl.1996.549297 | work           | proceedings-article |

  Scenario: Citation of a document we don't have metadata for should return null for work type

  In this example we haven't ingested cited-proceedings.xml so the type isn't known.

    Given UniXSD file "relationships/article-cites-proceedings.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "relationships/article-cites-proceedings.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                                  | Obj Collection | Object Type |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.1109/elinsl.1996.549297 | NULL           | NULL        |

  Scenario: No filters returns all relationships.

    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                             | Subj Collection | Subj Type           | Relationship Type | Object ID                                              | Obj Collection | Obj Type            |
      # Data from article-cites-everything. There are XML files for each of the cited items, so expect work types on both sides.
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | cites             | https://doi.org/10.1109/elinsl.1996.549297             | work           | proceedings-article |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | cites             | https://doi.org/10.50505/book                          | work           | book                |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | work           | grant               |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | cites             | https://doi.org/10.5555/demo_1.1                       | work           | component           |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | steward           | https://id.crossref.org/org/7822                       | NULL           | NULL                |
#      # Data from cited-proceedings.xml . No XML files for those, so objects have no type metadata.
      | https://doi.org/10.1109/elinsl.1996.549297             | work            | proceedings-article | cites             | https://doi.org/10.1109/icpadm.1994.414074             | NULL           | NULL                |
      | https://doi.org/10.1109/elinsl.1996.549297             | work            | proceedings-article | cites             | https://doi.org/10.1109/61.141837                      | NULL           | NULL                |
      | https://doi.org/10.1109/elinsl.1996.549297             | work            | proceedings-article | steward           | https://id.crossref.org/org/263                        | NULL           | NULL                |
      # Data from book-with-chapters
      | https://doi.org/10.50505/book                          | work            | book                | steward           | https://id.crossref.org/org/7822                       | NULL           | NULL                |
      # grants.xml
      | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | work            | grant               | steward           | https://id.crossref.org/org/960                        | NULL           | NULL                |
#      # sa-component.xml
      | https://doi.org/10.5555/demo_1.1                       | work            | component           | steward           | https://id.crossref.org/org/7822                       | NULL           | NULL                |

  Scenario: Subject 'collection' and 'type' filter.

  Compare with 'No filters returns all relationships'.

    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships for subject collection "work" type "journal-article" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                                              | Obj Collection | Obj Type            |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.1109/elinsl.1996.549297             | work           | proceedings-article |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.50505/book                          | work           | book                |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | work           | grant               |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.5555/demo_1.1                       | work           | component           |
      | https://doi.org/10.5555/12345678 | work            | journal-article | steward           | https://id.crossref.org/org/7822                       | NULL           | NULL                |

  Scenario: Object 'collection' and 'type' filter.

  Compare with 'No filters returns all relationships'.

    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships for object collection "work" type "component" are requested
    Then all of the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                        | Obj Collection | Obj Type  |
      # Data from article-cites-everything. There are XML files for each of the cited items, so expect work types on both sides.
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.5555/demo_1.1 | work           | component |

  Scenario: Subject 'collection' only filter.

    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships for subject collection "work" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                                             | Subj Collection | Subj Type           | Relationship Type | Object ID                                              | Obj Collection | Obj Type            |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | cites             | https://doi.org/10.1109/elinsl.1996.549297             | work           | proceedings-article |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | cites             | https://doi.org/10.50505/book                          | work           | book                |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | work           | grant               |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | cites             | https://doi.org/10.5555/demo_1.1                       | work           | component           |
      | https://doi.org/10.5555/12345678                       | work            | journal-article     | steward           | https://id.crossref.org/org/7822                       | NULL           | NULL                |
      | https://doi.org/10.1109/elinsl.1996.549297             | work            | proceedings-article | cites             | https://doi.org/10.1109/icpadm.1994.414074             | NULL           | NULL                |
      | https://doi.org/10.1109/elinsl.1996.549297             | work            | proceedings-article | cites             | https://doi.org/10.1109/61.141837                      | NULL           | NULL                |
      | https://doi.org/10.1109/elinsl.1996.549297             | work            | proceedings-article | steward           | https://id.crossref.org/org/263                        | NULL           | NULL                |
      | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | work            | grant               | steward           | https://id.crossref.org/org/960                        | NULL           | NULL                |
      | https://doi.org/10.5555/demo_1.1                       | work            | component           | steward           | https://id.crossref.org/org/7822                       | NULL           | NULL                |

  Scenario: Object 'collection' only filter.

    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships for object collection "work" are requested
    Then all of the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                                              | Obj Collection | Obj Type            |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.1109/elinsl.1996.549297             | work           | proceedings-article |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.50505/book                          | work           | book                |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | work           | grant               |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.5555/demo_1.1                       | work           | component           |


  Scenario: Subject 'type' only filter.

    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested

    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships for subject type "journal-article" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                                              | Obj Collection | Obj Type            |
      # Data from article-cites-everything. There are XML files for each of the cited items, so expect work types on both sides.
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.1109/elinsl.1996.549297             | work           | proceedings-article |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.50505/book                          | work           | book                |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | work           | grant               |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.5555/demo_1.1                       | work           | component           |
      | https://doi.org/10.5555/12345678 | work            | journal-article | steward           | https://id.crossref.org/org/7822                       | NULL           | NULL                |

  Scenario: Object 'type' only filter.

    Given UniXSD file "relationships/article-cites-everything.xml" has been ingested
    Given UniXSD file "relationships/cited-proceedings.xml" has been ingested
    Given UniXSD file "relationships/book-with-chapters.xml" has been ingested
    Given UniXSD file "relationships/grants.xml" has been ingested
    Given UniXSD file "relationships/sa-component.xml" has been ingested

    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships for object type "book" are requested
    Then all of the following relationships are returned from the item graph
      | Subject ID                       | Subj Collection | Subj Type       | Relationship Type | Object ID                     | Obj Collection | Obj Type |
      | https://doi.org/10.5555/12345678 | work            | journal-article | cites             | https://doi.org/10.50505/book | work           | book     |
