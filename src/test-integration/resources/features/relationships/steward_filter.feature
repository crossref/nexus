Feature: Filter relationships by steward

  Scenario: Filter for steward of object

    Given the following relationship assertions in the item graph
      | Subject ID                                   | Relationship Type | Object ID                                    | Asserted By                     |
      | https://doi.org/10.1002/elsc.200390040       | cites             | https://doi.org/10.1016/1350-4177(94)00014-j | https://id.crossref.org/org/311 |
      | https://doi.org/10.1016/1350-4177(94)00014-j | steward           | https://id.crossref.org/org/78               | https://ror.org/02twcfp32       |
      | https://doi.org/10.1002/elsc.200390040       | cites             | https://doi.org/10.1021/es991231c            | https://id.crossref.org/org/311 |
      | https://doi.org/10.1021/es991231c            | steward           | https://id.crossref.org/org/316              | https://ror.org/02twcfp32       |
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships with "object" steward "https://id.crossref.org/org/78" are requested

    Then all of the following relationships are returned from the item graph
      | Subject ID                             | Relationship Type | Object ID                                    | Asserted By                     |
      | https://doi.org/10.1002/elsc.200390040 | cites             | https://doi.org/10.1016/1350-4177(94)00014-j | https://id.crossref.org/org/311 |

  Scenario: Filter for steward of subject

    Given the following relationship assertions in the item graph
      | Subject ID                                | Relationship Type | Object ID                                    | Asserted By                     |
      | https://doi.org/10.1002/elsc.200390040    | cites             | https://doi.org/10.1016/1350-4177(94)00014-j | https://id.crossref.org/org/311 |
      | https://doi.org/10.1002/elsc.200390040    | steward           | https://id.crossref.org/org/311              | https://ror.org/02twcfp32       |
      | https://doi.org/10.1007/s10967-018-5830-4 | cites             | https://doi.org/10.1016/1350-4177(94)00014-j | https://id.crossref.org/org/297 |
      | https://doi.org/10.1007/s10967-018-5830-4 | steward           | https://id.crossref.org/org/297              | https://ror.org/02twcfp32       |
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When relationships with "subject" steward "https://id.crossref.org/org/311" are requested

    Then all of the following relationships are returned from the item graph
      | Subject ID                             | Relationship Type | Object ID                                    | Asserted By                     |
      | https://doi.org/10.1002/elsc.200390040 | cites             | https://doi.org/10.1016/1350-4177(94)00014-j | https://id.crossref.org/org/311 |
      | https://doi.org/10.1002/elsc.200390040 | steward           | https://id.crossref.org/org/311              | https://ror.org/02twcfp32       |
