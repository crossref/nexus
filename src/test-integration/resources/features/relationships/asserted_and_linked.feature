Feature: Asserted-by and linked-by filters in Relationship endpoint

  The asserting party and linking party for a relationship should be present in the Relationships API response, and it should be possible to
  filter by them.

  Scenario: No filters

    Given UniXSD file "relationships/article-cites-everything.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "relationships/article-cites-everything.xml" has been ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Relationship Type | Object ID                                              | Asserted By                      | Linked By                        |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.1109/elinsl.1996.549297             | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.50505/book                          | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.5555/demo_1.1                       | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | steward           | https://id.crossref.org/org/7822                       | https://ror.org/02twcfp32        | https://ror.org/02twcfp32       |

  Scenario: Filter "linked-by"
    Given UniXSD file "relationships/article-cites-everything.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "relationships/article-cites-everything.xml" has been ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    When relationships linked by "https://ror.org/02twcfp32" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Relationship Type | Object ID                                              | Asserted By                      | Linked By                        |
      | https://doi.org/10.5555/12345678 | steward           | https://id.crossref.org/org/7822                       | https://ror.org/02twcfp32        | https://ror.org/02twcfp32       |

  Scenario: Filter "asserted-by"
    Given UniXSD file "relationships/article-cites-everything.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "relationships/article-cites-everything.xml" has been ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    When relationships asserted by "https://id.crossref.org/org/7822" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Relationship Type | Object ID                                              | Asserted By                      | Linked By                        |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.1109/elinsl.1996.549297             | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.50505/book                          | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.5555/demo_1.1                       | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |

  Scenario: Filter "not-linked-by"
    Given UniXSD file "relationships/article-cites-everything.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "relationships/article-cites-everything.xml" has been ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    When relationships not linked by "https://ror.org/02twcfp32" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Relationship Type | Object ID                                              | Asserted By                      | Linked By                        |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.1109/elinsl.1996.549297             | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.50505/book                          | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.46936/cpcy.proj.2019.50733/60006578 | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |
      | https://doi.org/10.5555/12345678 | cites             | https://doi.org/10.5555/demo_1.1                       | https://id.crossref.org/org/7822 | https://id.crossref.org/org/7822 |

  Scenario: Filter "not-asserted-by"
    Given UniXSD file "relationships/article-cites-everything.xml" registers Article DOI "10.5555/12345678"
    And UniXSD file "relationships/article-cites-everything.xml" has been ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    When relationships not asserted by "https://id.crossref.org/org/7822" are requested
    Then the following relationships are returned from the item graph
      | Subject ID                       | Relationship Type | Object ID                                              | Asserted By                      | Linked By                        |
      | https://doi.org/10.5555/12345678 | steward           | https://id.crossref.org/org/7822                       | https://ror.org/02twcfp32        | https://ror.org/02twcfp32       |
