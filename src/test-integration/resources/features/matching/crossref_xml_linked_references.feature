Feature: Linked references in Crossref XML

  The Crossref deposit system performs reference matching for citations. These are applied upstream of the Item Graph,
  and the results are incorporated in the XML.

  When a citation link was provided by the publisher it looks like: <doi>10.5555/12345678</doi>
  When it's matched by Crossref it looks like: <doi provider='crossref'>10.5555/12345678</doi>

  This information is recorded in the item tree using the `doi-asserted-by` attribute on the citation object.

  These are reflected in Relationship Match assertions. They should be recorded as being matched by the asserting party
  for the item tree (a Crossref member using their Crossref org id), or by Crossref (using its ROR id).

  Scenario: Mixture of matched and unmatched references

  In this XML file, the first three citations in this file are matched by Crossref.
  The last three were supplied by the publisher.

    Given UniXSD file "matching/mixed-refs.xml" registers Article DOI "10.1016/j.orgel.2018.06.002"
    And UniXSD file "matching/mixed-refs.xml" has crm-item with Member ID "78"
    When UniXSD file "matching/mixed-refs.xml" is ingested
    And the item tree match queue has emptied
    And the relationships have been synced

    Then the following relationship statements are found in the item graph
      | Count | Subject ID                                  | Relationship Type | Object ID                                     | Asserted By                    | Linked By                      |
      | 1     | https://doi.org/10.1016/j.orgel.2018.06.002 | cites             | https://doi.org/10.1016/j.rser.2015.12.177    | https://id.crossref.org/org/78 | https://ror.org/02twcfp32      |
      | 1     | https://doi.org/10.1016/j.orgel.2018.06.002 | cites             | https://doi.org/10.1021/acs.chemmater.7b03035 | https://id.crossref.org/org/78 | https://ror.org/02twcfp32      |
      | 1     | https://doi.org/10.1016/j.orgel.2018.06.002 | cites             | https://doi.org/10.1002/aelm.201500017        | https://id.crossref.org/org/78 | https://ror.org/02twcfp32      |
      | 1     | https://doi.org/10.1016/j.orgel.2018.06.002 | cites             | https://doi.org/10.1038/ncomms7828            | https://id.crossref.org/org/78 | https://id.crossref.org/org/78 |
      | 1     | https://doi.org/10.1016/j.orgel.2018.06.002 | cites             | https://doi.org/10.1021/ar400282g             | https://id.crossref.org/org/78 | https://id.crossref.org/org/78 |
      | 1     | https://doi.org/10.1016/j.orgel.2018.06.002 | cites             | https://doi.org/10.1038/pj.2016.101           | https://id.crossref.org/org/78 | https://id.crossref.org/org/78 |

