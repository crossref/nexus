Feature: Eventdata evidence records conversion

  Scenario: hypothes.is evidence records are fully converted and ingested

  The hypothes.is evidence record exibits how two records about the same DOI stored in separate actions with different relationships
  are both identified and included.

    Given an empty Item Graph database
    And the evidence record "evidence-records/hypothesis.json" contains 2 matches to "https://doi.org/10.1016/j.celrep.2019.10.106"
    And the evidence record "evidence-records/hypothesis.json" has been converted and ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                   | Relationship Type | Object ID                                    |
      | https://hypothes.is/a/0IdDkDXBEeqdAoNCnM_37Q | annotates         | https://doi.org/10.1016/j.celrep.2019.10.106 |
      | https://hypothes.is/a/0IdDkDXBEeqdAoNCnM_37Q | discusses         | https://doi.org/10.1016/j.celrep.2019.10.106 |

  Scenario: Newsfeed evidence records are fully converted and ingested

  An interesting feature in this example is that two matches to the same DOI are contained within the same match of one action.
  Both matches have the same relationship though and they are therefore merged into one item tree with multiple properties.

    Given an empty Item Graph database
    And the evidence record "evidence-records/newsfeed.json" contains 2 matches to "https://doi.org/10.1002/bies.202100020"
    And the evidence record "evidence-records/newsfeed.json" contains 1 matches to "https://doi.org/10.1016/j.ympev.2018.05.015"
    And the evidence record "evidence-records/newsfeed.json" has been converted and ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                                                                  | Relationship Type | Object ID                                   |
      | https://www.sciencealert.com/evolution-keeps-making-and-unmaking-crabs-and-nobody-knows-why | discusses         | https://doi.org/10.1002/bies.202100020      |
      | https://www.sciencealert.com/evolution-keeps-making-and-unmaking-crabs-and-nobody-knows-why | discusses         | https://doi.org/10.1016/j.ympev.2018.05.015 |

  Scenario: Reddit evidence records are fully converted and ingested
    Given an empty Item Graph database
    And the evidence record "evidence-records/reddit.json" contains 1 matches to "https://doi.org/10.1101/2021.12.30.474376"
    And the evidence record "evidence-records/reddit.json" contains 1 matches to "https://doi.org/10.1101/2021.12.30.474612"
    And the evidence record "evidence-records/reddit.json" has been converted and ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                                                                              | Relationship Type | Object ID                                 |
      | https://reddit.com/r/BiologyPreprints/comments/rtso4g/escherichia_coli_chemotaxis_to_competing_stimuli/ | discusses         | https://doi.org/10.1101/2021.12.30.474376 |
      | https://reddit.com/r/BiologyPreprints/comments/rtre06/rsc3k_a_smv_resistance_gene_localized_on/         | discusses         | https://doi.org/10.1101/2021.12.30.474612 |

  Scenario: Reddit evidence records are fully converted and ingested
    Given an empty Item Graph database
    And the evidence record "evidence-records/reddit-links.json" contains 1 matches to "https://doi.org/10.13140/rg.2.2.14427.03366"
    And the evidence record "evidence-records/reddit-links.json" contains 1 matches to "https://doi.org/10.1016/j.jconrel.2015.08.051"
    And the evidence record "evidence-records/reddit-links.json" contains 1 matches to "https://doi.org/10.1016/j.cell.2022.01.018"
    And the evidence record "evidence-records/reddit-links.json" has been converted and ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                                                                                         | Relationship Type | Object ID                                     |
      | https://doctors4covidethics.org/vascular-and-organ-damage-induced-by-mrna-vaccines-irrefutable-proof-of-causality/ | discusses         | https://doi.org/10.13140/rg.2.2.14427.03366   |
      | https://doctors4covidethics.org/vascular-and-organ-damage-induced-by-mrna-vaccines-irrefutable-proof-of-causality/ | discusses         | https://doi.org/10.1016/j.jconrel.2015.08.051 |
      | https://doctors4covidethics.org/vascular-and-organ-damage-induced-by-mrna-vaccines-irrefutable-proof-of-causality/ | discusses         | https://doi.org/10.1016/j.cell.2022.01.018    |

  Scenario: Stackexchange evidence records are fully converted and ingested
    Given an empty Item Graph database
    And the evidence record "evidence-records/stackexchange.json" contains 1 matches to "https://doi.org/10.1016/0025-5564(93)90076-m"
    And the evidence record "evidence-records/stackexchange.json" contains 1 matches to "https://doi.org/10.5812/atr.6444"
    And the evidence record "evidence-records/stackexchange.json" contains 1 matches to "https://doi.org/10.1186/s13054-021-03671-w"
    And the evidence record "evidence-records/stackexchange.json" contains 1 matches to "https://doi.org/10.5683/sp2/nlb8it"
    And the evidence record "evidence-records/stackexchange.json" has been converted and ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                        | Relationship Type | Object ID                                    |
      | https://medicalsciences.stackexchange.com/q/31326 | discusses         | https://doi.org/10.1016/0025-5564(93)90076-m |
      | https://medicalsciences.stackexchange.com/q/31326 | discusses         | https://doi.org/10.5812/atr.6444             |
      | https://medicalsciences.stackexchange.com/q/31326 | discusses         | https://doi.org/10.1186/s13054-021-03671-w   |
      | https://medicalsciences.stackexchange.com/q/31326 | discusses         | https://doi.org/10.5683/sp2/nlb8it           |

  Scenario: Wikipedia evidence records are fully converted and ingested
    Given an empty Item Graph database
    And the evidence record "evidence-records/wikipedia.json" contains 1 matches to "https://doi.org/10.1038/s41467-019-11357-9"
    And the evidence record "evidence-records/wikipedia.json" contains 1 matches to "https://doi.org/10.14746/bp.2013.20.5"
    And the evidence record "evidence-records/wikipedia.json" has been converted and ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                                         | Relationship Type | Object ID                                  |
      | https://en.wikipedia.org/api/rest_v1/page/html/Roopkund/1063058559 | references        | https://doi.org/10.1038/s41467-019-11357-9 |
      | https://hr.wikipedia.org/api/rest_v1/page/html/Kera_Tamara/6236673 | references        | https://doi.org/10.14746/bp.2013.20.5      |

  Scenario: Wordpressdotcom evidence records are fully converted and ingested
    Given an empty Item Graph database
    And the evidence record "evidence-records/wordpressdotcom.json" contains 1 matches to "https://doi.org/10.4135/9781452204864.n2"
    And the evidence record "evidence-records/wordpressdotcom.json" contains 1 matches to "https://doi.org/10.1017/cbo9781139547369.005"
    And the evidence record "evidence-records/wordpressdotcom.json" has been converted and ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced

    When all relationships are requested

    Then the following relationships are returned from the item graph
      | Subject ID                                                                                                | Relationship Type | Object ID                                    |
      | https://ajazzyscholarslearningthoughts.wordpress.com/2022/08/13/an-instructional-plan-using-assure-model/ | discusses         | https://doi.org/10.4135/9781452204864.n2     |
      | https://ajazzyscholarslearningthoughts.wordpress.com/2022/08/13/an-instructional-plan-using-assure-model/ | discusses         | https://doi.org/10.1017/cbo9781139547369.005 |