Feature: Funding relationships

  Legacy funding relationships use the Funder Registry ID (a DOI). The ROR registry contains 'same-as' relationships
  that indicate equivalence between Funder Registry DOIs and ROR IDs.

  Where legacy funding relationships are found, they should also be asserted using the equivalent ROR identifier, where
  appropriate.

  Currently the item tree doesn't use a reified relationship item for 'funding'. Instead a simple relationship is used.
  This is a relatively simple refactor, which we will do in future future.
  For now, that means that we can't perform reference matching on funding relationships.

  When the refactor is made, we'll need to refactor the ROR matching implementation to use that model.

  Scenario: ROR ingestion creates same-as relationships for any and preferred FundRef External IDs
    Given an empty Item Graph database
    And "ror-data.json" contains Organization with ID "https://ror.org/019wvm592", Name "Australian National University" and Status "active"
    And "ror-data.json" contains Organization preferred FundRef External ID "501100000995"
    And ROR file "ror-data.json" has been ingested
    And UniXSD file "funding/acf363.xml" registers Article DOI "10.1088/2515-7620/acf363"
    And UniXSD file "funding/acf363.xml" has funder DOI "https://doi.org/10.13039/501100000995"
    And UniXSD file "funding/acf363.xml" has crm-item with Member ID "266"
    And UniXSD file "funding/acf363.xml" is ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    Then the following relationship statements are found in the item graph
      | Count | State | Current | Asserted By                     | Linked By                       | Subject ID                               | Relationship Type | Object ID                             |

      # Same-as relationship between the ROR and the Funder ID.
      | 1     | true  | true    | https://ror.org/02twcfp32       | https://ror.org/02twcfp32       | https://ror.org/019wvm592                | same-as           | https://doi.org/10.13039/501100000995 |

      # Funding relationship between the article and the Fundref DOI. Fundref link supplied by member.
      | 1     | true  | true    | https://id.crossref.org/org/266 | https://id.crossref.org/org/266 | https://doi.org/10.1088/2515-7620/acf363 | funder            | https://doi.org/10.13039/501100000995 |

      # And the funding relationship between the article and the ROR id, using the mapping.
      # Linked by Crossref
      | 1     | true  | true    | https://id.crossref.org/org/266 | https://ror.org/02twcfp32       | https://doi.org/10.1088/2515-7620/acf363 | funder            | https://ror.org/019wvm592             |

