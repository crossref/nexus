Feature: Research Organization Registry (ROR)

  We ingest the ROR registry and create relationships in the item graph.

  Scenario Outline: Organizations are ingested from ROR
    Given an empty Item Graph database
    And "ror-data.json" contains Organization with ID "<Identifier>", Name "<Name>" and Status "<Status>"
    When ROR file "ror-data.json" is ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    Then the Item Graph contains members
      | Name   | Identifier   |
      | <Name> | <Identifier> |

    Examples:
      | Identifier                | Name                           | Status |
      | https://ror.org/019wvm592 | Australian National University | active |
      | https://ror.org/02bfwt286 | Monash University              | active |
      | https://ror.org/00rqy9422 | University of Queensland       | active |

  Scenario Outline: Withdrawn Organizations are NOT ingested from ROR
    Given an empty Item Graph database
    And "ror-data.json" contains Organization with ID "<Identifier>", Name "<Name>" and Status "<Status>"
    When ROR file "ror-data.json" is ingested
    And the item tree match queue has emptied
    Then the Item Graph does not contain members
      | Name   | Identifier   |
      | <Name> | <Identifier> |

    Examples:
      | Identifier                | Name                 | Status    |
      | https://ror.org/01sf06y89 | Macquarie University | Withdrawn |
      | https://ror.org/01kj2bm70 | Newcastle University | Withdrawn |

  Scenario Outline: ROR ingestion creates same-as relationships for any and preferred FundRef External IDs
    Given an empty Item Graph database
    And "ror-data.json" contains Organization with ID "<Identifier>", Name "<Name>" and Status "<Status>"
    And "ror-data.json" contains Organization preferred FundRef External ID "<PreferredFundRefId>"
    And "ror-data.json" contains Organization all FundRef External ID "<PreferredFundRefId>"
    And "ror-data.json" contains Organization all FundRef External ID "<FundRefId2>"
    And "ror-data.json" contains Organization all FundRef External ID "<FundRefId3>"
    And ROR file "ror-data.json" has been ingested
    And the item tree match queue has emptied
    And the item tree update queue has emptied
    And the relationships have been synced
    When all relationships are requested
    Then the following relationships are returned from the item graph
      | Asserted By               | Subject ID   | Subj Collection | Relationship Type | Object ID                                     |
      | https://ror.org/02twcfp32 | <Identifier> | org             | same-as           | https://doi.org/10.13039/<PreferredFundRefId> |
      | https://ror.org/02twcfp32 | <Identifier> | org             | same-as           | https://doi.org/10.13039/<FundRefId2>         |
      | https://ror.org/02twcfp32 | <Identifier> | org             | same-as           | https://doi.org/10.13039/<FundRefId3>         |

    Examples:
      | Identifier                | Name                           | Status | PreferredFundRefId | FundRefId2   | FundRefId3 |
      | https://ror.org/019wvm592 | Australian National University | active | 501100000995       | 501100001151 | 100009020  |
