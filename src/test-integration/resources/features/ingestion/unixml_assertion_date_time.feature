Feature: When a UNIXML file is ingested the envelope asserted date-time should be its deposited date-time

  This means that the ingested_at date-time and the asserted_at date-time can and will differ.

  The file used in the following scenarios has a "last-update" date-time of 2021-10-14T15:04:08Z
  which corresponds to the deposited date-time in the item-tree. This value is used to populate the
  assertedAt attribute of the ItemTreeAssertion, resulting to the relationship_current.updated_at.

  Scenario: Results returned for relationships asserted within date range

    Given an empty Item Graph database
    And UniXSD file "conversion/f1f5fdfd846dacf1687416b2456bbf800a24e86d.xml" is ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    When relationships updated after "2021-10-13" and before "2021-10-15" are requested
    Then 4 relationships are returned from the item graph

  Scenario: No results for relationships asserted outside of date range

    Given an empty Item Graph database
    And UniXSD file "conversion/f1f5fdfd846dacf1687416b2456bbf800a24e86d.xml" is ingested
    And the item tree match queue has emptied
    And the relationships have been synced
    When relationships updated after "2021-10-15" and before "2021-10-16" are requested
    Then 0 relationships are returned from the item graph