package org.crossref.manifold.common

import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.mockk.every
import io.mockk.mockkConstructor
import io.mockk.slot
import org.crossref.manifold.lookup.DataCiteTypeResolver
import org.crossref.manifold.lookup.RAResolver

class Lookups {
    private val prefixRAs = mutableMapOf<String, String>()
    private val doiTypes = mutableMapOf<String, String>()

    @Before
    fun initRAResolver() {
        prefixRAs.clear()
        mockkConstructor(RAResolver::class)
        val doiPrefixesSlot = slot<Set<String>>()
        every {
            anyConstructed<RAResolver>().lookupRAs(capture(doiPrefixesSlot))
        } answers {
            doiPrefixesSlot.captured.map { it to (prefixRAs[it] ?: "Unknown") }
        }
    }

    @Before
    fun initDataCiteTypeLookup() {
        doiTypes.clear()
        mockkConstructor(DataCiteTypeResolver::class)
        val doisSlot = slot<Set<String>>()
        every {
            anyConstructed<DataCiteTypeResolver>().lookupTypes(capture(doisSlot))
        } answers {
            doisSlot.captured.map { it to doiTypes[it]!! }
        }
    }

    @Given("{string} is the RA for DOI prefix {string}")
    fun is_the_ra_for_doi_prefix(ra: String, doiPrefix: String) {
        prefixRAs[doiPrefix] = ra
    }

    @Given("{string} is the type for DOI {string}")
    fun is_the_type_for_doi(type: String, doi: String) {
        doiTypes[doi] = type
    }
}
