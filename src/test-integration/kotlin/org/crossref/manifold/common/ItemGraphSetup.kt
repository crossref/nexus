package org.crossref.manifold.common

import io.cucumber.java.After
import io.cucumber.java.Before
import io.cucumber.java.Scenario
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import io.mockk.clearMocks
import io.mockk.every
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.*
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.lookup.DataCiteTypeLookupService
import org.crossref.manifold.lookup.RALookupService
import org.crossref.manifold.matching.MatcherService
import org.crossref.manifold.relationship.RelationshipSyncService
import org.crossref.manifold.rendering.RenderQueueProcessor
import org.junit.Assert
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.jdbc.core.JdbcTemplate
import java.time.InstantSource

class ItemGraphSetup(
    private val itemGraph: ItemGraph,
    private val jdbcTemplate: JdbcTemplate,
    private val resolver: Resolver,
    private val instantSource: InstantSource,
    private val relationshipMatchDao: RelationshipMatchDao,
    private val itemTreeMatcher : MatcherService,
    private val itemTreeUpdater: ItemTreeUpdater,
    private val renderProcessor: RenderQueueProcessor,
    private val dataCiteTypeLookupService: DataCiteTypeLookupService,
    private val raLookupService: RALookupService,
    private val relationshipSyncService: RelationshipSyncService

) {
    companion object {
        private val PROVENANCE = EnvelopeBatchProvenance(UserAgentParser.parse("Crossref Test/1.0.0"), "1234")
    }

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Before
    fun logStart(scenario: Scenario) {
        logger.info("Start scenario: ${scenario.name} ... ")
    }

    @After
    fun logEnd(scenario: Scenario) {
        logger.info("Finish scenario: ${scenario.name}")
    }

    // By convention, scenarios start with an empty item graph.
    @Before
    fun truncateItemGraphTables() {
        jdbcTemplate.execute("TRUNCATE TABLE datacite_type_lookup RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE envelope RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE envelope_batch RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE host RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE identifier_ra_lookup RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_identifier RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_info RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_render_status_queue RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_subtype RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_tree RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_tree_assertion RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_tree_match_queue RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_tree_update_queue RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE item_type RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE open_cursor RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE ra_lookup RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE registration_agency RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE relationship_api RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE relationship_current RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE relationship_history RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE rendered_item RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE scheme RESTART IDENTITY CASCADE")
        jdbcTemplate.execute("TRUNCATE TABLE user_agent RESTART IDENTITY CASCADE")

        // Start the item identifiers PKs in a different range to item. They are usually kept in lock-step, which could
        // hide a bug.
        jdbcTemplate.execute("ALTER SEQUENCE item_identifier_pk_seq RESTART WITH 1000000")
    }

    @Before
    fun initInstantSource() {
        clearMocks(instantSource)
        // We can't spy on instantSource directly since SystemInstantSource is non-public.
        // Instead, we use a pure mock and replicate the default behaviour.
        every { instantSource.instant() } returns InstantSource.system().instant()
    }


    @When("the item tree match queue has emptied")
    fun the_item_tree_match_queue_has_emptied() {
        itemTreeMatcher.run()
    }

    @When("the item tree update queue has emptied")
    fun the_item_tree_update_queue_has_emptied() {
        itemTreeUpdater.run()
    }

    @When("the render queue has emptied")
    fun the_item_tree_render_queue_has_emptied() {
        renderProcessor.run()
    }


    @When("the DataCite lookup has run")
    fun the_datacite_lookup_has_run() {
        dataCiteTypeLookupService.lookupUntriedTypes()
    }


    @When("the registration agency lookup has run")
    fun the_ra_lookup_has_run() {
        raLookupService.lookupUntriedRAs()
    }

    @When("the relationships have been synced")
    fun the_relationships_have_been_synced() {
        relationshipSyncService.syncRelationships()
    }

    /**
     * Because relationships are asserted via item trees, all relationships about the same subject must be grouped into
     * one item tree. Therefore, take the first asserted-at date in each group of relationships by a party, about a
     * subject.
     */
    @Given("the following relationship assertions in the item graph")
    fun the_following_relationship_assertions_in_the_item_graph(statements: List<Statements.RelationshipAssertionStatement>) {
        if (statements.any { it.ingestedAt != null }) {
            clearMocks(instantSource)
            every { instantSource.instant() } returnsMany statements.map {
                it.ingestedAt?.toInstant() ?: InstantSource.system().instant()
            }
        }

        // Group these into assertions per root tree.
        // Take the first timestamp for each.
        val grouped = statements.groupBy { it.subjectId to it.assertedBy }
        val envelopes = grouped.map { group ->
            val assertedAt = group.value.first().assertedAt
            val assertedBy = group.key.second
            val rootIdentifier = group.key.first
            Envelope(
                listOf(
                    ItemTree(
                        Item()
                            .withIdentifier(IdentifierParser.parse(rootIdentifier))
                            .withRelationships(group.value.map {
                                Relationship(
                                    it.relationshipType,
                                    Item().withIdentifier(IdentifierParser.parse(it.objectId)),
                                    assertedAt = null
                                )
                            })
                    )
                ),
                ItemTreeAssertion(
                    assertedAt,
                    Item().withIdentifier(IdentifierParser.parse(assertedBy))
                ),
            )
        }

        itemGraph.ingest(
            listOf(
                EnvelopeBatch(envelopes, PROVENANCE)
            )
        )

    }


    @Given("the following property assertions in the item graph")
    fun the_following_property_assertions_in_the_item_graph(statements: List<Statements.PropertyAssertionStatement>) {

        val envelopes = statements.map { statement ->
            val item = ItemTree(
                Item()
                    .withIdentifier(IdentifierParser.parse(statement.subjectId))
                    .withPropertiesFromMap(mapOf(statement.property to statement.value))
            )

            val assertion = ItemTreeAssertion(
                statement.assertedAt,
                Item().withIdentifier(IdentifierParser.parse(statement.assertedBy))
            )

            Envelope(listOf(item), assertion)
        }

        itemGraph.ingest(
            listOf(
                EnvelopeBatch(
                    envelopes, PROVENANCE
                )
            )
        )
    }

    /**
     * This one specifically asserts all the property values in one assertion.
     */
    @Given("the following multi-valued property assertion in the item graph")
    fun the_following_multi_property_assertions_in_the_item_graph(statements: List<Statements.PropertyAssertionStatement>) {

        Assert.assertTrue(
            "All statements must be asserted by the same party",
            statements.map { it.assertedBy }.toSet().count() == 1
        )

        Assert.assertTrue(
            "All subject ids should be the same",
            statements.map { it.subjectId }.toSet().count() == 1
        )

        val assertedBy = statements.first().assertedBy
        val subjectId = statements.first().subjectId
        val assertedAt = statements.first().assertedAt

        val allProperties = statements.associate { it.property to it.value }
        val item = Item()
            .withIdentifier(IdentifierParser.parse(subjectId))
            .withPropertiesFromMap(allProperties)

        val assertion =
            ItemTreeAssertion(
                assertedAt,
                Item().withIdentifier(IdentifierParser.parse(assertedBy))
            )

        itemGraph.ingest(
            listOf(
                EnvelopeBatch(
                    listOf(Envelope(listOf(ItemTree(item)), assertion)), PROVENANCE
                )
            )
        )

    }


    /**
     * This Given doesn't do anything other than serve as a placeholder to make tests clear when there is no prior data
     * in the item graph.
     * The clearing is done by [truncateItemGraphTables].
     */
    @Given("an empty Item Graph database")
    fun empty_item_graph() = Unit

    @Then("the following relationship statements are found in the item graph")
    fun the_item_graph_contains_current_relationship_statements(
        expectedStatements: List<Statements.RelationshipAssertionStatement>,
    ) {
        expectedStatements.forEach { expected ->

            val foundStatements = relationshipMatchDao.getAllCurrentForRoot(
                setOf(resolver.resolveRO(IdentifierParser.parse(expected.subjectId)).pk!!)
            )

            // If these field are supplied, progressively filter rows that match them all.
            val subjPk = resolver.resolveRO(IdentifierParser.parse(expected.subjectId)).pk
            val foundSubj =
                foundStatements.filter { it.subjectPk == subjPk}

            val objPk = resolver.resolveRO(IdentifierParser.parse(expected.objectId)).pk
            val foundObj =
                foundSubj.filter { it.objectPk == objPk }

            val assertingPk = resolver.resolveRO(IdentifierParser.parse(expected.assertedBy)).pk
            val foundAsserting =
                foundObj.filter { it.itemTreeAssertingParty == assertingPk }

            val linkingPk = expected.linkedBy ?.let {
                resolver.resolveRO(
                    IdentifierParser.parse(
                        expected.linkedBy
                    )
                ).pk
            }

            val foundLinking = if (linkingPk != null) {
                foundAsserting.filter {
                    it.matchingParty == linkingPk
                }

            } else foundAsserting

            val count = foundLinking.count()

            Assertions.assertEquals(expected.count,count, "Expected subj: $subjPk, obj: $objPk, assertingParty: $assertingPk, linkingPk: $linkingPk in $foundStatements")
        }
    }

    @Given("the following Items in the Item Graph")
    fun the_following_items_are_in_the_item_graph(items: List<Statements.Item>) {
        items.forEach { item ->
            val resolvedItem = resolver.resolveRO(IdentifierParser.parse(item.identifier))
            Assertions.assertNotNull(resolvedItem, "Expected to resolved item $item")
            val createdItem = resolver.resolveSynchronousRW(listOf(ItemTree(resolvedItem))).firstOrNull()
            Assertions.assertNotNull(createdItem, "Expected to find item with identifier $item")
        }
    }
}
