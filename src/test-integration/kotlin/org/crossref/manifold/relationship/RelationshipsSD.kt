package org.crossref.manifold.relationship

import com.fasterxml.jackson.databind.ObjectMapper
import io.cucumber.java.Before
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.crossref.manifold.api.RelationshipView
import org.crossref.manifold.api.relationship.RelationshipsResponse
import org.crossref.manifold.common.ApiResponseContext
import org.crossref.manifold.common.Statements
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.fail
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get

class RelationshipsSD(
    private val apiResponseContext: ApiResponseContext, private val mapper: ObjectMapper
) {
    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    // Function that returns a RequestBuilder, with arguments built in.
    // This has to be wrapped in a function, not be a MockHttpServletRequestBuilder directly because it's mutable.
    // We need a fresh one for each page, because we mutate it to add cursors.
    private var nextRequest: (() -> MockHttpServletRequestBuilder)? = null

    @Before
    fun beforeEach() {
        nextRequest = null
    }

    @When("all relationships are requested")
    fun all_relationships_are_requested() {
        nextRequest = { get("/beta/relationships/") }
    }

    @When("relationships for item {string} are requested")
    fun relationships_for_item_are_requested(itemId: String) {
        nextRequest = {
            get("/beta/relationships/").param("item.id", itemId)
        }
    }

    @When("relationships for item registration agency {string} are requested")
    fun relationships_for_item_ra_requested(registrationAgency: String) {
        nextRequest = {
            get("/beta/relationships/").param("item.registration-agency", registrationAgency)
        }
    }


    @When("relationships for subject registration agency {string} are requested")
    fun relationships_for_subject_ra_requested(registrationAgency: String) {
        nextRequest = {
            get("/beta/relationships/").param("subject.registration-agency", registrationAgency)
        }
    }


    @When("relationships for object registration agency {string} are requested")
    fun relationships_for_object_ra_requested(registrationAgency: String) {
        nextRequest = {
            get("/beta/relationships/").param("object.registration-agency", registrationAgency)
        }
    }

    @When("relationships for subject {string} are requested")
    fun relationships_for_subject_are_requested(subjectId: String) {
        nextRequest = {
            get("/beta/relationships/").param("subject.id", subjectId)
        }

    }

    @When("relationships for object {string} are requested")
    fun relationships_for_object_are_requested(objectId: String) {
        nextRequest = {
            get("/beta/relationships/").param("object.id", objectId)
        }
    }

    @When("relationships linked by {string} are requested")
    fun relationships_linked_by(itemId: String) {
        nextRequest = {
            get("/beta/relationships/").param("linked-by", itemId)
        }
    }

    @When("relationships not linked by {string} are requested")
    fun relationships_not_linked_by(itemId: String) {
        nextRequest = {
            get("/beta/relationships/").param("not-linked-by", itemId)
        }
    }

    @When("relationships asserted by {string} are requested")
    fun relationships_asserted_by(itemId: String) {
        nextRequest = {
            get("/beta/relationships/").param("asserted-by", itemId)
        }
    }


    @When("relationships not asserted by {string} are requested")
    fun relationships_not_asserted_by(itemId: String) {
        nextRequest = {
            get("/beta/relationships/").param("not-asserted-by", itemId)
        }
    }


    @When("all relationships for subject {string} are requested")
    fun all_relationships_for_subj_are_requested(subjectId: String) {
        nextRequest = { get("/beta/relationships/").param("subject.id", subjectId) }
    }

    @When("relationships for subject collection {string} type {string} are requested")
    fun relationships_for_subj_collection_type_are_requested(collection: String?, type: String?) {
        nextRequest = {
            get("/beta/relationships/").param("subject.collection", collection).param("subject.type", type)
        }

    }

    @When("relationships for object collection {string} type {string} are requested")
    fun relationships_for_obj_collection_type_are_requested(collection: String?, type: String?) {
        nextRequest = {
            get("/beta/relationships/").param("object.collection", collection).param("object.type", type)
        }
    }

    @When("relationships for subject collection {string} are requested")
    fun relationships_for_subj_collection_are_requested(collection: String?) {
        nextRequest = {
            get("/beta/relationships/").param("subject.collection", collection)
        }

    }

    @When("relationships for object collection {string} are requested")
    fun relationships_for_obj_collection_are_requested(collection: String?) {
        nextRequest = {
            get("/beta/relationships/").param("object.collection", collection)
        }

    }


    @When("relationships for subject type {string} are requested")
    fun relationships_for_subj_type_are_requested(type: String?) {
        nextRequest = {
            get("/beta/relationships/").param("subject.type", type)
        }

    }

    @When("relationships for object type {string} are requested")
    fun relationships_for_obj_type_are_requested(type: String?) {
        nextRequest = {
            get("/beta/relationships/").param("object.type", type)
        }

    }

    @When("{int} relationships for subject {string} are requested")
    fun relationships_for_subj_are_requested(rows: Int, subjectId: String) {
        nextRequest = {
            get("/beta/relationships/").param("subject.id", subjectId).param("rows", rows.toString())
        }
    }

    @When("relationships for relationship type {string} are requested")
    fun relationships_for_relationship_type_are_requested(relationshipType: String) {
        nextRequest = {
            get("/beta/relationships/").param("relationship-type", relationshipType)
        }
    }

    @When("relationships created after {string} are requested")
    fun relationships_created_after_are_requested(fromCreatedTime: String) {
        nextRequest = {
            get("/beta/relationships/").param("from-created-time", fromCreatedTime)
        }
    }

    @When("relationships created before {string} are requested")
    fun relationships_created_before_are_requested(untilCreatedTime: String) {
        nextRequest = {
            get("/beta/relationships/").param("until-created-time", untilCreatedTime)
        }
    }

    @When("relationships updated after {string} are requested")
    fun relationships_updated_after_are_requested(fromUpdatedTime: String) {
        nextRequest = {
            get("/beta/relationships/").param("from-updated-time", fromUpdatedTime)
        }
    }

    @When("relationships updated before {string} are requested")
    fun relationships_updated_before_are_requested(untilUpdatedTime: String) {
        nextRequest = { get("/beta/relationships/").param("until-updated-time", untilUpdatedTime) }
    }

    @When("relationships updated after {string} and before {string} are requested")
    fun relationships_updated_after_and_before_are_requested(fromUpdatedTime: String, untilUpdatedTime: String) {
        nextRequest = {
            get("/beta/relationships/").param("from-updated-time", fromUpdatedTime)
                .param("until-updated-time", untilUpdatedTime)
        }
    }

    @When("relationships with {string} steward {string} are requested")
    fun relationships_with_steward_are_requested(subOrObj: String, stewardId: String) {
        nextRequest = {
            get("/beta/relationships/").param("$subOrObj.steward", stewardId)
        }
    }

    /**
     * Ensures that number of incoming [statements] == number of results
     * and that all [statements] exist in the response.
     */
    @Then("all of the following relationships are returned from the item graph")
    fun all_of_the_following_relationships_are_returned_from_the_item_graph(statements: List<Statements.RelationshipStatement>) {
        val result = retrieveAllPages()

        logger.info("Result ${result.count()}")
        val actual = result.flatMap { it.message.relationships }.count()
        Assertions.assertEquals(statements.size, actual, "Expected ${statements.size} results, got $actual")

        statements.forEach { statement ->
            val pageResults = retrieveAllPages().flatMap { it.message.relationships }

            val count = relationshipsResponseContainsStatementCount(
                pageResults, statement
            )

            Assertions.assertEquals(
                1, count, "Could not find $statement in the response exactly once, looking in $actual"
            )
        }
    }

    /**
     * Ensures that all incoming [statements] exist in the response, even if the response
     * contains more results than the number of statements.
     * It does not assert that number of statements == number of results.
     *
     * NULL for the subj and obj's collection and type mean we check that the value is indeed missing.
     */
    @Then("the following relationships are returned from the item graph")
    fun the_following_relationships_are_returned_from_the_item_graph(statements: List<Statements.RelationshipStatement>) {
        statements.forEach { statement ->
            val result = retrieveAllPages()
            val relationships = result.flatMap { it.message.relationships }

            Assertions.assertEquals(
                1, relationshipsResponseContainsStatementCount(
                    relationships, statement
                ), "Could not find $statement in the response exactly once, looking in $relationships"
            )
        }
    }

    @Then("{int} relationships are returned from the item graph")
    fun relationships_are_returned_from_the_item_graph(rows: Int) {
        val result = retrieveAllPages().flatMap { it.message.relationships }
        Assertions.assertEquals(result.count(), rows, "Expected to find $rows results, got ${result.count()}")
    }

    @Then("relationships are returned in pages of {int}")
    fun relationships_are_returned_in_pages_of(rows: Int) {
        val result = retrieveAllPages()

        val sizes = result.map { it.message.relationships.count() }

        sizes.forEach {
            Assertions.assertTrue(it <= rows, "Expected every page to be <= $rows, got $sizes")
        }
    }


    @Then("HTTP status {string} is returned")
    fun http_status_is_returned(httpStatus: String) {
        val result = retrieveAllPages()
        Assertions.assertEquals(
            httpStatus, result.first().status.name, "Expected status $httpStatus got ${result.first().status.name}"
        )
    }

    @Then("message type {string} is returned")
    fun message_type_is_returned(expected: String) {
        val result = retrieveAllPages()

        val actual = result.first().messageType.getLabel()
        Assertions.assertEquals(expected, actual, "Expected $expected results, got $actual")
    }

    private fun readRelationshipsResponse(): RelationshipsResponse {
        val rsp =
            mapper.readValue(apiResponseContext.andReturn().response.contentAsString, RelationshipsResponse::class.java)

        return rsp
    }


    private fun relationshipsResponseContainsStatementCount(
        relationships: List<RelationshipView>,
        relationshipStatement: Statements.RelationshipStatement,
    ): Int = relationships.count {
        val subjectOk = it.subject.id == relationshipStatement.subjectId
        val objectOk = it.`object`.id == relationshipStatement.objectId

        // Compare for equality only if supplied.
        val subjectCollectionOk = if (relationshipStatement.subjectCollection == NULL_LITERAL) {
            it.subject.collection == null
        } else if (relationshipStatement.subjectCollection != null) {
            it.subject.collection == relationshipStatement.subjectCollection
        } else {
            true
        }

        val subjectTypeOk = if (relationshipStatement.subjectType == NULL_LITERAL) {
            it.subject.type == null
        } else if (relationshipStatement.subjectType != null) {
            it.subject.type == relationshipStatement.subjectType
        } else {
            true
        }

        val objectCollectionOk = if (relationshipStatement.objectCollection == NULL_LITERAL) {
            it.`object`.collection == null
        } else if (relationshipStatement.objectCollection != null) {
            it.`object`.collection == relationshipStatement.objectCollection
        } else {
            true
        }

        val objectTypeOk = if (relationshipStatement.objectType == NULL_LITERAL) {
            it.`object`.type == null
        } else if (relationshipStatement.objectType != null) {
            it.`object`.type == relationshipStatement.objectType
        } else {
            true
        }

        val assertedByOk = if (relationshipStatement.assertedBy != null) {
            it.assertedBy == relationshipStatement.assertedBy
        } else {
            true
        }

        val linkedByOk = if (relationshipStatement.linkedBy != null) {
            it.linkedBy == relationshipStatement.linkedBy
        } else {
            true
        }

        val relationshipTypeOk = it.relationshipType == relationshipStatement.relationshipType

        subjectOk && subjectCollectionOk && subjectTypeOk && objectCollectionOk && objectOk && objectTypeOk && assertedByOk && linkedByOk && relationshipTypeOk
    }

    /**
     * For given request, paginate all cursor pages and retrieve.
     *
     * Takes a callback function, not a MockHttpServletRequestBuilder directly. This is because it's mutable, so you
     * can't keep re-building from the same base.
     */
    private fun retrieveAllPages(): List<RelationshipsResponse> {
        val result = mutableListOf<RelationshipsResponse>()

        fun recurse(cursor: String) {
            // Capture immutable reference.
            val request = nextRequest
            if (request == null) {
                fail("There was no API request to perform.")
            } else {
                val finalRequest = request().param("cursor", cursor)
                apiResponseContext.perform(finalRequest)
            }

            val rsp = readRelationshipsResponse()
            result.add(rsp)
            val nextCursor = rsp.message.nextCursor

            if (!nextCursor.isNullOrBlank()) {
                recurse(nextCursor)
            }
        }

        recurse("*")

        return result
    }

    companion object {
        // The string "NULL" for use in tests to denote a null response.
        const val NULL_LITERAL = "NULL"
    }

}
