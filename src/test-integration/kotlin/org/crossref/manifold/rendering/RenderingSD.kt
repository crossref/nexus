package org.crossref.manifold.rendering

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import io.cucumber.java.Before
import io.cucumber.java.en.Given
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.crossref.manifold.api.Feed
import org.crossref.manifold.api.Link
import org.crossref.manifold.common.ApiResponseContext
import org.crossref.manifold.common.ItemGraphSetup
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.itemgraph.Resolver
import org.crossref.manifold.itemtree.*
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.net.URI
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import java.util.*

class RenderingSD(
    @Autowired val renderStatusDao: RenderStatusDao,

    @Autowired val renderedItemDao: RenderedItemDao,

    @Autowired val renderedItemStorageDao: RenderedItemStorageDao,

    @Autowired val resolver: Resolver,

    @Autowired val jdbcTemplate: JdbcTemplate,

    @Autowired val itemGraph: ItemGraph,

    @Autowired val itemTreeRenderer: ItemTreeRenderer,

    @Autowired private val itemGraphSetup: ItemGraphSetup,

    @Autowired private val apiResponseContext: ApiResponseContext,
) {

    val logger: Logger = LoggerFactory.getLogger(this::class.java)
    private val mapper = XmlMapper()

    // Store feed results between steps.
    private var feedPages = mutableListOf<Feed>()

    // Store the request builder, so we can repeat requests until they pass.
    private var nextRequest: MockHttpServletRequestBuilder? = null

    @Before
    fun beforeEach() {
        nextRequest = null
        resetRendering()
    }


    /**
     * Reset the rendering system.
     * No items will be marked stale, pending render, or rendered.
     */
    private fun resetRendering() {
        // Remove all render flags, including anything stale.
        // This will avoid any extra items being added to the queue.
        jdbcTemplate.update("TRUNCATE item_render_status_queue RESTART IDENTITY CASCADE")

        // Remove all rendered items.
        jdbcTemplate.update("TRUNCATE rendered_item RESTART IDENTITY CASCADE")
    }

    /**
     * Tries to resolve an item by its identifier or primary key, and
     * asserts that the item has been resolved
     * @return the resolved [Item]
     */
    fun resolveAndCheckItem(identifier: String): Item {
        val resolvedItem: Item = identifier.toLongOrNull()?.let {
            resolver.resolveRO(ItemTree(Item(pk = identifier.toLong()))).root
        } ?: ItemTree(resolver.resolveRO(IdentifierParser.parse(identifier))).root
        Assertions.assertNotNull(resolvedItem, "Expected to find item with identifier $identifier")
        Assertions.assertNotNull(resolvedItem.pk, "Expected to find pk for $identifier")
        return resolvedItem
    }

    /**
     * Tries to resolve a string representation of [ContentType], and
     * asserts that the [contentType] has been resolved
     * @return the resolved Item
     */
    fun resolveAndCheckContentType(contentType: String): ContentType {
        val resolvedContentType = ContentType.fromMimeType(contentType)
        Assertions.assertNotNull(
            resolvedContentType!!, "Expected to be able to resolve $contentType value to ContentType enum"
        )
        return resolvedContentType
    }

    private fun getApiResponseAsFeed(): Feed {
        val result =
            apiResponseContext.perform(nextRequest!!).andDo(MockMvcResultHandlers.log()).andExpect(status().isOk)
                .andReturn()

        return mapper.readValue(result.response.contentAsString, Feed::class.java)
    }

    @Given("no Items have been rendered yet")
    fun no_items_have_been_rendered_yet() {
        resetRendering()
    }

    private fun retrieve() = nextRequest?.let {
        apiResponseContext.perform(it).andReturn()
    }

    @Then("the Content-Type header should be {string}")
    fun the_content_type_header_should_be(expectedContentType: String) {

        val status = retrieve()
        Assertions.assertEquals(200, status?.response?.status, "Expected response 200, got ${status?.response?.status}")

        val contentType = retrieve()?.response?.getHeader("Content-Type")
        Assertions.assertEquals(expectedContentType, contentType, "Expected content type $expectedContentType got headers: $contentType")
    }

    @Then("the Link header should be {string}")
    fun the_link_header_should_be(expectedLink: String) {
        val result = retrieve()

        val apiFeedLink = URLDecoder.decode(result?.response?.getHeader("Link"), StandardCharsets.UTF_8)

        Assertions.assertEquals(expectedLink, apiFeedLink , "Expected API Feed link to be $expectedLink, was: $apiFeedLink")
    }

    @Then("the feed has a {string} link href prefixed with the feed endpoint {string}")
    fun the_feed_has_a_link_href_prefixed_with_the_feed_endpoint(rel: String, endpoint: String) {
        val feed = getApiResponseAsFeed()
        val expected = "http://localhost:1234$endpoint"
        Assertions.assertEquals(expected, feed.links.first { l -> l.rel == rel }.href)
    }

    @Then("item {string} is rendered in {string} format")
    @When("the render for {string} in {string} has become available")
    fun render_for_item_in_format_has_become_available(
        identifier: String,
        contentType: String,
    ) {

        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)

        val found = renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType)

        Assertions.assertTrue( found.isNotEmpty(), "Expected to find item $identifier rendered in $contentType")
    }

    private fun findLinksMatching(check: (link: Link) -> Boolean): List<Link> =
        this.feedPages.flatMap { it.entries.flatMap { entries -> entries.links.filter(check) } }

    @Then("an entry has related link to the versioned, typed item endpoint {string} with type {string}")
    fun an_entry_related_link(
        identifier: String,
        contentType: String,
    ) {
        val relType = "related"

        val resolvedItem = resolveAndCheckItem(identifier)

        val expectedPath = "/beta/items/${resolvedItem.identifiers.first().uri}"

        val matchingLinks = findLinksMatching {
            it.rel == relType && it.type == contentType && URI(it.href).path == expectedPath
        }

        Assertions.assertTrue(
            matchingLinks.isNotEmpty(),
            "Expected to find 'related' links for type $contentType with path $expectedPath. Got: ${this.feedPages.map { it.entries }}"
        )

        // URI decodes encoded query strings.
        val queryStrings = matchingLinks.map { URI(it.href).query }

        Assertions.assertTrue(
            queryStrings.all { it.contains("version=") },
            "All 'related' links should have a version parameter. Got query strings: $queryStrings"
        )

        Assertions.assertTrue(
            queryStrings.all { it.contains("content-type=${contentType}") },
            "All 'related' links should include the content type $contentType. Got query strings: $queryStrings"
        )
    }

    @Then("an entry has enclosure link to the s3 path for {string} with type {string}")
    fun an_entry_enclosure_link(
        identifier: String,
        contentType: String,
    ) {
        val relType = "enclosure"

        val resolvedItem = resolveAndCheckItem(identifier)
        val resolvedContentType = resolveAndCheckContentType(contentType)
        val renderedItem = renderedItemDao.getRenderedItemHistory(resolvedItem.pk!!, resolvedContentType).last()

        val expectedPath = "/${renderedItem.getKey()}"

        val alternates = findLinksMatching {
            it.rel == relType && it.type == contentType && URI(it.href).path == expectedPath
        }

        Assertions.assertTrue(
            alternates.isNotEmpty(),
            "Expected to find 'enclosure' links for type $contentType with path $expectedPath. Got: ${this.feedPages.map { it.entries }}"
        )
    }

    @Then("an entry has alternate link to the un-versioned item endpoint for {string} with type {string}")
    fun an_entry_alternate_link_href_is_prefixed_with_the_item_endpoint_for_identifier(
        identifier: String,
        contentType: String,
    ) {
        val relType = "alternate"

        val resolvedItem = resolveAndCheckItem(identifier)

        val expectedPath = "/beta/items/${resolvedItem.identifiers.first().uri}"

        val alternates = findLinksMatching {
            it.rel == relType && it.type == contentType && URI(it.href).path == expectedPath
        }

        Assertions.assertTrue(
            alternates.isNotEmpty(),
            "Expected to find '$relType' links for type $contentType with path $expectedPath. Got: ${this.feedPages.map { it.entries }}"
        )

        Assertions.assertTrue(
            alternates.all { URI(it.href).query == null },
            "No '$relType' links should have query parameters, as they are for the un-versioned endpoint."
        )
    }

    @When("the feed is retrieved from the {string} endpoint")
    fun the_feed_is_retrieved_from_the_endpoint(endpoint: String) {
        nextRequest = get(URI("http://localhost:1234$endpoint"))
    }

    @When("all feed items are retrieved from the {string} endpoint, following cursors in pages of {int}")
    fun the_feed_for_item_and_content_type_is_retrieved_from_the_endpoint(
        endpoint: String,
        pageSize: Int,
    ) {
        retrieveAll("http://localhost:1234$endpoint?rows=$pageSize")

        val someEntries = this.feedPages.any {
            it.entries.isNotEmpty()
        }

        Assertions.assertTrue( someEntries, "Expected to retrieve a feed with some items in it")
    }

    /**
     * Iterate all results from the feed API, store result in this.feedPages.
     */
    private fun retrieveAll(url: String) {
        nextRequest = get(URI(url))


        while (nextRequest != null) {
            logger.info("GET $nextRequest.")
            val feed = getApiResponseAsFeed()
            feedPages.add(feed)

            val next = feed.links.firstOrNull { l -> l.rel == "next" }
            logger.info("NEXT $next")

            nextRequest = next?.let {
                get(URI(it.href))
            }
        }
    }

    @When("{string} is retrieved from the {string} endpoint")
    fun is_retrieved_from_the_endpoint(identifier: String, endpoint: String) {
        nextRequest = get(URI("http://localhost:1234$endpoint$identifier"))
    }

    @When("{string} is retrieved from the {string} endpoint with content type {string}")
    fun is_retrieved_from_the_endpoint_with_type(identifier: String, endpoint: String, contentType: String) {
        nextRequest = get(URI("http://localhost:1234$endpoint$identifier")).param("content-type", contentType)
    }
}
