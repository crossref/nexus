package org.crossref.manifold.xmlRendering

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.ItemTreeSerializer
import org.crossref.manifold.modules.crossrefxml.XmlUtil
import java.io.File

object Util {
    private val jsonMapper = JsonMapper.builder()
        .addModule(SimpleModule().addSerializer(ItemTree::class.java, ItemTreeSerializer()))
        .addModule(JavaTimeModule())
        .configure(SerializationFeature.INDENT_OUTPUT, true)
        .build().registerKotlinModule()

    private fun dumpItemTreeJson(fileName: String, itemTree: ItemTree) {
        val baseFname = fileName.split("/")[1]
        val jsonFname = baseFname.split(".").toMutableList().let {
            it[it.size - 1] = "json"
            it.joinToString(".")
        }
        val resourceDir = "./src/test-integration/resources"
        // serialize ItemTree to a JSON string and save it to a file in the "test-integration" resources
        File("${resourceDir}/json.itemtree/${jsonFname}").printWriter().use { out ->
            out.println(jsonMapper.writeValueAsString(itemTree))
        }
        // copy the deposit file to the "test-integration" resources
        XmlUtil.getXmlFile(
            fileName,
            "deposit.invalid"
        )?.copyTo(File("${resourceDir}/xml/xmlRendering/${baseFname}"), overwrite = true)
    }
}