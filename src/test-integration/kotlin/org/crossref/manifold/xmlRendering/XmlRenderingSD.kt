package org.crossref.manifold.xmlRendering

import io.cucumber.java.Before
import io.cucumber.java.en.Then
import io.cucumber.java.en.When
import org.crossref.manifold.common.*
import org.junit.jupiter.api.Assertions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper
import java.io.FileOutputStream
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths


class XmlRenderingSD(
    @Autowired private val apiResponseContext: ApiResponseContext,
) {
    private val objectMapper = ObjectMapper()
    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    // Store the request builder, so we can repeat requests until they pass.
    private var nextRequest: MockHttpServletRequestBuilder? = null

    // timestamp is changing every run, so we remove it from the test XML strings before comparing them
    private val timestampRegex = Regex(
        "(?<start></doi_batch_id>).+(?<end><depositor>)",
        setOf(RegexOption.MULTILINE, RegexOption.DOT_MATCHES_ALL)
    )

    @Before
    fun beforeEach() {
        nextRequest = null
    }

    private fun retrieve() = nextRequest?.let {
        apiResponseContext.perform(it).andReturn()
    }

    @When("a Deposit XML rendering is requested for {string}")
    fun a_conversion_is_requested_for(input: String) {
        val content = itemTreeJson(input).bufferedReader().use { it.readText() }
        val itemTree = objectMapper.readTree(content)
        val requestJson = hashMapOf(
            "itemTree" to itemTree,
            "userInfo" to hashMapOf(
                "doiBatchId" to "none",
                "depositorName" to "Test Depositor",
                "depositorEmail" to "depositor@dev.local",
                "registrant" to "Test Registrant",
            )
        )
        nextRequest = post(URI("http://localhost:1234/beta/convert"))
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .characterEncoding("UTF-8")
            .content(objectMapper.writeValueAsString(requestJson))
    }

    @When("web frontend sends JSON {string}")
    fun web_frontend_sends_json(input: String) {
        val content = apiRequestJson(input).bufferedReader().use { it.readText() }
        nextRequest = post(URI("http://localhost:1234/beta/convert"))
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .characterEncoding("UTF-8")
            .content(content)
    }

    @Then("the response status code should be {int} and body should be {string}")
    fun the_response_body_should_be(code: Int, expectedFile: String) {
        val expectedBody = removeTimestamp(xmlResource(expectedFile).bufferedReader().use { it.readText() })
        val result = retrieve()!!
        val response = result.response
        val responseBody = removeTimestamp(response.contentAsString)

        processDebugFiles(expectedFile, expectedBody, responseBody)
        Assertions.assertEquals(
            expectedBody,
            responseBody,
            "Rendered Deposit XML doesn't match the expected XML"
        )
        Assertions.assertEquals(code, response.status, "Expected response $code, got ${response.status}")
    }

    @Then("the response status code should be {int} and error message should be {string}")
    fun the_response_error_should_be(code: Int, errorJson: String) {
        val expectedBody = resource(errorJson).bufferedReader().use { it.readText() }
        val result = retrieve()!!
        val response = result.response

        Assertions.assertEquals(code, response.status, "Expected response $code, got ${response.status}")

        Assertions.assertEquals(
            expectedBody, response.contentAsString, "Expected body $expectedBody got body: ${response.contentAsString}"
        )
    }

    private fun removeTimestamp(xmlString: String): String =
        timestampRegex.replace(xmlString) { m ->
            m.groups["start"]?.value + m.groups["end"]?.value
        }

    private fun processDebugFiles(expectedFile: String, expectedBody: String, responseBody: String) {
        // to make debugging XML errors easier, save real output into a file next to the expected file
        val debugFilePath = Paths.get(".").normalize().toAbsolutePath()
            .resolve("src/test-integration/resources/xml")
            // `zfail` starts with `z` so it gets sorted after the expected file
            .resolve(expectedFile.replace(".xml", ".zfail.xml"))
        try {
            if (expectedBody != responseBody) {
                FileOutputStream(debugFilePath.toString()).use { outStream ->
                    outStream.write(responseBody.toByteArray())
                }
            } else {
                // if a test passes, remove its no-longer-relevant debug file
                Files.delete(debugFilePath)
            }
        } catch (e: Exception) {
            // ignore any errors when creating/deleting debug files, and just log them
            logger.error(e.toString())
        }
    }
}
