package org.crossref.manifold.modules.util

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemgraph.IdentifierPkItemPk
import org.crossref.manifold.itemgraph.ItemDao
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.PutObjectRequest


@SpringBootTest
internal class IngestS3IT {
    @Autowired
    lateinit var itemDao: ItemDao

    val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Autowired
    lateinit var s3client: S3Client

    @Test
    @Disabled
    fun testIngestS3() {

        val objectRequest = PutObjectRequest.builder()
            .bucket("md-bucket")
            .key("test.xml")
            .build()

        val resourceStream = Thread.currentThread().contextClassLoader
            .getResourceAsStream("test.xml")

        val requestBody = RequestBody
            .fromString(resourceStream!!.reader().use { it.readText() })

        logger.info("Posting file to S3...")
        s3client.putObject(objectRequest, requestBody)


        var response: IdentifierPkItemPk?
        var retries = 0
        do {
            logger.info("Searching for identifier...")
            response = itemDao.findItemAndIdentifierPk(IdentifierParser.parse("https://doi.org/10.1002/jnr.23992"))
            if (response == null) {
                runBlocking {
                    delay(100L)
                }
            }
        } while (response == null && ++retries < 300)

        assertNotNull(response)

    }

}