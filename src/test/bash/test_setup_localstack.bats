#!/usr/bin/env bats

load '../../../setup-localstack.sh'

@test "check_localstack returns success for valid-url" {
  curl() { return 0; }  # Simulate LocalStack is available

  run check_localstack "http://valid-url"
  [ "$status" -eq 0 ]
}

@test "check_localstack returns failure for invalid-url" {
  curl() { return 1; }  # Simulate LocalStack not available

  run check_localstack "http://invalid-url"
  [ "$status" -eq 1 ]
}

@test "check_aws_cli_available returns failure when AWS CLI is not installed" {
    # Mock the 'command' function to simulate the AWS CLI not available
    command() {
        if [ "$1" == "-v" ] && [ "$2" == "aws" ]; then
            return 1
        fi
    }
    run check_aws_cli_available
    [ "$status" -eq 1 ]
    echo "${lines[0]}"
    [ "${lines[0]}" = "AWS CLI is not installed. Visit https://aws.amazon.com/cli/ for installation instructions." ]
}

@test "is_psql_available returns success when psql command is found" {
  command() {
    if [ "$1" == "-v" ] && [ "$2" == "psql" ]; then
      return 0  # Simulate psql command is available
    fi
  }
  run is_psql_available
  [ "$status" -eq 0 ]
}

@test "is_psql_available returns failure when psql command is not found" {
  command() {
    if [ "$1" == "-v" ] && [ "$2" == "psql" ]; then
      return 1  # Simulate psql command is not available
    fi
  }
  run is_psql_available
  [ "$status" -eq 1 ]
  [ "${lines[0]}" = "psql command not found. PostgreSQL setup cannot proceed." ]
}

@test "parse_db_uri correctly parses a valid DB URI" {
  DB_URI_RW="jdbc:postgresql://localhost:5432/manifold?user=manifold&password=manifold"
  read db_host db_port db_name db_user db_password <<< $(parse_db_uri "$DB_URI_RW")
  [ "$db_host" = "localhost" ]
  [ "$db_port" = "5432" ]
  [ "$db_name" = "manifold" ]
  [ "$db_user" = "manifold" ]
  [ "$db_password" = "manifold" ]
}

@test "parse_db_uri correctly handles URIs without user and password" {
  DB_URI_RW="jdbc:postgresql://localhost:5432/manifold"
  read db_host db_port db_name db_user db_password <<< $(parse_db_uri "$DB_URI_RW")
  [ "$db_host" = "localhost" ]
  [ "$db_port" = "5432" ]
  [ "$db_name" = "manifold" ]
  [ -z "$db_user" ]  # Checks if db_user is empty
  [ -z "$db_password" ]  # Checks if db_password is empty
}

@test "parse_db_uri correctly parses URI with special characters in password" {
  DB_URI_RW="jdbc:postgresql://localhost:5432/manifold?user=manifold&password=ma%40n!fold123"
  read db_host db_port db_name db_user db_password <<< $(parse_db_uri "$DB_URI_RW")
  [ "$db_password" = "ma%40n!fold123" ]
}

@test "check_postgres returns success for valid connection" {
  command() {
    if [ "$1" == "-v" ] && [ "$2" == "psql" ]; then
      return 0  # Simulate psql command is available
    fi
  }
  psql() { return 0; }  # Simulate successful connection

  run check_postgres "jdbc:postgresql://valid:5432/test?user=manifold&password=manifold"
  [ "$status" -eq 0 ]
}

@test "check_postgres returns failure for invalid connection" {
  command() {
    if [ "$1" == "-v" ] && [ "$2" == "psql" ]; then
      return 0  # Simulate psql command is available
    fi
  }
  psql() { return 1; }  # Simulate failed connection

  run check_postgres "jdbc:postgresql://invalid:5432/test?user=manifold&password=manifold"
  [ "$status" -eq 1 ]
}

@test "setup_infrastructure creates resources successfully" {
  aws() { echo "Mock resource creation response"; return 0; }  # Mock AWS CLI

  run setup_infrastructure
  [ "$status" -eq 0 ]

  # Check if the output contains "mock response" for each command
  [[ "${output}" =~ "SQS Queue created: Mock resource creation response" ]] || fail "SQS Queue creation mock response not found"
  [[ "${output}" =~ "DLQ Queue created: Mock resource creation response" ]] || fail "DLQ Queue creation mock response not found"
  [[ "${output}" =~ "Rendering Queue created: Mock resource creation response" ]] || fail "Rendering Queue creation mock response not found"
  [[ "${output}" =~ "S3 Bucket created: Mock resource creation response" ]] || fail "S3 Bucket creation mock response not found"
}

@test "setup_infrastructure handles failure in resource creation" {
  aws() { echo "error creating resource"; return 1; }  # Mock AWS CLI failure

  run setup_infrastructure
  [ "$status" -eq 1 ]
}

@test "display_env_variables_for_intellij displays overridden variables correctly" {
  export AWS_ACCESS_KEY_ID="override_key_id"
  export AWS_SECRET_ACCESS_KEY="override_access_key"
  export AWS_REGION="override_region"
  export AWS_ENDPOINT_OVERRIDE="http://override:4566"
  export DB_URI_RW="jdbc:postgresql://localhost:5432/override?user=manifold&password=manifold"

  run display_env_variables_for_intellij
  echo "Output: ${lines[@]}"

  # Add an assertion based on the expected output
  [ "${lines[1]}" = "AWS_ACCESS_KEY_ID=override_key_id; AWS_SECRET_ACCESS_KEY=override_access_key; AWS_REGION=override_region; AWS_ENDPOINT_OVERRIDE=http://override:4566; DB_URI_RW=jdbc:postgresql://localhost:5432/override?user=manifold&password=manifold;" ]
}

@test "display_env_variables_for_intellij with predefined values" {
  run display_env_variables_for_intellij
  echo "Output: ${lines[@]}"

  # Add an assertion based on the expected output when variables are missing
  [ "${lines[1]}" = "AWS_ACCESS_KEY_ID=test; AWS_SECRET_ACCESS_KEY=test; AWS_REGION=us-east-1; AWS_ENDPOINT_OVERRIDE=http://localhost:4566; DB_URI_RW=jdbc:postgresql://localhost:5432/manifold?user=manifold&password=manifold;" ]
}

@test "create_sns_topic creates SNS topic successfully" {
  aws() {
    if [[ "$1" == "sns" && "$2" == "create-topic" ]]; then
      echo "Mock SNS topic creation response"
      return 0
    fi
  }
  run create_sns_topic
  [ "$status" -eq 0 ]
  [[ "${output}" =~ "Mock SNS topic creation response" ]] || fail "SNS topic creation mock response not found"
}

@test "create_sns_topic handles failure in SNS topic creation" {
  aws() {
    if [[ "$1" == "sns" && "$2" == "create-topic" ]]; then
      echo "Error creating SNS topic"
      return 1
    fi
  }
  run create_sns_topic
  [ "$status" -eq 1 ]
}

@test "subscribe_sqs_to_sns subscribes SQS queue to SNS topic successfully" {
  aws() {
    if [[ "$1" == "sns" && "$2" == "list-topics" ]]; then
      echo '{ "Topics": [{ "TopicArn": "arn:aws:sns:us-east-1:000000000000:mdtopic" }] }'
    elif [[ "$1" == "sns" && "$2" == "subscribe" ]]; then
      echo "Mock SQS subscription to SNS topic response"
      return 0
    fi
  }
  run subscribe_sqs_to_sns
  [ "$status" -eq 0 ]
  [[ "${output}" =~ "Mock SQS subscription to SNS topic response" ]] || fail "SQS subscription to SNS topic mock response not found"
}

@test "subscribe_sqs_to_sns handles failure in SQS queue subscription to SNS topic" {
  aws() {
    if [[ "$1" == "sns" && "$2" == "subscribe" ]]; then
      echo "Error subscribing SQS queue to SNS topic"
      return 1
    fi
  }
  run subscribe_sqs_to_sns
  [ "$status" -eq 1 ]
}

@test "check_postgres_version returns success for version 13.8" {
    psql() {
        echo "PostgreSQL 13.8 on x86_64-pc-linux-gnu, compiled by gcc ..."
    }
    run check_postgres_version
    [ "$status" -eq 0 ]
    [ "${lines[1]}" = "PostgreSQL server version is within the recommended range." ]
}

@test "check_postgres_version returns success for version 15.0" {
    psql() {
        echo "PostgreSQL 15.0 on x86_64-pc-linux-gnu, compiled by gcc ..."
    }
    run check_postgres_version
    [ "$status" -eq 0 ]
    [ "${lines[1]}" = "PostgreSQL server version is within the recommended range." ]
}

# test major.minor.patch version
@test "check_postgres_version returns success for version 15.99.9" {
    psql() {
        echo "PostgreSQL 15.99.9 on x86_64-pc-linux-gnu, compiled by gcc ..."
    }
    run check_postgres_version
    [ "$status" -eq 0 ]
    [ "${lines[1]}" = "PostgreSQL server version is within the recommended range." ]
}

@test "check_postgres_version returns failure for version 13.7" {
    psql() {
        echo "PostgreSQL 13.7 on x86_64-pc-linux-gnu, compiled by gcc ..."
    }
    run check_postgres_version
    [ "$status" -eq 1 ]
    [ "${lines[1]}" = "PostgreSQL server version is outside the recommended range." ]
}

@test "check_postgres_version returns failure for version 16.0" {
    psql() {
        echo "PostgreSQL 16.0 on x86_64-pc-linux-gnu, compiled by gcc ..."
    }
    run check_postgres_version
    [ "$status" -eq 1 ]
    [ "${lines[1]}" = "PostgreSQL server version is outside the recommended range." ]
}

