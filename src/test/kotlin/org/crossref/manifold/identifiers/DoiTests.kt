package org.crossref.manifold.identifiers

import org.crossref.manifold.identifiers.parsers.Doi
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * The behaviour of the DOI identifier type and how we encode them in the database are of critical importance.
 * Once an identifier is in the database, we expect it to match. Changing the encoding of a DOI would mean a failure
 * to match.
 *
 * This applies to all identifier types, but DOIs are especially important.
 *
 * Do not edit any test cases!
 */
class DoiTests {

    /**
     * Recognise a well expressed DOI and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
                "https://doi.org/10.5555/1234567",
                IdentifierType.DOI
        )
    }

    /**
     * DOIs are persistent identifiers.
     */
    @Test
    fun persistent() {
        IdentifierAssertions.assertNormalizesTo(
                "https://doi.org/10.1002/abc",
                IdentifierType.DOI,
                "http://dx.doi.org/10.1002/abc"
        )
    }

    /**
     * DOIs are case-insensitive. They are normalized to lower case.
     */
    @Test
    fun normalizeCase() {
        IdentifierAssertions.assertNormalizesTo(
                "https://doi.org/10.1002/abc",
                IdentifierType.DOI,
                "https://doi.org/10.1002/ABC"
        )
    }

    /**
     * SICIs can contain all manner of interesting characters, including a terminal '#'.
     */
    @Test
    fun sicis() {
        IdentifierAssertions.assertNormalizesTo(
                "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23",
                IdentifierType.DOI,
                "10.1002/(SICI)1099-050X(199823/24)37:3/4<197::AID-HRM2>3.0.CO;2-#",
                "10.1002/(SICI)1099-050X(199823/24)37:3/4<197::AID-HRM2>3.0.CO;2-#",
                // Everything possible encoded.
                "https://doi.org/10.1002%2F%28sici%291099-050x%28199823%2F24%2937%3A3%2F4%3C197%3A%3Aaid-hrm2%3E3.0.co%3B2-%23",
                // Encoded according to the guidelines in the DOI handbook.
                "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23"
        )
    }

    @Test
    fun normalizeEncodings() {
        /** DOI handbook https://www.doi.org/doi-handbook/HTML/encoding-rules-for-urls.html
         * Specifies mandatory encoding of:
         *
         * % %25
         * " %22
         * # %23
         * SPACE %20
         * ? %3F
         */
        IdentifierAssertions.assertNormalizesTo(
                "https://doi.org/10.5555/%25%22%23%20%3F",
                IdentifierType.DOI,
                // Plain DOI
                """10.5555/%"# ?""",
        )

        /**
         * DOI handbook specify recommended encoding of
         * < %3C
         * > %3E
         * { %7B
         * } %7D
         * ^ %5E
         * [ %5B
         * ] %5D
         * ` %60
         * | %7C
         * \ %5C
         * + %2B
         */
        IdentifierAssertions.assertNormalizesTo(
                "https://doi.org/10.5555/%3C%3E%7B%7D%5E%5B%5D%60%7C%5C%2B",
                IdentifierType.DOI,
                // Plain DOI
                """10.5555/<>{}^[]`|\+""",

                // Fully encoded URL
                "https://doi.org/10.5555/%3C%3E%7B%7D%5E%5B%5D%60%7C%5C%2B",

                // URL with some encoded, some not. The "+" is encoded.
                """https://doi.org/10.5555/<>{}^[]`|\%2B""",

                // Every other is encoded.
                """https://doi.org/10.5555/<%3E{%7D^[%5D`%7C\%2B""",
        )

    }

    /**
     * URLs on the DOI.org domain that aren't actual DOIs should be treated as URLs.
     */
    @Test
    fun nonDois() {
        IdentifierAssertions.assertNormalizesTo(
                "https://www.doi.org/the-identifier/what-is-a-doi/",
                IdentifierType.URI
        )
        IdentifierAssertions.assertNormalizesTo(
                "https://www.doi.org/images/logos/header_logo_cropped.svg",
                IdentifierType.URI
        )
    }

    /**
     * Bad encoding should fall-through to be treated as a literal.
     */
    @Test
    fun badEncoding() {
        IdentifierAssertions.assertNormalizesTo(
                "https://doi.org/10.5555/%25",
                IdentifierType.DOI,
                "https://doi.org/10.5555/%25"
        )
    }

    /**
     * Landing page URLs that contain DOIs are not considered to be DOIs.
     */
    @Test
    fun notDois() {
        IdentifierAssertions.assertNormalizesTo(
                "https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0190046",
                IdentifierType.URI
        )

        IdentifierAssertions.assertNormalizesTo(
                "https://onlinelibrary.wiley.com/doi/10.1111/j.1751-0813.2010.00564.x",
                IdentifierType.URI
        )
    }

    @Test
    fun funderDois() {
        val inputs = listOf(
                "https://doi.org/10.13039/501100008406",
                "http://dx.doi.org/10.13039/501100008406",
                "10.13039/501100008406"
        )

        inputs.forEach {
            Assertions.assertTrue(
                    Doi.isFunderDoi(IdentifierParser.parse(it)),
                    "Expected $it to be identified as a Funder DOI"
            )
        }
    }

    @Test
    fun notFunderDois() {
        val inputs = listOf(
                "https://doi.org/10.5555/123456789",
                "http://dx.doi.org/10.5555/123456789",
                "10.5555/123456789"
        )

        inputs.forEach {
            Assertions.assertFalse(
                    Doi.isFunderDoi(IdentifierParser.parse(it)),
                    "Expected $it to be not be identified as a Funder DOI"
            )
        }
    }

    @Test
    fun testDOIRegularExpressions() {
        val valid = listOf(
                "10.12345/12345678",
                "10.12345//12345678",
                "10.12345%2f12345678",
                "10.12345%2F12345678",
                "10.1103/physrevlett.103.157203",
                "10.1111/1467%20106478.00146",
                "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23{",
                "https://doi.org/10.1002/(sici)1099-050x(199823/24)37:3/4%3C197::aid-hrm2%3E3.0.co;2-%23%7C",
                "https://doi.org/10.1675/1524-4695(2003)026[0119:iosoga]2.0.co;2"
        )

        val invalid = listOf(
                "https://doi.org/1012345/12345678",
                "1012345/12345678",
                "10.12345%212345678",
                "10 12345/12345678",
                "10/12345/12345678",
                "101067",
                "10-092322",
                " 10.12345/12345678",
                "/10.12345/12345678",
                "-10.12345/12345678",
                "110.12345/12345678",
                "a10.12345/12345678",
        )

        valid.forEach {
            val identifier = IdentifierParser.parse(it)
            Assertions.assertEquals(IdentifierType.DOI, identifier.type, "Input: $it Output: $identifier ")
        }

        invalid.forEach {
            val identifier = IdentifierParser.parse(it)
            Assertions.assertNotEquals(IdentifierType.DOI, identifier.type, "Input: $it Output: $identifier ")
        }
    }
}