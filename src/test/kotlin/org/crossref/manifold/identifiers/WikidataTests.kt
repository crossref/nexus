package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class WikidataTests {

    /**
     * Recognise WikiData URIs and don't change them.
     */
    @Test
    fun unchanged() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://www.wikidata.org/entity/Q420330",
            IdentifierType.WIKIDATA
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://www.wikidata.org/entity/Q420330",
            IdentifierType.WIKIDATA,
            "https://www.wikidata.org/entity/Q420330/",
        )
    }


    @Test
    fun normalize() {
        IdentifierAssertions.assertNormalizesTo(
            "https://www.wikidata.org/entity/Q420330",
            IdentifierType.WIKIDATA,
            "https://www.wikidata.org/wiki/Q420330",
            "https://www.wikidata.org/entity/Q420330",
            "http://www.wikidata.org/wiki/Q420330"
        )
    }
}