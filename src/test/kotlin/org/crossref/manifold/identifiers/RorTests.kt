package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class RorTests {

    /**
     * Recognise correct ROR Ids and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://ror.org/02mhbdp94",
            IdentifierType.ROR
        )

        IdentifierAssertions.assertNormalizesUnchanged(
            "https://ror.org/02twcfp32",
            IdentifierType.ROR
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://ror.org/02twcfp32",
            IdentifierType.ROR,
            "https://ror.org/02twcfp32/",
        )
    }



    /**
     * ROR is case-sensitive. Upper case variant doesn't get recognised or normalized.
     */
    @Test
    fun caseSensitive() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://ror.org/02twcfp32",
            IdentifierType.ROR
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://ror.org/02twcfp32",
            IdentifierType.ROR,
            "HTTPS://ROR.ORG/02TWCFP32"
        )
    }

    /**
     * If not a URI, not recognised.
     */
    @Test
    fun needsUri() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "02twcfp32",
            IdentifierType.URI
        )
    }

    @Test
    fun needsChecksum() {
        // With correct checksum, recognised as a ROR.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://ror.org/02twcfp32",
            IdentifierType.ROR
        )

        // Same digits but bad checksum - not recognised as a ROR.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://ror.org/02twcfp99",
            IdentifierType.URI
        )

    }

    /**
     * Other URLs on that domain not classified as RORs.
     */
    @Test
    fun edgeCases() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://ror.org/",
            IdentifierType.URI
        )

        IdentifierAssertions.assertNormalizesUnchanged(
            "https://ror.org/registry",
            IdentifierType.URI
        )

        IdentifierAssertions.assertNormalizesUnchanged(
            "https://ror.org",
            IdentifierType.URI
        )
    }
}