package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

/**
 * Tests for [PubMed]
 */
class PubMedTests {
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://pubmed.ncbi.nlm.nih.gov/30288709",
            IdentifierType.PUB_MED
        )
    }

    /**
     * Normalize scheme.
     */
    @Test
    fun scheme() {
        IdentifierAssertions.assertNormalizesTo(
            "https://pubmed.ncbi.nlm.nih.gov/30288709",
            IdentifierType.PUB_MED,
            "https://pubmed.ncbi.nlm.nih.gov/30288709",
            "http://pubmed.ncbi.nlm.nih.gov/30288709",
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://pubmed.ncbi.nlm.nih.gov/30288709",
            IdentifierType.PUB_MED,
            "https://pubmed.ncbi.nlm.nih.gov/30288709/",
        )
    }


    @Test
    fun edgeCases() {

        // Random page not classified as PubMed ID.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://pubmed.ncbi.nlm.nih.gov/help/",
            IdentifierType.URI
        )


    }
}