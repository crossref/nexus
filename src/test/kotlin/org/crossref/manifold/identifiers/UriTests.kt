package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class UriTests {

    /**
     * Recognise URIs and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "http://www.example.com",
            IdentifierType.URI
        )
    }

    /**
     * When a URI isn't recognised as a particular kind of identifier, the URL protocol should not be changed.
     */
    @Test
    fun urlsSchemeNotChanged() {
        IdentifierAssertions.normalizeDifferent(
            "http://example.com",
            "https://example.com"
        )
    }

    /**
     * When a URI isn't recognised as a particular kind of identifier, the case should not change.
     */
    @Test
    fun urlsCaseNotChanged() {
        IdentifierAssertions.normalizeDifferent(
            "https://example.com/abc",
            "https://example.com/ABC"
        )
    }

    /**
     * When a URI has percent-encoded characters and all characters are members of RFC 3986 (reserved + unreserved),
     * it should be unchanged.
     */
    @Test
    fun preEncoded() {
        // Special chars in query
        IdentifierAssertions.assertNormalizesTo(
            "https://example.com/one?t%5Bw%5Do#three",
            IdentifierType.URI,
            "https://example.com/one?t%5Bw%5Do#three"
        )
    }

    /**
     * Any characters that are not members of RFC 3986 (reserved + unreserved) should be encoded.
     */
    @Test
    fun notEncoded() {
        // Special chars in path, query, fragment.
        IdentifierAssertions.assertNormalizesTo(
            "https://example.com/one%3Ctwo%3Ethree?one=%3Ctwo%3Ethree#one%3Ctwo%3Ethree",
            IdentifierType.URI,
            "https://example.com/one<two>three?one=<two>three#one<two>three"
        )
    }
}