package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

/**
 * Tests for [PubmedCentral]
 */
class PubMedCentralTests {

    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://europepmc.org/articles/PMC6849755",
            IdentifierType.PUB_MED_CENTRAL
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://europepmc.org/articles/PMC6849755",
            IdentifierType.PUB_MED_CENTRAL,
            "https://europepmc.org/articles/PMC6849755/",
        )
    }

    /**
     * Normalize scheme.
     */
    @Test
    fun scheme() {
        IdentifierAssertions.assertNormalizesTo(
            "https://europepmc.org/articles/PMC6849755",
            IdentifierType.PUB_MED_CENTRAL,
            "https://europepmc.org/articles/PMC6849755",
            "http://europepmc.org/articles/PMC6849755",
        )
    }

    /**
     * Accession numbers are case-sensitive.
     */
    @Test
    fun normalize() {
        IdentifierAssertions.assertNormalizesTo(
            "https://europepmc.org/articles/PMC6849755",
            IdentifierType.PUB_MED_CENTRAL,
            "https://europepmc.org/articles/pmc6849755"
        )
    }
}