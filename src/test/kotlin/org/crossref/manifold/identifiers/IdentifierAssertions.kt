package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Assertions
import java.net.URI

object IdentifierAssertions {
    /**
     * The given input string is unchanged on normalization.
     * This is a useful check to ensure that no other identifier type handler is changing it unexpectedly.
     */
    fun assertNormalizesUnchanged(input: String, expectedType: IdentifierType) {
        val expectedUri = URI(input)
        val result = IdentifierParser.parse(input)
        Assertions.assertEquals(expectedType, result.type, "Expected identifier $input to normalize to Type.")
        Assertions.assertEquals(expectedUri, result.uri, "Expected identifier $input to normalize URI unchanged.")

    }


    /**
     * Given input string should convert to the given URI.
     * Represent expected URI as a string for compact tests. It must be a valid URI.
     */
    fun assertNormalizesTo(expectedUriString: String, expectedType: IdentifierType, vararg inputs: String) {
        inputs.forEach {
            val result = IdentifierParser.parse(it)
            val expectedUri = URI(expectedUriString)

            Assertions.assertEquals(expectedType, result.type, "Expected identifier $it to normalize to Type.")
            Assertions.assertEquals(expectedUri, result.uri, "Expected identifier $it to normalize to URI.")
        }
    }

    /**
     * The supplied inputs do not normalize to the same URI.
     */
    fun normalizeEqual(vararg inputs: String) {
        val converted = inputs.map { IdentifierParser.parse(it) }
        Assertions.assertTrue(
            converted.toSet().count() == 1,
            "Expected all inputs to be equal, got: $converted"
        )
    }

    /**
     * The supplied inputs normalize to the same URI.
     */
    fun normalizeDifferent(vararg inputs: String) {
        val converted = inputs.map { IdentifierParser.parse(it) }
        Assertions.assertTrue(
            converted.toSet().count() == inputs.count(),
            "Expected all inputs to be parse to distinct values, got: $converted"
        )
    }
}