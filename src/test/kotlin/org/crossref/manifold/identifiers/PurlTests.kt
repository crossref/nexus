package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class PurlTests {

    /**
     * Recognise Purl in canonical form and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://purl.org/10.36990/jippm.v1i1.257.t001",
            IdentifierType.PURL
        )
    }

    /**
     * We normalize PURLs on recognised resolver to the `purl.org` resolver.
     */
    @Test
    fun normalizeResolvers() {
        IdentifierAssertions.assertNormalizesTo(
            "https://purl.org/10.36990/jippm.v1i1.257.t001",
            IdentifierType.PURL,
            "https://purl.org/10.36990/jippm.v1i1.257.t001",
            "http://purl.org/10.36990/jippm.v1i1.257.t001",
            "https://purl.oclc.org/10.36990/jippm.v1i1.257.t001",
            "http://purl.oclc.org/10.36990/jippm.v1i1.257.t001",
        )
    }
}