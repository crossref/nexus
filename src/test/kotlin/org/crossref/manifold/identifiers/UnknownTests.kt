package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class UnknownTests {

    @Test
    fun `Inputs with brackets can be parsed`(){
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/unknown/MOST+%255B2019%255D+110-2314-B-195-033",
            IdentifierType.UNKNOWN,
            "MOST [2019] 110-2314-B-195-033")
    }

    @Test
    fun `Special characters can be parsed`(){
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/unknown/MOST+%255B2019%255D+%2523%253E%253F%252F%255C%2528%2529+%2526%253D-%252B_%252C%257B%257D%2522%2527%253A%253B%2524%2525%255E%2523%2540%2521+110-2314-B-195-033",
            IdentifierType.UNKNOWN,
            "MOST [2019] #>?/\\() &=-+_,{}\"\':;$%^#@! 110-2314-B-195-033")
    }
}