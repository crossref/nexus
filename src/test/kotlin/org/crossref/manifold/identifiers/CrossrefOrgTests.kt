package org.crossref.manifold.identifiers

import org.crossref.manifold.identifiers.parsers.CrossrefOrg
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.net.URI

/**
 * Tests for [CrossrefOrg].
 */
class CrossrefOrgTests {

    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/org/1234",
            IdentifierType.CROSSREF_ORG
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/org/1234",
            IdentifierType.CROSSREF_ORG,
            "https://id.crossref.org/org/1234/",
        )
    }


    /**
     * Recognise and normalize members and organizations
     */
    @Test
    fun membersConvertToOrganizations() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/org/5",
            IdentifierType.CROSSREF_ORG,
            "https://id.crossref.org/member/5",
            "http://id.crossref.org/member/5",
            "https://id.crossref.org/org/5",
            "http://id.crossref.org/org/5"
        )
    }

    /**
     * Path, scheme, domain should be treated as case-insensitive.
     */
    @Test
    fun caseInsensitive() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/org/5",
            IdentifierType.CROSSREF_ORG,
            "HTTPS://ID.CROSSREF.ORG/ORG/5",
            "HTTPS://ID.CROSSREF.ORG/MEMBER/5"
        )
    }

    @Test
    fun getOrganizationId() {
        Assertions.assertEquals(
            5,
            CrossrefOrg.getOrganizationId(
                IdentifierParser.parse("https://id.crossref.org/org/5")
            ),
            "Should return member ID from canonical URI."
        )

        Assertions.assertEquals(
            5,
            CrossrefOrg.getOrganizationId(
                IdentifierParser.parse("https://id.crossref.org/member/5")
            ),
            "Should return member ID when transformed from the legacy member format."
        )

        Assertions.assertNull(
            CrossrefOrg.getOrganizationId(
                IdentifierParser.parse("https://id.crossref.org/org/")
            ),
            "Should cope with missing returning null."
        )

        Assertions.assertNull(
            CrossrefOrg.getOrganizationId(
                IdentifierParser.parse("https://example.com/org/5")
            ),
            "Should cope with non-org, returning null."
        )
    }

    @Test
    fun getLegacyMemberId() {
        val result = CrossrefOrg.toLegacyMemberId(IdentifierParser.parse("https://id.crossref.org/org/123"))
        Assertions.assertEquals(result?.type, IdentifierType.URI)
        Assertions.assertEquals(result?.uri, URI("https://id.crossref.org/member/123"))
    }
}
