package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class IssnTests {

    /**
     * Recognise ISSNs and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/issn/1931-7557",
            IdentifierType.ISSN
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/issn/1931-7557",
            IdentifierType.ISSN,
            "https://id.crossref.org/issn/1931-7557/",
        )
    }


    @Test
    fun checkDigit() {
        // Good
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/issn/1931-7557",
            IdentifierType.ISSN
        )

        // Bad
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/issn/1931-755X",
            IdentifierType.URI
        )

        val samples = listOf(
            "https://id.crossref.org/issn/2040-0330",
            "https://id.crossref.org/issn/2358-4130",
            "https://id.crossref.org/issn/2744-1261",
            "https://id.crossref.org/issn/2949-1061",
            "https://id.crossref.org/issn/1843-5912",
            "https://id.crossref.org/issn/2664-1682",
            "https://id.crossref.org/issn/2392-3113",
            "https://id.crossref.org/issn/2411-8524",
            "https://id.crossref.org/issn/2697-3294",
            "https://id.crossref.org/issn/2602-8476",
            "https://id.crossref.org/issn/2614-8587",
            "https://id.crossref.org/issn/1841-8317",
            "https://id.crossref.org/issn/2668-6678",
            "https://id.crossref.org/issn/2828-0709",
            "https://id.crossref.org/issn/2412-950X",
            "https://id.crossref.org/issn/2346-139X",
            "https://id.crossref.org/issn/2473-912X",
        )

        for (sample in samples) {
            IdentifierAssertions.assertNormalizesUnchanged(
                sample,
                IdentifierType.ISSN
            )

            // Replace last digit, it should fail to parse as an ISSN.
            val broken = sample.substring(0, sample.length - 1) + "Q"
            IdentifierAssertions.assertNormalizesUnchanged(
                broken,
                IdentifierType.URI
            )
        }
    }

    @Test
    fun normalize() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/issn/1931-7557",
            IdentifierType.ISSN,
            "http://id.crossref.org/issn/1931-7557",
            "urn:ISSN:1931-7557",
            "URN:issn:1931-7557",
            "https://urn.issn.org/urn:issn:1931-7557",
            "https://urn.issn.org/URN:ISSN:1931-7557",
            "https://urn.issn.org/UrN:IsSn:1931-7557",
            "http://issn.org/resource/ISSN/1931-7557"
        )
    }

    @Test
    fun edgeCases() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/issn/2358-4130",
            IdentifierType.ISSN
        )

        // Add a digit, and it's not classified as an ISSN.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/issn/2358-41301",
            IdentifierType.URI
        )

        // Subtract a digit fails too.
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/issn/2358-413",
            IdentifierType.URI
        )
    }
}