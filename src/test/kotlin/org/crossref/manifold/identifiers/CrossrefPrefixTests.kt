package org.crossref.manifold.identifiers

import org.crossref.manifold.identifiers.parsers.CrossrefPrefix
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * Identifier for a Crossref 'prefix'. This is not the same thing as a DOI prefix, and
 * should be treated as an opaque identifier.
 *
 * Crossref Prefixes are used to identify groups of DOIs for access control and reporting.
 */
class CrossrefPrefixTests {

    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/prefix/10.5555",
            IdentifierType.CROSSREF_PREFIX
        )
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/prefix/10.5555",
            IdentifierType.CROSSREF_PREFIX,
            "https://id.crossref.org/prefix/10.5555/",
        )
    }


    /**
     * We should cope with prefixes supplied in the wrong case.
     */
    @Test
    fun caseInsensitive() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/prefix/10.5555",
            IdentifierType.CROSSREF_PREFIX,
            "HTTPS://ID.CROSSREF.ORG/PREFIX/10.5555",
        )
    }


    @Test
    fun getDoiPrefix() {
        Assertions.assertEquals(
            "10.5555",
            CrossrefPrefix.getDoiPrefix(
                IdentifierParser.parse("https://id.crossref.org/prefix/10.5555")
            )
        )
    }

}