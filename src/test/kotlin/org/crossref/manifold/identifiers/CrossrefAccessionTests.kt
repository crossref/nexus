package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

/**
 * Tests for [CrossrefAccession]
 */
class CrossrefAccessionTests {

    /**
     * Recognise Accession URIs and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/accession/accession-4",
            IdentifierType.CROSSREF_ACCESSION
        )

        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/accession/SASDB69",
            IdentifierType.CROSSREF_ACCESSION
        )


        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/accession/6OO2",
            IdentifierType.CROSSREF_ACCESSION
        )


        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/accession/6SWR",
            IdentifierType.CROSSREF_ACCESSION
        )

        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/accession/EMPIAR-10805",
            IdentifierType.CROSSREF_ACCESSION
        )
    }

    /**
     * Normalize scheme.
     */
    @Test
    fun scheme() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/accession/accession-4",
            IdentifierType.CROSSREF_ACCESSION,
            "https://id.crossref.org/accession/accession-4",
            "http://id.crossref.org/accession/accession-4",
        )
    }

    /**
     * Accession numbers are case-sensitive.
     */
    @Test
    fun caseSensitive() {
        IdentifierAssertions.normalizeDifferent(
            "https://id.crossref.org/accession/accession-4",
            "https://id.crossref.org/accession/ACCESSION-4"
        )
    }
}