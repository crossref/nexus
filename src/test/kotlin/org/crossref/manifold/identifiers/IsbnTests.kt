package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class IsbnTests {


    /**
     * Recognise standard 13 digit examples, and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/isbn/9780306406157",
            IdentifierType.ISBN
        )

        listOf(
            "9781566199094",
            "9780123456472",
            "9781413304541"
        ).forEach {
            IdentifierAssertions.assertNormalizesUnchanged(
                "https://id.crossref.org/isbn/$it",
                IdentifierType.ISBN
            )
        }
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/isbn/9780306406157",
            IdentifierType.ISBN,
            "https://id.crossref.org/isbn/9780306406157/",
        )
    }


    /**
     * Hyphens can be accommodated in arbitrary places, recognised and adjusted.
     */
    @Test
    fun hyphens() {
        // 13 digit
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/isbn/9780306406157",
            IdentifierType.ISBN,
            "https://id.crossref.org/isbn/9780306406157",
            "https://id.crossref.org/isbn/9-7-8-0-3-0-6-4-0-6-1-5-7",
            "https://id.crossref.org/isbn/9780-30640-6157",
            "https://id.crossref.org/isbn/978-0306-4061-57-"
        )

        // 10 digit should cope with hyphens. It will be converted to 13 digit.
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/isbn/9788175257665",
            IdentifierType.ISBN,
            "https://id.crossref.org/isbn/8175257660",
            "https://id.crossref.org/isbn/817-5257-66-0-",
            "https://id.crossref.org/isbn/8-17525-7660",
            "https://id.crossref.org/isbn/-8175-257660"
        )
    }

    /**
     * Bad checksum should be rejected.
     */
    @Test
    fun badChecksum() {
        // 13 digit with bad checksum
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/isbn/978030640615X",
            IdentifierType.URI
        )

        // 10 digit with bad checksum
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/isbn/978817525766X",
            IdentifierType.URI
        )
    }

    /**
     * 10-digit ISBNs should be normalized to 13 digit ones, so they can be compared.
     */
    @Test
    fun normalizeTenToThirteen() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/isbn/9788175257665",
            IdentifierType.ISBN,
            "https://id.crossref.org/isbn/8175257660",
        )

        // Check that the result of conversion can be parsed, and is unchanged.
        IdentifierAssertions.assertNormalizesUnchanged(
            IdentifierParser.parse("https://id.crossref.org/isbn/8175257660").uri.toString(),
            IdentifierType.ISBN
        )
    }


}