package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

class OrcidTests {

    /**
     * Recognise ORCID URIs and don't change them. A few provided by the ORCID docs.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://orcid.org/0000-0002-1028-6941",
            IdentifierType.ORCID
        )

        // Number as checksum digit.
        IdentifierAssertions.assertNormalizesUnchanged("https://orcid.org/0000-0002-1825-0097", IdentifierType.ORCID)
        IdentifierAssertions.assertNormalizesUnchanged("https://orcid.org/0000-0001-5109-3700", IdentifierType.ORCID)

        // X as the checksum digit.
        IdentifierAssertions.assertNormalizesUnchanged("https://orcid.org/0000-0002-1694-233X", IdentifierType.ORCID)
    }

    /**
     * Remove trailing slash.
     */
    @Test
    fun trailingSlash() {
        IdentifierAssertions.assertNormalizesTo(
            "https://orcid.org/0000-0002-1028-6941",
            IdentifierType.ORCID,
            "https://orcid.org/0000-0002-1028-6941/",
        )
    }


    @Test
    fun normalize() {
        IdentifierAssertions.assertNormalizesTo(
            "https://orcid.org/0000-0002-1028-6941",
            IdentifierType.ORCID,
            "https://orcid.org/0000-0002-1028-6941",
            "http://orcid.org/0000-0002-1028-6941",
            "HTTPS://ORCID.ORG/0000-0002-1028-6941",
        )
    }

    /**
     * Don't match an ORCID ID if it isn't expressed as a full URI.
     * ORCIDs IDs also follow the same format as ISNI, ISO-44292 .
     * So don't try and guess.
     */
    @Test
    fun needsUri() {
        IdentifierAssertions.assertNormalizesTo(
            "0000-0002-1028-6941",
            IdentifierType.URI,
            "0000-0002-1028-6941"
        )
    }

    /**
     * If the checksum doesn't match, then it's not an ORCID ID.
     */
    @Test
    fun needsChecksum() {
        IdentifierAssertions.assertNormalizesTo(
            "https://orcid.org/0000-0002-9999-9999",
            IdentifierType.URI,
            "https://orcid.org/0000-0002-9999-9999"
        )
    }

    /**
     * ORCID domain isn't an ORCID ID.
     */
    @Test
    fun edgeCases() {
        IdentifierAssertions.assertNormalizesTo(
            "https://orcid.org/",
            IdentifierType.URI,
            "https://orcid.org/"
        )

        IdentifierAssertions.assertNormalizesTo(
            "https://orcid.org",
            IdentifierType.URI,
            "https://orcid.org"
        )
    }

}