package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

/**
 * Tests for [Ark].
 */
class ArkTests {
    /**
     * Recognise Ark URIs and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "ark:12345/x6np1wh8k/c3/s5.v7.xsl",
            IdentifierType.ARK
        )
    }

    /**
     * Convert ARKs to the n2t resolver for storage.
     */
    @Test
    fun normalize() {
        IdentifierAssertions.assertNormalizesTo(
            "ark:12345/x6np1wh8k/c3/s5.v7.xsl",
            IdentifierType.ARK,
            "https://n2t.net/ark:12345/x6np1wh8k/c3/s5.v7.xsl",
            "ark:12345/x6np1wh8k/c3/s5.v7.xsl"
        )
    }

    /**
     * Hyphens should be removed.
     * Examples taken from IETF RFC draft-kunze-ark-37.
     */
    @Test
    fun hyphens() {
        IdentifierAssertions.assertNormalizesTo(
            "ark:12345/x54xz321",
            IdentifierType.ARK,
            "ark:12345/x5-4-xz-321",
            "https://sneezy.dopey.com/ark:12345/x54--xz32-1"
        )
    }
}