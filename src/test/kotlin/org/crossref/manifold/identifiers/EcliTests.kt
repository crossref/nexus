package org.crossref.manifold.identifiers

import org.junit.jupiter.api.Test

/**
 * Tests for [Ecli].
 */
class EcliTests {
    /**
     * Recognise Ark URIs and don't change them.
     */
    @Test
    fun canonical() {
        IdentifierAssertions.assertNormalizesUnchanged(
            "https://id.crossref.org/ecli/ecli1234",
            IdentifierType.ECLI
        )
    }

    /**
     * Normalize scheme.
     */
    @Test
    fun normalize() {
        IdentifierAssertions.assertNormalizesTo(
            "https://id.crossref.org/ecli/ecli1234",
            IdentifierType.ECLI,
            "https://id.crossref.org/ecli/ecli1234",
            "http://id.crossref.org/ecli/ecli1234"
        )
    }
}