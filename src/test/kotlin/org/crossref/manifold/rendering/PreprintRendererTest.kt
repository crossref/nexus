package org.crossref.manifold.rendering

import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.preprints.PreprintRenderer
import org.crossref.manifold.rendering.Examples.preprint
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PreprintRendererTest {

    @Test
    fun testOfferTrue(){
        val aPreprint = ItemTree(Item().withPropertiesFromMap(mapOf("content-type" to "preprint")))
        assertTrue(PreprintRenderer().offer("work", "posted-content", aPreprint))
    }

    @Test
    fun testOfferFalse(){
        val aPreprint = ItemTree(Item().withPropertiesFromMap(mapOf("content-type" to "preprint")))
        val notAPreprint1 = ItemTree(Item().withPropertiesFromMap(mapOf("content-type" to "not-a-preprint")))
        val notAPreprint2 = ItemTree(Item())

        assertFalse(PreprintRenderer().offer("work", "posted-content", notAPreprint1))
        assertFalse(PreprintRenderer().offer("work", "posted-content", notAPreprint2))
        assertFalse(PreprintRenderer().offer("work", "journal-article", aPreprint))
        assertFalse(PreprintRenderer().offer("not-work", "posted-content", aPreprint))
    }

    /**
     * This tests ensures the following:
     * - A single DOI value is present without a resolver
     * - All titles are present
     * - All authors are present
     * - Only the earliest date is present
     * - Missing attributes are not rendered as empty or null
     * - The metadata attribute is a string concatenation of all titles and author names, space delimited and in no particular order
     */
    @Test
    fun ensurePreprintDataStructure(){
        val expected = """{"DOI":"10.5555/12345678","title":["Title A","Title B"],"created":"2023-01-05T00:00:00Z","issued":[2023,1,5],"author":[{"given":"Author-A-first-name","family":"Author-A-last-name"},{"given":"Author-B-first-name","family":"Author-B-last-name","ORCID":"https://orcid.org/0000-0002-1825-0097"},{"name":"Brown University"}],"metadata":"Title A Title B Author-A-last-name Author-B-last-name Brown University"}"""
        val actual = PreprintRenderer().render(preprint)
        Assertions.assertEquals(expected, actual)
    }
}