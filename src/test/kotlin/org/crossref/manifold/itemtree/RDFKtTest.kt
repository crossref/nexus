package org.crossref.manifold.retrieval.view

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship

/**
 * Recursively make assertions about all nodes in the tree.
 */
fun assertAllItems(itemTree: ItemTree, f: (item: Item) -> Unit) {
    fun recurse(item: Item) {
        f(item)

        for (rel in item.rels) {
            recurse(rel.obj)
        }
    }

    recurse(itemTree.root)
}

internal class RDFKtTest {
    private val allBlanks = ItemTree(
        Item().withRelationships(
            listOf(
                Relationship(
                    "citation",
                    Item().withRelationships(
                        listOf(
                            Relationship(
                                "UNKNOWN_ABC",
                                Item().withRelationships(listOf(Relationship("citation", Item(), assertedAt = null))),
                                assertedAt = null
                            )
                        )
                ),
                    assertedAt = null
                ), Relationship(
                    "citation",
                    Item(),
                    assertedAt = null
                ), Relationship(
                    "funder",
                    Item(),
                    assertedAt = null
                ), Relationship(
                // This relationship should be returned as it's unresolved.
                    "UNKNOWN_XYZ",
                    Item().withRelationships(
                        listOf(
                            Relationship(
                                "citation", Item(), assertedAt = null
                            )
                        )
                    ),
                    assertedAt = null
                )
            )
        )
    )

    private val noBlanks =
        ItemTree(
            Item().withIdentifiers(listOf(IdentifierParser.parse("http://dx.doi.org/10.5555/12345678")))
                .withRelationships(
                    listOf(
                        Relationship(
                            "citation",
                            Item().withIdentifiers(listOf(IdentifierParser.parse("http://dx.doi.org/10.6666/8764321")))
                                .withRelationships(
                                    listOf(
                                        Relationship(
                                            "UNKNOWN_ABC",
                                            Item().withIdentifiers(listOf(IdentifierParser.parse("http://dx.doi.org/10.0000/0000"))),
                                            assertedAt = null
                                        )
                            )
                        ),
                            assertedAt = null

                        ), Relationship(
                            "citation",
                            Item().withIdentifiers(listOf(IdentifierParser.parse("http://dx.doi.org/10.7777/24242424"))),
                            assertedAt = null
                        ), Relationship(
                            "funder",
                            Item().withIdentifiers(listOf(IdentifierParser.parse("http://dx.doi.org/10.8888/98989898"))),
                            assertedAt = null
                        ), Relationship(
                            // This relationship should be returned as it's unresolved.
                            "UNKNOWN_XYZ",
                            Item().withIdentifiers(listOf(IdentifierParser.parse("http://dx.doi.org/10.9999/9999"))),
                            assertedAt = null

                        )
                    )
                )
        )


}