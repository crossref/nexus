package org.crossref.manifold.matching

import org.crossref.manifold.matching.Examples.CROSSREF_ITEM_PK
import org.crossref.manifold.matching.strategies.ReifiedMatcher
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@Service
class ReifiedMatcherTests {

    /**
     * An item tree with two types of funding.
     *
     */
    @Test
    fun simple() {
        val tree = Examples.tree
        val itemTreeAssertingParty = 10L
        val relationshipMatchAssertingParty = itemTreeAssertingParty
        val assertionPk = 30L
        val assertedAt = OffsetDateTime.now()

        val context = MatcherContext(CROSSREF_ITEM_PK)
        val matcher = ReifiedMatcher()

        // Look at Examples.tree to see the structure.
        val result = matcher.match(tree, assertionPk, assertedAt, itemTreeAssertingParty, context)

        assertTrue("ItemTreeAssertingParty should match what's passed in.",
            result.all { it.itemTreeAssertingParty == itemTreeAssertingParty })

        assertEquals("Only linked, reified relationships should be returned", 6, result.count())

        assertEquals(
            result.toSet(),
            listOf(

                RelationshipMatch(
                    // Item Tree asserting party, root node, hash should be passed through
                    itemTreeAssertingParty, assertedAt, 10001, assertionPk,

                    // Node position should be 0, as these are at the root (i.e. first) node.
                    4,

                    // relationshipMatchAssertingParty should be passed through.
                    relationshipMatchAssertingParty, 2,

                    // Relationship triple should be as found.
                    10001, Examples.CONTRIBUTOR_REL, 30001, true
                ),

                RelationshipMatch(
                    itemTreeAssertingParty,
                    assertedAt,
                    10001,
                    assertionPk,
                    6,
                    relationshipMatchAssertingParty,
                    2,
                    10001,
                    Examples.CONTRIBUTOR_REL,
                    30002,
                    true
                ),
                RelationshipMatch(
                    itemTreeAssertingParty,
                    assertedAt,
                    10001,
                    assertionPk,
                    8,
                    relationshipMatchAssertingParty,
                    2,
                    10001,
                    Examples.CONTRIBUTOR_REL,
                    30003,
                    true
                ),

                // This one matched by Crossref.
                RelationshipMatch(
                    itemTreeAssertingParty,
                    assertedAt,
                    10001,
                    assertionPk,
                    10,
                    CROSSREF_ITEM_PK,
                    2,
                    10001,
                    Examples.CITATION_REL,
                    10003,
                    true
                ),
                RelationshipMatch(
                    itemTreeAssertingParty,
                    assertedAt,
                    10001,
                    assertionPk,
                    12,
                    relationshipMatchAssertingParty,
                    2,
                    10001,
                    Examples.CITATION_REL,
                    10004,
                    true
                ),
                RelationshipMatch(
                    itemTreeAssertingParty,
                    assertedAt,
                    10001,
                    assertionPk,
                    14,
                    relationshipMatchAssertingParty,
                    2,
                    10001,
                    Examples.CITATION_REL,
                    10005,
                    true
                )
            ).toSet()
        )
    }
}
