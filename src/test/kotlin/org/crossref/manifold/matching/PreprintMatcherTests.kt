package org.crossref.manifold.matching

import org.crossref.manifold.matching.Examples.preprintMatchingJournalArticle
import org.crossref.manifold.matching.strategies.PreprintMatcher
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PreprintMatcherTests {


    /**
     * This tests ensures the following:
     * - All titles are present
     * - All authors are present
     * - Only the earliest date is present
     * - Missing attributes are not rendered as empty or null
     */
    @Test
    fun ensureMarpleInputDataStructure(){
        val expected = """{"title":["Title A","Title B"],"issued":{"date-parts":[[2023,1,5]]},"author":[{"given":"Author-A-first-name","family":"Author-A-last-name"},{"given":"Author-B-first-name","family":"Author-B-last-name","ORCID":"https://orcid.org/0000-0002-1825-0097"},{"name":"Brown University"}]}"""
        val actual = PreprintMatcher.marpleQueryStringFrom(preprintMatchingJournalArticle)
        Assertions.assertEquals(expected, actual)
    }

}