package org.crossref.manifold.matching

import org.crossref.manifold.matching.Examples.CROSSREF_ITEM_PK
import org.crossref.manifold.matching.strategies.SimpleMatcher
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.stereotype.Service
import java.time.OffsetDateTime

@Service
class SimpleMatcherTests {
    @Test
    fun simple() {

        // Look at Examples.tree to see the structure.
        val tree = Examples.tree
        val itemTreeAssertingParty = 10L
        val relationshipMatchAssertingParty = itemTreeAssertingParty
        val assertedAt = OffsetDateTime.now()
        val assertionPk = 99L

        val context = MatcherContext(CROSSREF_ITEM_PK)
        val matcher = SimpleMatcher()

        val result = matcher.match(tree, assertionPk, assertedAt, itemTreeAssertingParty, context)

        assertTrue("ItemTreeAssertingParty should match what's passed in.",
            result.all { it.itemTreeAssertingParty == itemTreeAssertingParty })

        // These hang off the root node, so position should be zero.
        assertEquals(
            "Expected to find both simple relationships",
            result.toSet(),
            listOf(
                RelationshipMatch(
                    // Item Tree asserting party, root node, hash should be passed through
                    itemTreeAssertingParty, assertedAt, 10001, assertionPk,

                    // Node position should be 0, as these are at the root (i.e. first) node.
                    0,

                    // relationshipMatchAssertingParty should be passed through.
                    relationshipMatchAssertingParty, 1,

                    // Relationship triple should be as found.
                    10001, Examples.STEWARD_REL, 20001, true
                ),

                RelationshipMatch(
                    itemTreeAssertingParty,
                    assertedAt,
                    10001,
                    assertionPk,
                    0,
                    relationshipMatchAssertingParty,
                    1,
                    10001,
                    Examples.HAS_REVIEW_REL,
                    10002,
                    true
                )
            ).toSet()
        )

    }
}
