package org.crossref.manifold.modules.eventdata

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.itemtree.Relationship
import org.crossref.manifold.modules.eventdata.support.toItemTree
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory


internal class EventsConversionTest {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @Test
    @Deprecated("Evaluate this test's usefulness after refactoring events conversion")
    fun testToItemGeneric() {
        val expected = ItemTree(
            Item(
                identifiers = listOf(
                    IdentifierParser.parse(
                        "https://doi.org/10.1039/d0qi01407a",
                    )
                ), rels = listOf(
                    Relationship(
                        relTyp = "references", Item(
                            identifiers = listOf(
                                IdentifierParser.parse(
                                    "https://doi.org/10.1021/ja203695h",
                            )
                        ), rels = emptyList()
                    ), assertedAt = null
                    )
            )
        )
        )

        assertEquals(expected, inputEventGeneric.toItemTree())
    }

}