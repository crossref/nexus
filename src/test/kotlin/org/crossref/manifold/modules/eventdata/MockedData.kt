package org.crossref.manifold.modules.eventdata

import org.crossref.manifold.identifiers.IdentifierParser
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.ingestion.Envelope
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.ingestion.EnvelopeBatchProvenance
import org.crossref.manifold.ingestion.UserAgentParser
import org.crossref.manifold.itemgraph.ItemTreeAssertion
import org.crossref.manifold.itemtree.Identifier
import org.crossref.manifold.itemtree.Item
import org.crossref.manifold.modules.consts.Items
import org.crossref.manifold.modules.eventdata.support.*
import java.net.URI
import java.time.OffsetDateTime

val secrets = setOf("test", "test2")
const val token =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MzgzNTEzMTAsInN1YiI6ImNyb3NzcmVmIn0.g8dNKApyNu_FEUYFi1SaOBaImcqPwmFd-r8qsfeN4A0"
const val invalidToken =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MzgzNTgzOTMsInN1YiI6ImNyb3NzcmVmIn0.kkZ8JLCz13vkaDRsRRX3u0iylePpSsrhQG9BimO_Ttc"

val inputEventGeneric = Event(
    id = "71823282-06c6-4a8f-8df3-e1c1b6b43faa",
    sourceToken = "36c35e23-8757-4a9d-aacf-345e9b7eb50d",
    occurredAt = "2021-09-22T01:26:39.000Z",
    subj = EventSubj(
        pid = IdentifierParser.parse("https://doi.org/10.1039/d0qi01407a"),
        url = "https://doi.org/10.1039/d0qi01407a",
        null,null,null,null,null ,null
    ),
    obj = Obj(pid = IdentifierParser.parse("https://doi.org/10.1021/ja203695h"),"", "", ""),
    relationTypeId = "references",
    sourceId = "crossref",
    objId = Identifier(URI(""), IdentifierType.DOI),
    subjId = Identifier(URI(""), IdentifierType.DOI),
    evidenceRecord = "",
    action = ""
)

val envelopeBatchGeneric = EnvelopeBatch(
    envelopes = listOf(
        Envelope(
            listOf(inputEventGeneric.toItemTree()!!),
            ItemTreeAssertion(
                OffsetDateTime.parse("2021-09-22T01:26:39.000Z"), Item().withIdentifier(
                    IdentifierParser.parse(
                        Items.CROSSREF_AUTHORITY
                    )
                )
            )
        )
    ), provenance = EnvelopeBatchProvenance(
        UserAgentParser.parse("Crossref 36c35e23-8757-4a9d-aacf-345e9b7eb50d"),
        "71823282-06c6-4a8f-8df3-e1c1b6b43faa"
    )
)

val match = Match(
    "doi-url",
    "10.5555/12345680.",
    "10.5555/12345680",
    "method",
    "verification")


val action = Action(
    "123",
    "2021-09-22T01:26:39.000Z",
    listOf(match),
    "https://hr.wikipedia.org/w/index.php?title=Kera_Tamara&oldid=6236673",
    ActionSubject(
        title = "A test",
        issued = "2021-09-22T01:26:39.000Z"),
    "discusses",
    emptyList()
)

val hypothesisAnnotatesAction = Action(
    "123",
    "2021-09-22T01:26:39.000Z",
    listOf(match),
    "url here",
    ActionSubject(
        issued = "2021-09-22T01:26:39.000Z"),
    "annotates",
    emptyList()
)

val hypothesisDiscussesAction = Action(
    "123",
    "2021-09-22T01:26:39.000Z",
    listOf(match),
    "url here",
    ActionSubject(
        issued = "2021-09-22T01:26:39.000Z"),
    "discusses",
    emptyList()
)