package org.crossref.manifold.modules.eventdata

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.crossref.manifold.ingestion.EnvelopeBatch
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.eventdata.support.EvidenceRecord
import org.crossref.manifold.modules.eventdata.support.convertEvidenceRecordToEnvelopeBatch
import org.crossref.manifold.util.JsonValidator
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.time.OffsetDateTime

/**
 * To test the Evidence Record to Item Tree mappings we have created a set of curated
 * Envelope Batch instances in JSON format. There are two files per Eventdata agent, an evidence record,
 * and an EnvelopeBatch.
 *
 * For the tests we read all evidence record files, convert them into item trees and serialise them to JSON
 * to compare against the curated examples.
 *
 * The agents covered in these tests are:
 * - wikipedia
 * - reddit
 * - reddit-links
 * - stackexchange
 * - newsfeed
 * - wordpressdotcom
 * - hypothesis
 *
 */
internal class EvidenceRecordConversionTest {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    // Fetch all evidence records in resources
    private val evidenceRecordFiles = File(object {}.javaClass.getResource("/modules/eventdata/")!!.file).walk()
        .filter { it.name.endsWith("-evidence-record.json") }
        .map { it.name }

    private fun String.getAgent() = this.removeSuffix("-evidence-record.json")

    // The mapper is configured to ignore empty fields and
    // to convert timestamps to ISO 8601 string format when serialising.
    private val mapper: ObjectMapper =
        ObjectMapper().registerModule(JavaTimeModule()).disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .setSerializationInclusion(JsonInclude.Include.NON_EMPTY)
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .setDateFormat(StdDateFormat().withColonInTimeZone(true))
            .registerKotlinModule()

    /**
     * Items contain properties that can be validated against the item types' schema.
     * Gather all items properties, validate them and assert no errors.
     */
    private fun EnvelopeBatch.validateItemTypes() =
        envelopes.forEach { envelope ->
            (envelope.itemTrees + ItemTree(envelope.assertion.assertingParty))
                .forEach { item ->
                    (item.root.properties + item.root.rels.flatMap { it.obj.properties }).forEach { properties ->

                        val output = JsonValidator.validateItem(properties.values)

                        if (!output.isValid) {
                            println(output.toString())
                            println(properties.values)
                        }

                        assert(output.isValid)
                    }
                }
        }

    private fun String.fetchEnvelopeBatchFromResources(): JsonNode = mapper.readTree(
        EventsConversionTest::class.java
            .getResourceAsStream(
                "/modules/eventdata/$this-agent.json"
            )
    )

    private fun String.getEvidenceRecord(): EvidenceRecord = mapper.readValue(
        EventsConversionTest::class.java
            .getResourceAsStream(
                "/modules/eventdata/$this"
            ),
        EvidenceRecord::class.java
    )

    /**
     * This test is meant to ensure that the evidence record to item tree mapping is producing the expected output.
     * The expected output has been decided and recorded into JSON envelope batch examples for each agent.
     *
     * Convert evidence records to envelope batches, serialise the envelope batches to JSON and compare
     * them to their corresponding curated examples.
     */
    @Test
    fun `Evidence record to item tree mapping matches expected records`() {

        evidenceRecordFiles.forEach { evFile ->

            val agent = evFile.getAgent()

            logger.info("Testing $evFile")

            val evidenceRecord = evFile.getEvidenceRecord()

            val expected = agent.fetchEnvelopeBatchFromResources()

            val actual = convertEvidenceRecordToEnvelopeBatch(evFile, evidenceRecord).first()

            // Assert produced item trees match expected
            Assertions.assertEquals(expected, mapper.valueToTree(actual))
        }

    }

    /**
     * Each item will have a type, described in its properties.
     * Item types are recorded in the item-types schema.
     *
     * Collect and validate each item's properties against the item-types schema.
     *
     * @see [validateItemTypes]
     */
    @Test
    fun `Validate item properties against Item Types schema`() {

        evidenceRecordFiles.forEach { evFile ->

            logger.info("Testing $evFile records")

            val evidenceRecord = evFile.getEvidenceRecord()

            val envelopeBatches = convertEvidenceRecordToEnvelopeBatch(evFile, evidenceRecord)

            // Validate properties item types
            Assertions.assertNotNull(envelopeBatches)
            envelopeBatches.map { it.validateItemTypes() }
        }

    }

    /**
     * Regression test to ensure that when ambiguous relationships occur within the same evidence record then
     * they are merged and their properties (observations) are merged. The specific evidence records in this
     * test contains an action with two matches that result in ambiguous relationships.
     * Therefore, the resulting properties should contain two items.
     */
    @Test
    fun `Validate item properties merging for ambiguous relationships`() {

        val evFile = "ambiguous-relationships.json"
        val evidenceRecord = evFile.getEvidenceRecord()
        val envelopeBatches = convertEvidenceRecordToEnvelopeBatch(evFile, evidenceRecord)

        assert(envelopeBatches[0].envelopes[0].itemTrees[0].root.rels[0].obj.properties.size == 2)
    }

    /**
     * Not all actions produce records. Actually there are many observations that do not match a DOI.
     * Ensure that we do not produce envelope batches that do not contain any item trees.
     */
    @Test
    fun `Empty envelope batches are discarded`() {
        val filename = "newsfeed-evidence-record-empty.json"

        val evidenceRecord = filename.getEvidenceRecord()

        val envelopeBatches = convertEvidenceRecordToEnvelopeBatch(filename, evidenceRecord)

        assert(envelopeBatches.isEmpty())
    }

    /**
     * The envelope assertions are expected to carry their action's individual occurredAt timestamp.
     */
    @Test
    fun `Validate timestamps`() {

        evidenceRecordFiles.forEach { evFile ->

            logger.info("Testing $evFile records")

            val evidenceRecord = evFile.getEvidenceRecord()

            val envelopeBatches = convertEvidenceRecordToEnvelopeBatch(
                evFile,
                evidenceRecord
            )


            // Create a map of subjects to timestamps
            val urlToTimestampPairs = evidenceRecord.pages.flatMap { page ->
                page.actions.map { action ->
                    action.url to action.occurredAt
                }
            }.toMap()

            // Timestamps should come from actions
            envelopeBatches.map {
                it.envelopes.forEach { envelope ->
                    envelope.itemTrees.forEach { item ->
                        item.root.identifiers
                            .filter { identifier -> urlToTimestampPairs.containsKey(identifier.uri.toString()) }
                            .forEach { identifier ->
                                val expected = OffsetDateTime.parse(urlToTimestampPairs[identifier.uri.toString()])
                                val actual = envelope.assertion.assertedAt
                                Assertions.assertEquals(expected, actual)
                            }
                    }
                }
            }
        }
    }
}