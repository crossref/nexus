package org.crossref.manifold.modules.eventdata.bus

import com.auth0.jwt.interfaces.DecodedJWT
import io.mockk.*
import org.crossref.manifold.itemgraph.ItemGraph
import org.crossref.manifold.modules.eventdata.envelopeBatchGeneric
import org.crossref.manifold.modules.eventdata.inputEventGeneric
import org.crossref.manifold.modules.eventdata.support.Event
import org.crossref.manifold.modules.eventdata.support.bus.MultiJwtVerifier
import org.crossref.manifold.modules.eventdata.support.bus.extractToken
import org.crossref.manifold.modules.eventdata.support.bus.postEvent
import org.crossref.manifold.modules.eventdata.support.parseEventToEnvelopeBatch
import org.crossref.manifold.modules.eventdata.token
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.http.HttpHeaders
import org.springframework.web.server.ResponseStatusException

internal class BusControllerTest {

    @Test
    fun testExtractToken() {
        val headers = HttpHeaders()
        assertNull(extractToken(headers))

        headers.set("authorization", "Bearer abc")
        assertEquals("abc", extractToken(headers))
    }


    @Test
    fun `An event without a token or without a valid token should fail`() {
        val multiJwtVerifier = mockk<MultiJwtVerifier>()
        val headers = HttpHeaders()
        val itemGraph = mockk<ItemGraph>()

        val event = mockk<Event>()

        every { multiJwtVerifier.verify(null) } returns null

        assertThrows(ResponseStatusException::class.java) {
            postEvent(multiJwtVerifier, headers, itemGraph, setOf("crossref"), event)
        }

        verify { multiJwtVerifier.verify(null) }
        verify(exactly = 0) { itemGraph.ingest(any()) }
    }

    @Test
    fun `An event from a disallowed agent should be ignored`() {
        val multiJwtVerifier = mockk<MultiJwtVerifier>()
        val jwt = mockk<DecodedJWT>()
        mockkStatic(::parseEventToEnvelopeBatch)
        val headers = HttpHeaders()
        val itemGraph = mockk<ItemGraph>()
        headers.set("authorization", "Bearer $token")

        every { multiJwtVerifier.verify(any()) } returns jwt
        every { jwt.subject } returns "crossref"

        assertThrows(ResponseStatusException::class.java){
            postEvent(multiJwtVerifier, headers, itemGraph, setOf("abc"), inputEventGeneric)
        }

        verify(exactly = 0) { parseEventToEnvelopeBatch(inputEventGeneric) }
        verify(exactly = 0) { itemGraph.ingest(any()) }

        // Important
        unmockkStatic(::parseEventToEnvelopeBatch)
    }

    @Test
    fun `An event with a valid token should be ingested once`() {
        val multiJwtVerifier = mockk<MultiJwtVerifier>()
        val itemGraph = mockk<ItemGraph>()
        val jwt = mockk<DecodedJWT>()
        mockkStatic(::parseEventToEnvelopeBatch)
        val headers = HttpHeaders()
        headers.set("authorization", "Bearer $token")

        every { multiJwtVerifier.verify(any()) } returns jwt
        every { jwt.subject } returns "crossref"
        every { parseEventToEnvelopeBatch(inputEventGeneric) } returns envelopeBatchGeneric

        postEvent(multiJwtVerifier, headers, itemGraph, setOf("crossref"), inputEventGeneric)

        verify(exactly = 1) { parseEventToEnvelopeBatch(inputEventGeneric) }
        verify(exactly = 1) { multiJwtVerifier.verify(token) }
        verify(exactly = 1) { itemGraph.ingest(listOf(envelopeBatchGeneric))}

        // Important
        unmockkStatic(::parseEventToEnvelopeBatch)
    }

    @Test
    fun `Ingestion exceptions are caught`() {
        val multiJwtVerifier = mockk<MultiJwtVerifier>()
        val jwt = mockk<DecodedJWT>()
        val headers = HttpHeaders()
        headers.set("authorization", "Bearer $token")
        val itemGraph = mockk<ItemGraph>()

        every { multiJwtVerifier.verify(any()) } returns jwt
        every { jwt.subject } returns "crossref"
        mockkStatic(::parseEventToEnvelopeBatch)
        every { parseEventToEnvelopeBatch(inputEventGeneric) } throws Exception()

        postEvent(multiJwtVerifier, headers, itemGraph, setOf("crossref"), inputEventGeneric)

        every { parseEventToEnvelopeBatch(inputEventGeneric) } returns envelopeBatchGeneric
        every { itemGraph.ingest(any()) } throws Exception()

        postEvent(multiJwtVerifier, headers, itemGraph, setOf("crossref"), inputEventGeneric)

        verify(exactly = 2) { parseEventToEnvelopeBatch(inputEventGeneric) }

        verify(exactly = 1) { itemGraph.ingest(listOf(envelopeBatchGeneric)) }

        unmockkStatic(::parseEventToEnvelopeBatch)
    }
}