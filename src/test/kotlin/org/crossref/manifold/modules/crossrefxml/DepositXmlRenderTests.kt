package org.crossref.manifold.modules.crossrefxml

import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.crossrefxml.XmlUtil.getXmlFile
import org.crossref.manifold.modules.crossrefxml.contentTypeRenderers.DepositContext
import org.crossref.manifold.modules.crossrefxml.contentTypeRenderers.DepositXmlRenderer
import org.crossref.manifold.modules.unixml.support.parseUniXmlToEnvelopes
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource


/**
 * Render <crossref_result> into ItemTree and then into Deposit XML.
 * Some properties of the original XML are lost during conversion to Cayenne's ItemTree, and in the future
 * we want to eliminate these disparities. However, this means that the real "deposit" rendering test fails for now.
 * Temporary solution: compare the rendering result to a manually-edited "expected" XML instead of the real deposit XML.
 * Original file: `<filename>.xml`
 * Correct rendering result: `<filename>.deposit.xml`
 * Currently expected rendering: `<filename>.deposit.invalid.xml`
 * As we improve the rendering and fix its bugs, the expected XML should gradually morph into the correct deposit XML,
 * that's why we use the "invalid" tag in the filename to emphasize that it's not the final result.
 * We also check that the correct and expected XMLs don't match - when they do, it's time to remove the invalid file.
 * If `<filename>.deposit.invalid.xml` doesn't exist for a test, we compare the result to the `<filename>.deposit.xml`.
 */
class DepositXmlRenderTests {
    private val renderer = DepositXmlRenderer()

    @ParameterizedTest(name = "Rendering Deposit for {0}")
    @ValueSource(
        strings = [
            "journal_article_1.xml",
            "journal_article_2.xml",
            "journal_article_3.xml",
            "journal_article_4.xml"
        ]
    )
    fun testJournal(fileName: String) {
        testDepositRendering("journals/$fileName")
    }

    @ParameterizedTest(name = "Rendering Deposit for {0}")
    @ValueSource(
        strings = [
            "book_chapter_1.xml",
            "book_chapter_2.xml",
            "book_chapter_3.xml",
            "book_chapter_4.xml",
        ]
    )
    fun testBook(fileName: String) {
        testDepositRendering("books/$fileName")
    }

    private fun testDepositRendering(fileName: String) {
        org.crossref.cayenne.boot()

        val input = getXmlFile(fileName)!!.readText()
        val correct = getXmlFile(fileName, "deposit")!!.readText()
        val expected = getXmlFile(fileName, "deposit.invalid")?.readText() ?: correct
        val metadataTree = XmlUtil.parseXML(input)
        val result = renderer.render(
            itemTree = metadataTree,
            context = DepositContext(
                doiBatchId = "doi-batch-id-x",
                timestamp = 123123123.toLong(),
                depositorName = "depositor-name-x",
                depositorEmail = "depositor-email-x",
                registrant = "registrant-x"
            )
        )

        compareXMLs(correct, expected, result)
    }

    @Test
    fun testOffer() {
        val context = DepositContext(
            doiBatchId = "doi-batch-id-x",
            timestamp = 123123123.toLong(),
            depositorName = "depositor-name-x",
            depositorEmail = "depositor-email-x",
            registrant = "registrant-x"
        )
        Assertions.assertTrue(renderer.offer("work", "book-chapter", context = context))

        Assertions.assertFalse(renderer.offer("work", "book-chapter"))
        Assertions.assertFalse(renderer.offer("work", "book-chapter", null))
        Assertions.assertFalse(renderer.offer("non-work", "book-chapter", context = context))
    }

    private fun compareXMLs(correctXml: String, expectedXml: String, resultXml: String) {
        val expected = if (correctXml != expectedXml) {
            expectedXml
        } else {
            correctXml
        }

        Assertions.assertEquals(expected, resultXml)
    }
}