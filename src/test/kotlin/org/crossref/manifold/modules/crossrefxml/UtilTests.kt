package org.crossref.manifold.modules.crossrefxml

import org.crossref.manifold.modules.crossrefxml.elementRenderers.fromXmlValue
import org.crossref.manifold.modules.crossrefxml.xmlelements.*

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

/**
 * Test rendering helper functions
 */
class UtilTests {

    @Test
    fun testBookTypeFromXml() {
        // make sure enum entries have correct values
        for (v in BookType.values()) {
            Assertions.assertEquals(v, BookType::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(BookType.EDITED_BOOK, BookType::value.fromXmlValue("edited_book"))
        Assertions.assertEquals(BookType.MONOGRAPH, BookType::value.fromXmlValue("monograph"))
        Assertions.assertEquals(BookType.REFERENCE, BookType::value.fromXmlValue("reference"))
        Assertions.assertEquals(BookType.OTHER, BookType::value.fromXmlValue("other"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { BookType::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }

    @Test
    fun testComponentTypeFromXml() {
        // make sure enum entries have correct values
        for (v in ComponentType.values()) {
            Assertions.assertEquals(v, ComponentType::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(ComponentType.CHAPTER, ComponentType::value.fromXmlValue("chapter"))
        Assertions.assertEquals(ComponentType.SECTION, ComponentType::value.fromXmlValue("section"))
        Assertions.assertEquals(ComponentType.PART, ComponentType::value.fromXmlValue("part"))
        Assertions.assertEquals(ComponentType.TRACK, ComponentType::value.fromXmlValue("track"))
        Assertions.assertEquals(ComponentType.REFERENCE_ENTRY, ComponentType::value.fromXmlValue("reference_entry"))
        Assertions.assertEquals(ComponentType.OTHER, ComponentType::value.fromXmlValue("other"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { ComponentType::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }

    @Test
    fun testCrmItemNameFromXml() {
        // make sure enum entries have correct values
        for (v in CrmItemName.values()) {
            Assertions.assertEquals(v, CrmItemName::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(CrmItemName.CITATION_ID, CrmItemName::value.fromXmlValue("citation-id"))
        Assertions.assertEquals(CrmItemName.JOURNAL_TITLE, CrmItemName::value.fromXmlValue("journal-title"))
        Assertions.assertEquals(CrmItemName.JOURNAL_ID, CrmItemName::value.fromXmlValue("journal-id"))
        Assertions.assertEquals(CrmItemName.BOOK_ID, CrmItemName::value.fromXmlValue("book-id"))
        Assertions.assertEquals(CrmItemName.SERIES_ID, CrmItemName::value.fromXmlValue("series-id"))
        Assertions.assertEquals(CrmItemName.DEPOSIT_TIMESTAMP, CrmItemName::value.fromXmlValue("deposit-timestamp"))
        Assertions.assertEquals(CrmItemName.OWNER_PREFIX, CrmItemName::value.fromXmlValue("owner-prefix"))
        Assertions.assertEquals(CrmItemName.PRIME_DOI, CrmItemName::value.fromXmlValue("prime-doi"))
        Assertions.assertEquals(CrmItemName.OWNER, CrmItemName::value.fromXmlValue("owner"))
        Assertions.assertEquals(CrmItemName.LAST_UPDATE, CrmItemName::value.fromXmlValue("last-update"))
        Assertions.assertEquals(CrmItemName.CITEDBY_COUNT, CrmItemName::value.fromXmlValue("citedby-count"))
        Assertions.assertEquals(CrmItemName.PUBLISHER_NAME, CrmItemName::value.fromXmlValue("publisher-name"))
        Assertions.assertEquals(CrmItemName.PREFIX_NAME, CrmItemName::value.fromXmlValue("prefix-name"))
        Assertions.assertEquals(CrmItemName.MEMBER_ID, CrmItemName::value.fromXmlValue("member-id"))
        Assertions.assertEquals(CrmItemName.CREATED, CrmItemName::value.fromXmlValue("created"))
        Assertions.assertEquals(CrmItemName.RELATION, CrmItemName::value.fromXmlValue("relation"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { CrmItemName::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }

    @Test
    fun testCrmItemTypeFromXml() {
        // make sure enum entries have correct values
        for (v in CrmItemType.values()) {
            Assertions.assertEquals(v, CrmItemType::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(CrmItemType.ELEMENT, CrmItemType::value.fromXmlValue("element"))
        Assertions.assertEquals(CrmItemType.STRING, CrmItemType::value.fromXmlValue("string"))
        Assertions.assertEquals(CrmItemType.NUMBER, CrmItemType::value.fromXmlValue("number"))
        Assertions.assertEquals(CrmItemType.DATE, CrmItemType::value.fromXmlValue("date"))
        Assertions.assertEquals(CrmItemType.DOI, CrmItemType::value.fromXmlValue("doi"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { CrmItemType::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }

    @Test
    fun testDoiTypeFromXml() {
        // make sure enum entries have correct values
        for (v in DoiType.values()) {
            Assertions.assertEquals(v, DoiType::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(DoiType.JOURNAL_TITLE, DoiType::value.fromXmlValue("journal_title"))
        Assertions.assertEquals(DoiType.JOURNAL_ISSUE, DoiType::value.fromXmlValue("journal_issue"))
        Assertions.assertEquals(DoiType.JOURNAL_VOLUME, DoiType::value.fromXmlValue("journal_volume"))
        Assertions.assertEquals(DoiType.JOURNAL_ARTICLE, DoiType::value.fromXmlValue("journal_article"))
        Assertions.assertEquals(DoiType.CONFERENCE_TITLE, DoiType::value.fromXmlValue("conference_title"))
        Assertions.assertEquals(DoiType.CONFERENCE_SERIES, DoiType::value.fromXmlValue("conference_series"))
        Assertions.assertEquals(DoiType.CONFERENCE_PAPER, DoiType::value.fromXmlValue("conference_paper"))
        Assertions.assertEquals(DoiType.BOOK_TITLE, DoiType::value.fromXmlValue("book_title"))
        Assertions.assertEquals(DoiType.BOOK_SERIES, DoiType::value.fromXmlValue("book_series"))
        Assertions.assertEquals(DoiType.BOOK_CONTENT, DoiType::value.fromXmlValue("book_content"))
        Assertions.assertEquals(DoiType.COMPONENT, DoiType::value.fromXmlValue("component"))
        Assertions.assertEquals(DoiType.DISSERTATION, DoiType::value.fromXmlValue("dissertation"))
        Assertions.assertEquals(DoiType.REPORT_PAPER_TITLE, DoiType::value.fromXmlValue("report-paper_title"))
        Assertions.assertEquals(DoiType.REPORT_PAPER_SERIES, DoiType::value.fromXmlValue("report-paper_series"))
        Assertions.assertEquals(DoiType.REPORT_PAPER_CONTENT, DoiType::value.fromXmlValue("report-paper_content"))
        Assertions.assertEquals(DoiType.STANDARD_TITLE, DoiType::value.fromXmlValue("standard_title"))
        Assertions.assertEquals(DoiType.STANDARD_SERIES, DoiType::value.fromXmlValue("standard_series"))
        Assertions.assertEquals(DoiType.STANDARD_CONTENT, DoiType::value.fromXmlValue("standard_content"))
        Assertions.assertEquals(DoiType.PREPUBLICATION, DoiType::value.fromXmlValue("prepublication"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { DoiType::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }

    @Test
    fun testInstitutionIdTypeFromXml() {
        // make sure enum entries have correct values
        for (v in InstitutionIdType.values()) {
            Assertions.assertEquals(v, InstitutionIdType::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(InstitutionIdType.ROR, InstitutionIdType::value.fromXmlValue("ror"))
        Assertions.assertEquals(InstitutionIdType.ISNI, InstitutionIdType::value.fromXmlValue("isni"))
        Assertions.assertEquals(InstitutionIdType.WIKIDATA, InstitutionIdType::value.fromXmlValue("wikidata"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { InstitutionIdType::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }

    @Test
    fun testNoisbnReasonFromXml() {
        // make sure enum entries have correct values
        for (v in NoisbnReason.values()) {
            Assertions.assertEquals(v, NoisbnReason::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(NoisbnReason.ARCHIVE_VOLUME, NoisbnReason::value.fromXmlValue("archive_volume"))
        Assertions.assertEquals(NoisbnReason.MONOGRAPH, NoisbnReason::value.fromXmlValue("monograph"))
        Assertions.assertEquals(NoisbnReason.SIMPLE_SERIES, NoisbnReason::value.fromXmlValue("simple_series"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { NoisbnReason::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }

    @Test
    fun testIsbnMediaTypeFromXml() {
        // make sure enum entries have correct values
        for (v in IsbnMediaType.values()) {
            Assertions.assertEquals(v, IsbnMediaType::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(IsbnMediaType.PRINT, IsbnMediaType::value.fromXmlValue("print"))
        Assertions.assertEquals(IsbnMediaType.ELECTRONIC, IsbnMediaType::value.fromXmlValue("electronic"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { IsbnMediaType::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }

    @Test
    fun testIssnMediaTypeFromXml() {
        // make sure enum entries have correct values
        for (v in IssnMediaType.values()) {
            Assertions.assertEquals(v, IssnMediaType::value.fromXmlValue(v.value))
        }

        Assertions.assertEquals(IssnMediaType.PRINT, IssnMediaType::value.fromXmlValue("print"))
        Assertions.assertEquals(IssnMediaType.ELECTRONIC, IssnMediaType::value.fromXmlValue("electronic"))
        Assertions.assertThrows(
            NoSuchElementException::class.java,
            { IssnMediaType::value.fromXmlValue("test-error") },
            "Enum has no member `test-error`"
        )
    }
}