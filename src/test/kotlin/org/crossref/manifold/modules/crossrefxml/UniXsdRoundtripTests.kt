package org.crossref.manifold.modules.crossrefxml

import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.crossrefxml.XmlUtil.getNode
import org.crossref.manifold.modules.crossrefxml.XmlUtil.getXmlFile
import org.crossref.manifold.modules.crossrefxml.XmlUtil.serializeWithoutWhitespace
import org.crossref.manifold.modules.crossrefxml.contentTypeRenderers.UniXsdRenderer
import org.crossref.manifold.modules.unixml.support.parseUniXmlToEnvelopes
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource


/**
 * Roundtrip rendering of an <crossref_result> XML file.
 * Some properties of the original XML are lost during conversion to Cayenne's ItemTree, and in the future
 * we want to eliminate these disparities. However, this means that the real "roundtrip" rendering test fails for now.
 * Temporary solution: compare the rendering result to a manually-edited "expected" XML instead of the original XML.
 * Original file: `<filename>.xml`
 * Currently expected rendering: `<filename>.unixsd.invalid.xml`
 * As we improve the rendering and fix its bugs, the expected XML should gradually morph into the original XML,
 * that's why we use the "invalid" tag in the filename to emphasize that it's not the final result.
 * We also check that original and expected XMLs don't match - when they do, it's time to remove the invalid file.
 * If `<filename>.unixsd.invalid.xml` doesn't exist for a test, we compare the result to the original XML.
 */
class UniXsdRoundtripTests {
    private val renderer = UniXsdRenderer()

    @ParameterizedTest(name = "Rendering UniXsd for {0}")
    @ValueSource(
        strings = [
            "journal_article_1.xml",
            "journal_article_2.xml",
            "journal_article_3.xml",
            "journal_article_4.xml"
        ]
    )
    fun testJournal(fileName: String) {
        testRoundtrip("journals/$fileName")
    }

    @ParameterizedTest(name = "Rendering UniXsd for {0}")
    @ValueSource(
        strings = [
            "book_chapter_1.xml",
            "book_chapter_2.xml",
            "book_chapter_3.xml",
            "book_chapter_4.xml",
        ]
    )
    fun testBook(fileName: String) {
        testRoundtrip("books/$fileName")
    }

    private fun testRoundtrip(fileName: String) {
        org.crossref.cayenne.boot()

        val input = getXmlFile(fileName)!!.readText()
        val expected = getXmlFile(fileName, "unixsd.invalid")?.readText() ?: input
        val metadataTree = XmlUtil.parseXML(input)
        val result = renderer.render(metadataTree)

        compareXMLs(input, expected, result)
    }

    private fun compareXMLs(inputXml: String, expectedXml: String, resultXml: String) {
        val expected = serializeWithoutWhitespace(getNode(expectedXml, "//crossref_result"))
        val got = serializeWithoutWhitespace(getNode(resultXml, "//crossref_result"))
        Assertions.assertEquals(expected, got)

        if (inputXml != expectedXml) {
            val normalizedInput = serializeWithoutWhitespace(getNode(inputXml, "//crossref_result"))
            Assertions.assertNotEquals(
                normalizedInput, expected,
                "Original XML and `unixsd.invalid` XML must not match"
            )
        }
    }
}