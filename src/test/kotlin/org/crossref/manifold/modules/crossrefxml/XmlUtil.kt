package org.crossref.manifold.modules.crossrefxml

import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.unixml.support.parseUniXmlToEnvelopes
import org.junit.jupiter.api.Assertions
import org.w3c.dom.Node
import java.io.File
import java.io.StringWriter
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory

object XmlUtil {
    fun serializeWithoutWhitespace(input: Node): String {
        val factory = TransformerFactory.newInstance()

        // Mutate the DOM in-place.
        fun removeWhitespace(node: Node) {
            (0..node.childNodes.length).forEach { i ->
                node.childNodes.item(i)?.let {
                    if (it.nodeType == Node.TEXT_NODE) {
                        it.textContent = it.textContent?.trim()
                    }
                    removeWhitespace(it)
                }
            }
        }
        removeWhitespace(input)

        val transformer: Transformer = factory.newTransformer()

        transformer.setOutputProperty(OutputKeys.INDENT, "yes")

        val sw = StringWriter()
        transformer.transform(DOMSource(input), StreamResult(sw))
        return sw.toString()
    }

    fun getNode(input: String, path: String): Node {
        val factory = DocumentBuilderFactory.newInstance()
        val builder = factory.newDocumentBuilder()
        val document = builder.parse(input.byteInputStream())

        val xpath: XPath = XPathFactory.newInstance().newXPath()
        return xpath.evaluate(path, document, XPathConstants.NODE) as Node
    }

    fun getXmlFile(filename: String, qualifier: String? = null): File? {
        val fname = when (qualifier) {
            null -> filename
            else -> filename.split(".").toMutableList().let {
                it.add(1, qualifier); it.joinToString(".")
            }
        }
        return this.javaClass.getResource("/modules/crossrefxml/${fname}")?.file?.let { File(it) }
    }

    fun parseXML(xmlString: String): ItemTree {
        val envelopes = parseUniXmlToEnvelopes("TEST", xmlString)
        // Parser produces various item tree assertions (e.g. stewardship etc.).
        // Pick the one that asserts the XML metadata.
        val trees = envelopes.flatMap { it.envelopes.flatMap { e -> e.itemTrees } }
        val metadataTree = trees.first { it.root.getPropertyText("type") == "work" }
        Assertions.assertNotNull(metadataTree, "Item Tree should parse")
        return metadataTree
    }
}