package org.crossref.manifold.modules.crossrefxml

import org.crossref.manifold.common.resource
import org.crossref.manifold.identifiers.IdentifierType
import org.crossref.manifold.itemtree.ItemTree
import org.crossref.manifold.modules.crossrefxml.XmlUtil.getXmlFile
import org.crossref.manifold.util.JsonItemTreeMapper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test


/**
 * This test demonstrates that ItemTrees constructed from XML and JSON have different layouts.
 * Once https://crossref.atlassian.net/browse/CR-1853 is fixed, this test will start failing.
 */
class RegressionTests {
    @Test
    fun testCR1853() {
        org.crossref.cayenne.boot()

        val testFilePath = "modules/crossrefxml/regressions/CR-1853"

        val xmlInput = getXmlFile("regressions/CR-1853.xml")!!.readText()
        val xmlItemTree = XmlUtil.parseXML(xmlInput)

        val jsonInput = resource("$testFilePath.json")
        val jsonItemTree = JsonItemTreeMapper.mapper.readValue(jsonInput, ItemTree::class.java)

        val jsonOrcid = jsonItemTree.root.rels.filter { it.relTyp == "author" }.map {
            it.obj.identifiers.filter { it.type == IdentifierType.ORCID }.map {
                it.uri.toString()
            }.firstOrNull()
        }.firstOrNull()
        // ItemTree constructed from XML has one extra layer of objects
        val xmlOrcid = xmlItemTree.root.rels.filter { it.relTyp == "author" }.map {
            it.obj.rels.map {
                it.obj.identifiers.filter { it.type == IdentifierType.ORCID }.map {
                    it.uri.toString()
                }.firstOrNull()
            }.firstOrNull()
        }.firstOrNull()
        Assertions.assertEquals(jsonOrcid, xmlOrcid)

        // demonstrate that looking for JSON's path in XML ItemTree and vice versa produces no result
        val jsonEmptyOrcid = jsonItemTree.root.rels.filter { it.relTyp == "author" }.map {
            it.obj.rels.map {
                it.obj.identifiers.filter { it.type == IdentifierType.ORCID }.map {
                    it.uri.toString()
                }.firstOrNull()
            }.firstOrNull()
        }.firstOrNull()
        Assertions.assertNull(jsonEmptyOrcid)
        val xmlEmptyOrcid = xmlItemTree.root.rels.filter { it.relTyp == "author" }.map {
            it.obj.identifiers.filter { it.type == IdentifierType.ORCID }.map {
                it.uri.toString()
            }.firstOrNull()
        }.firstOrNull()
        Assertions.assertNull(xmlEmptyOrcid)
    }
}