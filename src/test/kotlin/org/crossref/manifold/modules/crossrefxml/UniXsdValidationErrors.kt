package org.crossref.manifold.modules.crossrefxml

object UniXsdError {
    const val DOI_RECORD_TYPE =
        "cvc-complex-type.2.4.a: Invalid content was found starting with element " +
                "'{\"http://www.crossref.org/qrschema/3.0\":doi_record}'. " +
                "One of '{\"http://www.crossref.org/qrschema/3.0\":msg, " +
                "\"http://www.crossref.org/qrschema/3.0\":issn, " +
                "\"http://www.crossref.org/qrschema/3.0\":isbn, " +
                "\"http://www.crossref.org/qrschema/3.0\":series_title, " +
                "\"http://www.crossref.org/qrschema/3.0\":journal_title, " +
                "\"http://www.crossref.org/qrschema/3.0\":volume_title, " +
                "\"http://www.crossref.org/qrschema/3.0\":author, " +
                "\"http://www.crossref.org/qrschema/3.0\":contributors, " +
                "\"http://www.crossref.org/qrschema/3.0\":volume, " +
                "\"http://www.crossref.org/qrschema/3.0\":issue, " +
                "\"http://www.crossref.org/qrschema/3.0\":first_page, " +
                "\"http://www.crossref.org/qrschema/3.0\":item_number, " +
                "\"http://www.crossref.org/qrschema/3.0\":last_page, " +
                "\"http://www.crossref.org/qrschema/3.0\":edition_number, " +
                "\"http://www.crossref.org/qrschema/3.0\":component_number, " +
                "\"http://www.crossref.org/qrschema/3.0\":year, " +
                "\"http://www.crossref.org/qrschema/3.0\":publication_type, " +
                "\"http://www.crossref.org/qrschema/3.0\":article_title, " +
                "\"http://www.crossref.org/qrschema/3.0\":institution_name, " +
                "\"http://www.crossref.org/qrschema/3.0\":identifier, " +
                "\"http://www.crossref.org/qrschema/3.0\":component_list, " +
                "\"http://www.crossref.org/qrschema/3.0\":crm-item}' is expected."

    const val EMPTY_CONTRIBUTORS = "cvc-complex-type.2.4.b: The content of element 'contributors' is not complete. " +
            "One of '{\"http://www.crossref.org/xschema/1.1\":organization, " +
            "\"http://www.crossref.org/xschema/1.1\":person_name, " +
            "\"http://www.crossref.org/xschema/1.1\":anonymous}' is expected."

    const val STANDARDS_BODY_CHILDREN =
        "cvc-complex-type.2.3: Element 'standards_body' cannot have character [children], " +
                "because the type's content type is element-only."

    const val STANDARDS_BODY_VALUE =
        "cvc-complex-type.2.4.b: The content of element 'standards_body' is not complete. " +
                "One of '{\"http://www.crossref.org/xschema/1.1\":standards_body_name}' is expected."
}
