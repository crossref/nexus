package org.crossref.manifold.provenance

import org.crossref.manifold.ingestion.UserAgent
import org.crossref.manifold.ingestion.UserAgentParser
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class UserAgentTests {

    @Test
    fun `a blank user agent string throws an exception`() {
        assertThrows<IllegalStateException> { UserAgentParser.parse("") }
        assertThrows<IllegalStateException> { UserAgentParser.parse("/") }
        assertThrows<IllegalStateException> { UserAgentParser.parse("//") }
        assertThrows<IllegalStateException> { UserAgentParser.parse( "/ /") }
        assertThrows<IllegalStateException> { UserAgentParser.parse( " / /") }
    }

    @Test
    fun testUserAgentParser() {

        // Triples of expected output, provided input, case description
        val tests = listOf(
            Triple(
                UserAgent("Crossref", "1.0.0"),
                "Crossref/1.0.0",
                "simple input"
            ),
            Triple(
                UserAgent("Crossref user agent", "1.0.0"),
                "Crossref user agent/1.0.0",
                "product with spaces"
            ),
            Triple(
                UserAgent("Crossref user agent", "1.0.0"),
                "Crossref user agent/1.0.0 beta",
                "version with spaces"
            ),
            Triple(
                UserAgent("Crossref user agent", "1.0.0"),
                " Crossref user agent /  1.0.0  ",
                "pending and leading spaces"
            ),
            Triple(
                UserAgent("Crossref user agent", "1.0.0"),
                "Crossref user agent/1.0.0  Web Agent/2.0",
                "multiple identifiers"
            ),
            Triple(
                UserAgent("Crossref user agent", "/1.0.0"),
                "Crossref user agent//1.0.0",
                "consecutive slashes"
            ),
            Triple(
                UserAgent("Crossref user agent", "unknown"),
                "Crossref user agent/",
                "no version, slash"
            ),
            Triple(
                UserAgent("Crossref user agent Web Agent", "2.0"),
                "Crossref user agent Web Agent/2.0",
                "no version, no slash, multiple identifiers"
            ),
            Triple(
                UserAgent("Crossref user agent 1.0.0", "unknown"),
                "Crossref user agent 1.0.0",
                "version, no slash"
            ),
            Triple(
                UserAgent("Crossref user agent 1.0.0", "unknown"),
                "Crossref user agent 1.0.0",
                "version, no slash"
            ),
        )

        tests.forEach { (expected, input, case) ->
            val actual = UserAgentParser.parse(input)
            Assertions.assertEquals(expected, actual, case)
        }

    }

    @Test
    fun testCommonAgents(){
        val tests = listOf(
            UserAgent("GroovyBib", "1.1") to "GroovyBib/1.1 (https://example.org/GroovyBib/; mailto:GroovyBib@example.org) BasedOnFunkyLib/1.4",
            UserAgent("Mozilla", "5.0") to "Mozilla/5.0 (iPad; U; CPU OS 3_2_1 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Mobile/7B405",
            UserAgent("Googlebot", "2.1") to "Googlebot/2.1 (+http://www.google.com/bot.html)"
        )

        tests.forEach { (expected, input) ->
            val actual = UserAgentParser.parse(input)
            Assertions.assertEquals(expected, actual)
        }
    }
}