# Package org.crossref.manifold.modules.itemTree

Experimental Module to expose the Item graph in Citeproc-JSON format, which the Cayenne REST API currently speaks. Work
in progress.

# Package org.crossref.manifold.modules.dump

Dump the SQL database to a file. Experimental.

# Package org.crossref.manifold.modules.graphviz

This will dump the contents of the Relationship Graph to a GraphViz format DOT file on shutdown. Supply the filename
in `GRAPHVIZ_DUMP=<path>.dot` argument. Experimental, most graphs will crash GraphViz or Gephi.

The GraphViz output can be rendered by installing GraphViz and running e.g.

```shell
neato  /tmp/dump.dot -Tpdf > /tmp/dump.pdf
```

## Configuration

- `GRAPHVIZ_DUMP` - Path to dump to on shutdown.
