# Package org.crossref.manifold.itemtree

Item Tree is the data structure that represents a point in the Item Graph, from someone's perspective. Item Trees are
the only way that data is inserted into the Item Graph, and are one of the ways data can be presented from the Item
Graph. An Item Tree just describes Items, Properties and Relationships and is neither true nor false.

Everything in this namespace is immutable. Most of it is recursive.

## Many uses

Item Trees can be used for a wide range of purposes. For example:

- Member sending a traditional metadata deposit.
- Member updating one, or many URLs or specific metadata elements.
- ORCID sending assertions.
- Authors sending their own assertions.
- Crossref sending cited-by counts.
- Crossref making reference matches.

## Item Tree Structure

At the top of an Item Tree is an Item. This could be any research object that we know about, or don't know about yet.
Each Item has zero, one or more of:

- Item Identifiers (using a shared vocabulary such as DOI or ORCID)
- Property Statements. For example, that it has a given title, or abstract, or date of publication.
- Relationship Statements. These include a relationship type (such as "cites") and a link to another Item.

That other Item in a Relationship is just like the root item, and can contain the same fields. Because an Item can
include a relationship to another Item, and that Item can do the same, it forms a recursive tree structure.

An Item may not have an Item Identifier. For example, an Article page may have a 'author' relationship to another item,
and we know the name but not the ORCID iD of that author.

### Item Identifiers

Item Identifiers are strings that can be associated with Items. They enable us to use a shared vocabulary to talk about
the same Item. They can include Persistent IDs, such as DOIs, ORCID iDs, and other IDs, such as ISSNs.

Not every Item has an Item Identifier. For example, an Author or Funder for whom we don't have an ORCID or Funder ID
yet.

Some Items can have two or more Item Identifiers. For example Journal may have a Journal DOI, an Electronic ISSN and a
Print ISSN.

We maintain strict rules about how Item Identifiers relate to Items. If we don't, it is simply impossible to combine all
of the data. The Manifold is the control point through which we merge all knowledge.

### Property Statements

These are chunks of statements that describe Items, for example the title or date of publication. Each Statement
