# Package org.crossref.manifold.meta

Meta contains information about Manifold, such as the size of the database, general metrics, etc. Data in this view is
concerned with the size and shape of the database, rather than the quality, origin or meaning of the data.