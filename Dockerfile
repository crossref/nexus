FROM maven:3-amazoncorretto-17

COPY target/manifold-*.jar ./manifold.jar

ENTRYPOINT ["java", "-jar", "manifold.jar"]